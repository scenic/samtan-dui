#pragma once
#include <WinUser.h>
#define UZWEB_NEWPAGE_TITLE _T("新标签页")
//#define UZWEB_START_PAGE    _T("http://www.yoozhe.com/us/hao.html")
//#define UZWEB_CLOSE_PAGE    _T("http://www.yoozhe.com/us/Start.html")

//#define UZWEB_BAOXIANG_PAGE    _T("http://www.yoozhe.com/us/baoxiang.html")
//#define UZWEB_GAME_GUIDE_PAGE    _T("http://www.yoozhe.com/us/datiqi.html")
//#define UZWEB_CALCULATOR_PAGE    _T("http://www.yoozhe.com/us/suan.html")
//#define UZWEB_CALENDAR_PAGE    _T("http://www.yoozhe.com/us/day.html")

//#define UZWEB_IPSERACH_PAGE    _T("http://www.yoozhe.com/us/ip.html")
#define UZWEB_SECOND_TIMER_PAGE    _T("http://www.yoozhe.com/us/miaobiao.html")
//#define UZWEB_NETWORK_TEST_PAGE    _T("http://www.yoozhe.com/us/cesu.html")
#define UZWEB_GIFTSPECK_PAGE  _T("http://www.50wanjia.com/libao/")

#define ID_HISTORY_FIRST	0x1000
#define HISTORY_LEN	10


static const int SET_HOOK_STATE=WM_USER+142;

/***************以下为Duilib"帐号管理"界面消息*******2014年6月13日 孙文*****************/
#define  ACCOUNT_ADD_ACCOUNT_LIST_MENU	WM_USER+1001				// 添加“帐号管理”中添加帐号List右键菜单消息
#define  ACCOUNT_ADD_GAME_BUTTON_OK			WM_USER+1002			// 添加“添加游戏”界面确定按钮消息
/***************以上为Duilib"帐号管理"界面消息*******2014年6月13日 孙文*****************/

/***************以下为Duilib"管理收藏夹"界面消息*******2014年6月13日 孙文*****************/
#define  FAV_ITEM_TREE_CLICK	WM_USER+1003	// 添加“管理收藏夹”中"新建文件夹"和"新建收藏"按钮中的下拉框树控件单击消息
#define  FAV_NEW_FOLDER_ADD		WM_USER+1004	// 添加“管理收藏夹”中"新建文件夹"按钮的添加消息

#define  FAV_LIST_MENU			WM_USER+1005	// 添加“管理收藏夹”中List右键菜单消息
#define  FAV_TREE_MENU			WM_USER+1006	// 添加“管理收藏夹”中Tree右键菜单消息
#define  FAV_EDIOTR_TREE_ADD	WM_USER+1007	// 添加“管理收藏夹”中Tree右键菜单“添加按钮”消息
/***************以上为Duilib"管理收藏夹"界面消息*******2014年6月13日 孙文*****************/
