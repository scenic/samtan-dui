#ifndef __LSZ_STRINGUTIL_H__
#define __LSZ_STRINGUTIL_H__

#include "LszTypes.h"
#include <string>

namespace Lsz
{

#if defined(_UNICODE) || defined(UNICODE)
	typedef wchar_t tchar;
	typedef std::wstring tstring;
#else
	typedef char tchar;
	typedef std::string tstring;
#endif

class Any;

class LSZCORE_EXPORT StringUtils {

public:
	static void stringToWchar(const Any& c, Any& wc);
	static void stringToUtf8(const Any& c, Any& utf8);
	static void stringToTchar(const Any& c, Any& tc);

	static void wcharToString(const Any& wc, Any& c);
	static void wcharToUtf8(const Any &wc, Any &utf8);
	static void wcharToTchar(const Any &wc, Any &tc);

	static void utf8ToWchar(const Any &utf8, Any &wc);
	static void utf8ToString(const Any &utf8, Any &wc);
	static void utf8ToTchar(const Any &utf8, Any &tc);
};

}

#endif // __LSZ_STRINGUTIL_H__
