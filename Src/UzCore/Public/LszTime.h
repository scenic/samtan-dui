#ifndef __LSZ_TIME_H__
#define __LSZ_TIME_H__

#include "LszAny.h"
#include "LszTypes.h"

namespace Lsz
{

class LSZCORE_EXPORT Time
{
public:
	Time();
	bool operator <(const Time &right) const;
	bool operator ==(const Time &right) const;
	double toSecDbl() const;
	Any pprint() const;

	int32_t hour;
	int32_t minute;
	int32_t second;
	int32_t millisecond;
	int32_t microsecond;
};

}

#endif // __LSZ_TIME_H__
