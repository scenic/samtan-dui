#ifndef __LSZ_DATETIME_H__
#define __LSZ_DATETIME_H__

#include "LszAny.h"
#include "LszDate.h"
#include "LszTime.h"
#include "LszTypes.h"

namespace Lsz
{

class LSZCORE_EXPORT DateTime
{
public:
	Date date;
	Time time;

	int64_t toI64() const;
	void fromI64(int64_t i64);
	bool operator==(const DateTime& right) const;
	static DateTime now();
	Any pprint() const;
};

}

#endif // __LSZ_DATETIME_H__
