#ifndef __LSZ_DATE_H__
#define __LSZ_DATE_H__

#include "LszTypes.h"

namespace Lsz
{

class LSZCORE_EXPORT Date
{
public:
	Date();
	bool operator <(const Date&) const;
	bool operator ==(const Date&) const;

	int32_t day;
	int32_t month;
	int32_t year;
};

}

#endif // __LSZ_DATE_H__
