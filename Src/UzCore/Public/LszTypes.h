#if !defined (__LSZ_TYPES_INCLUDED__)
#define __LSZ_TYPES_INCLUDED__ 

#ifdef int8_t
	#undef int8_t
#endif
  
#ifdef int16_t
	#undef int16_t
#endif

#ifdef int32_t
	#undef int32_t
#endif

#ifdef int64_t
	#undef int64_t
#endif

#ifdef uint8_t
	#undef uint8_t
#endif

#ifdef uint16_t
	#undef uint16_t
#endif

#ifdef uint32_t
	#undef uint32_t
#endif
  
#ifdef uint64_t
	#undef uint64_t
#endif

#ifdef INT8_C
	#undef INT8_C
#endif

#ifdef INT8_C
	#undef INT8_C
#endif

#ifdef INT16_C
	#undef INT16_C
#endif

#ifdef INT32_C
	#undef INT32_C
#endif

#ifdef INT64_C
	#undef INT64_C
#endif

#ifdef UINT8_C
	#undef UINT8_C
#endif

#ifdef UINT16_C
	#undef UINT16_C
#endif

#ifdef UINT32_C
	#undef UINT32_C
#endif

#ifdef UINT64_C
	#undef UINT64_C
#endif

typedef signed char         int8_t;
typedef signed short        int16_t;
typedef signed int          int32_t;
typedef signed __int64      int64_t;

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned __int64    uint64_t;

#define INT8_C(x)           x
#define INT16_C(x)          x##L
#define INT32_C(x)          x##L
#define INT64_C(x)          x##I64
                            
#define UINT8_C(x)          x##U
#define UINT16_C(x)         x##U
#define UINT32_C(x)         x##UL
#define UINT64_C(x)         x##UI64

#define McChar  char
#define McWChar wchar_t

#ifndef CDECL
	#define CDECL		__cdecl
#endif

#ifndef FASTCALL
	#define FASTCALL	__fastcall
#endif

#ifndef STDCALL
	#define STDCALL		__stdcall
#endif

#ifndef PASCAL
	#define PASCAL		__stdcall
#endif


#  if defined(LSZCORE_DLL_EXPORT)
#    define LSZCORE_EXPORT __declspec(dllexport)
#  elif defined(LSZCORE_LIB_STATIC)
#    define LSZCORE_EXPORT 
#  else
#    define LSZCORE_EXPORT __declspec(dllimport)
#  endif


//LSZCORE_EXPORT void mcAssertFail(const char* fl, const char* msg);
//#define ASSERT(x) if (!(x)) {mcAssertFail(MCFILELINE, 0);}
//#define ASSERT1(x, txt) if (!(x)) {mcAssertFail(MCFILELINE, (const char*) txt);}

#include <assert.h>
//#define ASSERT(x) assert(x);
//#define ASSERT1(x, txt) assert(x);

#endif // #if !defined (__LSZ_TYPES_INCLUDED__)
