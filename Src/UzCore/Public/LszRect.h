#ifndef __LSZ_RECT_H__
#define __LSZ_RECT_H__

#include "LszTypes.h"
#include "LszPoint.h"

namespace Lsz
{

class LSZCORE_EXPORT Rect
{
public:
	Rect(int32_t l=0, int32_t t=0, int32_t r=0, int32_t b=0);
	Rect(const Point &tl, const Point &br);
	const Point & topLeft() const;
	const Point & bottomRight() const;
	void setTop(int32_t t);
	void setLeft(int32_t l);
	void setBottom(int32_t b);
	void setRight(int32_t r);
	void setWidth(int32_t w);
	void setHeight(int32_t h);	
	void setTopLeft(const Point &tl);
	void setBottomRight(const Point &br);
	void move(const Point &delta);
	void moveTo(const Point &to);
	bool contains(const Rect &r) const;

	int32_t top() const;
	int32_t left() const;
	int32_t bottom() const;
	int32_t right() const;
	int32_t width() const;
	int32_t height() const;
	
	bool operator==(const Rect&) const;
	bool operator!=(const Rect&) const;

private:
	Point _tl;
	Point _br;
};

}

#endif	// __LSZ_RECT_H__
