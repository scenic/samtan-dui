#ifndef __LSZ_PAIR_H__
#define __LSZ_PAIR_H__

#include "LszTypes.h"

namespace Lsz
{

struct LSZCORE_EXPORT Pair
{
	union {
		int32_t first;
		int32_t x;
		int32_t w;
		int32_t min;
	};
	union {
		int32_t second;
		int32_t y;
		int32_t h;
		int32_t max;
	};

	Pair(int32_t inFirst=0, int32_t inSecond=0);
	bool operator==(const Pair&) const;
};

}

#endif
