#ifndef __LSZ_POINT_H__
#define __LSZ_POINT_H__

#include "LszTypes.h"

namespace Lsz
{

class LSZCORE_EXPORT Point
{
public:
	Point(int32_t x = 0, int32_t y = 0);

	int32_t x() const;
	int32_t y() const;
	void setX(int32_t);
	void setY(int32_t);

	bool operator==(const Point&) const;

private:
	int32_t _x;
	int32_t _y;
};

}

#endif	// __LSZ_POINT_H__
