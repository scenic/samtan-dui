#include "StdAfx.h"
#include "StopwatchRecordItemUI.h"

CStopwatchRecordItemUI::CStopwatchRecordItemUI()
{
	m_pContainer = new DuiLib::CHorizontalLayoutUI();

	m_pSplit1 = new DuiLib::CLabelUI();
	m_pSplit1->ApplyAttributeList(_T("width=\"10\""));

	m_pContainer->Add(m_pSplit1);
	m_pCheckbox = new DuiLib::COptionUI();
	m_pCheckbox->ApplyAttributeList(_T("padding=\"0,8,0,0\" width=\"15\" height=\"15\" bordercolor=\"#FFC2D2E1\" bordersize=\"1\""));
	m_pContainer->Add(m_pCheckbox);

	m_pContainer->Add(m_pSplit1);
	DuiLib::CLabelUI* pRecordIndex = new DuiLib::CLabelUI();
	pRecordIndex->ApplyAttributeList(_T("width=\"80\" align=\"center\""));

	m_pContainer->Add(m_pRecordIndex);

	m_pContainer->Add(m_pSplit1);
	DuiLib::CLabelUI* pCountRecord = new DuiLib::CLabelUI();
	pCountRecord->ApplyAttributeList(_T("width=\"150\" align=\"center\""));
	m_pContainer->Add(m_pCountRecord);

	this->Add(m_pContainer);
}

void CStopwatchRecordItemUI::SetIndex(int nIndex)
{
	CString strIndex;
	strIndex.Format(_T("%d"), nIndex);
	m_pRecordIndex->SetText(strIndex);
}

void CStopwatchRecordItemUI::SetCountRecord(DuiLib::CDuiString strCountRecord)
{
	m_pCountRecord->SetText(strCountRecord);
}

CStopwatchRecordItemUI::~CStopwatchRecordItemUI(void)
{
	delete m_pSplit1;
	delete m_pCheckbox;
	delete m_pRecordIndex;
	delete m_pCountRecord;
	//delete m_pContainer;
}