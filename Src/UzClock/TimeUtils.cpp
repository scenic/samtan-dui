#include "StdAfx.h"
#include "TimeUtils.h"

DWORD TimeUtils::m_StopwactchBegin;
DWORD TimeUtils::m_StopwactchEnd;
DWORD TimeUtils::m_StopwactchCount;

TimeUtils::TimeUtils(void) {}

Lsz::tstring TimeUtils::GetCurrTime()
{
	time_t t = time(0);
	Lsz::tchar buff[64];
	_tcsftime(buff, sizeof(buff), _T("%Y��%m��%d�� %X %A"), localtime(&t));
	return Lsz::tstring(buff);
}

void TimeUtils::BeginCounting(DWORD dwStartMilsec)
{
	m_StopwactchBegin = dwStartMilsec;
}

Lsz::tstring TimeUtils::GetCurrCount()
{
	DWORD deltaT = ::GetTickCount() - m_StopwactchBegin;
	StopwatchStruc watch;
	ZeroMemory(&watch, sizeof(watch));
	watch.hour = deltaT / 3600000;
	deltaT -= watch.hour * 3600000;
	watch.min = deltaT / 60000;
	deltaT -= watch.min * 60000;
	watch.sec = deltaT / 1000;
	deltaT -= watch.sec * 1000;
	watch.msec = deltaT;
	Lsz::tchar buff[64];
	_stprintf(buff, _T("%d:%d:%d:%d"), watch.hour, watch.min, watch.sec, watch.msec);
	return Lsz::tstring(buff);
}

TimeUtils::~TimeUtils(void) {}