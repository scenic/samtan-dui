#pragma once
#include "stdafx.h"

class CStopwatchRecordItemUI : public DuiLib::CListContainerElementUI
{
public:
	CStopwatchRecordItemUI(void);
	void SetIndex(int nIndex);
	void SetCountRecord(DuiLib::CDuiString strCountRecord);
	//void Notify(TNotifyUI& msg);
	~CStopwatchRecordItemUI(void);
	//virtual void Notify(TNotifyUI& msg) {}

private:
	DuiLib::CHorizontalLayoutUI* m_pContainer;
	DuiLib::CLabelUI* m_pSplit1;
	DuiLib::COptionUI* m_pCheckbox;
	DuiLib::CLabelUI* m_pRecordIndex;
	DuiLib::CLabelUI* m_pCountRecord;
};