#pragma once
#include "stdafx.h"

class CClockMainWnd : public DuiLib::CWindowWnd
					, public DuiLib::INotifyUI
{
public:
	CClockMainWnd(void);
	~CClockMainWnd(void);

	void	Init();
	void	OnPrepare();

	virtual	void	Notify(DuiLib::TNotifyUI& msg);
	virtual	LRESULT	HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual	LPCTSTR	GetWindowClassName() const { return _T("CClockMainWnd"); }

	LRESULT	OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void	OnUpdateCurrTime();	// 更新当前时间
	void	OnStartCounting();		// 开始计时

private:

	DuiLib::CPaintManagerUI	m_pm;
	HGLOBAL					m_hGlobalRsrc;

	DuiLib::CTabLayoutUI*	m_pMainVert;
	DuiLib::CLabelUI*		m_pCurrTimeLabl;

	/* 游戏闹钟区 */
	DuiLib::CVerticalLayoutUI*	m_pGameClockVert;
	DuiLib::CButtonUI*			m_pAddAlarmBtn;
	DuiLib::CVerticalLayoutUI*	m_pAddAlarmVert;
	DuiLib::CListUI*			m_pAlarmList;
	DuiLib::CButtonUI*			m_pAddAlarmOKBtn;		// 添加闹钟_确定
	DuiLib::CButtonUI*			m_pAddAlarmCancelBtn;	// 添加闹钟_取消

	/* 秒表区 */
	DuiLib::CButtonUI*	m_pStartCountBtn;		// 开始计时按钮
	DuiLib::CLabelUI*	m_pStopwatchLabl;		// 秒表Label
	DuiLib::CButtonUI*	m_pResetStopwatchBtn;	// 重置秒表按钮
	DuiLib::CListUI*	m_pStopwatchList;		// 计时记录列表

	/* 倒计时 */
	DuiLib::CVerticalLayoutUI*	m_pCountdownVert;
	DuiLib::CVerticalLayoutUI*	m_pAddCountdownVert;
	DuiLib::CButtonUI*			m_pAddCountdownBtn;
	DuiLib::CButtonUI*			m_pAddCountdownOKBtn;
	DuiLib::CButtonUI*			m_pAddCountdownCancelBtn;
};