#include "StdAfx.h"
#include "ControlEx.h"
#include "ClockMainWnd.h"
#include "TimeUtils.h"
#include "StopwatchRecordItemUI.h"

#define IDT_CURRTIME 1000
#define IDT_STOPWATCH 1001

CClockMainWnd::CClockMainWnd(void) {}

// 初始化
void CClockMainWnd::Init()
{
  
	DuiLib::CPaintManagerUI::SetResourcePath(DuiLib::CPaintManagerUI::GetInstancePath() + _T("..//skin//UzClock"));
	//CPaintManagerUI::SetResourceZip(lpRsrc, dwSize);

	DuiLib::CPaintManagerUI::SetInstance(GetModuleHandle(0));

	// 初始化COM组件
	CoInitialize(NULL);
}

void CClockMainWnd::OnPrepare()
{
	// 开始定时器
	::SetTimer(*this, IDT_CURRTIME, 1000, NULL);

	/* 控件容器 */
	m_pMainVert	=	static_cast<DuiLib::CTabLayoutUI*>(m_pm.FindControl(_T("mainVert")));

	/* 标题区 */
	m_pCurrTimeLabl	=	static_cast<DuiLib::CLabelUI*>(m_pm.FindControl(_T("currTimeLabl")));
	m_pCurrTimeLabl->SetText(TimeUtils::GetCurrTime().c_str());

	/* 游戏闹钟区 */
	m_pGameClockVert		=	static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("gameClockVert")));
	m_pAddAlarmVert			=	static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("addAlarmVert")));
	m_pAddAlarmBtn			=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addAlarmBtn")));
	m_pAlarmList			=	static_cast<DuiLib::CListUI*>(m_pm.FindControl(_T("alarmList")));
	m_pAddAlarmOKBtn		=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addAlarmOKBtn")));
	m_pAddAlarmCancelBtn	=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addAlarmCancelBtn")));

	/* 秒表区 */
	m_pStartCountBtn		=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("startCountBtn")));
	m_pStopwatchLabl		=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("stopwatchLabl")));
	m_pStopwatchList		=	static_cast<DuiLib::CListUI*>(m_pm.FindControl(_T("stopwatchList")));
	m_pResetStopwatchBtn	=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("resetStopwatchBtn")));

	m_pStopwatchList->SetTag(0); // 记录个数
	m_pStartCountBtn->SetTag(FALSE); // 未开始计时标志

	/* 倒计时区 */
	m_pCountdownVert			=	static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("countdownVert")));
	m_pAddCountdownVert			=	static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("addCountdownVert")));
	m_pAddCountdownBtn			=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addCountdownBtn")));
	m_pAddCountdownOKBtn		=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addCountdownOKBtn")));
	m_pAddCountdownCancelBtn	=	static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("addCountdownCancelBtn")));

	// -- test CString
	CString strDays		=	_T("周一");
	CString strHour		=	_T("1");
	CString strMinutes	=	_T("2");

	CString StrTime	=	_T("");
	StrTime.Format(_T("%s-%s:%s"), strDays, strHour, strMinutes);
}

// 控件通知消息处理
void CClockMainWnd::Notify(DuiLib::TNotifyUI& msg)
{
	if (msg.sType == DUI_MSGTYPE_WINDOWINIT)
	{
		OnPrepare();
	}
	else if (msg.sType == DUI_MSGTYPE_SELECTCHANGED)
	{
		if (msg.pSender->GetName() == _T("clockOpt"))
		{
			m_pMainVert->SelectItem(0);
		}
		else if (msg.pSender->GetName() == _T("stopwatchOpt"))
		{
			m_pMainVert->SelectItem(1);
		}
		else if (msg.pSender->GetName() == _T("countdownOpt"))
		{
			m_pMainVert->SelectItem(2);
		}
	}
	else if (msg.sType == DUI_MSGTYPE_CLICK)
	{
		if (msg.pSender->GetName() == _T("closeBtn"))
		{
			PostMessage(WM_CLOSE);
			return;
		}
		else if (msg.pSender->GetName() == _T("startRecordBtn"))
		{
		}
		else if (msg.pSender == m_pStartCountBtn)
		{
			m_pStartCountBtn->SetTag(!(BOOL)m_pStartCountBtn->GetTag()); // 切换是否开始计时状态
			if (m_pStartCountBtn->GetTag())
			{
				::SetTimer(*this, IDT_STOPWATCH, 1, NULL);
				TimeUtils::BeginCounting(::GetTickCount());
				m_pStartCountBtn->SetText(_T("停止计时"));
				m_pResetStopwatchBtn->SetText(_T("保存记录"));
			}
			else
			{
				::KillTimer(*this, IDT_STOPWATCH);
				m_pStartCountBtn->SetText(_T("开始计时"));
				m_pResetStopwatchBtn->SetText(_T("重置秒表"));
			}
		}
		else if (msg.pSender == m_pResetStopwatchBtn) // 保存/重置 按钮
		{
			if (m_pStartCountBtn->GetTag())
			{
				m_pStopwatchList->SetTag(m_pStopwatchList->GetTag()+1);

				DuiLib::CListContainerElementUI* pListElement = new DuiLib::CListContainerElementUI;
				pListElement->ApplyAttributeList(_T("padding=\"1,0,1,0\" width=\"200\" height=\"30\""));
				
				

				//DuiLib::CHorizontalLayoutUI* pContainer = new DuiLib::CHorizontalLayoutUI;


				DuiLib::CLabelUI* pSplit1 = new DuiLib::CLabelUI;
				pSplit1->ApplyAttributeList(_T("width=\"10\""));

				pListElement->Add(pSplit1);
				DuiLib::COptionUI* pCheckbox  = new DuiLib::COptionUI;
				pCheckbox->ApplyAttributeList(_T("padding=\"0,8,0,0\" width=\"15\" height=\"15\" bordercolor=\"#FFC2D2E1\" bordersize=\"1\""));
				pListElement->Add(pCheckbox);

				//pListElement->Add(pSplit1);
				DuiLib::CLabelUI* pRecordIndex = new DuiLib::CLabelUI;
				pRecordIndex->ApplyAttributeList(_T("width=\"80\" align=\"center\""));
				CString strIndex;
				strIndex.Format(_T("%d"), 1);
				pRecordIndex->SetText(strIndex);
				pListElement->Add(pRecordIndex);

				//pListElement->Add(pSplit1);
				DuiLib::CLabelUI* pCountRecord = new DuiLib::CLabelUI;
				pCountRecord->ApplyAttributeList(_T("width=\"150\" align=\"center\""));
				pCountRecord->SetText(m_pStopwatchLabl->GetText());
				pListElement->Add(pCountRecord);

				//pListElement->Add(pListElement);
				m_pStopwatchList->Add(pListElement);
				
			}
			else
			{
				m_pStopwatchList->RemoveAll();
			}
		}
		else if (msg.pSender == m_pAddAlarmBtn) // 添加闹钟按钮
		{
			m_pGameClockVert->SetVisible(false);
			m_pAddAlarmVert->SetVisible(true);
		}
		else if (msg.pSender == m_pAddAlarmOKBtn) // 添加闹钟_确定
		{
			m_pGameClockVert->SetVisible(true);
			m_pAddAlarmVert->SetVisible(false);
		}
		else if (msg.pSender == m_pAddAlarmCancelBtn)
		{
			m_pGameClockVert->SetVisible(true);
			m_pAddAlarmVert->SetVisible(false);
		}
		else if (msg.pSender == m_pAddCountdownBtn) // 添加倒计时
		{
			m_pCountdownVert->SetVisible(false);
			m_pAddCountdownVert->SetVisible(true);
		}
		else if (msg.pSender == m_pAddCountdownOKBtn)
		{
			m_pCountdownVert->SetVisible(true);
			m_pAddCountdownVert->SetVisible(false);
		}
		else if (msg.pSender == m_pAddCountdownCancelBtn)
		{
			m_pCountdownVert->SetVisible(true);
			m_pAddCountdownVert->SetVisible(false);
		}
	}
}

// 创建窗口函数
LRESULT CClockMainWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Init();
	LONG styleValue = ::GetWindowLong(*this, GWL_STYLE);
	styleValue &= ~WS_CAPTION;
	::SetWindowLong(*this, GWL_STYLE, styleValue | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	m_pm.Init(m_hWnd);
	DuiLib::CDialogBuilder builder;
	CDialogBuilderCallbackEx cb;

	DuiLib::CControlUI* pRoot = builder.Create(_T("main.xml"), (UINT)0,  &cb, &m_pm);
	ASSERT(pRoot && "Failed to parse XML");
	
	m_pm.AttachDialog(pRoot);
	m_pm.AddNotifier(this);

	Init();
	return 0;
}

// 关闭窗口函数
LRESULT CClockMainWnd::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

// 销毁窗口函数
LRESULT CClockMainWnd::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	//::PostQuitMessage(0L);
	bHandled = FALSE;
	return 0;
}

//
LRESULT CClockMainWnd::OnNcActivate(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (::IsIconic(*this))
	{
		bHandled = FALSE;
	}
	return (wParam == 0) ? TRUE : FALSE;
}

LRESULT CClockMainWnd::OnNcCalcSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CClockMainWnd::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CClockMainWnd::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	::ScreenToClient(*this, &pt);

	RECT rcClient;
	::GetClientRect(*this, &rcClient);

	if ( !::IsZoomed(*this) )
	{
		RECT rcSizeBox = m_pm.GetSizeBox();
		if ( pt.y < rcClient.top + rcSizeBox.top )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTTOPLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTTOPRIGHT;
			return HTTOP;
		}
		else if ( pt.y > rcClient.bottom - rcSizeBox.bottom )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTBOTTOMLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTBOTTOMRIGHT;
			return HTBOTTOM;
		}
		if ( pt.x < rcClient.left + rcSizeBox.left )
			return HTLEFT;
		if ( pt.x > rcClient.right - rcSizeBox.right )
			return HTRIGHT;
	}

	RECT rcCaption = m_pm.GetCaptionRect();
	if ( pt.x >= rcClient.left + rcCaption.left && pt.x < rcClient.right - rcCaption.right
		&& pt.y >= rcCaption.top && pt.y < rcCaption.bottom )
	{
		DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_pm.FindControl(pt));
		if ( pControl && _tcsicmp(pControl->GetClass(), _T("ButtonUI")) != 0
			&& _tcsicmp(pControl->GetClass(), _T("OptionUI")) != 0)
			return HTCAPTION;
	}

	return HTCLIENT;
}

LRESULT CClockMainWnd::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	SIZE szRoundCorner = m_pm.GetRoundCorner();
	if ( !::IsIconic(*this) && (szRoundCorner.cx != 0 || szRoundCorner.cy != 0) )
	{
		RECT rcClient;
		::GetClientRect(*this, &rcClient);
		RECT rc = { rcClient.left, rcClient.top + szRoundCorner.cx, rcClient.right, rcClient.bottom };
		HRGN hRgn1 = ::CreateRectRgnIndirect( &rc );
		HRGN hRgn2 = ::CreateRoundRectRgn(rcClient.left, rcClient.top, rcClient.right + 1,
			rcClient.bottom - szRoundCorner.cx, szRoundCorner.cx, szRoundCorner.cy);
		::CombineRgn( hRgn1, hRgn1, hRgn2, RGN_OR );
		::SetWindowRgn(*this, hRgn1, TRUE);
		::DeleteObject(hRgn1);
		::DeleteObject(hRgn2);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CClockMainWnd::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	MONITORINFO oMonitor = {};
	oMonitor.cbSize = sizeof(oMonitor);
	::GetMonitorInfo(::MonitorFromWindow(*this, MONITOR_DEFAULTTOPRIMARY), &oMonitor);
	//CRect rcWork = oMonitor.rcWork;
	RECT rcWork = oMonitor.rcWork;
	OffsetRect(&rcWork,-rcWork.left, -rcWork.top);

	LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
	lpMMI->ptMaxPosition.x = rcWork.left;
	lpMMI->ptMaxPosition.y = rcWork.top;
	lpMMI->ptMaxSize.x     = rcWork.right;
	lpMMI->ptMaxSize.y     = rcWork.bottom;

	bHandled = FALSE;
	return 0;
}

LRESULT CClockMainWnd::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if ( wParam == SC_CLOSE )
	{
		::PostQuitMessage(0L);
		bHandled = TRUE;
		return 0;
	}
	//BOOL bZoomed = ::IsZoomed(*this);
	LRESULT lRes = DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);
 
	return lRes;
}

void CClockMainWnd::OnUpdateCurrTime()
{
	m_pCurrTimeLabl->SetText(TimeUtils::GetCurrTime().c_str());
}

void CClockMainWnd::OnStartCounting()
{
	m_pStopwatchLabl->SetText(TimeUtils::GetCurrCount().c_str());
}

LRESULT CClockMainWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL bHandled = TRUE;
	switch (uMsg)
	{
	case WM_CREATE:
		lRes = OnCreate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_CLOSE:
		lRes = OnClose(uMsg, wParam, lParam, bHandled);
		break;
	case WM_DESTROY:
		lRes = OnDestroy(uMsg, wParam, lParam, bHandled);
		break;
	case WM_TIMER:
		if (wParam == IDT_CURRTIME)
		{
			OnUpdateCurrTime();
		}
		else if (wParam == IDT_STOPWATCH)
		{
			OnStartCounting();
		}

		break;
	case WM_NCACTIVATE:
		lRes = OnNcActivate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCCALCSIZE:
		lRes = OnNcCalcSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCPAINT:
		lRes = OnNcPaint(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCHITTEST:
		lRes = OnNcHitTest(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SIZE:
		lRes = OnSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_GETMINMAXINFO:
		lRes = OnGetMinMaxInfo(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SYSCOMMAND:
		lRes = OnSysCommand(uMsg, wParam, lParam, bHandled);
		break;
	case WM_KEYDOWN:
		break;
	default:
		bHandled = FALSE;
	}
	if (bHandled)
	{
		return lRes;
	}
	if (m_pm.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);
}

CClockMainWnd::~CClockMainWnd(void)
{
	FreeResource(m_hGlobalRsrc);
	CoUninitialize();
}