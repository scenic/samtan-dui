#pragma once

#include "SpinUI.h"

class CDialogBuilderCallbackEx : public DuiLib::IDialogBuilderCallback
{
public:
	DuiLib::CControlUI* CreateControl(LPCTSTR pstrClass) 
	{
		if( _tcscmp(pstrClass, _T("Spin")) == 0 )
			return new DuilibEx::CSpinUI;
		return NULL;

	}
};