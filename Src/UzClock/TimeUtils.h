#pragma once
#include "stdafx.h"
#include "..\UzCore\Public\LszStringUtil.h"

class TimeUtils
{
public:
	struct StopwatchStruc
	{
		int hour;
		int min;
		int sec;
		int msec;
	};
	TimeUtils(void);
	static Lsz::tstring GetCurrTime();
	static void BeginCounting(DWORD dwStartMilsec);
	static Lsz::tstring GetCurrCount();
	~TimeUtils(void);

private:
	static DWORD m_StopwactchBegin;
	static DWORD m_StopwactchEnd;
	static DWORD m_StopwactchCount;
};