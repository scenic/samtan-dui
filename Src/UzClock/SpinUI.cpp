#include "StdAfx.h"
#include "SpinUI.h"

namespace DuilibEx
{

	CSpinUI::CSpinUI(void)
	{
		m_pEdit		=	new DuiLib::CEditUI;
		m_pVerti	=	new DuiLib::CVerticalLayoutUI;
		m_pUpBtn	=	new DuiLib::CButtonUI;
		m_pDownBtn	=	new DuiLib::CButtonUI;
		
		this->SetFixedHeight(25);
		this->SetFixedWidth(70);
		this->ApplyAttributeList(_T("bordercolor=\"#FFC2D2E1\" bordersize=\"1\" bkcolor=\"#FFFFFFFF\""));
	
		//m_pVerti->SetFixedHeight(GetFixedHeight());
		//m_pEdit->SetFixedHeight(GetFixedHeight());
		m_pEdit->SetFixedWidth(50);
		m_pEdit->ApplyAttributeList(_T("padding=\"1,1,0,1\""));
		
		m_pUpBtn->SetFixedWidth(GetFixedWidth() - m_pEdit->GetFixedWidth());
		m_pUpBtn->SetFixedHeight(GetFixedHeight() / 2);
		m_pUpBtn->ApplyAttributeList(_T("text=\"+\" bordercolor=\"#FFC2D2E1\" bordersize=\"1\" bkcolor=\"#FFF2F4F5\" padding=\"1,-1,1,1\" pushedtextcolor=\"#FFFD9D3C\""));

		m_pDownBtn->SetFixedWidth(GetFixedWidth() - m_pEdit->GetFixedWidth());
		m_pUpBtn->SetFixedHeight(GetFixedHeight() / 2);
		m_pDownBtn->ApplyAttributeList(_T("text=\"-\" bordercolor=\"#FFC2D2E1\" bordersize=\"1\" bkcolor=\"#FFF2F4F5\" padding=\"1,-1,1,1\" pushedtextcolor=\"#FFFD9D3C\""));

		m_pVerti->ApplyAttributeList(_T("padding=\"0,1,0,1\""));

		m_pUpBtn->SetMouseEnabled(false);
		m_pDownBtn->SetMouseEnabled(false);

		m_pVerti->Add(m_pUpBtn);
		m_pVerti->Add(m_pDownBtn);

		this->Add(m_pEdit);
		this->Add(m_pVerti);

		


	}

	LPVOID CSpinUI::GetInterface(LPCTSTR pstrName)
	{
		if (_tcscmp(pstrName, _T("Spin")) == 0)
			return static_cast<CSpinUI*>(this);
		return __super::GetInterface(pstrName);
	}

	void CSpinUI::DoEvent(DuiLib::TEventUI& event)
	{
		if (event.Type == DuiLib::UIEVENT_BUTTONDOWN)
		{
			DuiLib::CDuiString strOldNum = m_pEdit->GetText();
			int curNum = _tstoi((LPCTSTR)strOldNum);
			DuiLib::CDuiString strNewNum;

			if (::PtInRect(&m_pUpBtn->GetPos(), event.ptMouse))
			{
				strNewNum.Format(_T("%d"), curNum+1);
				m_pEdit->SetText((LPCTSTR)strNewNum);
				event.pSender = m_pUpBtn;
				m_pUpBtn->SetMouseEnabled(true);
				m_pUpBtn->DoEvent(event);
				m_pUpBtn->SetMouseEnabled(false);
			}
			
			else if (::PtInRect(&m_pDownBtn->GetPos(), event.ptMouse))
			{
				strNewNum.Format(_T("%d"), curNum-1);
				m_pEdit->SetText((LPCTSTR)strNewNum);
			}
		}
		else if (event.Type ==  DuiLib::UIEVENT_BUTTONUP)
		{
			if (::PtInRect(&m_pUpBtn->GetPos(), event.ptMouse))
			{
				m_pUpBtn->SetMouseEnabled(true);
				m_pUpBtn->DoEvent(event);
				m_pUpBtn->SetMouseEnabled(false);
			}
			else if (::PtInRect(&m_pDownBtn->GetPos(), event.ptMouse))
			{
				m_pDownBtn->SetMouseEnabled(true);
				m_pDownBtn->DoEvent(event);
				m_pDownBtn->SetMouseEnabled(false);
			}
		}
		DuiLib::CHorizontalLayoutUI::DoEvent(event);
	}

	//************************************
	// Method:    SetAttribute
	// FullName:  CSpinUI::SetAttribute
	// Access:    public 
	// Returns:   void
	// Qualifier: Sam.Tan
	// Parameter: DWORD dwColor
	// Note:	  ���ÿؼ�����
	//************************************
	void CSpinUI::SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue)
	{
		if(_tcscmp(pstrName, _T("upbtnattr")) == 0)
			m_pUpBtn->ApplyAttributeList(pstrValue);
		else if (_tcscmp(pstrName, _T("downbtnattr")) == 0)
			m_pDownBtn->ApplyAttributeList(pstrValue);
		else if (_tcscmp(pstrName, _T("editattr")) == 0)
			m_pEdit->ApplyAttributeList(pstrValue);
		
		else
			DuiLib::CHorizontalLayoutUI::SetAttribute(pstrName, pstrValue);
	}

	CSpinUI::~CSpinUI(void)
	{
	}

} // namespace DuilibEx
