#pragma once

namespace DuilibEx
{
	class CSpinUI : public DuiLib::CHorizontalLayoutUI
	{
	public:
		CSpinUI(void);
		~CSpinUI(void);

		LPCTSTR	GetClass() const { return _T("SpinUI"); }
		LPVOID	GetInterface(LPCTSTR pstrName);
		void	DoEvent(DuiLib::TEventUI& event);

		virtual	void	SetAttribute(LPCTSTR pstrName, LPCTSTR pstrValue);

	private:
		DuiLib::CEditUI*	m_pEdit;

		DuiLib::CVerticalLayoutUI*	m_pVerti;

		DuiLib::CButtonUI*	m_pUpBtn;
		DuiLib::CButtonUI*	m_pDownBtn;
	};

}

