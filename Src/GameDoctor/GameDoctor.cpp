// GameDoctor.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "GameDoctor.h"
#include "GameDoctorMainWnd.h"

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER (hInstance);
	UNREFERENCED_PARAMETER (hPrevInstance);
	UNREFERENCED_PARAMETER (lpCmdLine);
	UNREFERENCED_PARAMETER (nCmdShow);

	GameDoctorMainWnd* pGameDoctorWnd = new GameDoctorMainWnd;
	pGameDoctorWnd->Create(NULL, _T("��Ϸҽ��"), 0, 0);
	pGameDoctorWnd->CenterWindow();
	pGameDoctorWnd->ShowModal();

	return 0;
}