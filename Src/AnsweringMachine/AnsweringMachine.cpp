// AnsweringMachine.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "AnsweringMachine.h"
#include "AnsweringMachineWnd.h"

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER (hInstance);
	UNREFERENCED_PARAMETER (hPrevInstance);
	UNREFERENCED_PARAMETER (lpCmdLine);
	UNREFERENCED_PARAMETER (nCmdShow);

	CAnsweringMachineWnd* pAnswerMachineWnd = new CAnsweringMachineWnd;
	pAnswerMachineWnd->Create(NULL, _T("������"), 0, 0);
	pAnswerMachineWnd->CenterWindow();
	pAnswerMachineWnd->ShowModal();

	return 0;
}