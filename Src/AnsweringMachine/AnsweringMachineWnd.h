#pragma once
class CAnsweringMachineWnd : public DuiLib::CWindowWnd, public DuiLib::INotifyUI
{
public:
	CAnsweringMachineWnd(void);
	~CAnsweringMachineWnd(void);

	void Init();
	void OnPrepare();

	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LPCTSTR GetWindowClassName() const { return _T("CAnsweringMachineWnd"); }
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	DuiLib::CPaintManagerUI m_pm;
	HGLOBAL m_hGlobalRsrc;
	DuiLib::CTabLayoutUI* m_ptlayBrowserClient;
};