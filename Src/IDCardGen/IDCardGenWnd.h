#pragma once
class CIDCardGenWnd : public DuiLib::CWindowWnd
					, public DuiLib::INotifyUI
{
public:
	CIDCardGenWnd(void);
	~CIDCardGenWnd(void);

	void Init();
	void OnPrepare();

	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LPCTSTR GetWindowClassName() const { return _T("CIDCardGenWnd"); }
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
	HGLOBAL	m_hGlobalRsrc;
	DuiLib::CPaintManagerUI	m_pm;
	DuiLib::CTabLayoutUI*	m_ptlayBrowserClient;
};