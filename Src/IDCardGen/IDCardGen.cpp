// IDCardGen.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "IDCardGen.h"
#include "IDCardGenWnd.h"

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER (hInstance);
	UNREFERENCED_PARAMETER (hPrevInstance);
	UNREFERENCED_PARAMETER (lpCmdLine);
	UNREFERENCED_PARAMETER (nCmdShow);
	
	CIDCardGenWnd* pIDCardGenWnd = new CIDCardGenWnd;
	pIDCardGenWnd->Create(NULL, _T("����֤"), 0, 0);
	pIDCardGenWnd->CenterWindow();
	pIDCardGenWnd->ShowModal();

	return 0;
}