#include "StdAfx.h"
#include "Util.h"

#include "GlobalFunc.h"
#include <Shellapi.h>

namespace EHXLib
{
	CUtil::CUtil(void)
	{
	}


	CUtil::~CUtil(void)
	{
	}

	CString CUtil::GetHistroyDir()
	{
		return GetExePath() +  _T("\\favicon\\");
	}

	void CUtil::OpenNewWindowPage(CString strTitle,CString strUrl)
	{
		CString strExePath = GetExePath();;
		strExePath += L"\\UzExplorer.exe";

		if(!strExePath.IsEmpty())
		{
			CString strCommand = L"opennewurl";
			CString strCommandLine = L"param " + strCommand + L" " + strUrl + L" 1";
			STARTUPINFO si;
			PROCESS_INFORMATION pi;
			ZeroMemory(&si, sizeof(si));
			si.cb = sizeof(si);
			ZeroMemory(&pi, sizeof(pi));
			BOOL bRet = CreateProcess(strExePath.GetBuffer(),strCommandLine.GetBuffer() , NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
			UNREFERENCED_PARAMETER (bRet);
			CloseHandle(pi.hProcess);
			CloseHandle(pi.hThread);
		}
	}


	BOOL CUtil::IsFileExist(CString strFile)
	{
		if(strFile== L"")   
			return   FALSE;   
		BOOL   bExist   =   TRUE;   
		HANDLE   hFind;   
		WIN32_FIND_DATA   dataFind;   
		hFind   =   FindFirstFile(strFile,&dataFind);   
		if(hFind==   INVALID_HANDLE_VALUE)   
		{   
			bExist   =   FALSE;   
		}   
		FindClose(hFind);   
		return   bExist;  
	}

	
	BOOL CUtil::IsUrl(CString str)
	{
		BOOL bRet =FALSE;
		if(str.IsEmpty())
			return bRet;
		int iLen = str.GetLength();
		int iPos = str.Find(L'.');
		if( iPos != -1 && iPos < iLen)
			bRet = TRUE;
		return bRet;				
	}

	CString CUtil::FormatUTF8Str(CString strSource)
	{
		CString strRet = L"";
		CString strTemp = L"";
		char c;
		if(!strSource.IsEmpty())
		{
			int len = ::WideCharToMultiByte(CP_UTF8, 0, strSource.GetBuffer(), -1, NULL, 0, NULL, NULL);  
			if (len == 0) 
				return strRet;  

			std::vector<char> utf8(len);  
			::WideCharToMultiByte(CP_UTF8, 0, strSource.GetBuffer(), -1, &utf8[0], len, NULL, NULL);  

			for(int i=0;i<len;i++)
			{
				c = utf8[i];
				if(c == '\0')
					break;
				strTemp.Format(_T("%02x"),c);
				strTemp = strTemp.Right(2);
				strRet = strRet + L"%" +strTemp;
			}	
		}
		return strRet;
	}

	BOOL CUtil::IsDigitString( CString str )
	{
		BOOL bIsDigit = TRUE;
		int nCount = str.GetLength(); // 获得字符个数
		for ( int i = 0; i < nCount; i ++ )
		{
			if ( 0 == isdigit( str.GetAt(i) ) ) // 不是数字就置标志位
			{
				bIsDigit = FALSE;
				break;// 退出
			}
		}
		return bIsDigit;
	}

	CStringA CUtil::GenerateRandString(int len)
	{
		char* szbuff = new char[len+1];
		memset(szbuff,0,len+1);
		if(szbuff!=NULL && len > 0)
		{
			srand(GetTickCount());
			CStringA strRand="";
			int a=0;
			for(;a<len;a++) 
			{   
				szbuff[a] = ((rand()%74)+48);   
				while(!IsCharAlphaNumeric(szbuff[a]))   
				{   
					szbuff[a]=((rand()%74)+48); 
				}    
			} 
		}	
		CStringA str =  szbuff;
		delete[] szbuff;
		return str;
	}

	BOOL CUtil::IsMatched(const CString& strSrc,const CString& strDesc)
	{
		BOOL bRet = FALSE;
		if(strSrc.Find(strDesc) != -1)
			bRet = TRUE;
		return bRet;

		int nDescLen = strDesc.GetLength();
		if(strSrc.Left(nDescLen) == strDesc)
			return TRUE;
		return bRet;
	}
	
}
