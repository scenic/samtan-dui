#pragma once

class CUzUrl
{
public:
	CUzUrl(const CString& strUrl);
	~CUzUrl();

	CString	GetUrl() const;			// http://www.mozilla.org/en-US/
	CString	GetTopUrl() const;		// http://www.mozilla.org
	CString	GetTopName() const;		// www.mozilla.org

private:
	CString m_strUrl;
};
