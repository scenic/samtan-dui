#include "StdAfx.h"
#include <algorithm>
#include "BossKeyWnd.h"
#include "BrowserWnd.h"

CBossKeyWnd::CBossKeyWnd(void) {}
CBossKeyWnd::~CBossKeyWnd(void) {}

// 初始化
void CBossKeyWnd::Init() {}

void CBossKeyWnd::OnPrepare()
{
	
	this->m_bEnableBossKey = m_pMainWnd->m_bEnableBossKey;
	m_pcbxEnable = static_cast<DuiLib::CCheckBoxUI*>(m_pm.FindControl(_T("cbxEnable")));
	m_pStatusLabel = static_cast<DuiLib::CLabelUI*>(m_pm.FindControl(_T("statusLabel")));

	if (m_bEnableBossKey)
	{
		m_pcbxEnable->SetCheck(true);
		m_pStatusLabel->SetText(_T("已开启"));
	}
	else
	{
		m_pcbxEnable->SetCheck(false);
		m_pStatusLabel->SetText(_T("未开启"));
	}
}

// 控件通知消息处理
void CBossKeyWnd::Notify(DuiLib::TNotifyUI& msg)
{
	if (msg.sType == _T("windowinit"))
	{
		OnPrepare();
	}
	else if (msg.sType == _T("selectchanged"))
	{
		if (msg.pSender == m_pcbxEnable)
		{
			m_bEnableBossKey = m_pcbxEnable->GetCheck();
			if (m_bEnableBossKey)
			{
				RegHotKey();
				m_pStatusLabel->SetText(_T("已开启"));
			}
			else
			{
				UnRegHotKey();
				m_pStatusLabel->SetText(_T("未开启"));
			}
		}
	}
	else if (msg.sType == _T("click"))
	{
		if (msg.pSender->GetName() == _T("btnClose"))
		{
			PostMessage(WM_CLOSE);
			return;
		}
		
	}
}

// 创建窗口函数
LRESULT CBossKeyWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Init();
	LONG styleValue = ::GetWindowLong(*this, GWL_STYLE);
	styleValue &= ~WS_CAPTION;
	::SetWindowLong(*this, GWL_STYLE, styleValue | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	m_pm.Init(m_hWnd);
	DuiLib::CDialogBuilder builder;

	DuiLib::CControlUI* pRoot = builder.Create(_T("BossKey//bosskey_wnd.xml"), (UINT)0,  NULL, &m_pm);
	ASSERT(pRoot && "Failed to parse XML");

	m_pm.AttachDialog(pRoot);
	m_pm.AddNotifier(this);

	return 0;
}

// 关闭窗口函数
LRESULT CBossKeyWnd::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

// 销毁窗口函数
LRESULT CBossKeyWnd::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	std::vector<DuiLib::CWindowWnd*>::iterator it = find(m_pMainWnd->m_childWindows.begin(),
		m_pMainWnd->m_childWindows.end(), this);
	m_pMainWnd->m_childWindows.erase(it);

	bHandled = FALSE;
	return 0;
}

//
LRESULT CBossKeyWnd::OnNcActivate(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (::IsIconic(*this))
	{
		bHandled = FALSE;
	}
	return (wParam == 0) ? TRUE : FALSE;
}

LRESULT CBossKeyWnd::OnNcCalcSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CBossKeyWnd::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CBossKeyWnd::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	::ScreenToClient(*this, &pt);

	RECT rcClient;
	::GetClientRect(*this, &rcClient);

	if ( !::IsZoomed(*this) )
	{
		RECT rcSizeBox = m_pm.GetSizeBox();
		if ( pt.y < rcClient.top + rcSizeBox.top ) {
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTTOPLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTTOPRIGHT;
			return HTTOP;
		}
		else if ( pt.y > rcClient.bottom - rcSizeBox.bottom )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTBOTTOMLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTBOTTOMRIGHT;
			return HTBOTTOM;
		}
		if ( pt.x < rcClient.left + rcSizeBox.left )
			return HTLEFT;
		if ( pt.x > rcClient.right - rcSizeBox.right )
			return HTRIGHT;
	}

	RECT rcCaption = m_pm.GetCaptionRect();
	if ( pt.x >= rcClient.left + rcCaption.left && pt.x < rcClient.right - rcCaption.right
			&& pt.y >= rcCaption.top && pt.y < rcCaption.bottom )
	{
		DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_pm.FindControl(pt));
		if ( pControl && _tcsicmp(pControl->GetClass(), _T("ButtonUI")) != 0
				&& _tcsicmp(pControl->GetClass(), _T("OptionUI")) != 0)
			return HTCAPTION;
	}

	return HTCLIENT;
}

LRESULT CBossKeyWnd::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	SIZE szRoundCorner = m_pm.GetRoundCorner();
	if ( !::IsIconic(*this) && (szRoundCorner.cx != 0 || szRoundCorner.cy != 0) )
	{
		RECT rcClient;
		::GetClientRect(*this, &rcClient);
		RECT rc = { rcClient.left, rcClient.top + szRoundCorner.cx, rcClient.right, rcClient.bottom };
		HRGN hRgn1 = ::CreateRectRgnIndirect( &rc );
		HRGN hRgn2 = ::CreateRoundRectRgn(rcClient.left, rcClient.top, rcClient.right + 1,
			rcClient.bottom - szRoundCorner.cx, szRoundCorner.cx, szRoundCorner.cy);
		::CombineRgn( hRgn1, hRgn1, hRgn2, RGN_OR );
		::SetWindowRgn(*this, hRgn1, TRUE);
		::DeleteObject(hRgn1);
		::DeleteObject(hRgn2);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CBossKeyWnd::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	MONITORINFO oMonitor = {};
	oMonitor.cbSize = sizeof(oMonitor);
	::GetMonitorInfo(::MonitorFromWindow(*this, MONITOR_DEFAULTTOPRIMARY), &oMonitor);
	CRect rcWork = oMonitor.rcWork;
	rcWork.OffsetRect(-rcWork.left, -rcWork.top);

	LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
	lpMMI->ptMaxPosition.x = rcWork.left;
	lpMMI->ptMaxPosition.y = rcWork.top;
	lpMMI->ptMaxSize.x     = rcWork.right;
	lpMMI->ptMaxSize.y     = rcWork.bottom;

	bHandled = FALSE;
	return 0;
}

LRESULT CBossKeyWnd::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// 有时会在收到WM_NCDESTROY后收到wParam为SC_CLOSE的WM_SYSCOMMAND
	if ( wParam == SC_CLOSE )
	{
		::PostQuitMessage(0L);
		bHandled = TRUE;
		return 0;
	}
	//BOOL bZoomed = ::IsZoomed(*this);
	LRESULT lRes = DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);

	return lRes;
}

void CBossKeyWnd::SetParent(CBrowserWnd* pParentWnd)
{
	m_pMainWnd = pParentWnd;
}

void CBossKeyWnd::RegHotKey()
{
	m_nBossKeyId = ::GlobalAddAtom(_T("bosskey"));
	
	m_pMainWnd->m_nBossKeyId = m_nBossKeyId;
	::RegisterHotKey(*m_pMainWnd, m_nBossKeyId, 0, VK_F10);
	m_pMainWnd->m_bEnableBossKey = true;
	
}

void CBossKeyWnd::UnRegHotKey()
{
	::UnregisterHotKey(*m_pMainWnd, m_nBossKeyId);
	m_pMainWnd->m_bEnableBossKey = false;
}
LRESULT CBossKeyWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL bHandled = TRUE;
	switch (uMsg)
	{
	case WM_CREATE:
		lRes = OnCreate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_CLOSE:
		lRes = OnClose(uMsg, wParam, lParam, bHandled);
		break;
	case WM_DESTROY:
		lRes = OnDestroy(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCACTIVATE:
		lRes = OnNcActivate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCCALCSIZE:
		lRes = OnNcCalcSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCPAINT:
		lRes = OnNcPaint(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCHITTEST:
		lRes = OnNcHitTest(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SIZE:
		lRes = OnSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_GETMINMAXINFO:
		lRes = OnGetMinMaxInfo(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SYSCOMMAND:
		lRes = OnSysCommand(uMsg, wParam, lParam, bHandled);
		break;
	case WM_KEYDOWN:
		break;
	default:
		bHandled = FALSE;
	}
	if (bHandled)
	{
		return lRes;
	}
	if (m_pm.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);
}