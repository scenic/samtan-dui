#include "StdAfx.h"
#include "LabelItemUI.h"

namespace DuilibEx
{

	CLabelItemUI::CLabelItemUI(void)
	{
		m_pSeparator = new DuiLib::CLabelUI;
		m_pSeparator->ApplyAttributeList(_T("width=\"3\" bkcolor=\"#FFCCDAE7\""));

		m_pTabItem = new DuiLib::COptionUI;
		m_pTabItem->ApplyAttributeList(_T("mouse=\"false\" padding=\"0,0,0,0\" name=\"optLabItem\" group=\"tabLables\" width=\"80\""));

		m_pCloseBtn = new DuiLib::CButtonUI;
		m_pCloseBtn->ApplyAttributeList(_T("name=\"btnLabClose\" width=\"24\" height=\"28\" bkcolor=\"#00000000\" normalimage=\"file='Browser/res/close_label_normal.png' dest='0,4,20,24'\" hotimage=\"file='Browser/res/close_label_hot.png' dest='0,4,20,24'\" pushedimage=\"Browser/res/close_label_pushed.png dest='0,4,20,24'\""));

		this->Add(m_pSeparator);
		this->Add(m_pTabItem);
		this->Add(m_pCloseBtn);

		this->ApplyAttributeList(_T("name=\"labItem\" padding=\"3,0,0,0\" width=\"104\" height=\"28\" padding=\"0,4,0,0\" bkcolor=\"#FFE1EBF5\""));
	}

	LPVOID CLabelItemUI::GetInterface(LPCTSTR pstrName)
	{
		if (_tcscmp(pstrName, _T("LabelItem")) == 0)
			return static_cast<CLabelItemUI*>(this);
		return __super::GetInterface(pstrName);
	}

	void CLabelItemUI::DoEvent(DuiLib::TEventUI& event)
	{
		if (event.Type == DuiLib::UIEVENT_BUTTONDOWN)
		{
			m_pManager->SendNotify(this, DUI_MSGTYPE_SELECTCHANGED, event.wParam, event.lParam);
			this->ApplyAttributeList(_T("bkcolor=\"FFFFFFFF\""));
		}
		DuiLib::CControlUI::DoEvent(event);
	}

	CLabelItemUI::~CLabelItemUI(void) {}

} // namespace DuilibEx