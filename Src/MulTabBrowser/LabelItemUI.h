#pragma once

namespace DuilibEx
{

	// 自定义标签卡UI控件
	class CLabelItemUI : public DuiLib::CHorizontalLayoutUI
	{
	public:
		CLabelItemUI(void);
		~CLabelItemUI(void);

		LPCTSTR	GetClass() const { return _T("CLabelItemUI"); }
		LPVOID	GetInterface(LPCTSTR pstrName);
		void	DoEvent(DuiLib::TEventUI& event);

	private:
		DuiLib::CLabelUI*	m_pSeparator;	// 分割占位Label
		DuiLib::COptionUI*	m_pTabItem;		// 选项Option
		DuiLib::CButtonUI*	m_pCloseBtn;	// 关闭按钮
	};

} // namespace DuilibEx