#pragma once

#include "../DuiLib/UIlib.h"
#include "InterViewKeyMess.h"
class CHistoryObj;
class CInterView: public DuiLib::CWebBrowserUI
{
public:
	CInterView(void);
	~CInterView(void);
	
	static  HWND GetCurGameWnd(HWND hFindWnd, LPCTSTR lpClassName, LPCTSTR lpAncestorWndClassName);
	void	SetLock(BOOL bLock){m_bLock = bLock;}
	BOOL	IsLocked(){return m_bLock;}
	CString	GetUrl();
	BOOL	SetViewUrl(CString strUrl);
	void	SetRemark(CString strRemark){m_strRemark = strRemark;}
	CString GetRemark(){return m_strRemark;}
	BOOL	UzNavigate(CString strUrl);
	CHistoryObj* AddURLToHistory (const CString& strTitle, const CString& strURL);
	CInterViewKeyMess* GetCurrentKeyMess();
private:
	CString m_strTitle;
	CString m_strLink;
	CString m_strRemark;
	BOOL	m_bLock;
	CArray<CHistoryObj*, CHistoryObj*>	m_arHistory;
	
private:
	CInterViewKeyMess* m_KeyMess;
};

