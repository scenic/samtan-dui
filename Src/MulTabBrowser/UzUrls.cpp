#include "StdAfx.h"
#include "UzUrl.h"

#define IS_HTTP(str)		(str.Left(7) == _T("http://") || str.Left(8) == _T("https://"))

CUzUrl::CUzUrl(const CString& strUrl)
	: m_strUrl (strUrl)
{
}


CUzUrl::~CUzUrl(void)
{
}


CString	CUzUrl::GetUrl() const
{
	return m_strUrl;
}


CString	CUzUrl::GetTopUrl() const
{
	CString strTop;
	strTop.Empty();

	CString strUrl = m_strUrl;

	if (!IS_HTTP(strUrl))
		return strUrl;
	
	int p1 = strUrl.Find(_T("://"));
	p1+=3;

	int p2 = strUrl.Find(_T("/"), p1);
	if (p2<0)
	{
		strTop = strUrl + _T("/");
		return strTop;
	}

	strTop = strUrl.Left(p2+1);
	return strTop;
}


CString	CUzUrl::GetTopName() const
{
	CString strName = GetTopUrl ();

	int p1 = strName.Find(_T("://"));
	if (p1 >= 0)
		strName = strName.Right (strName.GetLength() - p1 -3); 

	int p2 = strName.Find(_T("/"));
	if (p2 >= 0)
		strName = strName.Left (p2);

	return strName;
}
