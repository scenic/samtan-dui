#pragma once
#include "UzUrl.h"
#include <vector>
#include <map>

class CTypedUrl : public CUzUrl
{
public:
	CTypedUrl(const CString& strUrl, const CString& strTitle);
	~CTypedUrl();

	CString	GetTitle() const;

private:
	CString m_strTitle;
};


class CTypedUrlsMgr;

class CTypedUrlsList
{
public:
	CTypedUrlsList(const CString& strUrlPrefix = CString(_T("")));
	~CTypedUrlsList();

	UINT  GetTypedUrlCount() const;
	const CTypedUrl&	GetTypedUrl(UINT nIndex) const;
	enum {MAX_LISTCOUT=15};
private:
	friend CTypedUrlsMgr;

	void	LoadTypedUrls();
	void    LoadTypedUrls2();
	void	LoadUrlsHistory();

	CString	m_strUrlPrefix;
	std::vector<CTypedUrl>	m_vecTypedUrls;
	std::map<CString, CString>	m_mapUrlsToTitles;
};


class CTypedUrlsMgr
{
public:
	static CTypedUrlsMgr& Instance();

	static void SaveUrl(const CString& strUrl);
	static void SaveUrl2(const CString& strNewUrl);
	static void DeleteUrl(const CString& strUrl);
	static void DeleteUrl2(const CString& strDeletedUrl);
	CTypedUrlsList*	GetTypedUrlsList();
private:
	CTypedUrlsMgr(void);
	~CTypedUrlsMgr(void);

	CTypedUrlsMgr(const CTypedUrlsMgr&);
	void operator = (const CTypedUrlsMgr&);
};

