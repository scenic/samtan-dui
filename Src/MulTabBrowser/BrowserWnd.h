/*denorm_absent*/
#pragma once
#include <map>
#include "stdafx.h"
#include "TypedUrlsMgr.h"
#include <list>


namespace DuilibEx
{
	class CLabelItemUI;
	class CSidePanelUI;
	class CWrapPanelUI;
}

class CInterView;
class CBossKeyWnd;
class CSideFavsWnd;
class CSideAccountMgrWnd;
class CLoginWnd;
class CSideHistoryWnd;
class CSideWndBase;
class CUrlSearchListWnd;
class CBrowserWnd : public DuiLib::CWindowWnd, public DuiLib::INotifyUI
{
public:
	CBrowserWnd();
	~CBrowserWnd(void);

	void Init();
	void OnPrepare();

	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual LPCTSTR GetWindowClassName() const { return _T("CBrowserWnd"); }

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void OnLockSideWnd(CSideWndBase* pSideWnd);
	void SetUrlEditText(DuiLib::CDuiString strText);
	void NewView(CString str=_T("www.baidu.com"));
	CInterView* GetCurentView();
	CInterView* GetViewByPos(int nSel);
	int  GetTabNum();
	void OnChangeView();

	void	OnFullScreen(); // 窗口全屏处理函数
	void	EndFullScreen(); // 结束全屏

	void	OnBossKey();		// 老板键处理函数
	void  OnClickGoto();
	//地址栏

	void	ShowSideWnd(CSideWndBase* pWnd, int nIndex);

private:
	DuiLib::CPaintManagerUI m_pm;
	

	/* 导航栏控件 */
	DuiLib::CEditUI* m_peditUrl;                       // URL输入栏
	DuiLib::CButtonUI* m_pbtnUrlList;                  // URL列表

	/* 侧边栏控件 */
	DuiLib::CTabLayoutUI* m_ptlaySideToolWnd;          //
//  CVerticalLayoutUI* m_pvlaySideFavsTreeWnd; // 收藏夹树窗口
	DuiLib::CButtonUI* m_pbtnHiddenSide;               // 隐藏/显示侧边栏
//  CContainerUI* m_pcontSideToolWnd;          //
	DuilibEx::CSidePanelUI*	m_pCtnrSideAccountWnd;       //
	DuilibEx::CSidePanelUI*	m_pCtnrSideFavsWnd;          //
	DuilibEx::CSidePanelUI*	m_pCtnrSideHistoryWnd;       //

	DuiLib::COptionUI* m_poptSideFavs;
	DuiLib::COptionUI* m_poptAccount;                  // 账号按钮
	DuiLib::COptionUI* m_poptSideHistory;              // 历史记录

	/* 标签栏控件 */
	DuiLib::CVerticalLayoutUI* m_pvlayLables;          // 标签组
	DuiLib::CButtonUI* m_pbtnNewLabel;                 // 添加新标签
	DuiLib::CButtonUI* m_pbtnLabClose;                 // 关闭标签
	DuiLib::CButtonUI* m_pbtnHistoryList;              // 标签栏历史记录按钮

	DuilibEx::CWrapPanelUI* m_phlayLablesLine;        // 标签栏
	DuiLib::CVerticalLayoutUI* m_vlaySideMenu;         // 侧边栏

	DuiLib::CTabLayoutUI* m_ptlayBrowserClient;        // 浏览器客户区
	DuiLib::CTabLayoutUI* m_ptlayMain;                 // 主客户区

	/* 子窗口 */
	CSideFavsWnd* m_pSideFavsWnd;              // 收藏夹窗口
	CSideAccountMgrWnd* m_pSideAccountMgrWnd;  // 账号管理窗口
	CSideHistoryWnd* m_pSideHistoryWnd;        // 历史记录窗口
	CBossKeyWnd* m_pBossKeyWnd;                // 老板键窗口
	CLoginWnd* m_pLoginWnd;                    // 登录窗口
	CUrlSearchListWnd* pUrlWnd ;               // 编辑框Url

private:
	std::map<CSideWndBase*, DuilibEx::CSidePanelUI*>	m_mapCtnrWnd; // 容器窗口映射
	std::map<DuilibEx::CLabelItemUI*, CInterView*>	m_mapTableBrowser;
	std::vector<DuiLib::CWindowWnd*>				m_childWindows;	// 子窗口链表
	std::vector<CSideWndBase*>						m_dockWnds;		// 停靠窗口链表

	/* 窗口全屏 */
	bool	m_bFullScreen;		// 是否全屏标志
	RECT	m_oldScreenRect;	// 进入全屏前的窗口矩形

	/* 热键id */
	int		m_nBossKeyId;		// 老板键id
	bool	m_bEnableBossKey;	// 热键Id
	
	DuiLib::CWebBrowserEventHandler* m_pWebBrowserEventHandler;

public:
	/* 子窗口类 */
	friend	class	CBossKeyWnd;
};