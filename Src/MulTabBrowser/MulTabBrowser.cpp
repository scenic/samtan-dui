// MulTabBrowser.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "MulTabBrowser.h"

#include "UrlSearchListWnd.h"
#include "Util.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMulTabBrowserApp

BEGIN_MESSAGE_MAP(CMulTabBrowserApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

// CMulTabBrowserApp construction

CMulTabBrowserApp::CMulTabBrowserApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

// The one and only CMulTabBrowserApp object

CMulTabBrowserApp theApp;

// CMulTabBrowserApp initialization

BOOL CMulTabBrowserApp::InitInstance()
{
    CoInitialize(NULL);
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();
	InitHistory();
	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager* pShellManager = new CShellManager;

	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

    DuiLib::CPaintManagerUI::SetInstance(AfxGetInstanceHandle());
    DuiLib::CPaintManagerUI::SetResourcePath(DuiLib::CPaintManagerUI::GetInstancePath() + _T("..\\skin\\"));
   	pBrowser = new CBrowserWnd;
    pBrowser->Create(NULL, _T("�����"), WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
	pBrowser->CenterWindow();
    pBrowser->ShowModal();

	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CMulTabBrowserApp::ExitInstance()
{
    // TODO: Add your specialized code here and/or call the base class
    CoUninitialize();
    return CWinApp::ExitInstance();
}

void CMulTabBrowserApp::InitHistory()
{
	m_History.Load();
	m_History.LoadLastOpen(m_History.GetLastOpenList());
}