#include "StdAfx.h"
#include "InterViewKeyMess.h"
#include "DuLibKeySet.h"
#include "InterView.h"


CInterViewKeyMess::CInterViewKeyMess(void)
{
	InitKeySet();
}


CInterViewKeyMess::~CInterViewKeyMess(void)
{
	m_KeyMess.clear();
}

void CInterViewKeyMess::InitKeySet()
{
	m_KeyMess.clear();
	for(unsigned int i=0;i<CKeySetListMgr::Instance().GetCount();i++)
	{
		ViewKey temp;
		temp.nID=CKeySetListMgr::Instance().m_KeyList[i].nID;
		temp.m_used=false;
		m_KeyMess.push_back(temp);
	}
}

bool CInterViewKeyMess::IsKeyUsed(int key)
{
	for(unsigned i=0;i<m_KeyMess.size();i++)
	{
		if(m_KeyMess[i].nID==key)
		{
			return m_KeyMess[i].m_used;
		}
	}
	return false;
}

bool CInterViewKeyMess::FindKey(int key)
{

	for(unsigned i=0;i<m_KeyMess.size();i++)
	{
		if(m_KeyMess.at(i).nID==key)
		{
			return TRUE;
		}
	}
	return FALSE;
}
void  CInterViewKeyMess::SetCurrentKey(int key,bool used)
{
	//增加热键
	if(CKeySetListMgr::Instance().GetCount()>m_KeyMess.size())
	{
		ViewKey temp;
		temp.m_used=used;
		temp.nID=key;
		m_KeyMess.push_back(temp);
	}
	//修改热键
	else
	{
		std::vector<ViewKey>::iterator it=m_KeyMess.begin();
		for(;it!=m_KeyMess.end();it++)
		{
			if(!CKeySetListMgr::Instance().FindKey(it->nID))
			{
				m_KeyMess.erase(it);
				ViewKey temp;
				temp.m_used=used;
				temp.nID=key;
				m_KeyMess.push_back(temp);
				break;
			}
			else{
				if(it->nID==key)
				{
					it->m_used=used;
					break;
				}
			}
		}
	}
}

void CInterViewKeyMess::OnViewChange()
{
	//删除
	std::vector<ViewKey>::iterator it=m_KeyMess.begin();
	if(m_KeyMess.size()>CKeySetListMgr::Instance().GetCount())
	{
		for(;it!=m_KeyMess.end();it++)
		{
			if(!CKeySetListMgr::Instance().FindKey(it->nID))
			{

				m_KeyMess.erase(it);
			}
		}
	}
	//增加
	if(m_KeyMess.size()<CKeySetListMgr::Instance().GetCount())
	{
		for(unsigned int i=0;i<CKeySetListMgr::Instance().GetCount();i++)
		{
			if(!FindKey(CKeySetListMgr::Instance().m_KeyList[i].nID))
			{
				ViewKey temp; 
				temp.nID=CKeySetListMgr::Instance().m_KeyList[i].nID;
				temp.m_used=false;
				m_KeyMess.push_back(temp);
			}
		}
	}
	if(CKeySetListMgr::Instance().GetKeyListView()!=NULL)
	{
		((CDulibKeySetWnd*)CKeySetListMgr::Instance().GetKeyListView())->ReloadKeyMess();
	}
	HWND hCurGameWnd=NULL;
	if(CKeySetListMgr::Instance().ISGameView(hCurGameWnd))
	{
		CKeySetListMgr::Instance().AddHookKey();
	}
}