#pragma once

namespace DuilibEx
{
	class CSidePanelUI;
}

//////////////////////////////////////////////////////////////////////////
// ��������ڻ���
class CSideWndBase : public DuiLib::CWindowWnd, public DuiLib::INotifyUI
{
public:
	CSideWndBase(void);
	~CSideWndBase(void);
	
	void	SetOwner(DuilibEx::CSidePanelUI* pOwner) { m_pOwner = pOwner; }
	bool IsLock() { return m_bLock; }

protected:
	bool m_bLock;
	DuilibEx::CSidePanelUI* m_pOwner;

	DuiLib::CPaintManagerUI m_pm;
};

