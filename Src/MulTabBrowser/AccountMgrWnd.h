/*!@file
*******************************************************************************************************
<PRE>
模块名		："帐号管理"界面类
文件名		：AccountMgrWnd
相关文件		：AccountMgrWnd.cpp/Duilib相关库
文件实现功能：实现帐号管理界面的逻辑，使外部只需要调用这一个类即可实现帐号管理界面所有操作
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了Duilib库
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/6/12 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
class CAccountMgrWnd : public DuiLib::WindowImplBase
{
public:
	CAccountMgrWnd(void);
	~CAccountMgrWnd(void);

public:
	virtual LPCTSTR    GetWindowClassName() const   {   return _T("CAccountMgr");  }
	virtual DuiLib::CDuiString GetSkinFile()                {   return _T("SideAccountWnd//account_mgr_wnd.xml");    }
	virtual DuiLib::CDuiString GetSkinFolder()              {   return _T("");  }
	void Init(HWND hWndParent, CAccountMgrWnd** pAccountWnd);

	void InitWindow();
protected:
	// 消息响应:
	virtual void OnClick(DuiLib::TNotifyUI& msg);
	virtual void OnHeaderClick(DuiLib::TNotifyUI& msg);
	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);	//自定义消息

public:
	// 添加游戏,strName为游戏名称
	bool AddGameName(CString& strName);
	// 新建帐号,strName为用户名，strPassWord为密码，strURL为游戏网址，strNote为备注
	bool AddAccount(CString& strName, CString& strPassWord, CString& strURL, CString& strNote, bool bEdit = false);

private:
	// 设置编辑框是否响应,bAtt为是否编辑，nIndex为索引号, 当bAccountIndexValid为True时，nIndex为当前索引号
	bool SetEditEnabled(bool bAtt, int& nIndex);
	// 删除选择的帐号,nIndex为索引号
	bool DelAccount(int& nIndex);
	// 上移一行,nIndex为索引号
	bool AccountUp(int& nIndex);
	// 下移一行,nIndex为索引号
	bool AccountDown(int& nIndex);

private:
	DuiLib::CListUI* m_pListAccount;	// 添加帐号List列表
	DuiLib::CListUI* m_pListAddGame;	// 添加游戏List列表

	int m_nListAddGame;					// 定义点击的添加游戏List Index
	int m_nListAccount;					// 定义点击的添加帐号List Index
	RECT  m_AccountRect;				// 定义添加帐号的Index矩形坐标

	CAccountMgrWnd** m_pAccountWnd;		// 本类的指针
	HWND m_parentHwnd;					// 父窗口指针
};

