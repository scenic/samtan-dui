#pragma once

#include "StdAfx.h"
#include <iostream>

class CLog
{
public:
	enum LogLevel // Log级别
	{
		lvInfo = 0, // 一般
		lvWarning = 1, // 警告
		lvError = 2 // 错误
	};

	CLog ();
	~CLog ();

	bool Open(LPCTSTR pszFileName);
	void Close(); // 关闭日志记录
	void Write(int nLogLevel, LPCTSTR pszFormat, ...); // 格式化写入日志信息

private:
	FILE* m_out;
};