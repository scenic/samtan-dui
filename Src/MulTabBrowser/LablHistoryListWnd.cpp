#include "StdAfx.h"

#include "LablHistoryListWnd.h"

//////////////////////////////////////////////////////////////////////////
// CLablHistoryListWnd 标签栏历史记录列表窗口

LRESULT CLablHistoryListWnd::OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	Close();
	bHandled = FALSE;
	return 0;
}

void CLablHistoryListWnd::SetPos(HWND hWndParent, POINT ptPos)
{
	Create(hWndParent, _T("CLablHistoryListWnd"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);

	::SetWindowPos(*this, NULL, ptPos.x - 350, ptPos.y + 140, 0, 0, SWP_NOSIZE);
}

void CLablHistoryListWnd::ResizeClient(int /*cx*/ , int /*cy*/)
{
	//  ::SetWindowPos(*this, NULL, 100, 100);
}

void CLablHistoryListWnd::Notify(DuiLib::TNotifyUI &/*msg*/) {}

LRESULT CLablHistoryListWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL    bHandled = TRUE;

	switch ( uMsg )
	{
	case WM_KILLFOCUS:
		lRes = OnKillFocus(uMsg, wParam, lParam, bHandled);
		break;
	default:
		bHandled = FALSE;
	}

	if (bHandled || m_PaintManager.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return __super::HandleMessage(uMsg, wParam, lParam);
}