/*!@file
*******************************************************************************************************
<PRE>
模块名		："管理收藏夹"XML文件读写类
文件名		：CManageFavXML
相关文件		：CManageFavXML.cpp/ManageFavHeader/tinyxml2相关库
文件实现功能：实现收藏夹XML文件的读写等相关操作
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了tinyxml2项目
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/6/14 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
#include "ManageFavHeader.h"
#include "../tinyxml2/Public/tinyxml2.h"
#include <list>

class CManageFavXML
{
public:
	CManageFavXML(void);
	~CManageFavXML(void);

public:
	// 添加一个收藏夹，strFolderName为收藏夹名，strParentName为收藏夹父结点名
	bool AddFolder(CString& strFolderName, CString& strParentName);
	// 添加一个网址连接, strTitle为标题，strURL为网址， strParentName为父结点收藏夹名
	bool AddLink(CString& strTitle, CString& strURL, CString strParentName);
	// 删除一个收藏夹, strFolderName为收藏夹名
	bool DelFolder(CString& strFolderName);
	// 删除一个网址连接， strTitle为标题,  strParentName为父结点收藏夹名
	bool DelLink(CString& strTitle, CString& strParentName);
	// 删除全部收藏
	bool DelFav();
	// 获取XML全部信息,TreeFolder用来存放文件夹信息，ListLink用来存放List列表信息
	bool GetAllXML(std::list<TREESORT>& TreeFolder, std::list<LIST_LINK>& ListLink);
private:
	// 获取XML全部信息
	bool GetAllXML(std::list<TREESORT>& TreeFolder, std::list<LIST_LINK>& ListLink, tinyxml2::XMLElement* root);

	// 查找指定Item，root为父结点，strName为结点名, ，nType为查找的结点类型（文件夹或网址）
	tinyxml2::XMLElement* FindItem(tinyxml2::XMLElement* root, CString& strName, int nType);
	// 创建XML
	void CreateXmlFile();
	// 判断XML文件是否存在
	static  BOOL    IsXmlFileExist(const CString& strFile);
	// 获取XML文件名
	CStringA GetXmlFileName();
private:
	CString m_strXMLPath;			// 收藏夹XML路径
	CFavToolFunc* m_FavToolFunc;	// 工具函数对象
};

