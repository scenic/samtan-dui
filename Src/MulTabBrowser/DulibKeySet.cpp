#include "stdafx.h"
#include "DuLibKeySet.h"
#include "resource.h"
#include "KeySetListMgr.h"
#include "../Include/UzDefine.h"
CDulibKeySetWnd::CDulibKeySetWnd()
{
	m_addmess=TRUE;
	m_bMouseLXClickCapture=false;
	m_bMouseClickCapture=false;
}
LPCTSTR CDulibKeySetWnd::GetWindowClassName()const
{
	return _T("CDulibKeySetWnd");
}

UINT CDulibKeySetWnd::GetClassStyle() const 
{
	return CS_DBLCLKS;
}

void CDulibKeySetWnd::OnFinalMessage(HWND /*hWnd*/) 
{
	delete this;
}

LRESULT CDulibKeySetWnd::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	CKeySetListMgr::Instance().DestroyKeySetWindow();
	bHandled=FALSE;
	return 0;
}

LRESULT CDulibKeySetWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL bHandled = TRUE;
	switch( uMsg ) {
	case WM_CREATE:        lRes = OnCreate(uMsg, wParam, lParam, bHandled); break;
	case WM_NCACTIVATE:    lRes = OnNcActivate(uMsg, wParam, lParam, bHandled); break;
	case WM_NCCALCSIZE:    lRes = OnNcCalcSize(uMsg, wParam, lParam, bHandled); break;
	case WM_NCPAINT:       lRes = OnNcPaint(uMsg, wParam, lParam, bHandled); break;
	case WM_NCHITTEST:     lRes = OnNcHitTest(uMsg, wParam, lParam, bHandled); break;
	case WM_DESTROY:	   lRes =OnDestroy(uMsg, wParam, lParam, bHandled);break;
	case WM_SIZE:          lRes = OnSize(uMsg, wParam, lParam, bHandled); break;
	case WM_NCLBUTTONDBLCLK:lRes = OnNCLButtonDblclick(uMsg, wParam, lParam, bHandled); break;
	case WM_GETMINMAXINFO: lRes = OnGetMinMaxInfo(uMsg, wParam, lParam, bHandled); break;
	case WM_SYSCOMMAND:    lRes = OnSysCommand(uMsg, wParam, lParam, bHandled); break;
	case WM_LBUTTONDOWN:   lRes = OnLButtonDown(uMsg, wParam, lParam, bHandled); break;
	case WM_LBUTTONUP:   lRes = OnLButtonUp(uMsg, wParam, lParam, bHandled); break;
	case WM_CLOSE:			lRes=OnClose(uMsg, wParam, lParam, bHandled); break;
	case SET_HOOK_STATE:   lRes=OnSetState(uMsg, wParam, lParam, bHandled); break;
	default:
		bHandled = FALSE;
	}
	if( bHandled ) return lRes;
	if( m_PaintMsg.MessageHandler(uMsg, wParam, lParam, lRes) ) return lRes;
	return CWindowWnd::HandleMessage(uMsg, wParam, lParam);
}

LRESULT CDulibKeySetWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	LONG styleValue = ::GetWindowLong(*this, GWL_STYLE);
	//styleValue &= ~WS_CAPTION;
	::SetWindowLong(*this, GWL_STYLE, styleValue | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	m_PaintMsg.Init(m_hWnd);
	DuiLib::CDialogBuilder builder;
	DuiLib::CControlUI* pRoot = builder.Create(_T("KeySet//KeySetSkin.xml"), (UINT)0,  NULL, &m_PaintMsg);
	ASSERT(pRoot && "Failed to parse XML");
	m_PaintMsg.AttachDialog(pRoot);
	m_PaintMsg.AddNotifier(this);
	InitControl();
	return 0;
}

LRESULT CDulibKeySetWnd::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CDulibKeySetWnd::OnSetState(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	if(CKeySetListMgr::Instance().GetHookState())
	{
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetText(_T("热键已启用"));
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetTextColor(0xFF4DB849);

	}
	else
	{
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetText(_T("热键未启用"));
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetTextColor(0xFFE94D43);
	}
	bHandled=TRUE;
	return 0;
}

LRESULT CDulibKeySetWnd::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CDulibKeySetWnd::OnNCLButtonDblclick(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CDulibKeySetWnd::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	
	bHandled=FALSE;
	return 0;
}

LRESULT CDulibKeySetWnd::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if( wParam == SC_CLOSE ) {
		this->Close();
		bHandled = TRUE;
		return 0;
	}
	return CWindowWnd::HandleMessage(uMsg,wParam,lParam);
}

LRESULT CDulibKeySetWnd::OnLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	POINT pt; 
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_PaintMsg.FindControl(pt));
	if(pControl->GetName()==_T("MouseClickIcon"))
	{
		HCURSOR hCur = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_RED));
		SetSystemCursor(hCur, 32512);
		ShowWindow(SW_HIDE);
		::ShowWindow(GetParent(this->m_hWnd),SW_SHOW);
		SetCapture(this->m_hWnd);
		m_bMouseClickCapture=true;
		bHandled=TRUE;
		return 0;
	}
	else if(pControl->GetName()==_T("LXMouseIcon"))
	{
		HCURSOR hCur = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_GREEN));
		SetSystemCursor(hCur, 32512);
		ShowWindow(SW_HIDE);
		SetCapture(this->m_hWnd);
		m_bMouseLXClickCapture=true;
		bHandled=TRUE;
		return 0;
	}
	bHandled=FALSE;
	return 0;
}

LRESULT CDulibKeySetWnd::OnLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	
		SystemParametersInfo(SPI_SETCURSORS, 0, NULL, SPIF_SENDCHANGE);
		ReleaseCapture();
		ShowWindow(true);
		if(m_bMouseClickCapture)
		{
			CString str=_T("");
			POINT pt; 
			pt.x = GET_X_LPARAM(lParam);
			pt.y = GET_Y_LPARAM(lParam);
			ClientToScreen(this->m_hWnd,&pt);
			str.Format(_T("%d,%d"),pt.x,pt.y);
			DuiLib::CEditUI* MousePoint=static_cast<DuiLib::CEditUI*>(m_PaintMsg.FindControl(_T("MouseClick_point")));
			MousePoint->SetText(str);
			m_bMouseClickCapture=false;
		}
		else if(m_bMouseLXClickCapture)
		{
			CString str=_T("");
			POINT pt; 
			pt.x = GET_X_LPARAM(lParam);
			pt.y = GET_Y_LPARAM(lParam);
			ClientToScreen(this->m_hWnd,&pt);
			str.Format(_T("(%d,%d)"),pt.x,pt.y);
			DuiLib::CEditUI* MousePoint=static_cast<DuiLib::CEditUI*>(m_PaintMsg.FindControl(_T("MouseLXClick_MousePoint")));
			if(strMouseLXText.GetLength()==0)
			{
				strMouseLXText+=str;
			}
			else
			{
				strMouseLXText+=_T(",");
				strMouseLXText+=str;
			}
			MousePoint->SetText(strMouseLXText);
			m_bMouseLXClickCapture=false;
		}
		bHandled = FALSE;
	return 0;
}


LRESULT CDulibKeySetWnd::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled=FALSE;
	return 0;
}

LRESULT CDulibKeySetWnd::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	POINT pt; pt.x = GET_X_LPARAM(lParam); pt.y = GET_Y_LPARAM(lParam);
	::ScreenToClient(*this, &pt);

	RECT rcClient;
	::GetClientRect(*this, &rcClient);

	RECT rcCaption =m_PaintMsg.GetCaptionRect();
	if( pt.x >= rcClient.left + rcCaption.left && pt.x < rcClient.right - rcCaption.right \
		&& pt.y >= rcCaption.top && pt.y < rcCaption.bottom ) 
	{
		DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_PaintMsg.FindControl(pt));
		if( pControl && _tcscmp(pControl->GetClass(), _T("ButtonUI")) != 0)
		{
			return HTCAPTION;
		}
	}

	return HTCLIENT;
}

LRESULT CDulibKeySetWnd::OnNcActivate(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if( ::IsIconic(*this) )bHandled = FALSE;
	return (wParam == 0) ? TRUE : FALSE;
}

LRESULT CDulibKeySetWnd::OnNcCalcSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

void CDulibKeySetWnd::ReloadKeyMess()
{
	m_listKey->RemoveAll();
	int m_count=CKeySetListMgr::Instance().GetCount();
	for(int i=0;i<m_count;i++){
	DuiLib::CDialogBuilder builder;
	DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("KeySet//listElement.xml"), (UINT)0, NULL, &m_PaintMsg);   // duilib.xml需要放到exe目录下
	
	CString strkeyID=CKeySetListMgr::KeyValue(CKeySetListMgr::Instance().m_KeyList[i].nID);
	DuiLib::CLabelUI*m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_key_value"));
	m_KeyValue->SetText(strkeyID);


	HOTKET_TYPE keyType=CKeySetListMgr::Instance().m_KeyList[i].type;
	CString strDes=_T("");
	switch(keyType)
	{
		{
	case TYPE_LXKEY:
		strDes=_T("连续按键");
		break;
	case TYPE_SETKEY:
		strDes=_T("改键");
		break;
	case TYPE_MOUSEPRESS:
		strDes=_T("鼠标点击");
		break;
	case TYPE_MOUSELXPRESS:
		strDes=_T("鼠标连点");
		break;
	case TYPE_STOP:
		continue;
		break;
		}
	}
	CString strDetails=CKeySetListMgr::Instance().m_KeyList[i].detils;
	strDetails=strDes+_T(" ")+strDetails;
	DuiLib::CLabelUI*m_KeyDetails=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_key_detail"));
	m_KeyDetails->SetText(strDetails);
	BOOL m_IsUsed=CKeySetListMgr::IsCurrentViewKeyUsed(CKeySetListMgr::Instance().m_KeyList[i].nID);
	if(m_IsUsed)
	{
		DuiLib::CLabelUI*m_KeyUsed=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_key_used"));
		m_KeyUsed->SetText(_T("已启用"));
		m_KeyUsed->SetTextColor(0xFF4DB849);
	}
	else
	{
		DuiLib::CLabelUI*m_KeyUsed=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_key_used"));
		m_KeyUsed->SetText(_T("未启用"));
		m_KeyUsed->SetTextColor(0xFFE94D43);
	}
	m_listKey->Add(pRoot);
	}
	if(CKeySetListMgr::Instance().GetHookState())
	{
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetText(_T("热键已启用"));
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetTextColor(0xFF4DB849);
		
	}
	else
	{
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetText(_T("热键未启用"));
		((DuiLib::CTextUI*)m_PaintMsg.FindControl(_T("Hook_State")))->SetTextColor(0xFFE94D43);
	}
	
}

void CDulibKeySetWnd::Notify(DuiLib::TNotifyUI& msg)
{
	
		if( msg.sType == _T("windowinit") )
		{
			ReloadKeyMess();
			InitChildWnd();
			return;
		}
		else if( msg.sType == _T("click") ) 
		{
			if(msg.pSender->GetName()==_T("closebtn"))
			{
				this->Close();
				return; 
			}
			else if(msg.pSender->GetName()==_T("btnCancel"))
			{
				this->Close();
				return; 
			}
			else if(msg.pSender->GetName()==_T("Confirm"))
			{
				OnClickAddKeyInfor();
				return;
			}
			else if(msg.pSender->GetName()==_T("resetpoint"))
			{
				DuiLib::CEditUI* MousePoint=static_cast<DuiLib::CEditUI*>(m_PaintMsg.FindControl(_T("MouseLXClick_MousePoint")));
				strMouseLXText=_T("");
				MousePoint->SetText(_T(""));
				return;
			}
			
			
		}

		else if(msg.sType==_T("selectchanged"))
		{
			DuiLib::CDuiString name = msg.pSender->GetName();
			DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintMsg.FindControl(_T("switch")));
			if(name==_T("keyset_option"))	
			{
				pControl->SelectItem(0);
			}	
			else if(name==_T("LXkey_option"))
			{
				pControl->SelectItem(1);
			}
			else if(name==_T("MouseCLick_option"))
			{
				pControl->SelectItem(2);
			}
			else if(name==_T("MouseLXClick_option"))
			{
				pControl->SelectItem(3);
			}	
		}
		else if(msg.sType== DUI_MSGTYPE_ITEMCLICK)
		{

			int nSel = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();
			if(-1==nSel)return;
			if(m_PaintMsg.FindControl(msg.ptMouse)->GetName()==_T("item_del"))
			{
				DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_listKey->GetItemAt(nSel);
				DuiLib::CLabelUI*m_text=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_key_value"));
				CString strID=m_text->GetText();
				int nID=CKeySetListMgr::GetKeyValueByStr(strID);
				CKeySetListMgr::Instance().RemoveKey(nID);
				m_listKey->RemoveAt(nSel);
				m_addmess=TRUE;
				((DuiLib::CButtonUI*)m_PaintMsg.FindControl(_T("Confirm")))->SetText(_T("添加"));
				HWND wnd;
				if(CKeySetListMgr::Instance().ISGameView(wnd))
				{
					CKeySetListMgr::Instance().AddHookKey();
				}
				return;
			}
			else if(m_PaintMsg.FindControl(msg.ptMouse)->GetName()==_T("item_edit"))
			{
				DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_listKey->GetItemAt(nSel);
				DuiLib::CLabelUI*m_text=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_key_value"));
				CString strID=m_text->GetText();
				int nID=CKeySetListMgr::GetKeyValueByStr(strID);
				oldmess=CKeySetListMgr::Instance().FindKeyMess(nID);
				oldmess.used=CKeySetListMgr::IsCurrentViewKeyUsed(oldmess.nID);
				m_addmess=FALSE;
				((DuiLib::CButtonUI*)m_PaintMsg.FindControl(_T("Confirm")))->SetText(_T("保存"));
				ShowMessToChildDlg(oldmess.type,oldmess.detils);
				return;
			}
			else
			{
				InitChildWnd();
				return;
			}
		}
		else if(msg.sType==DUI_MSGTYPE_TEXTCHANGED)
		{
			
		}
}

int CDulibKeySetWnd::GetCurrentKeyType()
{
	DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintMsg.FindControl(_T("switch")));
	return pControl->GetCurSel();
}

void CDulibKeySetWnd::OnClickAddKeyInfor()
{
	switch(GetCurrentKeyType())
	{
	case 0:
		//改键
		if(!OnAddKeySetInfor())return;
		break;

	case 1:
		//连续按键
		if(!OnAddContinuesKeyInfor())return;
		break;
	case 2:
		//鼠标点击
		if(!OnAddMouseClickInfor())return;
		break;
	case 3:
		//鼠标连点
		if(!OnAddMouseLXClickInfor())return;
		break;
	default:
		break;
	}
	ReloadKeyMess();
	InitChildWnd();
}

void CDulibKeySetWnd::InitControl()
{
	m_listKey=static_cast<DuiLib::CListUI*>(m_PaintMsg.FindControl(_T("ListKey")));
}

//改键
bool CDulibKeySetWnd::OnAddKeySetInfor()
{
	KeyMess m_keySetMess;
	if(static_cast<DuiLib::CCheckBoxUI*>(m_PaintMsg.FindControl(_T("key_used")))->GetCheck())
	{
		m_keySetMess.used=true;
	}
	else
	{
		m_keySetMess.used=false;
	}
	CString strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->GetText();
	if(strValue.GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("热键内容不能为空"),_T("错误"),MB_OK);
		((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->SetFocus();
		return false;
	}
	m_keySetMess.nID=CKeySetListMgr::GetKeyValueByStr(strValue);
	DuiLib::CEditUI *m_keysetDetials=static_cast<DuiLib::CEditUI*>(m_PaintMsg.FindControl(_T("KeySet_Details")));
	CString strDetials=m_keysetDetials->GetText();
	if(!CKeySetListMgr::CheckString(strDetials))
	{
		MessageBox(this->m_hWnd,_T("字符串不符合规定"),_T("错误"),MB_OK);
		return false;
	}
	m_keySetMess.detils=strDetials;
	m_keySetMess.type=TYPE_SETKEY;

	//添加热键
	if(m_addmess)
	{
		if(CKeySetListMgr::Instance().IsKeyUsed(m_keySetMess.nID))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在，请重新设置"),_T("提醒"),MB_OK|MB_ICONWARNING);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->SetFocus();
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().AddKey(m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("添加失败"),_T("错误"),MB_OK);
			return FALSE;
		}
		CKeySetListMgr::Instance().SaveKey(m_keySetMess);
	}
	//修改热键
	else{
		if(m_keySetMess==oldmess)
		{
			MessageBox(this->m_hWnd,_T("没有任何修改"),_T(""),MB_OK);
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().EditKey(oldmess.nID,m_keySetMess))
		{
			
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->SetFocus();
			return FALSE;
		}
	}
	bool oldState=CKeySetListMgr::IsCurrentViewKeyUsed(m_keySetMess.nID);
	CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
	if(m_keySetMess.used==true||oldState==true)
	{
		if (!CKeySetListMgr::Instance().AddHookKey())
		{
			m_keySetMess.used = FALSE;
			CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
		}
	}

	return true;
}

//连续按键
bool CDulibKeySetWnd::OnAddContinuesKeyInfor()
{
	KeyMess m_keySetMess;
	if(static_cast<DuiLib::CCheckBoxUI*>(m_PaintMsg.FindControl(_T("LXkey_used")))->GetCheck())
	{
		m_keySetMess.used=true;
	}
	else
	{
		m_keySetMess.used=false;
	}
	TCHAR KeyStart='0';
	int time=-1;
	int num=-1;
	CString strLXKeyValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_StartKey")))->GetText();
	if(strLXKeyValue.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("热键内容不能为空"),_T("错误"),MB_OK);

		return false;
	}
	m_keySetMess.nID=CKeySetListMgr::GetKeyValueByStr(strLXKeyValue);
	
	CString strKeyStart=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Begin")))->GetText();
	if(strKeyStart.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("按键内容不能为空"),_T("错误"),MB_OK);
		return false;
	}
	KeyStart=strKeyStart[0];

	strKeyStart=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Num")))->GetText();
	if(_ttoi(strKeyStart)==0)
	{
		MessageBox(this->m_hWnd,_T("次数不能为空"),_T("错误"),MB_OK);
		return FALSE;
	}
	num=_ttoi(strKeyStart);

	strKeyStart=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Time")))->GetText();
	if(_ttoi(strKeyStart)==0)
	{
		MessageBox(this->m_hWnd,_T("请输入时间间隔"),_T("错误"),MB_OK);
		return FALSE;
	}
	time=_ttoi(strKeyStart);

	strKeyStart=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_End")))->GetText();
	if(strKeyStart.GetLength()==0){
		MessageBox(this->m_hWnd,_T("终止键内容不能为空"),_T("错误"),MB_OK);
		return FALSE;
	}
	CString EndKey=strKeyStart;
	strKeyStart.Format(_T("%c,%d,%d,%s"),KeyStart,num,time,EndKey);
	m_keySetMess.detils=strKeyStart;
	m_keySetMess.type=TYPE_LXKEY;


	//防止终止键被占用
	if(m_addmess)
	{
		if(CKeySetListMgr::Instance().IsKeyUsed(m_keySetMess.nID))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_StartKey")))->SetFocus();
			return FALSE;
		}
		if(CKeySetListMgr::Instance().IsKeyUsed(CKeySetListMgr::GetKeyValueByStr(EndKey)))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在,请重新设置终止键!"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_End")))->SetFocus();
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().AddKey(m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("添加失败"),_T(""),MB_OK);
			return FALSE;
		}
		CKeySetListMgr::Instance().SaveKey(m_keySetMess);
	}
	//修改热键
	else{
		if(m_keySetMess==oldmess)
		{
			MessageBox(this->m_hWnd,_T("没有任何修改!"),_T(""),MB_OK);
			return FALSE;
		}
		if(CKeySetListMgr::Instance().IsKeyUsed(oldmess.nID,CKeySetListMgr::GetKeyValueByStr(EndKey)))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在,请重新设置终止键!"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_End")))->SetFocus();
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().EditKey(oldmess.nID,m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在，请重新设置"),_T("提醒"),MB_OK|MB_ICONWARNING);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_StartKey")))->SetFocus();
			return FALSE;
		}
	}
	bool oldState=CKeySetListMgr::IsCurrentViewKeyUsed(m_keySetMess.nID);
	CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
	if(m_keySetMess.used==true||oldState==true){
		if (!CKeySetListMgr::Instance().AddHookKey())
		{
			m_keySetMess.used = FALSE;
			CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);

		}
	}

	return true;
}

//鼠标单击
bool CDulibKeySetWnd::OnAddMouseClickInfor()
{
	KeyMess m_keySetMess;
	int MouseEvent=0;//鼠标单击或双击
	CString MousePos=_T("");
	int num=-1;        //点击次数
	int time=-1;       //时间间隔
	if(static_cast<DuiLib::CCheckBoxUI*>(m_PaintMsg.FindControl(_T("MouseClick_Used")))->GetCheck())
	{
		m_keySetMess.used=true;
	}
	else
	{
		m_keySetMess.used=false;
	}
	CString strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->GetText();
	if(strValue.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("热键内容不能为空"),_T("错误"),MB_OK);
		return false;
	}
	m_keySetMess.nID=CKeySetListMgr::GetKeyValueByStr(strValue);

	MouseEvent= static_cast<DuiLib::CComboUI*>(m_PaintMsg.FindControl(_T("Combo_MouseClick")))->GetCurSel()+1;
	
	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_point")))->GetText();
	if(strValue.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("请拖拽鼠标位置"),_T("错误"),MB_OK);
		return false;
	}
	MousePos=strValue;

	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseCLick_Num")))->GetText();
	if(_ttoi(strValue)==0)
	{
		MessageBox(this->m_hWnd,_T("次数不能为空"),_T("错误"),MB_OK);
		return FALSE;
	}
	num=_ttoi(strValue);

	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_Time")))->GetText();
	if(_ttoi(strValue)==0)
	{
		MessageBox(this->m_hWnd,_T("请输入时间间隔"),_T("错误"),MB_OK);
		return FALSE;
	}
	time=_ttoi(strValue);

	strValue.Format(_T("%d,%d,%d,%s"),MouseEvent,num,time,MousePos);
	m_keySetMess.detils=strValue;
	m_keySetMess.type=TYPE_MOUSEPRESS;

	if(m_addmess)
	{
		if(CKeySetListMgr::Instance().IsKeyUsed(m_keySetMess.nID))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetFocus();
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().AddKey(m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("添加失败"),_T("错误"),MB_OK);
			return FALSE;
		}
		CKeySetListMgr::Instance().SaveKey(m_keySetMess);
	}
	//修改热键
	else{
		if(m_keySetMess==oldmess)
		{
			MessageBox(this->m_hWnd,_T("没有任何修改"),_T(""),MB_OK);
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().EditKey(oldmess.nID,m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetFocus();
			return FALSE;
		}
	}
	
	bool oldState=CKeySetListMgr::IsCurrentViewKeyUsed(m_keySetMess.nID);
	CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
	if(m_keySetMess.used==true||oldState==true){
		if (!CKeySetListMgr::Instance().AddHookKey())
		{
			m_keySetMess.used = FALSE;
			CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
		}
	}
	return true;
}

//鼠标连点
bool CDulibKeySetWnd::OnAddMouseLXClickInfor()
{


	KeyMess m_keySetMess;
	int MouseEvent=0;//鼠标单击或双击
	CString MousePos=_T("");
	int num=-1;        //点击次数
	int time=-1;       //时间间隔
	if(static_cast<DuiLib::CCheckBoxUI*>(m_PaintMsg.FindControl(_T("MouseLXClick_Used")))->GetCheck())
	{
		m_keySetMess.used=true;
	}
	else
	{
		m_keySetMess.used=false;
	}
	CString strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_StartKey")))->GetText();
	if(strValue.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("热键内容不能为空"),_T("错误"),MB_OK);
		return false;
	}
	m_keySetMess.nID=CKeySetListMgr::GetKeyValueByStr(strValue);

	MouseEvent= static_cast<DuiLib::CComboUI*>(m_PaintMsg.FindControl(_T("MouseLXCLick_Combo")))->GetCurSel()+1;

	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_MousePoint")))->GetText();
	if(strValue.Trim().GetLength()==0)
	{
		MessageBox(this->m_hWnd,_T("请拖拽鼠标位置"),_T("错误"),MB_OK);
		return false;
	}
	MousePos=ReSetStr(strValue);
	if(GetPointNum(MousePos)>10)
	{
		MessageBox(this->m_hWnd,_T("设置的坐标数不能超过10个"),_T("错误"),MB_OK|MB_ICONHAND);
		return FALSE;
	}

	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Num")))->GetText();
	if(_ttoi(strValue)==0)
	{
		MessageBox(this->m_hWnd,_T("次数不能为空"),_T("错误"),MB_OK);
		return FALSE;
	}
	num=_ttoi(strValue);

	strValue=((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Time")))->GetText();
	if(_ttoi(strValue)==0)
	{
		MessageBox(this->m_hWnd,_T("请输入时间间隔"),_T("错误"),MB_OK);
		return FALSE;
	}
	time=_ttoi(strValue);

	strValue.Format(_T("%d,%d,%d,%s"),MouseEvent,num,time,MousePos);
	m_keySetMess.detils=strValue;
	m_keySetMess.type=TYPE_MOUSEPRESS;

	if(m_addmess)
	{
		if(CKeySetListMgr::Instance().IsKeyUsed(m_keySetMess.nID))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetFocus();
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().AddKey(m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("添加失败"),_T("错误"),MB_OK);
			return FALSE;
		}
		CKeySetListMgr::Instance().SaveKey(m_keySetMess);
	}
	//修改热键
	else{
		if(m_keySetMess==oldmess)
		{
			MessageBox(this->m_hWnd,_T("没有任何修改"),_T(""),MB_OK);
			return FALSE;
		}
		if(!CKeySetListMgr::Instance().EditKey(oldmess.nID,m_keySetMess))
		{
			MessageBox(this->m_hWnd,_T("按键已经存在"),_T("错误"),MB_OK);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetFocus();
			return FALSE;
		}
	}
	
	bool oldState=CKeySetListMgr::IsCurrentViewKeyUsed(m_keySetMess.nID);
	CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
	if(m_keySetMess.used==true||oldState==true){
		if (!CKeySetListMgr::Instance().AddHookKey())
		{
			m_keySetMess.used = FALSE;
			CKeySetListMgr::SetCurrentViewKey(m_keySetMess.nID,m_keySetMess.used);
		}
	}
	return true;
}

CString CDulibKeySetWnd::ReSetStr(const CString&str)
{
	CString res;
	for(int i=0;i<str.GetLength();i++)
	{
		if(str[i]!='('){
			if(str[i]!=')')
			{
				res+=str[i];
			}
		}
	}
	return res;
}

int CDulibKeySetWnd::GetPointNum(const CString&str)
{
	
	int num=0;
	for(int i=0;i<str.GetLength();i++)
	{
		if(str[i]==',')
		{
			num++;
		}
	}
	return (num+1)/2;
}

void CDulibKeySetWnd::ShowMessToChildDlg(const HOTKET_TYPE& type,const CString& Detils)
{
	switch(type)
	{
	case TYPE_SETKEY:
		{
			if(oldmess.used)
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("key_used")))->SetCheck(TRUE);
			}
			else
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("key_used")))->SetCheck(FALSE);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->SetText(CKeySetListMgr::KeyValue(oldmess.nID));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_Details")))->SetText(oldmess.detils);
			SetCurrentPage(0);
		}
		break;
	case TYPE_LXKEY:
		{
			if(oldmess.used)
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("LXkey_used")))->SetCheck(TRUE);
			}
			else
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("LXkey_used")))->SetCheck(FALSE);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_StartKey")))->SetText(CKeySetListMgr::KeyValue(oldmess.nID));
			vector<CString> strArray; 
			CKeySetListMgr::GetEachItemText(Detils,strArray);
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Begin")))->SetText(strArray.at(0));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Num")))->SetText(strArray.at(1));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Time")))->SetText(strArray.at(2));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_End")))->SetText(strArray.at(3));	
			SetCurrentPage(1);
		}
		break;
	case TYPE_MOUSEPRESS:
		{
			if(oldmess.used)
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseClick_Used")))->SetCheck(TRUE);
			}
			else
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseClick_Used")))->SetCheck(FALSE);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetText(CKeySetListMgr::KeyValue(oldmess.nID));
			vector<CString> strArray; 
			CKeySetListMgr::GetEachItemText(Detils,strArray);
			if(_ttoi(strArray.at(0))==1)
			{
				((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("Combo_MouseClick")))->SelectItem(0);
			}
			else
			{
				((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("Combo_MouseClick")))->SelectItem(1);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseCLick_Num")))->SetText(strArray.at(1));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_Time")))->SetText(strArray.at(2));
			CString strValues;
			strValues.Format(L"%d,%d",_ttoi(strArray.at(3)),_ttoi(strArray.at(4)));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_point")))->SetText(strValues);
			SetCurrentPage(2);
		}
		break;
	case TYPE_MOUSELXPRESS:
		{
			if(oldmess.used)
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Used")))->SetCheck(TRUE);
			}
			else
			{
				((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Used")))->SetCheck(FALSE);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_StartKey")))->SetText(CKeySetListMgr::KeyValue(oldmess.nID));
			vector<CString> strArray; 
			CString PointStr=GetEachText(Detils,strArray);
			if(_ttoi(strArray.at(0))==1)
			{
				((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("Combo_MouseClick")))->SelectItem(0);
			}
			else
			{
				((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("Combo_MouseClick")))->SelectItem(1);
			}
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Num")))->SetText(strArray.at(1));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Time")))->SetText(strArray.at(2));
			((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_MousePoint")))->SetText(PointStr);
			SetCurrentPage(3);
		}
		break;
	default:
		break;
	}
}

CString  CDulibKeySetWnd::GetEachText(const CString&str, vector<CString>&strArray)
{
	int pos=-1;
	int flag=0;
	int count=0;
	CString strText=_T("");
	CString MouseText=_T("");
	for(int i=0;i<=str.GetLength();i++)
	{
		pos++;
		if(str[i]==','||str[i]=='\0')
		{
			CString temp=str.Mid(flag,pos);
			strArray.push_back(temp);
			flag=i+1;
			pos=-1;
			count++;
		}
		if(count==3)
		{
			strText=str.Right(str.GetLength()-i-1);
			break;
		}
	}
	pos=-1;
	flag=0;
	int posNum=0;
	for(int i=0;i<=strText.GetLength();i++)
	{
		pos++;
		if(strText[i]==','||strText[i]=='\0')
		{
			posNum++;
			if(posNum==2)
			{
				CString temp=strText.Mid(flag-1,pos+1);
				if(i==strText.GetLength())
				{
					temp+=_T(")");
				}
				else{
					temp+=_T("),");
				}
				MouseText+=temp;
				posNum=0;
			}
			else if(posNum==1)
			{
				CString temp=strText.Mid(flag,pos);
				MouseText+=_T("(");
				MouseText+=temp;
			}
			flag=i+1;
			pos=-1;
		}
	}
	return MouseText;
}

void CDulibKeySetWnd::SetCurrentPage(int nPage)
{
	DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintMsg.FindControl(_T("switch")));
	pControl->SelectItem(nPage);
	switch(nPage)
	{
	case 0:
		((DuiLib::COptionUI*)m_PaintMsg.FindControl(_T("keyset_option")))->Selected(true);
		break;
	case 1:
		((DuiLib::COptionUI*)m_PaintMsg.FindControl(_T("LXkey_option")))->Selected(true);
		break;
	case 2:
		((DuiLib::COptionUI*)m_PaintMsg.FindControl(_T("MouseCLick_option")))->Selected(true);
		break;
	case 3:
		((DuiLib::COptionUI*)m_PaintMsg.FindControl(_T("MouseLXClick_option")))->Selected(true);
		break;
	default:
		break;
	}
}

void CDulibKeySetWnd::InitChildWnd()
{
	((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("key_used")))->SetCheck(true);
	((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("LXkey_used")))->SetCheck(true);
	((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseClick_Used")))->SetCheck(true);
	((DuiLib::CCheckBoxUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Used")))->SetCheck(true);

	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_BeginKey")))->SetText(_T(""));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("KeySet_Details")))->SetText(_T(""));

	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_StartKey")))->SetText(_T(""));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Begin")))->SetText(_T(""));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Num")))->SetText(_T("1"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_Time")))->SetText(_T("200"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("LXKey_End")))->SetText(_T(""));

	((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("Combo_MouseClick")))->SelectItem(0);
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_StartKey")))->SetText(_T(""));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseCLick_Num")))->SetText(_T("1"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_Time")))->SetText(_T("200"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseClick_point")))->SetText(_T(""));

	((DuiLib::CComboUI*)m_PaintMsg.FindControl(_T("MouseLXCLick_Combo")))->SelectItem(0);
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_StartKey")))->SetText(_T(""));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Num")))->SetText(_T("1"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_Time")))->SetText(_T("200"));
	((DuiLib::CEditUI*)m_PaintMsg.FindControl(_T("MouseLXClick_MousePoint")))->SetText(_T(""));
	((DuiLib::CButtonUI*)m_PaintMsg.FindControl(_T("Confirm")))->SetText(_T("添加"));

}