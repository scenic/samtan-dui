#include "StdAfx.h"
#include "HotKeyMgr.h"

#include "GlobalFunc.h"
#include "MulTabBrowser.h"

#define HOTKEY_SECTION  _T("HotKey")


CHotKeyMgr::CHotKeyMgr(void)
{
    CString szPath = GetExePath() + _T("\\Sysdata");
    if (!PathFileExists(szPath))
        CreateDirectory(szPath, NULL);
    m_strIniFile = szPath + _T("\\Hotkey.dat");
}


CHotKeyMgr::~CHotKeyMgr(void)
{
    for (std::list<HotKey*>::iterator it = m_lsHotkeys.begin(); it != m_lsHotkeys.end(); it++)
        delete (*it);
    m_lsHotkeys.clear();
}

CHotKeyMgr& CHotKeyMgr::Instance()
{
    static CHotKeyMgr hotKeyMgr;
    return hotKeyMgr;
}

const TCHAR* CHotKeyMgr::KeyValue(int nKey)
{
    const TCHAR* pStr = NULL;
    if ((nKey >= 0) && (nKey < 0x100))
        pStr = VK_VALUE[nKey];
    return pStr;
}

BOOL CHotKeyMgr::AddHotKey(int nID, bool bCtrl, bool bShift, int nKey, bool bSave)
{
    if (NULL != FindHotKey(nID))
        RemoveHotKey(nID);

    UINT nFlag = 0;
    if (bCtrl)
        nFlag |= MOD_CONTROL;
    if (bShift)
        nFlag |= MOD_SHIFT;
    BOOL ret = RegisterHotKey(theApp.pBrowser->GetHWND(), nID, nFlag, nKey);
    if (ret)
    {
        HotKey* pHotKey = new HotKey();
        pHotKey->nHotKeyID = nID;
        pHotKey->bCtrl = bCtrl;
        pHotKey->bShift = bShift;
        pHotKey->nHotKey = nKey;
        m_lsHotkeys.push_back(pHotKey);

        if (bSave)
            SaveHotKey(nID, bCtrl, bShift, nKey);
    }
    return ret;
}

BOOL CHotKeyMgr::RemoveHotKey(int nID)
{
    BOOL ret = UnregisterHotKey(theApp.pBrowser->GetHWND(), nID);
    if (ret)
    {
        HotKey* pRemove = FindHotKey(nID);
        if (NULL != pRemove)
        {
            m_lsHotkeys.remove(pRemove);
            delete pRemove;

            DeleteHotKey(nID);
        }
    }
    return ret;
}

BOOL CHotKeyMgr::FindHotKey(int nID, bool& bCtrl, bool& bShift, int& nKey)
{
    BOOL ret = FALSE;
    bCtrl = false;
    bShift = false;
    nKey = INVALID_KEY;

    HotKey* pFind = FindHotKey(nID);
    if (NULL != pFind)
    {
        ret = TRUE;
        bCtrl = pFind->bCtrl;
        bShift = pFind->bShift;
        nKey = pFind->nHotKey;
    }
    return ret;
}

void CHotKeyMgr::LoadHotKeys()
{
    ReadHotKey(HOTKEY_AUTOSPEAK);
    ReadHotKey(HOTKEY_BOSSKEY);
	ReadHotKey(HOTKEY_OPEN_FAV);

}

void CHotKeyMgr::UnLoadHotKeys()
{
    for (std::list<HotKey*>::iterator it = m_lsHotkeys.begin(); it != m_lsHotkeys.end(); it++)
    {
        UnregisterHotKey(theApp.pBrowser->GetHWND(), (*it)->nHotKeyID);
        delete (*it);
    }
    m_lsHotkeys.clear();
}

HotKey* CHotKeyMgr::FindHotKey(int nID)
{
    HotKey* pFind = NULL;
    for (std::list<HotKey*>::iterator it = m_lsHotkeys.begin(); it != m_lsHotkeys.end(); it++)
    {
        if ((*it)->nHotKeyID == nID)
        {
            pFind = (*it);
            break;
        }
    }
    return pFind;
}

void CHotKeyMgr::SaveHotKey(int nID, bool bCtrl, bool bShift, int nKey)
{
    CString strKey;
    strKey.Format(_T("%d"), nID);
    CString str;
    str.Format(_T("%d,%d,%d"), bCtrl ? 1 : 0, bShift ? 1 : 0, nKey);
    WritePrivateProfileString(HOTKEY_SECTION, strKey, str, m_strIniFile);
}

void CHotKeyMgr::DeleteHotKey(int nID)
{
    CString strKey;
    strKey.Format(_T("%d"), nID);
    WritePrivateProfileString(HOTKEY_SECTION, strKey, NULL, m_strIniFile);
}

BOOL CHotKeyMgr::ReadHotKey(int nID)
{
    BOOL ret = FALSE;
    TCHAR ac_Result[255];
    CString strKey;
    strKey.Format(_T("%d"), nID);
    GetPrivateProfileString(HOTKEY_SECTION, (LPCTSTR)strKey, _T(""), ac_Result, 255, m_strIniFile);
    CString str(ac_Result);
    int nPos = str.Find(_T(','));
    if (-1 != nPos)
    {
        try
        {
            bool bCtrl = (0 != _tstol(str.Left(nPos)));
            CString strR;
            if (str.GetLength() > nPos + 1)
            {
                strR = str.Mid(nPos+1);
                nPos = strR.Find(_T(','));
                if (-1 != nPos)
                {
                    bool bShift = (0 != _tstol(strR.Left(nPos)));
                    if (strR.GetLength() > nPos + 1)
                    {
                        int nKey = _tstol(strR.Mid(nPos+1));
                        ret = AddHotKey(nID, bCtrl, bShift, nKey, false);
                    }
                }
            }
        }
        catch (...)
        {
        }
    }
    return ret;
}

