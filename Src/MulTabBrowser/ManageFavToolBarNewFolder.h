/*!@file
*******************************************************************************************************
<PRE>
模块名		："新建文件夹"界面类
文件名		：ManageFavToolBarNewFolder.h
相关文件		：ManageFavToolBarNewFolder.cpp/Duilib相关库
文件实现功能：实现管理收藏夹工具栏的新建文件夹
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了Duilib库
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/5/31 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
#include <atlstr.h>
#include "ManageFavHeader.h"
#include <list>

class CManageFavToolBarTree;
class CManageFavToolBarNewFolder : public DuiLib::WindowImplBase
{
protected:
	virtual ~CManageFavToolBarNewFolder(){};        // 私有化析构函数，这样此对象只能通过new来生成，而不能直接定义变量。就保证了delete this不会出错
	DuiLib::CDuiString  m_strXMLPath;

public:
	explicit CManageFavToolBarNewFolder(LPCTSTR pszXMLPath): m_strXMLPath(pszXMLPath){}
	virtual LPCTSTR    GetWindowClassName()const{ return _T("CManageFavToolBarNewFolder");}
	virtual DuiLib::CDuiString GetSkinFolder()          { return _T("");}
	virtual DuiLib::CDuiString GetSkinFile()            { return m_strXMLPath;      }
	virtual void       OnFinalMessage(HWND /*hWnd*/){ delete this;              }
	void Init(HWND hWndParent);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual void OnClick(DuiLib::TNotifyUI& msg);

public:
	// 添加树链表
	void AddTreeSort(int nIndex, CString strParentText, CString strText);
	// 从链表添加结点
	bool AddListToTree(CManageFavToolBarTree* ToolBarTree, DuiLib::CTreeNodeUI* parent);

private:
	HWND	m_parentHwnd;					// 保存父窗口的指针
	std::list<TREESORT> m_TreeSort;			// 树排序的链表
};
