/*!@file
*******************************************************************************************************
<PRE>
模块名		："Tree右键菜单"界面类
文件名		：CManageFavTreeMenu
相关文件		：CManageFavTreeMenu.cpp/Duilib相关库
文件实现功能：实现管理收藏夹Tree右键菜单界面的逻辑
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了Duilib库
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/5/31 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
#include "stdafx.h"
#include <atlstr.h>

class CManageFavTreeMenu : public DuiLib::WindowImplBase
{
protected:
	virtual ~CManageFavTreeMenu(){};        // 私有化析构函数，这样此对象只能通过new来生成，而不能直接定义变量。就保证了delete this不会出错
	DuiLib::CDuiString  m_strXMLPath;

public:
	explicit CManageFavTreeMenu(LPCTSTR pszXMLPath): m_strXMLPath(pszXMLPath){}
	virtual LPCTSTR    GetWindowClassName()const{ return _T("CManageFavTreeMenu");}
	virtual DuiLib::CDuiString GetSkinFolder()          { return _T("");}
	virtual DuiLib::CDuiString GetSkinFile()            { return m_strXMLPath;      }
	virtual void       OnFinalMessage(HWND /*hWnd*/){ delete this;              }

	virtual LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void Init(HWND hWndParent, POINT ptPos);

	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

public:
	virtual void Notify(DuiLib::TNotifyUI& msg);

private:
	HWND	m_paintWnd;			// 父窗口
};
