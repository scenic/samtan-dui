#include "StdAfx.h"
#include "ScriptRecordMgr.h"

//////////////////////////////////////////////////////////////////////////
// CScriptRecordMgr 脚本录制管理类

HHOOK CScriptRecordMgr::m_hHook;
vector<EVENTMSG> CScriptRecordMgr::m_evenArr;

LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	LPEVENTMSG lpEvent= (LPEVENTMSG)lParam;

	EVENTMSG even;
	even.message = lpEvent->message;
	even.paramL = lpEvent->paramH;
	even.time = GetTickCount();
	even.hwnd = NULL;

	CScriptRecordMgr::m_evenArr.push_back(even);

	return CallNextHookEx(CScriptRecordMgr::m_hHook, nCode, wParam, lParam);
}

// 开始录制
void CScriptRecordMgr::StartRecord(HWND /*hWnd*/)
{
	m_hHook = SetWindowsHookEx(WH_MOUSE_LL, MouseProc, GetModuleHandle(NULL), 0);
}

// 停止录制
void CScriptRecordMgr::StopRecord(void)
{
	UnhookWindowsHookEx(m_hHook);
}

// 开始播放
void CScriptRecordMgr::StartPlay(LPCTSTR /*lpszFileName*/)
{
}

// 停止播放
void StopPlay(LPCTSTR /*lpszFileName*/)
{
}

CScriptRecordMgr::~CScriptRecordMgr(void) {}