#pragma once

class CBrowserWnd;

class CBossKeyWnd : public DuiLib::CWindowWnd, public DuiLib::INotifyUI
{
public:
	CBossKeyWnd();
	~CBossKeyWnd(void);

	void Init();
	void OnPrepare();

	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual LPCTSTR GetWindowClassName() const { return _T("CBossKeyWnd"); }

	LRESULT OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void	SetParent(CBrowserWnd* pParentWnd);

	void	RegHotKey();		// ע���ȼ�
	void	UnRegHotKey();		// ע���ȼ�

private:
	DuiLib::CPaintManagerUI	m_pm;
	DuiLib::CCheckBoxUI*	m_pcbxEnable;	// ��ѡ��ť
	DuiLib::CLabelUI*		m_pStatusLabel;	// ����״̬

	CBrowserWnd*	m_pMainWnd;

	bool	m_bEnableBossKey;	// �ϰ������
	ATOM	m_nBossKeyId;		// �ȼ�Id
};