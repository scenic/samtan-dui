#pragma once
class CScriptRecordDao
{
public:
	CScriptRecordDao(void);
	void Save(); // 保存脚本文件
	void Load(); // 加载脚本文件
	void Delete(LPCTSTR lpFileName); // 删除文件
	void Rename(LPCTSTR lpszOldFileName, LPCTSTR lpszNewFileName); // 重命名
	void SaveAs(); // 脚本文件另存为
	void Edit(LPCTSTR lpszFileName); // 编辑脚本文件
	~CScriptRecordDao(void);
};