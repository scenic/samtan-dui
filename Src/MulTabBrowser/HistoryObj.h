//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// This is a sample for BCGControlBar Library Professional Edition
// Copyright (C) 1998-2010 BCGSoft Ltd.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
//
// HistoryObj.h: interface for the CHistoryObj class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTORYOBJ_H__F83C8E1D_F33E_11D2_A713_0090274409AC__INCLUDED_)
#define AFX_HISTORYOBJ_H__F83C8E1D_F33E_11D2_A713_0090274409AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "../History/Public/UzHistory.h"
#include "../History/Public/UzHistoryObj.h"
#include <vector>

class CHistoryObj  
{
public:
	CHistoryObj(const CString& strTitle, const CString& strURL, const UINT uiCommand);
	CHistoryObj(const CHistoryObj& rObj);
	
	virtual ~CHistoryObj();

	LPCTSTR GetTitle () const
	{
		return m_strTitle;
	}

	LPCTSTR GetURL () const
	{
		return m_strURL;
	}

	UINT GetCommand () const
	{
		return m_uiCommand;
	}

	CString GetDomain() const;

private:
	const CString	m_strTitle;
	const CString	m_strURL;
	const UINT		m_uiCommand;
};

typedef struct HISTORYINF0
{
	HWND hWnd;
	CString strTitle;
	CString strUrl;
}sHistoryInfo,*pHistoryInfo;

typedef struct HISTORYRESTORELBLMENU
{	CUzHistoryObj uzobj;
	DWORD   menuId;
}sHistoryRestoreLblMenu,*pHistoryRestoreLblMenu;

class CHistoryManager
{
public:
	CHistoryManager();
	virtual ~CHistoryManager();

	void Clean();

	void Load();
	void Save() const;

	BOOL Add(const CHistoryObj& obj);
	BOOL Add(const CString& strTitle,const CString& strUrl,const CString& strIconPath);
	void AnalyseHistory(HWND hTab,CString strTitle,CString strUrl);
	void UpdateTitle(const sHistoryInfo& his);	
	std::list<CUzHistoryObj>& GetHistroyList();
	void DeleteHistory();
	void DeleteHistoryDir(int days,CTime& timeLoad);
	void DeleteHistoryItem(CUzHistoryObj& obj);

	//记录键盘输入的URL
	void SaveGoToUrl(const CString& strUrl);
	void LoadGoToUrl(std::list<CString>& lst);
	void DeleteGoToUrl(const CString& strUrl);

	//处理Restore标签
	std::list<CUzHistoryObj>& GetRestoreLblList();
	std::list<CUzHistoryObj>& LoadRestoreLbl();
	void SaveRestoreLbl();
	void InsertToRestoreLblList(CUzHistoryObj& obj);
	void RestoreLastLbl();
	void DeleteRestore();
	//显示Restore列表相关的菜单
	void DeleteRestore(CString strLink);
	void ShowRestoreLblMenu(POINT pt);


	//处理最后一次打开的标签信息
	void SaveLastOpen(std::list<CUzHistoryObj>& list);
	void LoadLastOpen(std::list<CUzHistoryObj>& list);
	std::list<CUzHistoryObj>& GetLastOpenList();
	void ShowLastOpen();
	void RemoveLastOpen();

protected:
	typedef CArray<CHistoryObj*, CHistoryObj*> XHistoryArray;
	typedef CMap<CString, LPCTSTR, XHistoryArray*, XHistoryArray*> XHistoryMap;

private:
	void LoadHistoryRestorlLblMenu(CMenu* menu,DWORD& id);
	void DeleteImage();
	std::vector<HBITMAP>  m_listHbitmapMenu;
	XHistoryMap m_Map;

	CMenu m_menuHistoryRestoreLbl;
	
	CUzHistory m_UzHistory;
	CString    m_strHistoryPath;
	std::list<sHistoryInfo> m_lstHistoryInfo;  //此列表用来分析要加入的历史记录信息
	std::list<CUzHistoryObj> m_lstLastOpen;  //此列表记录最后打开的几个标签页信息
	std::list<sHistoryRestoreLblMenu> m_lstHistroyRestoreLblMenu;  //此列表记录最近打开的标签页

};

CHistoryManager* GetHistory();
#endif // !defined(AFX_HISTORYOBJ_H__F83C8E1D_F33E_11D2_A713_0090274409AC__INCLUDED_)
