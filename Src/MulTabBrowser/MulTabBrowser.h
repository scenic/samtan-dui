// MulTabBrowser.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

// CMulTabBrowserApp:
// See MulTabBrowser.cpp for the implementation of this class
//
#include "BrowserWnd.h"
#include "HistoryObj.h"
class CMulTabBrowserApp : public CWinApp
{
public:
	CMulTabBrowserApp();

// Overrides
public:
	CHistoryManager* GetHisotry() {return &m_History;}
	virtual BOOL InitInstance();
	CBrowserWnd* pBrowser;
// Implementation

	DECLARE_MESSAGE_MAP()
    virtual int ExitInstance();
	void InitHistory();
	
private:
	CHistoryManager m_History; 
};

extern CMulTabBrowserApp theApp;