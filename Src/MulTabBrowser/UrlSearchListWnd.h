#pragma once
#include "TypedUrlsMgr.h"
#include <list>
class CUrlSearchListWnd : public DuiLib::WindowImplBase
{
public:
	explicit CUrlSearchListWnd(LPCTSTR pszXmlPath):m_strXmlPath(pszXmlPath) {}
	virtual LPCTSTR GetWindowClassName() const { return _T("CUrlSearchListWnd"); }
	virtual DuiLib::CDuiString GetSkinFolder() { return  _T(""); }
	virtual DuiLib::CDuiString GetSkinFile() { return m_strXmlPath; }
	virtual void OnFinalMessage(HWND /*hWnd*/) { delete this; }
	virtual LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual void InitWindow();
	virtual void Notify(DuiLib::TNotifyUI &msg);
	void	CreateWnd(HWND hWndParent);
	void	SetPos(POINT ptPos, int nWidth);
	void	SetParentWnd(DuiLib::CWindowWnd* pParent);
	void	EditPageDown();
	void	EditPageUp();
	void	AddTypedUrl(const CTypedUrl* pTypedUrl);
	BOOL	AlreadyInList(const CString& strUrl);
	void	OnAddressEditChange(CString strText);
protected:
	virtual ~CUrlSearchListWnd() {}
	DuiLib::CDuiString m_strXmlPath;

private:
	DuiLib::CListUI* m_pUrlList;
	DuiLib::CWindowWnd* m_pParent;
	//��ַ��
	std::list<CTypedUrl> m_lstSearch;
};