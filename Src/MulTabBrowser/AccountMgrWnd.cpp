#include "StdAfx.h"
#include "AccountMgrWnd.h"
#include "AccountMgrListMenu.h"
#include "AddGameAccountWnd.h"
#include "../Include/UzDefine.h"

#define LIST_ADD_GAME 1		// 此为添加游戏List的标签
#define LIST_ADD_ACCOUNT 2	// 此为添加帐号List的标签

CAccountMgrWnd::CAccountMgrWnd(void)
{
	m_pAccountWnd = NULL;
	m_nListAddGame = -1;					// 定义点击的添加游戏List Index
	m_nListAccount = -1;					// 定义点击的添加帐号List Index
	// 将添加帐号Index矩形设为-1
	m_AccountRect.bottom = -1;
	m_AccountRect.left = -1;
	m_AccountRect.right = -1;
	m_AccountRect.top = -1;
}


CAccountMgrWnd::~CAccountMgrWnd(void)
{
}

void CAccountMgrWnd::Init(HWND hWndParent, CAccountMgrWnd** pAccountWnd)
{
	Create(hWndParent, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
	m_parentHwnd = hWndParent;
	CenterWindow();

	m_pAccountWnd = pAccountWnd;
	return;
}

void CAccountMgrWnd::InitWindow()
{
	// 加载添加帐号List
	m_pListAccount = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("listAccount")));
	ASSERT(m_pListAccount);
	if (NULL == m_pListAccount)
	{
		return;
	}

	// 加载添加游戏List
	m_pListAddGame = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("listex")));
	ASSERT(m_pListAddGame);
	if (NULL == m_pListAddGame)
	{
		return;
	}
}

// 单击事件响应
void CAccountMgrWnd::OnClick( DuiLib::TNotifyUI &msg )
{
	if (msg.pSender->GetName() == _T("btnClose"))
	{
		Close();
		*m_pAccountWnd = NULL;
		return;
	}
	// 当选择“添加游戏”按钮时
	if (msg.pSender->GetName() == _T("Button_AddGame"))
	{
		CAddGameAccountWnd* pAddGameAccountWnd = new CAddGameAccountWnd;
		pAddGameAccountWnd->Create(this->m_hWnd, NULL, WS_OVERLAPPEDWINDOW, 0);
		pAddGameAccountWnd->CenterWindow();
		pAddGameAccountWnd->SetParentWnd(m_parentHwnd);
		pAddGameAccountWnd->ShowWindow(true);
		return;
	}
	// 当选择“新建帐号”按钮时
	if (msg.pSender->GetName() == _T("Button_NewAccount"))
	{
		CString strName = _T("oooooo");
		AddAccount(strName, strName, strName, strName, true);
		return;
	}
	// 当选择“保存”按钮时
	if (msg.pSender->GetName() == _T("Button_Save"))
	{
		return;
	}
	// 当选择“关闭”按钮时
	if (msg.pSender->GetName() == _T("Button_Close"))
	{
		Close();
		*m_pAccountWnd = NULL;
		return;
	}
	// 当选择“勾选”按钮时
	if (msg.pSender->GetName() == _T("Button_OK"))
	{
		// 判断是ListAccount是否为当前Index
		if (-1 == m_nListAccount)
		{
			return;
		}

		POINT pt = {msg.ptMouse.x, msg.ptMouse.y};
		// 判断是否在ListIndex矩形内,是就做出响应
		if (pt.x >= m_AccountRect.left && pt.x <= m_AccountRect.right && pt.y >= m_AccountRect.top && pt.y <= m_AccountRect.bottom)
		{
			SetEditEnabled(false, m_nListAccount);
		}
		return;
	}
	// 当选择“上移”按钮时
	if (msg.pSender->GetName() == _T("Button_Up"))
	{
		// 判断是ListAccount是否为当前Index
		if (-1 == m_nListAccount)
		{
			return;
		}

		POINT pt = {msg.ptMouse.x, msg.ptMouse.y};
		// 判断是否在ListIndex矩形内,是就做出响应
		if (pt.x >= m_AccountRect.left && pt.x <= m_AccountRect.right && pt.y >= m_AccountRect.top && pt.y <= m_AccountRect.bottom)
		{
			AccountUp(m_nListAccount);
		}
		return;
	}
	// 当选择“下移”按钮时
	if (msg.pSender->GetName() == _T("Button_Down"))
	{
		// 判断是ListAccount是否为当前Index
		if (-1 == m_nListAccount)
		{
			return;
		}
		POINT pt = {msg.ptMouse.x, msg.ptMouse.y};
		// 判断是否在ListIndex矩形内,是就做出响应
		if (pt.x >= m_AccountRect.left && pt.x <= m_AccountRect.right && pt.y >= m_AccountRect.top && pt.y <= m_AccountRect.bottom)
		{
			AccountDown(m_nListAccount);
		}
		return;
	}
	// 当选择“编辑”按钮时
	if (msg.pSender->GetName() == _T("Button_Edit"))
	{
		// 判断是ListAccount是否为当前Index
		if (-1 == m_nListAccount)
		{
			return;
		}

		POINT pt = {msg.ptMouse.x, msg.ptMouse.y};
		// 判断是否在ListIndex矩形内,是就做出响应
		if (pt.x >= m_AccountRect.left && pt.x <= m_AccountRect.right && pt.y >= m_AccountRect.top && pt.y <= m_AccountRect.bottom)
		{
			SetEditEnabled(true, m_nListAccount);
		}
		return;
	}
	// 当选择“删除”按钮时
	if (msg.pSender->GetName() == _T("Button_Del"))
	{
		// 判断是ListAccount是否为当前Index
		if (-1 == m_nListAccount)
		{
			return;
		}

		POINT pt = {msg.ptMouse.x, msg.ptMouse.y};
		// 判断是否在ListIndex矩形内,是就做出响应
		if (pt.x >= m_AccountRect.left && pt.x <= m_AccountRect.right && pt.y >= m_AccountRect.top && pt.y <= m_AccountRect.bottom)
		{
			DelAccount(m_nListAccount);
		}
		return;
	}
}

void CAccountMgrWnd::OnHeaderClick(DuiLib::TNotifyUI& /*msg*/)
{
}

// 消息响应
void CAccountMgrWnd::Notify(DuiLib::TNotifyUI& msg)
{
	// 当消息为itemclick时
	if (msg.sType == DUI_MSGTYPE_ITEMCLICK)
	{
		// 判断是否为添加游戏List中的Item
		if(m_pListAddGame && LIST_ADD_GAME == msg.pSender->GetTag())
		{
			m_nListAddGame =((DuiLib::CListLabelElementUI *)msg.pSender)->GetIndex();

			// 将添加帐号Index设为-1
			m_nListAccount = -1;
			// 将添加帐号Index矩形设为-1
			m_AccountRect.bottom = -1;
			m_AccountRect.left = -1;
			m_AccountRect.right = -1;
			m_AccountRect.top = -1;
		}
		// 判断是否为添加帐号List中的Item
		else if(m_pListAccount && LIST_ADD_ACCOUNT == msg.pSender->GetTag())
		{
			m_nListAccount =((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();
			m_AccountRect = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetPos();
			// 将添加游戏Index设为-1
			m_nListAddGame = -1;
		}
	}
	// 枚举消息时
	if (msg.sType == DUI_MSGTYPE_MENU)
	{
		CString strName = msg.pSender->GetName();
		// 判断是否为添加帐号List消息
		if (-1 != m_nListAccount && strName == _T("listAccount"))
		{
			// 获取当前选中item的矩形区
			RECT ItemRect = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetItemAt(m_nListAccount)->GetPos();
			POINT pt = {msg.ptMouse.x, msg.ptMouse.y};

			// 判断是否在矩形内,是就创建枚举窗口
			if (pt.x >= ItemRect.left && pt.x <= ItemRect.right && pt.y >= ItemRect.top && pt.y <= ItemRect.bottom)
			{
				CAccountMgrListMenu* pMenu = new CAccountMgrListMenu(_T("SideAccountWnd//Account_Mgr_ListMenu.xml"));
				if (pMenu == NULL)
				{
					return ;
				}

				pMenu->Init(*this, pt);
				pMenu->ShowWindow(TRUE);
			}
		}
	}

	return WindowImplBase::Notify(msg);
}

// 自定义消息
LRESULT CAccountMgrWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	//	添加帐号List右键菜单
	if (uMsg == ACCOUNT_ADD_ACCOUNT_LIST_MENU)
	{
		int nIndex = (int)lParam;
		switch(nIndex)
		{
		case 0:	// 新建账户
			{
				// 账户为空，可编辑状态
				CString strName = _T("");
				AddAccount(strName, strName, strName, strName);
				m_nListAccount = m_pListAccount->GetCount() -1;
				SetEditEnabled(true, m_nListAccount);
				break;
			}
		case 1:	// 修改账户
			{
				SetEditEnabled(true, m_nListAccount);
				break;
			}
		case 2:	// 删除账户
			{
				DelAccount(m_nListAccount);
				break;
			}
		case 3:	// 上移
			{
				AccountUp(m_nListAccount);
				break;
			}
		case 4:	// 下移
			{
				AccountDown(m_nListAccount);
				break;
			}
		default:
			break;
		}
	}

	return __super::HandleMessage(uMsg, wParam, lParam);
}
// 添加游戏,strName为游戏名称
bool CAccountMgrWnd::AddGameName(CString& strName)
{
	DuiLib::CListLabelElementUI* pLabelName = new DuiLib::CListLabelElementUI;
	ASSERT(pLabelName);
	if (NULL == pLabelName)
	{
		return false;
	}

	CStringW strWName = strName;
	pLabelName->SetText(strWName);

	pLabelName->SetTag(LIST_ADD_GAME);

	m_pListAddGame->Add(pLabelName);

	return true;
}

// 新建帐号,strName为用户名，strPassWord为密码，strURL为游戏网址，strNote为备注
bool CAccountMgrWnd::AddAccount(CString& strName, CString& strPassWord, CString& strURL, CString& strNote, bool bEdit)
{
	DuiLib::CDialogBuilder builder;
	DuiLib::CListContainerElementUI* m_pListCE = (DuiLib::CListContainerElementUI*)(builder.Create(_T("SideAccountWnd//Account_Mgr_ListElement.xml"),(UINT)0));

	if (NULL == m_pListAccount || NULL == m_pListCE)
	{
		return false;
	}

	// 给名字编辑框赋值
	DuiLib::CEditUI* pEditName = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_Name"));
	ASSERT(pEditName);
	if (NULL == pEditName)
	{
		return false;
	}
	CStringW strWName = strName;
	pEditName->SetText(strWName);

	// 给密码编辑框赋值
	DuiLib::CEditUI* pEditPass = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_PassWord"));
	ASSERT(pEditPass);
	if (NULL == pEditPass)
	{
		return false;
	}
	CStringW strWPassWord = strPassWord;
	pEditPass->SetText(strWPassWord);

	// 给网址编辑框赋值
	DuiLib::CEditUI* pEditURL = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_URL"));
	ASSERT(pEditURL);
	if (NULL == pEditURL)
	{
		return false;
	}
	CStringW strWURL = strURL;
	pEditURL->SetText(strWURL);

	// 给备注编辑框赋值
	DuiLib::CEditUI* pEditNote = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_Note"));
	ASSERT(pEditNote);
	if (NULL == pEditNote)
	{
		return false;
	}
	CStringW strWNote = strNote;
	pEditNote->SetText(strWNote);

	m_pListCE->SetTag(LIST_ADD_ACCOUNT);
	if (bEdit)
	{
		bool bo = m_pListAccount->Add(m_pListCE);
		int nIndex = m_pListAccount->GetCount() -1;
		SetEditEnabled(true, nIndex);
		return bo;
	}
	else
	{
		return m_pListAccount->Add(m_pListCE);
	}
}

// 删除选择的帐号
bool CAccountMgrWnd::DelAccount(int& nIndex)
{
	if (nIndex < 0)
	{
		return false;
	}
	// 删除当前行
	m_pListAccount->RemoveAt(nIndex);
	// 判断是否删除的最后一条数据,如果是，则Index-1；矩形框上移一行
	if (m_pListAccount->GetCount() == nIndex)
	{
		// 设置Item
		nIndex = nIndex-1;
		if (nIndex > -1)
		{
			// 设置矩形
			m_AccountRect =((DuiLib::CListContainerElementUI *)( m_pListAccount->GetItemAt(nIndex)))->GetPos();
		}
	}
	else if (m_pListAccount->GetCount() == 0)
	{
		nIndex = -1;
		// 将添加帐号Index矩形设为-1
		m_AccountRect.bottom = -1;
		m_AccountRect.left = -1;
		m_AccountRect.right = -1;
		m_AccountRect.top = -1;
	}

	return true;
}

// 上移一行,nIndex为索引号
bool CAccountMgrWnd::AccountUp(int& nIndex)
{
	if (-1 == nIndex)
	{
		return false;
	}

	if (nIndex > 0)
	{
		m_pListAccount->SetItemIndex(m_pListAccount->GetItemAt(nIndex), nIndex--);
		// 设置当前焦点
		m_pListAccount->SetFocus();
		// 设置矩形
		m_AccountRect =((DuiLib::CListContainerElementUI *)( m_pListAccount->GetItemAt(nIndex+1)))->GetPos();

		if (nIndex < 0)
		{
			nIndex = 0;
		}
	}
	return true;
}

// 下移一行,nIndex为索引号
bool CAccountMgrWnd::AccountDown(int& nIndex)
{
	if (-1 == nIndex)
	{
		return false;
	}

	if (nIndex < m_pListAccount->GetCount()-1)
	{
		m_pListAccount->SetItemIndex(m_pListAccount->GetItemAt(nIndex), nIndex++);
		// 设置当前焦点
		m_pListAccount->SetFocus();
		// 设置矩形
		m_AccountRect =((DuiLib::CListContainerElementUI *)( m_pListAccount->GetItemAt(nIndex-1)))->GetPos();

		if (nIndex > m_pListAccount->GetCount()-1)
		{
			nIndex = m_pListAccount->GetCount()-1;
		}
	}

	return true;
}

// 设置编辑框是否响应
bool CAccountMgrWnd::SetEditEnabled(bool bAtt, int& nIndex)
{
	if (-1 == nIndex)
	{
		return false;
	}


	// 获取当前行的Item
	DuiLib::CListContainerElementUI * m_pListCE = (DuiLib::CListContainerElementUI *)( m_pListAccount->GetItemAt(nIndex));
	// 获取勾选按钮，并显示
	DuiLib::CButtonUI* pOK = (DuiLib::CButtonUI*)m_pListCE->FindSubControl(_T("Button_OK"));
	pOK->SetVisible(bAtt);
	// 获取用户名编辑框，并设置可响应
	DuiLib::CEditUI* pName = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_Name"));
	pName->SetMouseEnabled(bAtt);
	pName->SetEnabled(bAtt);
	// 获取密码编辑框，并设置可响应
	DuiLib::CEditUI* pPassWord = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_PassWord"));
	pPassWord->SetMouseEnabled(bAtt);
	pPassWord->SetEnabled(bAtt);
	// 获取游戏网址编辑框，并设置可响应
	DuiLib::CEditUI* pURL = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_URL"));
	pURL->SetMouseEnabled(bAtt);
	pURL->SetEnabled(bAtt);
	// 获取备注编辑框，并设置可响应
	DuiLib::CEditUI* pNote = (DuiLib::CEditUI*)m_pListCE->FindSubControl(_T("Edit_Note"));
	pNote->SetMouseEnabled(bAtt);
	pNote->SetEnabled(bAtt);

	return true;
}