#include "StdAfx.h"
#include "TypedUrlsMgr.h"
#include "HistoryObj.h"
#include "GlobalFunc.h"
#include <UrlHist.h>
#include <locale>
#include <list>

#define IS_HTTP(str)		(str.Left(7) == _T("http://") || str.Left(8) == _T("https://"))

CTypedUrl::CTypedUrl(const CString& strUrl, const CString& strTitle)
	: CUzUrl (strUrl)
	, m_strTitle (strTitle)
{
}


CTypedUrl::~CTypedUrl(void)
{
}


CString CTypedUrl::GetTitle() const
{
	return m_strTitle;
}


CTypedUrlsList::CTypedUrlsList(const CString& strUrlPrefix)
	: m_strUrlPrefix (strUrlPrefix)
{
}

CTypedUrlsList::~CTypedUrlsList()
{
	m_vecTypedUrls.clear();
	m_mapUrlsToTitles.clear();
}

static CString GetFavConfigFilePath()
{
	return GetExePath() + _T("\\favicon\\favconfig.ini");
}

void CTypedUrlsList::LoadTypedUrls2()
{
	m_vecTypedUrls.clear();
	std::list<CString> list ;
	GetHistory()->LoadGoToUrl(list);
	LoadUrlsHistory();	

	std::list<CString>::iterator it = list.begin();
	int iMax = 0;
	for(;it!=list.end();it++)
	{
		if(iMax++ >= MAX_LISTCOUT)
			break;
		CUzUrl url (*it);
		CString strTitle = m_mapUrlsToTitles[url.GetTopName()];
		CTypedUrl typedUrl (*it, strTitle);
		m_vecTypedUrls.push_back (typedUrl);
	}
	m_mapUrlsToTitles.clear ();
}

void CTypedUrlsList::LoadTypedUrls()
{
	CString strFileName = GetFavConfigFilePath();
	if (!::PathFileExists(strFileName))
		return;

	m_vecTypedUrls.clear ();
	LoadUrlsHistory();	

	char* old_locale = _strdup( setlocale(LC_CTYPE,NULL) ); 
	setlocale( LC_CTYPE, "chs" );//设定

	CStdioFile sFile;
	if (sFile.Open(strFileName, CFile::modeRead |  CFile::typeText) == FALSE)
	{
		return;
	}

	CStringList strUrls;
	CString strUrl;
	while (sFile.ReadString(strUrl) == TRUE)
	{
		strUrl.Trim();
		if ((strUrl.IsEmpty() == TRUE) 
			|| (strUrl.GetAt(0) == L';'))//过滤注释信息
			continue;
		
		if (NULL == strUrls.Find(strUrl))
			strUrls.AddTail (strUrl);
	}
	sFile.Close();

	setlocale( LC_CTYPE, old_locale ); 
	free( old_locale );//还原区域设定 

	POSITION pos = strUrls.GetHeadPosition();
	while (NULL != pos)
	{
		strUrl = strUrls.GetNext(pos);

		//if (IS_HTTP(strUrl))
		{
			CUzUrl url (strUrl);
			CString strTitle = m_mapUrlsToTitles[url.GetTopName()];
			CTypedUrl typedUrl (strUrl, strTitle);
			m_vecTypedUrls.push_back (typedUrl);
		}
	}

	m_mapUrlsToTitles.clear ();
}

void CTypedUrlsList::LoadUrlsHistory()
{
	IUrlHistoryStg2* puhs = NULL;
	HRESULT hr = ::CoCreateInstance (CLSID_CUrlHistory, NULL, CLSCTX_INPROC_SERVER, IID_IUrlHistoryStg, (LPVOID*)&puhs);
	if (SUCCEEDED(hr))
	{
		IEnumSTATURL* pesu = NULL;
		
		hr = puhs->EnumUrls (&pesu);
		if (SUCCEEDED(hr))
		{
			STATURL su;
			ZeroMemory (&su, sizeof(su));

			ULONG celt;
			_bstr_t bstr;
			while (SUCCEEDED(pesu->Next(1, &su, &celt)) && celt > 0)
			{
				USES_CONVERSION;

				CString strUrl = OLE2T(su.pwcsUrl);
				::CoTaskMemFree(su.pwcsUrl);

				CString strTitle = OLE2T(su.pwcsTitle);
				::CoTaskMemFree(su.pwcsTitle);

				ZeroMemory (&su, sizeof(su));

				if (IS_HTTP(strUrl) && !strTitle.IsEmpty())
				{
					CUzUrl url (strUrl);
					m_mapUrlsToTitles[url.GetTopName()] = strTitle;
				}
			}

			pesu->Release();
		}
		
		puhs->Release();
	}
}


UINT CTypedUrlsList::GetTypedUrlCount() const
{
	return m_vecTypedUrls.size();
}


const CTypedUrl m_cTypedUrl(CString(_T("")), CString(_T("")));

const CTypedUrl& CTypedUrlsList::GetTypedUrl(UINT nIndex) const
{
	if (nIndex >= GetTypedUrlCount())
		return m_cTypedUrl;

	return m_vecTypedUrls[nIndex];
}


CTypedUrlsMgr::CTypedUrlsMgr(void)
{
}


CTypedUrlsMgr::~CTypedUrlsMgr(void)
{
}


CTypedUrlsMgr& CTypedUrlsMgr::Instance()
{
	static CTypedUrlsMgr mgr;
	return mgr;
}


CTypedUrlsList*	CTypedUrlsMgr::GetTypedUrlsList()
{
	CTypedUrlsList* pTypedUrlsList = new CTypedUrlsList();
	pTypedUrlsList->LoadTypedUrls2();
	return pTypedUrlsList;
}

void CTypedUrlsMgr::SaveUrl2(const CString& strNewUrl)
{
	GetHistory()->SaveGoToUrl(strNewUrl);
}

void CTypedUrlsMgr::SaveUrl(const CString& strNewUrl)
{
	CString strFileName = GetFavConfigFilePath();

	char* old_locale = _strdup( setlocale(LC_CTYPE,NULL) ); 
	setlocale( LC_CTYPE, "chs" );//设定

	CString strUrl;
	CStringList strUrls;
	CStdioFile sFile;

	if (::PathFileExists(strFileName))
	{
		if (sFile.Open(strFileName, CFile::modeRead |  CFile::typeText))
		{
			BOOL fExists = FALSE;
			while (sFile.ReadString(strUrl) == TRUE)
			{
				strUrl.Trim();
				if ((strUrl.IsEmpty() == TRUE) 
					|| (strUrl.GetAt(0) == L';'))//过滤注释信息
					continue;
				
				if (strUrl == strNewUrl)
				{
					fExists = TRUE;
					break;
				}

				if (NULL == strUrls.Find(strUrl))
					strUrls.AddTail (strUrl);
			}
			sFile.Close();
			
			if (fExists)
				return;
		}
	}
	
	if (strUrls.GetCount() >= 24)
		strUrls.RemoveTail();
	strUrls.AddHead (strNewUrl);

	if (sFile.Open(strFileName, CFile::modeWrite | CFile::modeCreate | CFile::typeText) == FALSE)
	{
		return;
	}

	POSITION pos = strUrls.GetHeadPosition();
	while (NULL != pos)
	{
		strUrl = strUrls.GetNext(pos);
		sFile.WriteString(strUrl);
		sFile.WriteString(_T("\r\n"));
	}
	sFile.Close();

	setlocale( LC_CTYPE, old_locale ); 
	free( old_locale );//还原区域设定 
}


void CTypedUrlsMgr::DeleteUrl2(const CString& strDeletedUrl)
{ 
 	CString strUrl = strDeletedUrl;
	GetHistory()->DeleteGoToUrl(strUrl);
	IUrlHistoryStg2* puhs = NULL;
	HRESULT hr = ::CoCreateInstance (CLSID_CUrlHistory, NULL, CLSCTX_INPROC_SERVER, IID_IUrlHistoryStg, (LPVOID*)&puhs);
	if (SUCCEEDED(hr))
	{
		CString strValue (strUrl);
		USES_CONVERSION;
		LPCOLESTR pocsUrl = T2OLE (strValue.GetBuffer(strUrl.GetLength()));
		strValue.ReleaseBuffer();
		hr = puhs->DeleteUrl (pocsUrl, 0);
		if (SUCCEEDED(hr))
		{
		}
	}
}

void CTypedUrlsMgr::DeleteUrl(const CString& strDeletedUrl)
{
	CString strFileName = GetFavConfigFilePath();

	char* old_locale = _strdup( setlocale(LC_CTYPE,NULL) ); 
	setlocale( LC_CTYPE, "chs" );//设定

	CStdioFile sFile;
	if (sFile.Open(strFileName, CFile::modeRead |  CFile::typeText) == FALSE)
	{
		return;
	}

	CStringList strUrls;
	CString strUrl;
	while (sFile.ReadString(strUrl) == TRUE)
	{
		strUrl.Trim();
		if ((strUrl.IsEmpty() == TRUE) 
			|| (strUrl.GetAt(0) == L';'))//过滤注释信息
			continue;
		
		if (NULL == strUrls.Find(strUrl))
			strUrls.AddTail (strUrl);
	}
	sFile.Close();

	if (sFile.Open(strFileName, CFile::modeWrite | CFile::modeCreate | CFile::typeText) == FALSE)
	{
		return;
	}

	POSITION pos = strUrls.GetHeadPosition();
	while (NULL != pos)
	{
		strUrl = strUrls.GetNext(pos);
		if (strUrl != strDeletedUrl) 
		{
			sFile.WriteString(strUrl);
			sFile.WriteString(_T("\r\n"));
		}
	}
	sFile.Close();

	setlocale( LC_CTYPE, old_locale ); 
	free( old_locale );//还原区域设定 

	IUrlHistoryStg2* puhs = NULL;
	HRESULT hr = ::CoCreateInstance (CLSID_CUrlHistory, NULL, CLSCTX_INPROC_SERVER, IID_IUrlHistoryStg, (LPVOID*)&puhs);
	if (SUCCEEDED(hr))
	{
		CString strValue (strUrl);
		USES_CONVERSION;
		LPCOLESTR pocsUrl = T2OLE (strValue.GetBuffer(strUrl.GetLength()));
		strValue.ReleaseBuffer();
		hr = puhs->DeleteUrl (pocsUrl, 0);
		if (SUCCEEDED(hr))
		{
		}
	}
}