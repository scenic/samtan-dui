#include "StdAfx.h"
#include "SideAccountMgrWnd.h"
#include "AddGameAccountWnd.h"
#include "AccountSetupWnd.h"
#include "AccountMgrWnd.h"
#include "SidePanelUI.h"
#include "../Include/UzDefine.h"

CSideAccountMgrWnd::CSideAccountMgrWnd(void)
{
	m_pAccountMgr = NULL;
}
CSideAccountMgrWnd::~CSideAccountMgrWnd(void)
{
	if (NULL != m_pAccountMgr)
	{
		delete m_pAccountMgr;
		m_pAccountMgr = NULL;
	}
}

// 初始化
void CSideAccountMgrWnd::Init() {}

void CSideAccountMgrWnd::OnPrepare() {}

// 控件通知消息处理
void CSideAccountMgrWnd::Notify(DuiLib::TNotifyUI& msg)
{
	if (msg.sType == _T("windowinit"))
	{
		OnPrepare();
	}
	else if (msg.sType == _T("selectchanged"))
	{
	}
	else if (msg.sType == _T("click"))
	{
		if (msg.pSender->GetName() == _T("btnClose"))
		{
			PostMessage(WM_CLOSE);
			return;
		}
		else if (msg.pSender->GetName() == _T("optLockWnd"))
		{
			m_bLock = !m_bLock;
			if (m_bLock)
			{
				m_pOwner->UnAttcah();
				m_pOwner->GetParent()->SetVisible(false);
				RECT rect = m_pOwner->GetPos();
				::MoveWindow(*this, rect.left, rect.top, rect.right-rect.left, 500, TRUE);
			}
			else
			{
				m_pOwner->Attach(this);
				m_pOwner->GetParent()->SetVisible(true);
			}
		}
		if (msg.pSender->GetName() == _T("btnAddGameAccount"))
		{
			CAddGameAccountWnd* pAddGameAccountWnd = new CAddGameAccountWnd;
			pAddGameAccountWnd->Create(this->m_hWnd, NULL, WS_OVERLAPPEDWINDOW, 0);
			pAddGameAccountWnd->CenterWindow();
			pAddGameAccountWnd->SetParentWnd(*this);
			pAddGameAccountWnd->ShowWindow(true);
		}
		if (msg.pSender->GetName() == _T("btnAccountSetting"))
		{
			CAccountSetupWnd* pAccountSetupWnd = new CAccountSetupWnd;
			pAccountSetupWnd->Create(*this, NULL, WS_OVERLAPPEDWINDOW, 0);
			pAccountSetupWnd->CenterWindow();
			pAccountSetupWnd->ShowWindow(true);
		}
		if (msg.pSender->GetName() == _T("btnAccountMgr"))
		{
			if (NULL != m_pAccountMgr)
			{
				return;
			}
			m_pAccountMgr = new CAccountMgrWnd;
			m_pAccountMgr->Init(*this, &m_pAccountMgr);
			m_pAccountMgr->ShowWindow();
		}
	}
}

// 创建窗口函数
LRESULT CSideAccountMgrWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Init();
	LONG styleValue = ::GetWindowLong(*this, GWL_STYLE);
	styleValue &= ~WS_CAPTION;
	::SetWindowLong(*this, GWL_STYLE, styleValue | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	m_pm.Init(m_hWnd);
	DuiLib::CDialogBuilder builder;

	DuiLib::CControlUI* pRoot = builder.Create(_T("SideAccountWnd//side_account_wnd.xml"), (UINT)0,  NULL, &m_pm);
	ASSERT(pRoot && "Failed to parse XML");

	m_pm.AttachDialog(pRoot);
	m_pm.AddNotifier(this);

	::SetWindowPos(*this, NULL, 40, 141, 0,0, SWP_NOSIZE );

	return 0;
}

// 关闭窗口函数
LRESULT CSideAccountMgrWnd::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	this->ShowWindow(false);
	DuiLib::CControlUI* pPanel = m_pOwner->GetParent();
	pPanel->SetVisible(false);
	return 0;
}

// 销毁窗口函数
LRESULT CSideAccountMgrWnd::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	//::PostQuitMessage(0L);
	bHandled = FALSE;
	return 0;
}

//
LRESULT CSideAccountMgrWnd::OnNcActivate(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (::IsIconic(*this))
	{
		bHandled = FALSE;
	}
	return (wParam == 0) ? TRUE : FALSE;
}

LRESULT CSideAccountMgrWnd::OnNcCalcSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CSideAccountMgrWnd::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CSideAccountMgrWnd::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	::ScreenToClient(*this, &pt);

	RECT rcClient;
	::GetClientRect(*this, &rcClient);

	if ( !::IsZoomed(*this) )
	{
		RECT rcSizeBox = m_pm.GetSizeBox();
		if ( pt.y < rcClient.top + rcSizeBox.top ) {
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTTOPLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTTOPRIGHT;
			return HTTOP;
		}
		else if ( pt.y > rcClient.bottom - rcSizeBox.bottom )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTBOTTOMLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTBOTTOMRIGHT;
			return HTBOTTOM;
		}
		if ( pt.x < rcClient.left + rcSizeBox.left )
			return HTLEFT;
		if ( pt.x > rcClient.right - rcSizeBox.right )
			return HTRIGHT;
	}

	RECT rcCaption = m_pm.GetCaptionRect();
	if ( pt.x >= rcClient.left + rcCaption.left && pt.x < rcClient.right - rcCaption.right
		&& pt.y >= rcCaption.top && pt.y < rcCaption.bottom )
	{
		DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_pm.FindControl(pt));
		if ( pControl && _tcsicmp(pControl->GetClass(), _T("ButtonUI")) != 0
			&& _tcsicmp(pControl->GetClass(), _T("OptionUI")) != 0)
			return HTCAPTION;
	}

	return HTCLIENT;
}

LRESULT CSideAccountMgrWnd::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	SIZE szRoundCorner = m_pm.GetRoundCorner();
	if( !::IsIconic(*this) && (szRoundCorner.cx != 0 || szRoundCorner.cy != 0) )\
	{
		RECT rcClient;
		::GetClientRect(*this, &rcClient);
		RECT rc = { rcClient.left, rcClient.top + szRoundCorner.cx, rcClient.right, rcClient.bottom };
		HRGN hRgn1 = ::CreateRectRgnIndirect( &rc );
		HRGN hRgn2 = ::CreateRoundRectRgn(rcClient.left, rcClient.top, rcClient.right + 1,
			rcClient.bottom - szRoundCorner.cx, szRoundCorner.cx, szRoundCorner.cy);
		::CombineRgn( hRgn1, hRgn1, hRgn2, RGN_OR );
		::SetWindowRgn(*this, hRgn1, TRUE);
		::DeleteObject(hRgn1);
		::DeleteObject(hRgn2);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CSideAccountMgrWnd::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	MONITORINFO oMonitor = {};
	oMonitor.cbSize = sizeof(oMonitor);
	::GetMonitorInfo(::MonitorFromWindow(*this, MONITOR_DEFAULTTOPRIMARY), &oMonitor);
	//CRect rcWork = oMonitor.rcWork;
	RECT rcWork = oMonitor.rcWork;
	OffsetRect(&rcWork,-rcWork.left, -rcWork.top);

	LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
	lpMMI->ptMaxPosition.x = rcWork.left;
	lpMMI->ptMaxPosition.y = rcWork.top;
	lpMMI->ptMaxSize.x     = rcWork.right;
	lpMMI->ptMaxSize.y     = rcWork.bottom;

	bHandled = FALSE;
	return 0;
}

LRESULT CSideAccountMgrWnd::OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if (m_bLock)
		ShowWindow(false, false);
	bHandled = false;
	return 0;
}


LRESULT CSideAccountMgrWnd::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// 有时会在收到WM_NCDESTROY后收到wParam为SC_CLOSE的WM_SYSCOMMAND
	if( wParam == SC_CLOSE )
	{
		::PostQuitMessage(0L);
		bHandled = TRUE;
		return 0;
	}
	//BOOL bZoomed = ::IsZoomed(*this);
	LRESULT lRes = DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);

	return lRes;
}

LRESULT CSideAccountMgrWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL bHandled = TRUE;
	switch (uMsg)
	{
	case WM_CREATE:
		lRes = OnCreate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_CLOSE:
		lRes = OnClose(uMsg, wParam, lParam, bHandled);
		break;
	case WM_DESTROY:
		lRes = OnDestroy(uMsg, wParam, lParam, bHandled);
		break;
	case WM_TIMER:
		break;
	case WM_NCACTIVATE:
		lRes = OnNcActivate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCCALCSIZE:
		lRes = OnNcCalcSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCPAINT:
		lRes = OnNcPaint(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCHITTEST:
		lRes = OnNcHitTest(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SIZE:
		lRes = OnSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_GETMINMAXINFO:
		lRes = OnGetMinMaxInfo(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SYSCOMMAND:
		lRes = OnSysCommand(uMsg, wParam, lParam, bHandled);
		break;
	case WM_KEYDOWN:
		break;
	case WM_KILLFOCUS:
		OnKillFocus(uMsg, wParam, lParam, bHandled);
		break;
	case ACCOUNT_ADD_GAME_BUTTON_OK:	// 添加游戏界面确定按钮消息
		{
			// 获取游戏名
			CString strGameName = *((CString*)wParam);
			// 获取网址
			CString strGameURL = *((CString*)lParam);
			CString strName = _T("");
			if (NULL == m_pAccountMgr)
			{
				m_pAccountMgr = new CAccountMgrWnd;
				m_pAccountMgr->Init(*this, &m_pAccountMgr);
				m_pAccountMgr->ShowWindow();
			}

			m_pAccountMgr->AddGameName(strGameName);
			m_pAccountMgr->AddAccount(strName, strName, strGameURL, strName, true);
			break;
		}
	default:
		bHandled = FALSE;
	}
	if (bHandled)
	{
		return lRes;
	}
	if (m_pm.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);
}