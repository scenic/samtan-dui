#include "StdAfx.h"
#include "ManageFavToolBarTree.h"
#include "../Include/UzDefine.h"

#define TREE_TAG_FAVFOLDER 2	// 此为Tree Node的标签
LRESULT CManageFavToolBarTree::OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	Close();
	bHandled = FALSE;
	return 0;
}

void CManageFavToolBarTree::Init(HWND hWndParent, POINT ptPos, int nWidth)
{
	Create(hWndParent, _T("MenuWnd"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	::ClientToScreen(hWndParent, &ptPos);
	::SetWindowPos(*this, NULL, ptPos.x, ptPos.y, nWidth, 200, SWP_NOZORDER |  SWP_NOACTIVATE);

	m_ParentWnd = hWndParent;

	// 加载树控件
	m_pTreeView = static_cast<DuiLib::CTreeViewUI*>(m_PaintManager.FindControl(_T("ToolBarTree")));

	ASSERT(m_pTreeView);
	if (NULL == m_pTreeView)
	{
		return;
	}

	AddLocalFavNode();
}

LRESULT CManageFavToolBarTree::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return __super::HandleMessage(uMsg, wParam, lParam);
}


// 添加一行树
bool CManageFavToolBarTree::AddTreeNode(CString& strText, DuiLib::CTreeNodeUI* parent, int nIndex)
{
	if (NULL == m_pTreeCocalFav || "" == strText)
	{
		return false;
	}

	// 判断树的名字是否唯一
	if (NULL != GetNode(strText))
	{
		MessageBox(NULL, _T("此收藏夹名已经存在!"), _T("提示"), MB_OK);
		return false;
	}

	if (NULL == parent)
	{
		parent = m_pTreeCocalFav;
	}

	DuiLib::CTreeNodeUI* pNodeChild = new DuiLib::CTreeNodeUI(parent);
	pNodeChild->SetAttribute(_T("folderattr"),_T("normalimage=\"file='SideFavWnd//res//wenjian2.png'\" selectedimage=\"file='SideFavWnd//res//wenjian1.png'\""));
	CStringW strWText = strText;
	pNodeChild->SetItemText(strWText);
	pNodeChild->SetTag(TREE_TAG_FAVFOLDER);
	pNodeChild->SetTreeView(m_pTreeView);


	if (-1 != nIndex)
	{
		// 添加一个结点
		parent->AddAt(pNodeChild, nIndex);
	}
	else
	{	
		// 获取子节点的个数
		int nCount = parent->GetCountChild();
		// 添加一个结点
		parent->AddAt(pNodeChild, nCount);
	}

	return true;
}

// 添加本地收藏夹树结点
bool CManageFavToolBarTree::AddLocalFavNode()
{
	DuiLib::CTreeNodeUI* pNode = new DuiLib::CTreeNodeUI;
	pNode->SetAttribute(_T("folderattr"),_T("normalimage=\"file='SideFavWnd//res//xingxing.png'\" selectedimage=\"file='SideFavWnd//res//xingxing.png'\""));
	pNode->SetItemText(_T("本地收藏夹"));
	pNode->SetName(_T("LocalFav"));
	//pNode->SetBkColor(0xFFEBEADB);
	pNode->SetTag(TREE_TAG_FAVFOLDER);
	// 将本地收藏夹Node保存到m_pTreeCocalFav
	m_pTreeCocalFav = pNode;
	m_pTreeView->Add(pNode);
	return true;
}

// 通过名字获取本地收藏夹下的子结点
DuiLib::CTreeNodeUI* CManageFavToolBarTree::GetNode(CString& strText)
{
	// 判断本地收藏夹结点是否存在
	if (NULL == m_pTreeCocalFav)
	{
		return NULL;
	}

	return GetChildNode(strText, m_pTreeCocalFav);
}

// 通过名字获取指定父结点下的子节点，返回为NULL时表示未找到
DuiLib::CTreeNodeUI* CManageFavToolBarTree::GetChildNode(CString& strText, DuiLib::CTreeNodeUI* parent)
{
	if ("" == strText)
	{
		return NULL;
	}

	// 获取子节点的个数
	int nCocalFavCount = parent->GetCountChild();
	// 创建一个Node指针用来保存查找的结点
	DuiLib::CTreeNodeUI* pTargetNode = NULL;
	for (int i = 0; i < nCocalFavCount; i++)
	{
		// 获取子节点
		DuiLib::CTreeNodeUI* pChildNode = parent->GetChildNode(i);

		// 当子节点下还有结点时，调用递归函数
		if (pChildNode->GetCountChild() > 0)
		{
			pTargetNode = GetChildNode(strText, pChildNode);
			// 当递归调用找到文件名时，跳出循环
			if (NULL != pTargetNode)
			{
				break;
			}
		}

		// 获取当前结点的文本名
		CStringW strWTargetText = pChildNode->GetItemText();
		CStringW strWtext = strText;
		// 判断文本名是否是要查找的名字，如果是，跳出循环
		if (strWTargetText == strWtext)
		{
			pTargetNode = pChildNode;
			break;
		}
	}

	return pTargetNode;
}

void CManageFavToolBarTree::Notify(DuiLib::TNotifyUI& msg)
{
	// 当消息为itemclick时
	if (msg.sType == DUI_MSGTYPE_ITEMCLICK)
	{
		// 判断是否为树控件
		if (m_pTreeView && TREE_TAG_FAVFOLDER == msg.pSender->GetTag())
		{
			// 获取树控件所点击的文本
			CString strText = ((DuiLib::CTreeNodeUI *)msg.pSender)->GetItemText();
			// 发送文本消息到父窗口
			::SendMessage(m_ParentWnd, FAV_ITEM_TREE_CLICK, 0,(LPARAM)(&strText));
			Close();
		}
	}
	return WindowImplBase::Notify(msg);
}