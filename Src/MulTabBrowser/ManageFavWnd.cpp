#include "StdAfx.h"
#include "ManageFavWnd.h"
#include "ManageFavListMenu.h"
#include "ManageFavTreeMenu.h"
#include "ManageFavToolBarNewFav.h"
#include "ManageFavToolBarNewFolder.h"
#include "ManageFavToolBarImportAndExport.h"
#include "ManageFavToolBarEdiotr.h"
#include "ManageFavToolBarEdiotrTree.h"
#include "InterView.h"
#include "MulTabBrowser.h" 
#include "../Include/UzDefine.h"
#include "ManageFavXML.h"

#define LIST_TAG_FAVURL 1		// 此为List的标签
#define TREE_TAG_FAVFOLDER 2	// 此为Tree Node的标签
#define TREE_TAG_FAVLOCAL 3		// 此为本地收藏夹的标签

CManageFavWnd::CManageFavWnd(void)
{
	m_pListCE = NULL;
	m_nListIndex = -1;
	m_pTreeCocalFav = NULL;
	m_pTreeView = NULL;
	m_strTreeNodeText = "";

	m_InterView = theApp.pBrowser->GetCurentView();
	m_FavToolFunc = new CFavToolFunc;
	m_ManageFavXML = new CManageFavXML;
}


CManageFavWnd::~CManageFavWnd(void)
{
	m_TreeSort.clear();

	if (NULL != m_FavToolFunc)
	{
		delete m_FavToolFunc;
		m_FavToolFunc = NULL;
	}
	if (NULL != m_InterView)
	{
		m_InterView = NULL;
	}
}

void CManageFavWnd::Init(HWND hWndParent)
{
	Create(hWndParent, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
	CenterWindow();
	return;
}

void CManageFavWnd::InitWindow()
{
	// 加载List
	m_pList = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("listex")));
	ASSERT(m_pList);
	if (NULL == m_pList)
	{
		return;
	}

	// 加载树控件
	m_pTreeView = static_cast<DuiLib::CTreeViewUI*>(m_PaintManager.FindControl(_T("FavTree")));
	ASSERT(m_pTreeView);
	if (NULL == m_pTreeView)
	{
		return;
	}

	// 添加本地收藏夹，在函数中给m_pTreeCocalFav初始化
	AddLocalFavNode();

	// 加载XML到List链表
	LoadXmlToList();
/*****************************         以下为测试程序     *****************************************************/

	// 添加List一行 控件-------------------------------------------------------------
	//CString strIconURL = _T("checkbox_p.png");
	//CString strTitle = _T("测试程序标题");
	//CString strURL = _T("www.baidu.com");
	//AddListLine(strIconURL, strTitle, strURL);
	//CString strTitle1 = _T("2222");
	//CString strURL1 = _T("www.tudou.com");
	//AddListLine(strIconURL, strTitle1, strURL1);
	CString parent = _T("本地收藏夹");
	ShowLinkListToList(parent);
	// 添加List一行 控件-----------------------------------------------------------------

	// 添加一行Tree 控件--------------------------------------------------------------
	//CString strTreeNode1 = _T("aaaa");
	//AddTreeNode(strTreeNode1,NULL, 0);

	//CString strTreeNode2 = _T("bbbbb");
	//AddTreeNode(strTreeNode2, NULL, 1);

	//CString strTreeNode3 = _T("ccccc");
	//AddTreeNode(strTreeNode3, GetNode(strTreeNode2), 0);	// 设置ccc为bbbbb的子节点

	//CString strTreeNode6 = _T("fffff");
	//AddTreeNode(strTreeNode6, GetNode(strTreeNode2), 1);	// 设置fff为bbbbb的子节点


	//CString strTreeNode4 = _T("ddddd");
	//AddTreeNode(strTreeNode4,  GetNode(strTreeNode3), 0);	// 设置ddd为ccccc的子节点

	//CString strTreeNode7 = _T("ggggg");
	//AddTreeNode(strTreeNode7,  GetNode(strTreeNode3), 1);	// 设置ggggggg为ccccc的子节点

	//CString strTreeNode5 = _T("eeeee");
	//AddTreeNode(strTreeNode5, NULL, 2);
	ShowFolderListToTree();

	// 添加一行Tree 控件--------------------------------------------------------------

/*****************************         以上为测试程序     *****************************************************/
}

// 添加List一行
bool CManageFavWnd::AddListLine(CString& strIconURL, CString& strTitle, CString& strURL, int nIndex)
{
	if ("" == strIconURL || "" == strTitle || "" == strURL)
	{
		return false;
	}

	DuiLib::CDialogBuilder builder;
	DuiLib::CListContainerElementUI* m_pListCE = (DuiLib::CListContainerElementUI*)(builder.Create(_T("SideFavWnd//ManageFav_List.xml"),(UINT)0));
	if (NULL == m_pList || NULL == m_pListCE)
	{
		return false;
	}
	
	// 根据标题判断是否存在
	if (NULL != GetListLine(strTitle))
	{
		MessageBox(NULL, _T("此标题已经存在!"), _T("提示"), MB_OK);
		return false;
	}

	// 给图标赋值
	DuiLib::CLabelUI* pLabelIcon = (DuiLib::CLabelUI*)m_pListCE->FindSubControl(_T("IconLabel"));
	ASSERT(pLabelIcon);
	if (NULL == pLabelIcon)
	{
		return false;
	}
	CStringW strWIconURL = _T("file='SideFavWnd//res//") + strIconURL + _T("'");
	pLabelIcon->SetBkImage(strWIconURL);

	// 给标题赋值
	DuiLib::CLabelUI* pLabelTitle = (DuiLib::CLabelUI*)m_pListCE->FindSubControl(_T("TitleLabel"));
	ASSERT(pLabelTitle);
	if (NULL == pLabelTitle)
	{
		return false;
	}
	CStringW strWTitle = strTitle;
	pLabelTitle->SetText(strTitle);

	// 给网址赋值
	DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)m_pListCE->FindSubControl(_T("URLLabel"));
	ASSERT(pLabel);
	if (NULL == pLabel)
	{
		return false;
	}
	CStringW strWURL = strURL;
	pLabel->SetText(strWURL);

	// 添加一行控件
	m_pListCE->SetTag(LIST_TAG_FAVURL);

	// 添加到最后一行
	if (-1 == nIndex)
	{
		return m_pList->Add(m_pListCE);
	}
	
	return m_pList->AddAt(m_pListCE, nIndex);
}

// 删除一行List数据
bool CManageFavWnd::DelListLine(int& nIndex)
{
	if (-1 == nIndex)
	{
		return false;
	}

	if(m_pList->RemoveAt(nIndex))
	{
		// 改变索引值，以免重复删除
		nIndex = -1;
		return true;
	}
	else
	{
		return false;
	}
}

// 通过标题获取控件
DuiLib::CListContainerElementUI* CManageFavWnd::GetListLine(CString& strTitle)
{
	int nCount = m_pList->GetCount();
	DuiLib::CListContainerElementUI* pTargetLine = NULL;
	for (int i = 0; i < nCount; i++)
	{
		DuiLib::CListContainerElementUI* pLine =(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
		CString Title = pLine->GetSubControlText(_T("TitleLabel"));
		if (Title == strTitle)
		{
			pTargetLine = pLine;
			break;
		}
	}
	return pTargetLine;
}


// 删除一行Tree数据，strText为删除行的Text
bool CManageFavWnd::DelTreeLine(CString& strText)
{
	if ("" == strText)
	{
		return false;
	}
	
	// 保存要删除的结点
	DuiLib::CTreeNodeUI* pDelNode = GetNode(strText);
	// 判断结点是否有子节点
	int nCount = pDelNode->GetCountChild();
	if (nCount > 0)
	{
		if(IDYES == MessageBox(NULL, _T("此收藏夹内还有文件，确定一并删除？"), _T("提示"), MB_YESNO))
		{
			strText = "";
			
			return DelAllTreeNode(pDelNode);
		}
		else
		{
			return false;
		}
	}

	strText = "";
	return pDelNode->GetParentNode()->RemoveAt(pDelNode);
}

// 删除TreeNode所有子结点
bool CManageFavWnd::DelAllTreeNode(DuiLib::CTreeNodeUI* pParentNode)
{
	if (pParentNode->GetCountChild() > 0)
	{
		for (int i = 0; i < pParentNode->GetCountChild(); i++)
		{
			DelAllTreeNode(pParentNode->GetChildNode(i));
		}
	}

	return pParentNode->GetParentNode()->RemoveAt(pParentNode);
}

// 添加一行树
bool CManageFavWnd::AddTreeNode(CString& strText, DuiLib::CTreeNodeUI* parent, int nIndex)
{
	if (NULL == m_pTreeCocalFav || "" == strText)
	{
		return false;
	}

	// 判断树的名字是否唯一
	if (NULL != GetNode(strText))
	{
		MessageBox(NULL, _T("此收藏夹名已经存在!"), _T("提示"), MB_OK);
		return false;
	}

	if (NULL == parent)
	{
		parent = m_pTreeCocalFav;
	}

	DuiLib::CTreeNodeUI* pNodeChild = new DuiLib::CTreeNodeUI(parent);
	pNodeChild->SetAttribute(_T("folderattr"),_T("normalimage=\"file='SideFavWnd//res//wenjian2.png'\" selectedimage=\"file='SideFavWnd//res//wenjian1.png'\""));
	CStringW strWText = strText;
	pNodeChild->SetItemText(strWText);
	pNodeChild->SetTag(TREE_TAG_FAVFOLDER);
	pNodeChild->SetTreeView(m_pTreeView);


	if (-1 != nIndex)
	{
		// 添加一个结点
		parent->AddAt(pNodeChild, nIndex);
	}
	else
	{	
		// 获取子节点的个数
		int nCount = parent->GetCountChild();
		// 添加一个结点
		parent->AddAt(pNodeChild, nCount);
	}

	return true;
}

// 添加本地收藏夹树结点
bool CManageFavWnd::AddLocalFavNode()
{
	DuiLib::CTreeNodeUI* pNode = new DuiLib::CTreeNodeUI;
	pNode->SetAttribute(_T("folderattr"),_T("normalimage=\"file='SideFavWnd//res//xingxing.png'\" selectedimage=\"file='SideFavWnd//res//xingxing.png'\""));
	pNode->SetItemText(_T("本地收藏夹"));
	pNode->SetName(_T("LocalFav"));
	pNode->SetBkColor(0xFFEBEADB);
	pNode->SetTag(TREE_TAG_FAVLOCAL);
	// 将本地收藏夹Node保存到m_pTreeCocalFav
	m_pTreeCocalFav = pNode;
	m_pTreeView->Add(pNode);
	return true;
}

// 通过名字获取本地收藏夹下的子结点
DuiLib::CTreeNodeUI* CManageFavWnd::GetNode(CString& strText)
{
	// 判断本地收藏夹结点是否存在
	if (NULL == m_pTreeCocalFav)
	{
		return NULL;
	}

	return GetChildNode(strText, m_pTreeCocalFav);
}

// 上移树控件,pTreeNode为要上移的Node
bool CManageFavWnd::UpTreeNode(DuiLib::CTreeNodeUI* pTreeNode)
{
	// 清空list链表
	m_TreeSort.clear();
	int nCount = pTreeNode->GetParentNode()->GetCountChild();

	CString strText = pTreeNode->GetItemText();

	// 添加list链表
	if (!AddNodeToList(pTreeNode))
	{
		return false;
	}

	// 删除所有同父级子节点
	if (!DelParentChildNode(pTreeNode))
	{
		return false;
	}

	// 遍历结构体,交换结构体要上移的两个结点文本
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if ((strText == i->strText) && (i->nIndex < nCount))
		{
			for (std::list<TREESORT>::iterator m = m_TreeSort.begin(); m != m_TreeSort.end(); m++)
			{
				// 找到同父节点的上一个子结点
				if ((m->strParentText == i->strParentText) && (m->nIndex == i->nIndex + 1))
				{
					// 交换结构体文本
					TREESORT TreeSort(i->nIndex, i->strParentText, i->strText);
					i->strText = m->strText;
					m->strText = TreeSort.strText;
					break;
				}
			}
			break;
		}
	}

	AddListToTree(pTreeNode->GetParentNode());
	return true;
}

// 下移树控件，pTreeNode为要下移的Node
bool CManageFavWnd::DownTreeNode(DuiLib::CTreeNodeUI* pTreeNode)
{
	// 清空list链表
	m_TreeSort.clear();
	//int nCount = pTreeNode->GetParentNode()->GetCountChild();

	CString strText = pTreeNode->GetItemText();

	// 添加list链表
	if (!AddNodeToList(pTreeNode))
	{
		return false;
	}

	// 删除所有同父级子节点
	if (!DelParentChildNode(pTreeNode))
	{
		return false;
	}

	// 遍历结构体,交换结构体要上移的两个结点文本
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if ((strText == i->strText) && (i->nIndex > 0))
		{
			for (std::list<TREESORT>::iterator m = m_TreeSort.begin(); m != m_TreeSort.end(); m++)
			{
				// 找到同父节点的上一个子结点
				if ((m->strParentText == i->strParentText) && (m->nIndex == i->nIndex - 1))
				{
					// 交换结构体文本
					TREESORT TreeSort(i->nIndex, i->strParentText, i->strText);
					i->strText = m->strText;
					m->strText = TreeSort.strText;
					break;
				}
			}
			break;
		}
	}

	AddListToTree(pTreeNode->GetParentNode());
	return true;
}

// 通过名字获取指定父结点下的子节点，返回为NULL时表示未找到
DuiLib::CTreeNodeUI* CManageFavWnd::GetChildNode(CString& strText, DuiLib::CTreeNodeUI* parent)
{
	if ("" == strText)
	{
		return NULL;
	}

	// 获取子节点的个数
	int nCocalFavCount = parent->GetCountChild();
	// 创建一个Node指针用来保存查找的结点
	DuiLib::CTreeNodeUI* pTargetNode = NULL;
	for (int i = 0; i < nCocalFavCount; i++)
	{
		// 获取子节点
		DuiLib::CTreeNodeUI* pChildNode = parent->GetChildNode(i);

		// 当子节点下还有结点时，调用递归函数
		if (pChildNode->GetCountChild() > 0)
		{
			pTargetNode = GetChildNode(strText, pChildNode);
			// 当递归调用找到文件名时，跳出循环
			if (NULL != pTargetNode)
			{
				break;
			}
		}

		// 获取当前结点的文本名
		CStringW strWTargetText = pChildNode->GetItemText();
		CStringW strWtext = strText;
		// 判断文本名是否是要查找的名字，如果是，跳出循环
		if (strWTargetText == strWtext)
		{
			pTargetNode = pChildNode;
			break;
		}
	}

	return pTargetNode;
}

// 添加当前同父级节点到链表
bool CManageFavWnd::AddNodeToList(DuiLib::CTreeNodeUI* NodeChild)
{
	bool Isb = false;
	if (NULL == NodeChild)
	{
		return Isb;
	}

	DuiLib::CTreeNodeUI* ParentNode = NodeChild->GetParentNode();
	for (int i = 0; i < ParentNode->GetCountChild(); i++)
	{
		TREESORT TreeSort;
		TreeSort.nIndex = i;
		TreeSort.strParentText = ParentNode->GetItemText();
		TreeSort.strText = ParentNode->GetChildNode(i)->GetItemText();

		// 将结点添加到List头
		m_TreeSort.push_back(TreeSort);

		// 如果当前结点存在子结点则递归，将子节点的同级结点添加到List
		if (ParentNode->GetChildNode(i)->GetCountChild() > 0)
		{
			AddNodeToList(ParentNode->GetChildNode(i)->GetChildNode(0));
		}

	}

	Isb = true;
	return Isb;
}

// 删除当前同父级所有子结点,NodeChild为父节点下一级子节点
bool CManageFavWnd::DelParentChildNode(DuiLib::CTreeNodeUI* NodeChild)
{
	bool Isb = false;
	if (NULL == NodeChild)
	{
		return Isb;
	}
	// 获取父节点
	DuiLib::CTreeNodeUI* ParentNode = NodeChild->GetParentNode();
	// 删除父节点下所有子节点
	while (ParentNode->GetCountChild() > 0)
	{
		ParentNode->RemoveAt(ParentNode->GetChildNode(0));
	}
	Isb = true;
	return Isb;
}

// 从链表添加结点
bool CManageFavWnd::AddListToTree(DuiLib::CTreeNodeUI* parent)
{
	CString strParentText = parent->GetItemText();
	// 遍历结构体,添加第一层子结点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText == i->strParentText)
		{
			AddTreeNode(i->strText, parent, i->nIndex);
		}
	}
	// 分两次添加的原因是避免因为交换链表结点导致的子节点的父节点未被创建导致的添加失败问题
	// 遍历树结构，添加后面层的子节点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText != i->strParentText)
		{
			AddTreeNode(i->strText, GetNode(i->strParentText), i->nIndex);
		}
	}
	return true;
}

// 加载XML到List链表
bool CManageFavWnd::LoadXmlToList()
{
	// 清空List
	m_TreeFolderXML.clear();
	m_ListLinkXML.clear();
	// 添加List
	return m_ManageFavXML->GetAllXML(m_TreeFolderXML, m_ListLinkXML);
}

// 从文件夹链表添加到树控件
bool CManageFavWnd::ShowFolderListToTree()
{
	// 删除树视图列表
	while (0 != m_pTreeCocalFav->GetCountChild())
	{
		m_pTreeCocalFav->RemoveAt(m_pTreeCocalFav->GetChildNode(0));
	}

	// 遍历结构体,添加第一层子结点
	for (std::list<TREESORT>::iterator i = m_TreeFolderXML.begin(); i != m_TreeFolderXML.end(); i++)
	{
		if ("本地收藏夹" == i->strText)
		{
			continue;
		}
		if ("" == i->strParentText)
		{
			i->strParentText = _T("本地收藏夹");
		}
		AddTreeNode(i->strText, GetNode(i->strParentText), i->nIndex);
	}
	return true;
}

// 从网址收藏链表添加到List控件
bool CManageFavWnd::ShowLinkListToList(const CString& strParentName)
{
	// 清空当前List列表界面
	m_pList->RemoveAll();
	// 遍历结构体,添加第一层子结点
	for (std::list<LIST_LINK>::iterator i = m_ListLinkXML.begin(); i != m_ListLinkXML.end(); i++)
	{
		if ("" == i->strParentFolderName)
		{
			i->strParentFolderName = _T("本地收藏夹");
		}
		if (strParentName == i->strParentFolderName)
		{
			CString strIcon = _T("checkbox_p.png");
			AddListLine(strIcon, i->strTitle, i->strURL, -1);
		}
	}
	return true;
}


// 单击事件响应
void CManageFavWnd::OnClick(DuiLib::TNotifyUI &msg )
{
	// 删除按钮的事件
	if (msg.pSender->GetName() == _T("ToolBarDel"))
	{
		// 判断是删除List还是Tree
		if (-1 != m_nListIndex)
		{
			DelListLine(m_nListIndex);
		}
		else if ("" != m_strTreeNodeText)
		{
			DelTreeLine(m_strTreeNodeText);
		}
	}
	// 新建收藏
	else if (msg.pSender->GetName() == _T("ToolBarNew"))
	{
		//CManageFavToolBarNewFav* pNewFav = new CManageFavToolBarNewFav(_T("SideFavWnd//ManageFav_ToolBarNew.xml"));
		//if (pNewFav == NULL)
		//{
		//	return ;
		//}

		//pNewFav->Init(*this);
		//pNewFav->ShowWindow(TRUE);

		//// 当树下没有结点直接返回
		//if (m_pTreeCocalFav->GetCountChild() < 1)
		//{
		//	return;
		//}

		//// 清空链表
		//m_TreeSort.clear();
		//// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
		//AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

		//// 添加结点到新建收藏
		//for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
		//{
		//	pNewFav->AddTreeSort(i->nIndex, i->strParentText, i->strText);
		//}

		CString strIconURL = _T("checkbox_p.png");
		CString strTitle = _T("测试程序标题");
		CString strURL = _T("www.baidu.com");
		AddListLine(strIconURL, strTitle, strURL);

	}
	// 新建文件夹
	else if (msg.pSender->GetName() == _T("ToolBarFile"))
	{
		CManageFavToolBarNewFolder* pNewFolder = new CManageFavToolBarNewFolder(_T("SideFavWnd//ManageFav_ToolBarNewFolder.xml"));
		if (pNewFolder == NULL)
		{
			return ;
		}

		pNewFolder->Init(*this);
		pNewFolder->ShowWindow(TRUE);

		// 当树下没有结点直接返回
		if (m_pTreeCocalFav->GetCountChild() < 1)
		{
			return;
		}

		// 清空链表
		m_TreeSort.clear();
		// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
		AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

		// 添加结点到新建文件夹
		for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
		{
			pNewFolder->AddTreeSort(i->nIndex, i->strParentText, i->strText);
		}
	}
	// 上移按钮
	else if (msg.pSender->GetName() == _T("ToolBarUp"))
	{
		// 判断是List还是Tree
		if (-1 != m_nListIndex)
		{
			if (m_nListIndex > 0)
			{
				m_pList->SetItemIndex(m_pList->GetItemAt(m_nListIndex), m_nListIndex--);
			}
		}
		else if ("" != m_strTreeNodeText)
		{
			UpTreeNode(GetNode(m_strTreeNodeText));
			m_strTreeNodeText = "";
		}
	}
	// 下移按钮
	else if (msg.pSender->GetName() == _T("ToolBarDown"))
	{
		// 判断是List还是Tree
		if (-1 != m_nListIndex)
		{
			if (m_nListIndex < m_pList->GetCount() - 1)
			{
				m_pList->SetItemIndex(m_pList->GetItemAt(m_nListIndex), m_nListIndex++);
			}
		}
		else if ("" != m_strTreeNodeText)
		{
			DownTreeNode(GetNode(m_strTreeNodeText));
			m_strTreeNodeText = "";
		}

	}
	//导入导出按钮
	else if (msg.pSender->GetName() == _T("ToolBarImportAndExport"))
	{
		CManageFavToolBarImportAndExport* pMenu = new CManageFavToolBarImportAndExport(_T("SideFavWnd//ManageFav_ToolBarImportAndExport.xml"));
		if (pMenu == NULL)
		{
			return ;
		}

		pMenu->Init(*this);
		pMenu->ShowWindow(TRUE);
	}
	// 关闭按钮
	else if (msg.pSender->GetName() == _T("closebtn"))
	{
		this->Close();
		//PostQuitMessage(0);
	}
}

void CManageFavWnd::OnHeaderClick(DuiLib::TNotifyUI& /*msg*/)
{
}

// 消息响应
void CManageFavWnd::Notify(DuiLib::TNotifyUI& msg)
{
	// 当消息为itemclick时
	if (msg.sType == DUI_MSGTYPE_ITEMCLICK)
	{
		// 判断是否为List中的Item
		if(m_pList && LIST_TAG_FAVURL == msg.pSender->GetTag())
		{
			m_nListIndex =((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();

			// 将树控件的文本设为""
			m_strTreeNodeText = "";
		}
		// 判断是否为树控件
		else if (m_pTreeView && TREE_TAG_FAVFOLDER == msg.pSender->GetTag())
		{
			// 获取树控件所点击的文本
			m_strTreeNodeText = ((DuiLib::CTreeNodeUI *)msg.pSender)->GetItemText();
			// 将List控件的Index设为-1
			m_nListIndex = -1;
			// 显示对应的List列表
			ShowLinkListToList(m_strTreeNodeText);
		}
		// 判断是否为本地收藏夹
		else if (m_pTreeView && TREE_TAG_FAVLOCAL == msg.pSender->GetTag())
		{
			CString strLocal = _T("本地收藏夹");
			// 显示对应的List列表
			ShowLinkListToList(strLocal);
		}
	}

	// 枚举消息时
	if (msg.sType == DUI_MSGTYPE_MENU)
	{
		CString strName = msg.pSender->GetName();
		// 判断是否为List消息
		if (-1 != m_nListIndex && strName == _T("listex"))
		{
			// 获取当前选中item的矩形区
			RECT ItemRect = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetItemAt(m_nListIndex)->GetPos();
			POINT pt = {msg.ptMouse.x, msg.ptMouse.y};

			// 判断是否在矩形内,是就创建枚举窗口
			if (pt.x >= ItemRect.left && pt.x <= ItemRect.right && pt.y >= ItemRect.top && pt.y <= ItemRect.bottom)
			{
				CManageFavListMenu* pMenu = new CManageFavListMenu(_T("SideFavWnd//ManageFav_ListMenu.xml"));
				if (pMenu == NULL)
				{
					return ;
				}

				pMenu->Init(*this, pt);
				pMenu->ShowWindow(TRUE);
			}
		}
		// 判断是否为Tree消息
		else if ("" != m_strTreeNodeText && strName == _T("FavTree"))
		{
			// 获取当前选中item的矩形区
			RECT ItemRect = GetNode(m_strTreeNodeText)->GetPos();
			POINT pt = {msg.ptMouse.x, msg.ptMouse.y};

			// 判断是否在矩形内,是就创建枚举窗口
			if (pt.x >= ItemRect.left && pt.x <= ItemRect.right && pt.y >= ItemRect.top && pt.y <= ItemRect.bottom)
			{
				CManageFavTreeMenu* pMenu = new CManageFavTreeMenu(_T("SideFavWnd//ManageFav_TreeMenu.xml"));
				if (pMenu == NULL)
				{
					return ;
				}

				pMenu->Init(*this, pt);
				pMenu->ShowWindow(TRUE);
			}
		}
	}

	return WindowImplBase::Notify(msg);
}

// 自定义消息
LRESULT CManageFavWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// 下拉框新建文件夹时
	if (uMsg == FAV_NEW_FOLDER_ADD)
	{
		// 获取编辑框
		CString strFolderName = *((CString*)wParam);

		// 获取下拉框
		CString strNewPos = *((CString*)lParam);

		TREESORT TreeSort;
		TreeSort.strParentText = strNewPos;
		TreeSort.strText = strFolderName;

		if (_T("本地收藏夹")== TreeSort.strParentText)
		{
			AddTreeNode(TreeSort.strText, NULL);
		}
		else
		{
			AddTreeNode(TreeSort.strText, GetNode(TreeSort.strParentText));
		}
	}

	//	List右键菜单
	if (uMsg == FAV_LIST_MENU)
	{
		int nIndex = (int)lParam;
		switch(nIndex)
		{
		case 0:	// 打开
			{
				// 获取Item
				DuiLib::CListContainerElementUI* pListCE = (DuiLib::CListContainerElementUI*)m_pList->GetItemAt(m_nListIndex);
				// 获取网址
				DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("URLLabel"));
				ASSERT(pLabel);
				if (NULL == pLabel)
				{
					return false;
				}
				CString strURL = pLabel->GetText();
				// 新建窗口，设置当前网址
				theApp.pBrowser->NewView(strURL);
				//// 在当前窗口设置网址
				//m_InterView->UzNavigate(strURL);
				break;
			}
		case 1:	// 复制网址
			{
				// 获取Item
				DuiLib::CListContainerElementUI* pListCE = (DuiLib::CListContainerElementUI*)m_pList->GetItemAt(m_nListIndex);
				// 获取网址
				DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("URLLabel"));
				ASSERT(pLabel);
				if (NULL == pLabel)
				{
					return false;
				}
				CString strURL = pLabel->GetText();
				CStringW strWURL = strURL; 
				CStringA strTemp = "";
				m_FavToolFunc->WStringTOAsinString(strWURL, strTemp);
				CopyToClipboard(strTemp);
				break;
			}
		case 2:	// “中间的————不需要做响应”
			break;
		case 3:	// 新建文件夹
			{
				CManageFavToolBarNewFolder* pNewFolder = new CManageFavToolBarNewFolder(_T("SideFavWnd//ManageFav_ToolBarNewFolder.xml"));
				if (pNewFolder == NULL)
				{
					break;
				}

				pNewFolder->Init(*this);
				pNewFolder->ShowWindow(TRUE);

				// 当树下没有结点直接返回
				if (m_pTreeCocalFav->GetCountChild() < 1)
				{
					break;
				}

				// 清空链表
				m_TreeSort.clear();
				// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
				AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

				// 添加结点到新建文件夹
				for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
				{
					pNewFolder->AddTreeSort(i->nIndex, i->strParentText, i->strText);
				}
				break;
			}
		case 4:	// 删除
			{
				// 判断List的Index是否合法
				if (-1 != m_nListIndex)
				{
					DelListLine(m_nListIndex);
				}
				break;
			}
		case 5:	// 编辑
			{
				// 获取Item
				DuiLib::CListContainerElementUI* pListCE = (DuiLib::CListContainerElementUI*)m_pList->GetItemAt(m_nListIndex);
				// 获取网址
				DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("URLLabel"));
				ASSERT(pLabel);
				if (NULL == pLabel)
				{
					return false;
				}
				CString strURL = pLabel->GetText();

				// 获取标题
				DuiLib::CLabelUI* pTitleLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("TitleLabel"));
				ASSERT(pTitleLabel);
				if (NULL == pTitleLabel)
				{
					return false;
				}
				CString strTitle = pTitleLabel->GetText();

				CManageFavToolBarEdiotr* pNewFav = new CManageFavToolBarEdiotr(_T("SideFavWnd//ManageFav_ToolBarEditor.xml"));
				if (pNewFav == NULL)
				{
					break;
				}

				pNewFav->Init(*this);
				// 设置标题和网址
				pNewFav->SetTitleURL(strTitle, strURL);
				pNewFav->ShowWindow(TRUE);

				// 当树下没有结点直接返回
				if (m_pTreeCocalFav->GetCountChild() < 1)
				{
					break;
				}

				// 清空链表
				m_TreeSort.clear();
				// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
				AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

				// 添加结点到新建收藏
				for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
				{
					pNewFav->AddTreeSort(i->nIndex, i->strParentText, i->strText);
				}

				break;
			}
		case 6:	// 替换为当前网址
			{
				// 获取Item
				DuiLib::CListContainerElementUI* pListCE = (DuiLib::CListContainerElementUI*)m_pList->GetItemAt(m_nListIndex);
				// 给网址赋值
				DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("URLLabel"));
				ASSERT(pLabel);
				if (NULL == pLabel)
				{
					return false;
				}
				
				// 获取当前网址
				CString strURL = m_InterView->GetUrl();
				CStringW strWURL = strURL;
				pLabel->SetText(strWURL);
				break;
			}
		default:
			break;
		}
	}

	//	Tree右键菜单
	if (uMsg == FAV_TREE_MENU)
	{
		int nIndex = (int)lParam;
		switch(nIndex)
		{
		case 0:	// 添加到文件夹
			{
				CString strIconURL = _T("checkbox_p.png");
				CString strTitle = m_InterView->GetUrl();
				// 获取当前网址
				CString strURL = m_InterView->GetUrl();

				// 根据标题判断是否存在,不存在时添加
				if (NULL == GetListLine(strTitle))
				{
					AddListLine(strIconURL, strTitle, strURL);
				}

				/*****测试程序******/
				
				CString a = _T("新的文件夹");
				CString b = _T("本地收藏夹");
				CString aa = _T("网址地址");
				CString bb = _T("www.baidu.com");
				m_ManageFavXML->AddFolder(a,b);
				m_ManageFavXML->AddLink(aa, bb, b);
				/*m_ManageFavXML->DelFolder(a);*/
				/*m_ManageFavXML->DelLink(a,b);*/
				//m_ManageFavXML->DelFav();
				/*****测试程序******/
				break;
			}
		case 1:	// 打开全部
			{
				// 遍历List列表
				for (int i = 0; i < m_pList->GetCount(); i++)
				{
					// 获取Item
					DuiLib::CListContainerElementUI* pListCE = (DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
					// 给网址赋值
					DuiLib::CLabelUI* pLabel = (DuiLib::CLabelUI*)pListCE->FindSubControl(_T("URLLabel"));
					ASSERT(pLabel);
					if (NULL == pLabel)
					{
						return false;
					}

					CString strURL = pLabel->GetText();
					// 新建窗口，设置当前网址
					theApp.pBrowser->NewView(strURL);
				}
				break;
			}
		case 2:	// “中间的————不需要做响应”
			break;
		case 3:	// 新建文件夹
			{
				CManageFavToolBarNewFolder* pNewFolder = new CManageFavToolBarNewFolder(_T("SideFavWnd//ManageFav_ToolBarNewFolder.xml"));
				if (pNewFolder == NULL)
				{
					break;
				}

				pNewFolder->Init(*this);
				pNewFolder->ShowWindow(TRUE);

				// 当树下没有结点直接返回
				if (m_pTreeCocalFav->GetCountChild() < 1)
				{
					break;
				}

				// 清空链表
				m_TreeSort.clear();
				// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
				AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

				// 添加结点到新建文件夹
				for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
				{
					pNewFolder->AddTreeSort(i->nIndex, i->strParentText, i->strText);
				}
				break;
			}
		case 4:	// 删除
			{
				if ("" != m_strTreeNodeText)
				{
					DuiLib::CTreeNodeUI* pNode = GetNode(m_strTreeNodeText);

					pNode->GetParentNode()->RemoveAt(pNode);
					m_strTreeNodeText = "";
				}
				 break;

			}
		case 5:	// 编辑
			{
				CManageFavToolBarEdiotrTree* pNewFav = new CManageFavToolBarEdiotrTree(_T("SideFavWnd//ManageFav_ToolBarEditorTree.xml"));
				if (pNewFav == NULL)
				{
					break;
				}

				pNewFav->Init(*this);
				pNewFav->SetFolderName(m_strTreeNodeText);
				pNewFav->ShowWindow(TRUE);

				// 当树下没有结点直接返回
				if (m_pTreeCocalFav->GetCountChild() < 1)
				{
					break;
				}

				// 清空链表
				m_TreeSort.clear();
				// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
				AddNodeToList(m_pTreeCocalFav->GetChildNode(0));

				// 添加结点到新建收藏
				for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
				{
					pNewFav->AddTreeSort(i->nIndex, i->strParentText, i->strText);
				}
				break;
			}
		default:
			break;
		}
	}
	// Tree编辑窗口添加按钮
	if (uMsg == FAV_EDIOTR_TREE_ADD)
	{
		// 获取文件夹名
		CString strName = *((CString*)wParam);
		// 获取下拉框
		CString strNewPos = *((CString*)lParam);
		// 通过名字获取新的父结点
		DuiLib::CTreeNodeUI* pPanrent;
		if (strNewPos != _T("本地收藏夹"))
		{
			pPanrent = GetNode(strNewPos);
		}
		else
		{
			pPanrent = m_pTreeCocalFav;
		}

		// 通过名字获取子节点
		DuiLib::CTreeNodeUI* pChild = GetNode(m_strTreeNodeText);
		// 检测父结点是否为当前结点的子结点
		if (NULL == GetChildNode(strNewPos, pChild))
		{
			// 获取子节点下结点数量
			int nChildCount = pChild->GetCountChild();
			// 如果子节点树下有结点
			if (nChildCount > 0)
			{
				// 清空树链表
				m_TreeSort.clear();
				// 添加所有子节点到列表
				AddNodeToList(pChild->GetChildNode(0));
				// 删除结点
				pChild->GetParentNode()->RemoveAt(pChild);
				// 添加这棵新的树结点
				AddTreeNode(m_strTreeNodeText, pPanrent);
				// 通过名字获取子节点
				DuiLib::CTreeNodeUI* pNewChild = GetNode(m_strTreeNodeText);
				// 从链表添加结点的子节点
				AddListToTree(pNewChild);
			}
			// 如果子节点没有结点
			else
			{
				// 删除结点
				pChild->GetParentNode()->RemoveAt(pChild);
				// 添加这棵新的树结点，既新的文件夹名
				AddTreeNode(m_strTreeNodeText, pPanrent);
			}
			// 通过名字获取子节点
			DuiLib::CTreeNodeUI* pNameNode = GetNode(m_strTreeNodeText);
			CStringW strWName = strName;
			pNameNode->SetItemText(strWName);
		}
	}
	return __super::HandleMessage(uMsg, wParam, lParam);
}

//void CManageFavWnd::WStringTOAsinString(CStringW strWide,CStringA& strout)
//{
//	int len = WideCharToMultiByte( CP_ACP,0,strWide.GetBuffer(),-1,NULL,0,NULL,NULL );  
//	if(len>0)
//	{
//		char* szbuf = new char[len+1];
//		memset(szbuf,0,len+1);
//		WideCharToMultiByte(CP_ACP,0,strWide.GetBuffer(),strWide.GetLength(),szbuf,len,NULL,NULL);
//		if(szbuf!=NULL)
//		{
//			strout=szbuf;
//			delete[] szbuf;
//			szbuf=NULL;
//		}
//	}
//}

void CManageFavWnd::CopyToClipboard(CStringA strData)
{
	if(OpenClipboard(NULL)) //首先打开一个剪切板，如果成功则返回非0值  
	{  
		HANDLE hClip;      //声明一个句柄  
		char *pBuf; 
		EmptyClipboard();  //置空这个剪切板，且得到剪切板的所有权  
		hClip=GlobalAlloc(GMEM_MOVEABLE,strData.GetLength()+1); 
		//申请锁定一块存放数据的内存区域  
		pBuf=(char*)GlobalLock(hClip);//得到指向内存区域的第一个字节指针  
		strcpy(pBuf,strData.GetBuffer());//将文本框的值拷贝到内存中  
		GlobalUnlock(hClip);//解除内存锁定  
		SetClipboardData(CF_TEXT,hClip);//设置数据到剪切板中  
		CloseClipboard();//关闭剪切板  
	} 
}
