#include "StdAfx.h"
#include "SidePanelUI.h"
#include "SideWndBase.h"

namespace DuilibEx
{
	CSidePanelUI::CSidePanelUI(void) : m_pHostWnd(NULL)
	{
	}

	CSidePanelUI::~CSidePanelUI(void)
	{
	}

	//************************************
	// Method:    Attach
	// FullName:  CSidePanelUI::Attach
	// Access:    public
	// Returns:   void
	// Qualifier:
	// Parameter: DuiLib::CWindowWnd* pHostWnd
	// Note:      绑定宿主窗口
	//************************************
	void CSidePanelUI::Attach(CSideWndBase* pHostWnd)
	{
		m_pHostWnd = pHostWnd;
	}

	//************************************
	// Method:    UnAttcah
	// FullName:  CSidePanelUI::UnAttcah
	// Access:    public
	// Returns:   void
	// Qualifier: Sam.Tan
	// Parameter: void
	// Note:      解绑宿主窗口
	//************************************
	void CSidePanelUI::UnAttcah()
	{
		m_pHostWnd = NULL;
	}

	CSideWndBase* CSidePanelUI::GetHostWindow()
	{
		return m_pHostWnd;
	}

	LPVOID CSidePanelUI::GetInterface(LPCTSTR pstrName)
	{
		if (_tcscmp(pstrName, _T("SidePanel")) == 0)
			return static_cast<CSidePanelUI*>(this);
		return __super::GetInterface(pstrName);
	}

	void CSidePanelUI::DoEvent(DuiLib::TEventUI& event)
	{
		if (event.Type == DuiLib::UIEVENT_BUTTONDOWN)
		{
			m_pManager->SendNotify(this, DUI_MSGTYPE_SELECTCHANGED, event.wParam, event.lParam);
			this->ApplyAttributeList(_T("bkcolor=\"FFFFFFFF\""));
		}
		DuiLib::CContainerUI::DoEvent(event);
	}

	void CSidePanelUI::SetVisible(bool bVisible)
	{
		CControlUI::SetVisible(bVisible);
		if( m_pHostWnd != NULL)
			::ShowWindow(*m_pHostWnd, IsVisible() ? SW_SHOW : SW_HIDE);
	}

	void CSidePanelUI::SetInternVisible(bool bVisible)
	{
		CControlUI::SetInternVisible(bVisible);
		if (m_pHostWnd != NULL)
		{
			::ShowWindow(*m_pHostWnd, IsVisible() ? SW_SHOW : SW_HIDE);
		}
	}

	void CSidePanelUI::SetPos(RECT rc)
	{
		CControlUI::SetPos(rc);
		if (m_pHostWnd != NULL)
		{
			::MoveWindow(*m_pHostWnd, m_rcItem.left, m_rcItem.top,
				m_rcItem.right - m_rcItem.left, m_rcItem.bottom - m_rcItem.top, TRUE);
		}
	}
}