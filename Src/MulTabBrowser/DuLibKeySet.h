#include "..\\DuiLib\\UIlib.h"
#include "KeySetListMgr.h"

class CDulibKeySetWnd : public DuiLib::CWindowWnd, public DuiLib::INotifyUI
{
public:
	CDulibKeySetWnd() ;
	LPCTSTR	GetWindowClassName()const;
	UINT	GetClassStyle() const ;
	void	OnFinalMessage(HWND /*hWnd*/) ;
	void	Notify(DuiLib::TNotifyUI& msg);
	LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
public:
	static CString ReSetStr(const CString&str);
	static int     GetPointNum(const CString&str);
	static CString	GetEachText(const CString&str, vector<CString>&strArray);
	
private:
	LRESULT	OnSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT	OnCreate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcActivate(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcCalcSize(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNcHitTest(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnGetMinMaxInfo(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnNCLButtonDblclick(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnSetState(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

public:
	void	ReloadKeyMess();
	void	InitChildWnd();
	void	OnClickAddKeyInfor();
	void	InitControl();
	int		GetCurrentKeyType();
	bool	OnAddKeySetInfor();
	bool	OnAddContinuesKeyInfor();
	bool	OnAddMouseClickInfor();
	bool	OnAddMouseLXClickInfor();
	void	ShowMessToChildDlg(const HOTKET_TYPE& type,const CString& Detils);
	void	SetCurrentPage(int nPage);

private:
	KeyMess	oldmess;
	bool	m_bMouseClickCapture ;
	bool	m_bMouseLXClickCapture;
	CString	strMouseLXText;
	DuiLib::CPaintManagerUI	m_PaintMsg;
	DuiLib::CListUI*	m_listKey;
	BOOL	m_addmess;
};

