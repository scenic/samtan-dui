#pragma once
class CLablHistoryListWnd : public DuiLib::WindowImplBase
{
public:
	explicit CLablHistoryListWnd(LPCTSTR pszXmlPath):m_strXmlPath(pszXmlPath) {}
	virtual LPCTSTR GetWindowClassName() const { return _T("CLablHistoryListWnd"); }
	virtual DuiLib::CDuiString GetSkinFolder() { return  _T(""); }
	virtual DuiLib::CDuiString GetSkinFile() { return m_strXmlPath; }
	virtual void OnFinalMessage(HWND /*hWnd*/) { delete this; }
	virtual void Notify(DuiLib::TNotifyUI &msg);
	virtual LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	void SetPos(HWND hWndParent, POINT ptPos);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	void ResizeClient(int cx /* = -1 */, int cy /* = -1 */);

protected:
	virtual ~CLablHistoryListWnd() {}
	DuiLib::CDuiString m_strXmlPath;
};