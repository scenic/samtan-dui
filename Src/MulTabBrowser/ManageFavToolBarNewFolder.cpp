#include "StdAfx.h"
#include "ManageFavToolBarNewFolder.h"
#include "ManageFavToolBarTree.h"
#include "../Include/UzDefine.h"

void CManageFavToolBarNewFolder::Init(HWND hWndParent)
{
	m_parentHwnd = hWndParent;
	Create(hWndParent, _T(""), UI_WNDSTYLE_FRAME, 0L, 0, 0, 800, 572);
	CenterWindow();
}


	LRESULT CManageFavToolBarNewFolder::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// 下拉框选中时
	if (uMsg == FAV_ITEM_TREE_CLICK)
	{
		// 设置下拉按钮文本
		CString strText = *((CString*)lParam);
		CStringW strWText = strText;
		m_PaintManager.FindControl(_T("Button_ToolBarNewPos"))->SetText(strWText);
	}
	return __super::HandleMessage(uMsg, wParam, lParam);
}

void CManageFavToolBarNewFolder::OnClick(DuiLib::TNotifyUI &msg )
{
	// 按下关闭按钮或者取消按钮时
	if (msg.pSender->GetName() == _T("ToolBarClosebtn") || msg.pSender->GetName() == _T("ToolBarCancel"))
	{
		Close();
	}
	// 按下添加按钮时
	else if (msg.pSender->GetName() == _T("ToolBarAdd"))
	{
		// 获取编辑框信息
		CString strFolderName = ((DuiLib::CEditUI*)m_PaintManager.FindControl(_T("ToolBarFolderName")))->GetText();
		// 获取下拉按钮文本信息
		CString strNewPos = ((DuiLib::CButtonUI*)m_PaintManager.FindControl(_T("Button_ToolBarNewPos")))->GetText();
		// 发送添加消息
		::SendMessage(m_parentHwnd, FAV_NEW_FOLDER_ADD, (WPARAM)(&strFolderName),(LPARAM)(&strNewPos));
		Close();
	}
	// 下拉按钮
	else if (msg.pSender->GetName() == _T("Button_ToolBarNewPos"))
	{
		RECT ItemRect = ((DuiLib::CButtonUI *)msg.pSender)->GetPos();
		POINT pt = {ItemRect.left, ItemRect.bottom};

		// 创建树控件为下拉框
		CManageFavToolBarTree* m_ToolBarTree = new CManageFavToolBarTree(_T("SideFavWnd//ManageFav_ToolBarTree.xml"));
		if (m_ToolBarTree == NULL)
		{
			return ;
		}
		m_ToolBarTree->Init(*this, pt, ItemRect.right - ItemRect.left);
		// 添加树列表
		AddListToTree(m_ToolBarTree, m_ToolBarTree->GetCocalFavNode());		
		m_ToolBarTree->ShowWindow(TRUE);
	}
}

// 添加树链表
void CManageFavToolBarNewFolder::AddTreeSort(int nIndex, CString strParentText, CString strText)
{
	TREESORT TreeSort;
	TreeSort.nIndex = nIndex;
	TreeSort.strParentText = strParentText;
	TreeSort.strText = strText;

	// 将结点添加到List头
	m_TreeSort.push_back(TreeSort);
}

// 从链表添加结点
bool CManageFavToolBarNewFolder::AddListToTree(CManageFavToolBarTree* ToolBarTree, DuiLib::CTreeNodeUI* parent)
{
	CString strParentText = parent->GetItemText();
	// 遍历结构体,添加第一层子结点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText == i->strParentText)
		{
			ToolBarTree->AddTreeNode(i->strText, parent, i->nIndex);
		}
	}
	// 分两次添加的原因是避免因为交换链表结点导致的子节点的父节点未被创建导致的添加失败问题
	// 遍历树结构，添加后面层的子节点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText != i->strParentText)
		{
			ToolBarTree->AddTreeNode(i->strText, ToolBarTree->GetNode(i->strParentText), i->nIndex);
		}
	}
	return true;
}