#pragma once
#include <vector>
#include "ScriptRecordDao.h"

//////////////////////////////////////////////////////////////////////////
// CScriptRecordMgr 脚本录制管理文件

class CScriptRecordMgr
{
public:
	CScriptRecordMgr(void) {}
	void StartRecord(HWND hWndParent);    // 开始录制
	void StopRecord(void);                // 停止录制
	void StartPlay(LPCTSTR lpszFileName); // 开始播放
	void StopPlay(LPCTSTR lpszFileName);  // 停止播放
	friend LRESULT CALLBACK MouseProc(int nCode, WPARAM wParam, LPARAM lParam);
	~CScriptRecordMgr(void);

protected:
	static HHOOK m_hHook;                 // 钩子句柄
	static vector<EVENTMSG> m_evenArr;    // 事件链表
	CScriptRecordDao m_dao;               // I/O 操作类
};