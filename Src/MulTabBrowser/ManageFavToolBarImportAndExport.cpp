#include "StdAfx.h"
#include "ManageFavToolBarImportAndExport.h"

void CManageFavToolBarImportAndExport::Init(HWND hWndParent)
{
	Create(hWndParent, _T(""), UI_WNDSTYLE_FRAME, 0L, 0, 0, 800, 572);

	CenterWindow();
}

LRESULT CManageFavToolBarImportAndExport::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{


	return __super::HandleMessage(uMsg, wParam, lParam);
}

void CManageFavToolBarImportAndExport::OnClick(DuiLib::TNotifyUI &msg )
{
	if (msg.pSender->GetName() == _T("ToolBarClosebtn") || msg.pSender->GetName() == _T("ToolBarCancel"))
	{
		Close();
		//PostQuitMessage(0);
	}
}