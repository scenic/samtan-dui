#pragma once
typedef struct _ViewKey
{
	int  nID;
	bool m_used;
}ViewKey;

#include <vector>
#include "KeySetListMgr.h"

class CInterViewKeyMess
{
public:
	CInterViewKeyMess(void);
	~CInterViewKeyMess(void);
	
	void   InitKeySet();
	void   SetCurrentKey(int key,bool used);
	bool   IsKeyUsed(int key);
	bool   FindKey(int key);
	void   OnViewChange();
private:
	std::vector<ViewKey> m_KeyMess;
};

