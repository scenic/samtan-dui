#include "StdAfx.h"
#include "ManageFavXML.h"
#include "GlobalFunc.h"

#define ELEMENT_TYPE_FOLDER	1	// Item类型为文件夹类型
#define ELEMENT_TYPE_LINK	2	// Item类型为网址连接类型
CManageFavXML::CManageFavXML(void)
{
	m_FavToolFunc = new CFavToolFunc;

	CString szPath = GetUzFavPath();
	if (!PathFileExists(szPath))
		CreateDirectory(szPath, NULL);
	m_strXMLPath = GetUzDefaultXmlFavPath();
	if(!IsXmlFileExist(m_strXMLPath))
	{
		CreateXmlFile();
	}
}

CManageFavXML::~CManageFavXML(void)
{
	if (NULL != m_FavToolFunc)
	{
		delete m_FavToolFunc;
		m_FavToolFunc = NULL;
	}
}

// 添加一个收藏夹
bool CManageFavXML::AddFolder(CString& strFolderName, CString& strParentName)
{
	char* pFolderName = m_FavToolFunc->CStringToChar(strFolderName);
	//char* pParentName = m_FavToolFunc->CStringToChar(strParentName);
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	if (NULL != root)
	{
		// 获取父结点
		tinyxml2::XMLElement* pParentItem = FindItem(root, strParentName, ELEMENT_TYPE_FOLDER);
		if (NULL != pParentItem)
		{
			tinyxml2::XMLElement* pGoToUrl = doc.NewElement("UzFavItem");
			pGoToUrl->SetAttribute("ItemName", pFolderName);
			pGoToUrl->SetAttribute("ItemLink", "");
			pGoToUrl->SetAttribute("ItemType", "ItemDir");
			pParentItem->LinkEndChild(pGoToUrl);

			doc.SaveFile(GetXmlFileName());
			return true;
		}
	}
	return false;
}

// 添加一个网址连接, strTitle为标题，strURL为网址， strParentName为收藏夹父结点名
bool CManageFavXML::AddLink(CString& strTitle, CString& strURL, CString strParentName)
{
	char* pTitle = m_FavToolFunc->CStringToChar(strTitle);
	char* pURL = m_FavToolFunc->CStringToChar(strURL);
	//char* pParentName = m_FavToolFunc->CStringToChar(strParentName);
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	if (NULL != root)
	{
		// 获取父结点
		tinyxml2::XMLElement* pParentItem = FindItem(root, strParentName, ELEMENT_TYPE_FOLDER);
		if (NULL != pParentItem)
		{
			tinyxml2::XMLElement* pGoToUrl = doc.NewElement("UzFavItem");
			pGoToUrl->SetAttribute("ItemName", pTitle);
			pGoToUrl->SetAttribute("ItemLink", pURL);
			pGoToUrl->SetAttribute("ItemType", "ItemLink");
			pParentItem->LinkEndChild(pGoToUrl);

			doc.SaveFile(GetXmlFileName());
			return true;
		}
	}
	return false;
}

// 删除一个收藏夹, strFolderName为收藏夹名，strParentName为收藏夹父结点名
bool CManageFavXML::DelFolder(CString& strFolderName)
{
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	if (NULL != root)
	{
		// 获取父结点
		tinyxml2::XMLElement* pDelItem = FindItem(root, strFolderName, ELEMENT_TYPE_FOLDER);
		if (NULL != pDelItem)
		{
			doc.DeleteNode(pDelItem);
			doc.SaveFile(GetXmlFileName());
			return true;
		}
	}
	return false;

}

// 删除一个网址连接， strTitle为标题,  strParentName为父结点收藏夹名
bool CManageFavXML::DelLink(CString& strTitle, CString& strParentName)
{
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	if (NULL != root)
	{
		// 获取父结点
		tinyxml2::XMLElement* pParentItem = FindItem(root, strParentName, ELEMENT_TYPE_FOLDER);
		if (NULL != pParentItem)
		{
			// 获取子结点
			tinyxml2::XMLElement* pChildItem = FindItem(root, strTitle, ELEMENT_TYPE_LINK);

			pParentItem->DeleteChild(pChildItem);
			doc.SaveFile(GetXmlFileName());
			return true;
		}
	}
	return false;

}

// 删除全部收藏
bool CManageFavXML::DelFav()
{
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	if (NULL != root)
	{
		// 获取本地收藏夹节点
		tinyxml2::XMLElement* pUzFavItem = root->FirstChildElement("UzFavItem");
		// 本地收藏夹下的子结点
		tinyxml2::XMLElement* pChild = pUzFavItem->FirstChildElement("UzFavItem");
		while (NULL != pChild)
		{
			tinyxml2::XMLElement* p = pChild;
			pChild = pChild->NextSiblingElement();
			doc.DeleteNode(p);
		}
		doc.SaveFile(GetXmlFileName());
		return true;
	}
	return false;
}
// 获取XML全部信息
bool CManageFavXML::GetAllXML(std::list<TREESORT>& TreeFolder, std::list<LIST_LINK>& ListLink)
{
	tinyxml2::XMLDocument doc;
	if(doc.LoadFile(GetXmlFileName())!=tinyxml2::XML_SUCCESS)return false;
	tinyxml2::XMLElement* root = doc.RootElement()->ToElement();

	return GetAllXML(TreeFolder, ListLink, root);
}

// 获取XML全部信息
bool CManageFavXML::GetAllXML(std::list<TREESORT>& TreeFolder, std::list<LIST_LINK>& ListLink, tinyxml2::XMLElement* root)
{
	bool bResult = false;
	// 获取子节点
	tinyxml2::XMLElement* pUzFavItem = root->FirstChildElement("UzFavItem");
	while (NULL != pUzFavItem)
	{
		// 当子节点还有子节点时，调用递归
		if (NULL != pUzFavItem->FirstChildElement("UzFavItem"))
		{
			bResult = GetAllXML(TreeFolder, ListLink, pUzFavItem);
		}

		// 获取数据
		const char* pItemName = pUzFavItem->Attribute("ItemName");
		const char* pItemLink = pUzFavItem->Attribute("ItemLink");
		const char* pParentName = root->Attribute("ItemName");
		const char* pType =	pUzFavItem->Attribute("ItemType");
		// 当为文件夹类型时
		if (0 == strcmp(pType, "ItemDir"))
		{
			TREESORT Data;
			Data.strParentText = pParentName;
			Data.strText = pItemName;
			TreeFolder.push_front(Data);
			bResult = true;
		}
		// 当为连接地址类型时
		if (0 == strcmp(pType, "ItemLink"))
		{
			LIST_LINK DataList;
			DataList.strParentFolderName = pParentName;
			DataList.strTitle = pItemName;
			DataList.strURL = pItemLink;
			ListLink.push_back(DataList);
			bResult = true;
		}

		pUzFavItem = pUzFavItem->NextSiblingElement();
	}

	return bResult;
}

// 查找指定Item，root为父结点，strName为结点名
tinyxml2::XMLElement* CManageFavXML::FindItem(tinyxml2::XMLElement* root, CString& strName, int nType)
{
	// CString转Char*
	char* pName = m_FavToolFunc->CStringToChar(strName);
	// 用来保存查找到的Item
	tinyxml2::XMLElement* pResult = NULL;
	// 获取子节点
	tinyxml2::XMLElement* pUzFavItem = root->FirstChildElement("UzFavItem");
	while (NULL != pUzFavItem)
	{
		// 当子节点还有子节点时，调用递归
		if (NULL != pUzFavItem->FirstChildElement("UzFavItem"))
		{
			pResult = FindItem(pUzFavItem, strName, nType);
		}

		if (NULL != pResult)
		{
			break;
		}
		// 判断名字是否为所查名字
		const char* pItemName = pUzFavItem->Attribute("ItemName");
		const char* pType =	pUzFavItem->Attribute("ItemType");
		
		// 当为文件夹类型时
		if (nType == ELEMENT_TYPE_FOLDER)
		{
			if ((0 == strcmp(pName, pItemName)) && (0 == strcmp(pType, "ItemDir")))
			{
				pResult = pUzFavItem;
				return pResult;
			}
		}
		// 当为连接地址类型时
		if (nType == ELEMENT_TYPE_LINK)
		{
			if ((0 == strcmp(pName, pItemName)) && (0 == strcmp(pType, "ItemLink")))
			{
				pResult = pUzFavItem;
				return pResult;
			}
		}

		pUzFavItem = pUzFavItem->NextSiblingElement();
	}
	return pResult;
}

// 创建XML
void CManageFavXML::CreateXmlFile()
{
	tinyxml2::XMLDocument doc;
	doc.LoadFile(GetXmlFileName());
	tinyxml2::XMLDeclaration * decl =doc.NewDeclaration("xml version=\"1.0\" encoding=\"GBK\"");
	doc.LinkEndChild(decl);
	tinyxml2::XMLElement* root = doc.NewElement("UzFav");
	root->SetAttribute("author","uz");
	doc.LinkEndChild(root);
	tinyxml2::XMLElement* pGoToUrl = doc.NewElement("UzFavItem");
	pGoToUrl->SetAttribute("ItemName", "本地收藏夹");
	pGoToUrl->SetAttribute("ItemType", "ItemDir");
	root->LinkEndChild(pGoToUrl);
	doc.SaveFile(GetXmlFileName());
}

// 判断XML文件是否存在
BOOL CManageFavXML::IsXmlFileExist(const CString& strFile)
{
	if(strFile== L"")   
		return   FALSE;   
	BOOL   bExist   =   TRUE;   
	HANDLE   hFind;   
	WIN32_FIND_DATA   dataFind;   
	hFind   =   FindFirstFile(strFile,&dataFind);   
	if(hFind==   INVALID_HANDLE_VALUE)   
	{   
		bExist   =   FALSE;   
	}   
	FindClose(hFind);   
	return   bExist; 
}

// 获取XML文件名
CStringA CManageFavXML::GetXmlFileName()
{
	CStringA strFileName = "";
	m_FavToolFunc->WStringTOAsinString(m_strXMLPath,strFileName);
	return strFileName;
}
