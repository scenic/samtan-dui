#pragma once

namespace DuilibEx
{
	class CWrapPanelUI : public DuiLib::CHorizontalLayoutUI
	{
	public:
		CWrapPanelUI(void);
		~CWrapPanelUI(void);

		LPCTSTR	GetClass() const { return _T("WrapPanelUI"); }
		LPVOID	GetInterface(LPCTSTR pstrName);

		void	DoEvent(DuiLib::TEventUI& event);

		void	SetPos(RECT rc);
		void	DoPaint(HDC hDC, const RECT& rcPaint);
		bool	Add(CControlUI* pControl);
		bool	Remove(CControlUI* pControl);

	private:
		int		m_nLineNum;			// 容器行数
		int		m_nLineHeight;		// 一行高度
		int		m_nMaxCount;	// 一行容量
	};

}