/*!@file
*******************************************************************************************************
<PRE>
模块名		："Tree"界面类
文件名		：CManageFavToolBarTree
相关文件		：CManageFavToolBarTree.cpp/Duilib相关库
文件实现功能：实现管理收藏夹新建文件夹的下拉框树界面
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了Duilib库
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/5/31 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
#include "stdafx.h"
#include <atlstr.h>

class CManageFavToolBarTree : public DuiLib::WindowImplBase
{
protected:
	virtual ~CManageFavToolBarTree(){};        // 私有化析构函数，这样此对象只能通过new来生成，而不能直接定义变量。就保证了delete this不会出错
	DuiLib::CDuiString  m_strXMLPath;

public:
	explicit CManageFavToolBarTree(LPCTSTR pszXMLPath): m_strXMLPath(pszXMLPath){}
	virtual LPCTSTR    GetWindowClassName()const{ return _T("CManageFavToolBarTree");}
	virtual DuiLib::CDuiString GetSkinFolder()          { return _T("");}
	virtual DuiLib::CDuiString GetSkinFile()            { return m_strXMLPath;      }
	virtual void       OnFinalMessage(HWND /*hWnd*/){ delete this;              }

	virtual LRESULT OnKillFocus(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

	void Init(HWND hWndParent, POINT ptPos, int nWidth);

	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);

	virtual void Notify(DuiLib::TNotifyUI& msg);

public:
	// 添加一行树strText为树的Text,parent为父节点默认为本地收藏夹，nIndex为-1时表示在父节点最后添加
	bool AddTreeNode(CString& strText, DuiLib::CTreeNodeUI* parent = NULL, int nIndex = -1);
	// 获取本地收藏夹结点
	DuiLib::CTreeNodeUI* GetCocalFavNode(){return m_pTreeCocalFav;}
	// 通过名字获取本地收藏夹下的子结点，返回为NULL时表示未找到
	DuiLib::CTreeNodeUI* GetNode(CString& strText);

private:
	// 添加"本地收藏夹"树结点
	bool AddLocalFavNode();
	// 通过名字获取指定父结点下的子节点，返回为NULL时表示未找到
	DuiLib::CTreeNodeUI* GetChildNode(CString& strText, DuiLib::CTreeNodeUI* parent);


private:
	HWND m_ParentWnd;					// 父窗口
	DuiLib::CTreeViewUI*	m_pTreeView;		// 树控件
	DuiLib::CTreeNodeUI*	m_pTreeCocalFav;	// 树控件的本地收藏夹结点
};


