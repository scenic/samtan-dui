#pragma once
#include "LabelItemUI.h"
#include "SidePanelUI.h"
#include "WrapPanelUI.h"

class CDialogBuilderCallbackEx : public DuiLib::IDialogBuilderCallback
{
public:
	DuiLib::CControlUI* CreateControl(LPCTSTR pstrClass) 
	{
		if( _tcscmp(pstrClass, _T("LabelItem")) == 0 )
			return new DuilibEx::CLabelItemUI;

		if( _tcscmp(pstrClass, _T("WrapPanel")) == 0 )
			return new DuilibEx::CWrapPanelUI;

		if (_tcscmp(pstrClass, _T("SidePanel")) == 0)
			return new DuilibEx::CSidePanelUI;

		return NULL;
	}
};