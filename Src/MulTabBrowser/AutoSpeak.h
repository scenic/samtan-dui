#pragma once
class CAutoSpeak
{
public:
	CAutoSpeak(void);
	CAutoSpeak(HWND h);
	~CAutoSpeak(void);

	

	CString GetLink() const;
	CString GetTitle() const;
	CString GetIconPath() const;
	BOOL    IsEnable() const;
	BOOL    IsSet() const;
	BOOL    IsInRecycle() const;
	CPoint  GetSpeakEditPt() const;
	CPoint  GetSpeakButtonPt() const;

	void	SetInRecycle(BOOL b);

	void    SetLink(const CString& str);
	void    SetTitle(const CString& strTitle);
	void    SetIconPath(const CString& str);
	void    SetSet(const bool b);
	void    SetEnable(const bool b);
	void    SetSpeakWnd(HWND h);
	HWND    GetSpeakWnd() const;
	void    SetSpeakEditPt(const CPoint& pt);
	void    SetSpeakButtonPt(const CPoint& pt);
	CString GetLabelWndStr();
	CString GetSpeakWndStr();

	BOOL	IsSpeakTIme();

	void	 NextSpeakOrder();

	CStringA GetSpeakString();
	CString	 GetContent1() const {return m_strContent1;}
	CString	 GetContent2() const {return m_strContent2;}
	CString  GetContent3() const {return m_strContent3;}
	void	SetContent1(CString str) {m_strContent1 = str;}
	void	SetContent2(CString str) {m_strContent2 = str;}
	void	SetContent3(CString str) {m_strContent3 = str;}
	int		GetIntervalTime() const { return m_nIntervalTime; }
	void	SetIntervalTime(int val) {m_nIntervalTime = val;}
	HWND	GetLabelWnd() const {return m_hLabelWnd;};
	void	SetLabelWnd(HWND h) {m_hLabelWnd=h;}
	void	SetStartTime(DWORD dw) {m_dwStartTime = dw;}
	DWORD	GetStartTime() const {return m_dwStartTime;}
	BOOL	GetAddRandString()const {return m_bAddRandString;}
	void	SetAddRandString(bool b) {m_bAddRandString = b;}
	BOOL	IsSel() const {return m_bSel;}
	void	SetSel(BOOL b) {m_bSel = b;}

	
private:
	CStringA GenerateRandString(int len);
	CString m_strLink;
	CString m_strTitle;
	CString m_strIconPath;
	CString m_strContent1;
	CString m_strContent2;
	CString m_strContent3;
	BOOL    m_bSet;
	BOOL    m_bEnable;
	BOOL    m_bInRecycle;
	BOOL    m_bSel;
	HWND    m_hSpeakWnd;
	HWND    m_hLabelWnd;
	int     m_nIntervalTime;
	CPoint  m_ptSpeakEdit;
	CPoint  m_ptSpeakButton;
	DWORD   m_dwStartTime;
	DWORD   m_dwCurrentSpeakOrder;
	BOOL    m_bAddRandString;
};

