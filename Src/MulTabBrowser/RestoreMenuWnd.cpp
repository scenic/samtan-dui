#include "StdAfx.h"
#include "RestoreMenuWnd.h"
#include "MulTabBrowser.h"
#include "BrowserWnd.h"
CRestoreMenuWnd::CRestoreMenuWnd(void)
{
}


CRestoreMenuWnd::~CRestoreMenuWnd(void)
{
}


LPCTSTR CRestoreMenuWnd::GetWindowClassName()const
{
	return _T("RestoreMenuWnd");
}

DuiLib::CDuiString CRestoreMenuWnd::GetSkinFile()
{
	return _T("Browser//RestoreMenu.xml");
}

DuiLib::CDuiString CRestoreMenuWnd::GetSkinFolder()
{
	return _T("");

}

void CRestoreMenuWnd::InitWindow()
{
	m_pList = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("DropDownList")));

	restoreHistory=((CMulTabBrowserApp*)AfxGetApp())->GetHisotry()->LoadRestoreLbl();
	std::list<CUzHistoryObj>::iterator it=restoreHistory.begin();
	while(it!=restoreHistory.end())
	{
		DuiLib::CDialogBuilder builder;
		DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("Browser//RestoreElement.xml"), (UINT)0, NULL, &m_PaintManager);  
		DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
		pTitle->SetText(it->GetUrlTitle());
		DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("Item_icon"));
		pIcon->SetBkImage(_T("Browser//res//sina.png"));
		m_pList->Add(pRoot);
		it++;
	}
	if(restoreHistory.empty())
	{
		DuiLib::CDialogBuilder builder;
		DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("Browser//RestoreElement.xml"), (UINT)0, NULL, &m_PaintManager);  
		DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
		pTitle->SetText(_T("无记录"));
		m_pList->Add(pRoot);
	}
	DuiLib::CDialogBuilder builder;
	DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("Browser//RestoreElement.xml"), (UINT)0, NULL, &m_PaintManager);  
	DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
	pTitle->SetText(_T("清空此列表"));
	m_pList->Add(pRoot);
	int height=m_pList->GetCount()*25+20;

	int cx= GetSystemMetrics(SM_CXFULLSCREEN);
	int cy= GetSystemMetrics(SM_CYFULLSCREEN);
	int posx=point.x;
	int posy=point.y;
	if(point.x+450>=cx)
	{
		posx=cx-500;
	}
	if(point.y+height>=cy)
	{
		posy=posy-height;
	}
	::SetWindowPos(*this,NULL,posx,posy,500,height,SWP_SHOWWINDOW);
}

void CRestoreMenuWnd::Notify(DuiLib::TNotifyUI& msg)
{
	if(msg.sType==DUI_MSGTYPE_ITEMCLICK)
	{
		int nSel = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();

		if(nSel==m_pList->GetCount()-1)
		{
			((CMulTabBrowserApp*)AfxGetApp())->GetHisotry()->DeleteRestore();
			Close();
		}
		else
		{
			DuiLib::CListContainerElementUI* pElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(nSel);
			DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_title"));
			CString strTitle=pTitle->GetText();
			std::list<CUzHistoryObj>::iterator it=restoreHistory.begin();
			while(it!=restoreHistory.end())
			{
				if(it->GetUrlTitle().Left(6)==strTitle.Left(6))
				{
					((CBrowserWnd*)theApp.pBrowser)->NewView(it->GetUrlLink());
					((CMulTabBrowserApp*)AfxGetApp())->GetHisotry()->DeleteRestore(it->GetUrlLink());
					Close();
					return;
				}
				it++;
			}
		}
	}
	
}

void CRestoreMenuWnd::Init(POINT pt)
{
	::ClientToScreen(theApp.pBrowser->GetHWND(),&pt);
	point=pt;
	Create(NULL, _T(""), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	
	
}

 LRESULT CRestoreMenuWnd::OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
 {
	 Close();
	 bHandled=FALSE;
	 return 0;
 }