#pragma once
#include <list>
#include <vector>
#include "HotKeyMgr.h"
#define USE_DUI
class CDulibKeySetWnd;
enum HOTKET_TYPE
{
	TYPE_NULL=-1,
	TYPE_LXKEY=0,
	TYPE_SETKEY,//改键
	TYPE_MOUSEPRESS,//鼠标单击
	TYPE_MOUSELXPRESS,//鼠标连点
	TYPE_STOP        //终止按键
};
typedef struct _KeyMess
{
	int     nID;        //启动按键的键值
	bool    used;       //是否被启用
	CString	detils;	     // 用来存储长串的字符串,按键执行的具体内容
	HOTKET_TYPE type;    //按键的类型
	// 结构体的构造函数
	_KeyMess()
	{
		nID=-1;
		used=false;
		detils=_T("");
		type=TYPE_NULL;
	}

	// 结构体的构造函数
	~_KeyMess()
	{
	}
	bool operator==(const _KeyMess& mess) // 操作运算符重载
	{
		return( nID == mess.nID) && (used == mess.used) && (detils==mess.detils) &&(type==mess.type);
	}

}KeyMess;


class CInterView;
class CKeySetListMgr
{

public:
	CKeySetListMgr(void);
	~CKeySetListMgr(void);
	static CKeySetListMgr& Instance();
	static const TCHAR* KeyValue(int nKey);         //根据按键键值查询内容
	static  int    GetKeyValueByStr(CString str);
	static  bool CheckString(const CString& str);
	static  void GetEachItemText(const CString &str,std::vector<CString>& strArray);
	bool    ISGameView(HWND& hCurGameWnd);
	std::vector<KeyMess> m_KeyList;		


public:
	//List操作
	BOOL	AddKey(KeyMess m_keymess);
	BOOL    InsertKey(int pos,KeyMess m_keymsss);
	BOOL	FindKey(int nID);
	int     GetKeyPos(int nID);
	void    EnableKey(bool flag);
	BOOL    RemoveKey(int nID);
	BOOL    IsKeyUsed(int nID);
	BOOL    IsKeyUsed(int nID,int endKey);
	void	RemoveAllKeys();
	KeyMess FindKeyMess(int nID);
	BOOL     EditKey(int nID,const KeyMess& m_mess);
	void    SetKeyUsed(int nID,bool used);       //设置按键是否启用
	
	CString  m_strIniFile;                 //配置文件
	CString  GetLastItem(CString str);  //获取字符串最后一个逗号后面的文本
	KeyMess  GetLastKey();
	unsigned int GetCount();
// 关于Hook的外用API
	//将设置的键添加至hook
	bool	AddHookKey(); 
	// 卸载Hook
	bool	DelHookKey();
	// 获取钩子状态
	bool	GetHookState();

//文件操作
	void	SaveKey(KeyMess m_keymess);         //保存至配置文件
	void    ReadKeyToList();
	void    DelKey(int nID);                 //删除配置文件中的节
	BOOL   SetKeyValue(int nID,KeyMess m_keymess);
	static  BOOL    IsXmlFileExist(const CString& strFile);
	void    CreateXmlFile();
//界面
	void ShowKeyListView(DuiLib::CWindowWnd* pParentWnd);
	void ChangeLableState();
	CDulibKeySetWnd*GetKeyListView();
	void DestroyKeySetWindow();
	

//VIEW管理
	static  bool IsCurrentViewKeyUsed(int key);
	static  void SetCurrentViewKey(int key,bool used);
private:
	CDulibKeySetWnd* pFrame;
private:
	HMODULE			m_hChangeKey;						// ChangeKey.DLL接口指针
	bool			m_HookState;		// 设置钩子状态
};

