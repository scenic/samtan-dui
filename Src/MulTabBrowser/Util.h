#pragma once


#include <vector>

namespace EHXLib
{
	class CUtil
	{
	public:
		CUtil(void);
		~CUtil(void);

		static CString GetHistroyDir();
		static void OpenNewWindowPage(CString strTitle,CString strUrl);
		static CString GetFileName(CString strFile);
		static CString GetFileExt(CString strFile);
		static CString GetSkinDirPath();
		static  CString GetIconName(CString strUrl);
		static BOOL IsFileExist(CString strFile);
		static BOOL IsUrl(CString str);	
		static CString FormatUTF8Str(CString strSource);
		static BOOL IsDigitString( CString str );
		static CStringA GenerateRandString(int len);
		static BOOL IsMatched(const CString& strSrc,const CString& strDesc);
		
	};
}
using namespace EHXLib;
