#include "stdAfx.h"
#include "GlobalFunc.h"
#include <wincrypt.h>
#include <tlhelp32.h>
#include <OLEACC.H>
#include <vdmdbg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _GetDlgCtrlID(hWnd)          ((UINT)(WORD)::GetDlgCtrlID(hWnd))


/************************************************************************/
/* 得到当前exe文件的上一级路径                                          */
/************************************************************************/

CString GetExePath()
{
	CString strFileName;
	TCHAR szFilePath[MAX_PATH];
	
	GetModuleFileName(NULL, szFilePath, MAX_PATH);

	strFileName = szFilePath;
	// Get  exe directory
	int i = strFileName.ReverseFind('\\');
	strFileName = strFileName.Left(i);

	return strFileName;
}


CString GetSysDataPath()
{
	CString strDir (_T("D:\\YzGame"));

	if (!PathFileExists(strDir))
		CreateDirectory(strDir, NULL);

	strDir += _T("\\SysData");
	if (!PathFileExists(strDir))
		CreateDirectory(strDir, NULL);

	return strDir;
}

CString GetUzFavPath()
{
	CString strDir (_T("D:\\YzGame"));

	if (!PathFileExists(strDir))
		CreateDirectory(strDir, NULL);

	strDir += _T("\\uzfav");
	if (!PathFileExists(strDir))
		CreateDirectory(strDir, NULL);

	return strDir;
}

CString GetUzDefaultXmlFavPath()
{
	return GetUzFavPath() + L"\\defaultfav.xml";
}

CString GetPathName(CString Filename )
{
	CString strFileName;
	strFileName = Filename;
	// Get  exe directory
	int i = strFileName.ReverseFind('\\');
	strFileName = strFileName.Left(i);

	return strFileName;
}
/************************************************************************/
/*   实现API FindWindow相同的功能,这里是为了便于调试                    */
/************************************************************************/

HWND FindChildWindow(HWND hParentWnd,LPCTSTR lpClassName)
{
	HWND hChildwnd = ::GetWindow(hParentWnd,GW_CHILD);
	TCHAR szClassName[MAX_CLASSNAME]={0};
	while(hChildwnd)
	{
		::GetClassName(hChildwnd,szClassName,MAX_CLASSNAME);
		if(lstrcmp(szClassName,lpClassName)==0)
			return hChildwnd;
		hChildwnd = ::GetWindow(hChildwnd,GW_HWNDNEXT);
	}
	return NULL;
}

/************************************************************************/
/*   查找一个当前激活的窗口												*/
/************************************************************************/

HWND FindActiveWindow(HWND hParentWnd,LPCTSTR lpClassName)
{
	HWND hChildwnd = ::GetWindow(hParentWnd,GW_CHILD);
	TCHAR szClassName[MAX_CLASSNAME]={0};
	while(hChildwnd)
	{
		::GetClassName(hChildwnd,szClassName,MAX_CLASSNAME);
		if(lstrcmp(szClassName,lpClassName)==0)
		{
			if (::IsWindowVisible(hChildwnd))
			{
				return hChildwnd;
			}
		}
		hChildwnd = ::GetWindow(hChildwnd,GW_HWNDNEXT);
	}
	return NULL;
}

/************************************************************************/
/*  此函数进行多级查找相应的后代窗口                                    */
/************************************************************************/

HWND FindWindowEx2(HWND hParentWnd,LPCTSTR lpClassName,LPCTSTR lpWindowName)
{
	//	HWND hWnd=FindWindowEx(hParentWnd,NULL,lpClassName,lpWindowName);
	HWND hWnd=FindChildWindow(hParentWnd,lpClassName);
	if(hWnd!=NULL)
		return hWnd;
	HWND hChildHwnd = ::GetWindow(hParentWnd,GW_CHILD);
	while(hChildHwnd)
	{
		hWnd=FindWindowEx2(hChildHwnd,lpClassName,lpWindowName);
		if(hWnd!=NULL)
			return hWnd;
		hChildHwnd = ::GetWindow(hChildHwnd,GW_HWNDNEXT);
	}	
	return NULL;
}

/************************************************************************/
/*  此函数进行多级查找相应的后代窗口                                   */
/************************************************************************/

HWND FindActiveWindowEx2(HWND hParentWnd,LPCTSTR lpClassName,LPCTSTR lpWindowName)
{
	//	HWND hWnd=FindWindowEx(hParentWnd,NULL,lpClassName,lpWindowName);
	HWND hWnd=FindActiveWindow(hParentWnd,lpClassName);
	if(hWnd!=NULL)
		return hWnd;
	HWND hChildHwnd = ::GetWindow(hParentWnd,GW_CHILD);
	while(hChildHwnd)
	{
		hWnd=FindActiveWindowEx2(hChildHwnd,lpClassName,lpWindowName);
		if(hWnd!=NULL)
			return hWnd;
		hChildHwnd = ::GetWindow(hChildHwnd,GW_HWNDNEXT);
	}	
	return NULL;
}

/************************************************************************/
/*  是否被其他窗口覆盖,TOPMOST窗口除外                                  */
/************************************************************************/

BOOL  IsHaveWndOn(HWND hWnd)
{
	ASSERT(hWnd != NULL);

	hWnd = GetAncestor(hWnd,GA_ROOT);
	CRect rectWnd;
	CRect rectInter;
	::GetWindowRect(hWnd, rectWnd);
	// check child windows
	HWND hWndPrev = ::GetWindow(hWnd, GW_HWNDPREV);
	for (; hWndPrev != NULL; hWndPrev = ::GetWindow(hWndPrev, GW_HWNDPREV))
	{
		if ((::GetWindowLong(hWndPrev, GWL_STYLE) & WS_VISIBLE)
			&&(!(::GetWindowLong(hWndPrev, GWL_EXSTYLE) & WS_EX_TOPMOST)))
		{
			// see if point hits the child window
			CRect rectPrev;
			::GetWindowRect(hWndPrev, rectPrev);
			if (rectInter.IntersectRect(rectWnd,rectPrev))
			{
				return TRUE;
			}
		}
	}

	return FALSE;    // not found
} 

BOOL _StretchImage (CDC* pDC, const CRect& rectClient, CImage& image)
{
	ASSERT_VALID (pDC);

	if ((HBITMAP)image != NULL)
	{
		CDC dc;
		dc.CreateCompatibleDC (pDC);

		// Select the bitmap into the in-memory DC

		HBITMAP hBmpOld = (HBITMAP)dc.SelectObject((HBITMAP)image);

		int nOldStretchBltMode = pDC->SetStretchBltMode (COLORONCOLOR);

		pDC->StretchBlt(rectClient.left, rectClient.top, rectClient.Width (), rectClient.Height (), &dc, 
			0, 0, image.GetWidth(), image.GetHeight(), SRCCOPY);

		dc.SelectObject (hBmpOld);

		pDC->SetStretchBltMode (nOldStretchBltMode);

		return true;
	}

	return false;
}

BOOL _Stretch_ImageList (CDC* pDC, const CRect& rectClient, CImageList& ImageList, int iIndex)
{
	ASSERT_VALID (pDC);


	CDC dc;
	dc.CreateCompatibleDC (pDC);
	IMAGEINFO  imageInfo;
	ImageList.GetImageInfo(0, &imageInfo);
	POINT pt;
	pt.x = 0;
	pt.y = 0;
	ImageList.Draw(&dc,iIndex,CPoint(0,0),ILD_NORMAL);

	// Select the bitmap into the in-memory DC

	int nOldStretchBltMode = pDC->SetStretchBltMode (COLORONCOLOR);
	int cx = imageInfo.rcImage.right - imageInfo.rcImage.left;
	int cy = imageInfo.rcImage.bottom - imageInfo.rcImage.top;

	pDC->StretchBlt(0, 0, rectClient.Width (), rectClient.Height (), &dc, 
		0, 0, cx, cy, SRCCOPY);


	pDC->SetStretchBltMode (nOldStretchBltMode);

	return true;
}

BOOL _StretchBitmap (CDC* pDC, const CRect& rectClient, CBitmap& bmp)
{
	ASSERT_VALID (pDC);

	if (bmp.GetSafeHandle () != NULL)
	{
		BITMAP bm;
		ZeroMemory (&bm, sizeof (BITMAP));

		bmp.GetBitmap (&bm);

		CDC dc;
		dc.CreateCompatibleDC (pDC);

		// Select the bitmap into the in-memory DC
		CBitmap* pOldBitmap = dc.SelectObject (&bmp);

		int nOldStretchBltMode = pDC->SetStretchBltMode (COLORONCOLOR);

		pDC->StretchBlt(0, 0, rectClient.Width (), rectClient.Height (), &dc, 
			0, 0, bm.bmWidth, bm.bmHeight, SRCCOPY);


		dc.SelectObject (pOldBitmap);

		pDC->SetStretchBltMode (nOldStretchBltMode);

		return true;
	}

	return false;
}


void _DrawReflectedGradient (CDC* pDC, COLORREF clrS, COLORREF clrM, COLORREF clrF,
	const CRect& rect, BOOL bHorz)
{
	ASSERT_VALID (pDC);

	CDrawingManager dm (*pDC);

	long x1 = rect.left;
	long x2 = rect.right;
	long y1 = rect.top;
	long y2 = rect.bottom;

	if (bHorz)
	{
		CRect rt (x1, y1, x2, (y1 + y2) / 2);
		dm.FillGradient (rt, clrM, clrS, TRUE);
		rt.top = rt.bottom;
		rt.bottom = y2;
		dm.FillGradient (rt, clrF, clrM, TRUE);
	}
	else
	{
		CRect rt (x1, y1, (x1 + x2) / 2, y2);
		dm.FillGradient (rt, clrS, clrM, FALSE);
		rt.left  = rt.right;
		rt.right = x2;
		dm.FillGradient (rt, clrM, clrF, FALSE);
	}
}


/*
*	将指定的字节数组转换为16进制字符串，目标字符串的长度为数组长度的两倍。
*/
PCHAR Bytes2HexString(const BYTE * pSrc, PCHAR pDst, int nSrcLength)
{
	const char tab[] = "0123456789ABCDEF";
	
	for(int i=0; i<nSrcLength; i++)
	{
		*pDst++ = tab[*pSrc >> 4];
		*pDst++ = tab[*pSrc & 0x0f];
		pSrc++;
	}
	
	*pDst = '\0';
	
	return pDst;
}

void HandleError(char *s)
{
	UNREFERENCED_PARAMETER (s);

//	TCHAR szError[1024]={0};
//	sprintf_s(szError,
//		"An error occurred in running the program. \n%s\nError number %x.\n",
	//	s,GetLastError());
	//TraceToLog(szError);

} // End of HandleError

void TraceToLog(LPCTSTR lpInfo)
{
	UNREFERENCED_PARAMETER (lpInfo);

#ifndef _LOG_DEBUG
	TRACE1("'%s'\n", lpInfo);
	return;
#else
	FILE* fp = NULL;
	CString strTemp=GetExePath()+_T("\\UzGameInfo.log");
	_tfopen_s(&fp, strTemp,_T("a+"));
	if(fp==NULL)
		return;
	CTime t = CTime::GetCurrentTime();
	strTemp=t.Format(_T("[%Y-%m-%d %H:%M:%S]"));
	_fputts(strTemp,fp);
	_fputts(lpInfo,fp);
	fclose(fp);
#endif
}

void SetProcessCookiePath(CString strPath)
{
	TCHAR           sz[MAX_PATH];
	HKEY            hKey;
	if(RegOpenKey(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders\\"), &hKey) != ERROR_SUCCESS)
	{
		TRACE0("Cookies not found\n");
		return ;
	}
	_stprintf_s(sz, _T("%s\\%s"),GetExePath(),strPath);

	int nStrLen = lstrlen(sz);
#ifdef _UNICODE
	nStrLen *= 2;
#endif

	RegSetValueEx(hKey, _T("Cookies"), NULL, REG_EXPAND_SZ, (LPBYTE)(sz), nStrLen);
	RegCloseKey(hKey);	
}

void SetDefaultCookiePath()
{
	TCHAR           sz[MAX_PATH]=_T("%USERPROFILE%\\Cookies");
	HKEY            hKey;
	if(RegOpenKey(HKEY_CURRENT_USER, _T("Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\User Shell Folders\\"), &hKey) != ERROR_SUCCESS)
	{
		TRACE0("Cookies not found\n");
		return ;
	}

	int nStrLen = lstrlen(sz);
#ifdef _UNICODE
	nStrLen *= 2;
#endif
	RegSetValueEx(hKey, _T("Cookies"), NULL, REG_EXPAND_SZ, (LPBYTE)(sz), nStrLen);
	RegCloseKey(hKey);	
}


void Split(CString source, CStringArray& dest, CString division)
{
	dest.RemoveAll();
	int pos = 0;
	int pre_pos = 0;
	int iDivLen =  lstrlen(division);
	if (iDivLen == 0)
	{
		return ;
	}
	while( -1 != pos )
	{
		pre_pos = pos;
		pos = source.Find(division,(pos+1));
		if (pre_pos > 0)
		{
			pre_pos += iDivLen;
		}
        if (-1 == pos)
            dest.Add(source.Mid(pre_pos));
        else
            dest.Add(source.Mid(pre_pos,(pos-pre_pos)));
	}
}

CComPtr<IWebBrowser2> GetIWebBrowserInterface(HWND BrowserWnd) 
{
	CComPtr<IWebBrowser2> pWebBrowser2=NULL;
	HRESULT hr = E_FAIL;

	// Explicitly load MSAA so we know if it's installed
	HINSTANCE hInst = ::LoadLibrary( _T("OLEACC.DLL") );
	if ( hInst )
	{
		LRESULT lRes;
		UINT nMsg = ::RegisterWindowMessage( _T("WM_HTML_GETOBJECT") );
		::SendMessageTimeout( BrowserWnd, nMsg, 0L, 0L, SMTO_ABORTIFHUNG, 1000, (DWORD*)&lRes );
		
		CComPtr<IServiceProvider> spServiceProv;
		hr = ObjectFromLresult( lRes, IID_IServiceProvider, 0, (void**)&spServiceProv );
		if ( SUCCEEDED(hr) )
		{
			hr = spServiceProv->QueryService(SID_SWebBrowserApp,
					IID_IWebBrowser2,(void**)&pWebBrowser2);

		} // else document not ready
	} // else Active Accessibility is not installed

	return SUCCEEDED(hr) ? pWebBrowser2 : NULL;
}


/************************************************************************/
/*  查询指定窗口中(ie browser)的IWebBrowser接口                         */
/************************************************************************/


IWebBrowser2* FindWebBrowser(HWND hFindWnd)
{
	IShellWindows* pIShellWnd=NULL;
	HRESULT hr=::CoCreateInstance(CLSID_ShellWindows,NULL,CLSCTX_LOCAL_SERVER ,__uuidof(IShellWindows),(void**)&pIShellWnd);
	if ( FAILED ( hr ) )
	{
		TraceToLog(_T("获取 IShellWindows 接口错误\n"));
		return NULL;
	}
	IWebBrowser2* pIWebBrowser=NULL;
	long nCount = 0;		// 取得浏览器实例个数(Explorer 和 IExplorer)
	hr=pIShellWnd->get_Count( &nCount );
	if( 0 == nCount )
	{
		TraceToLog(_T("没有浏览器在运行\n"));
		pIShellWnd->Release();
		return NULL;
	}
	IDispatch* pIDisp=NULL;

	for (long i = 0; i < nCount; i++)
	{
		hr=pIShellWnd->Item(COleVariant(i), &pIDisp);
		if ( FAILED ( hr ) || pIDisp==NULL )	continue;

		hr=pIDisp->QueryInterface(IID_IWebBrowser2,(void **) &pIWebBrowser);
		if ( FAILED ( hr ) || pIDisp==NULL )	continue;

		if (pIWebBrowser != NULL)
		{
			HWND hWnd = NULL;
			pIWebBrowser->get_HWND ((long*)(&hWnd));
			if(hWnd==hFindWnd)
			{
				pIShellWnd->Release();
				return pIWebBrowser;
			}
			pIWebBrowser->Release ();
		}
	}
	pIShellWnd->Release();
	return NULL;
}

CString GetWebSiteUrl(int nID)
{
	switch (nID)
	{
	//case WEBUrl_Update:
	//	return _T("http://www.yoozhe.com/update/update.txt");
	//case WEBUrl_SFZ:
	//	return _T("http://www.yoozhe.com/us/sfz.html");
	case WEBUrl_RecordingScripts:
		return _T("http://www.yoozhe.com/scripts/ScriptSample.xml");
	//case WEBUrl_HomePage:
	//	return _T("http://www.yoozhe.com/us/zhuye.html");
	case WEBUrl_FeedBack:
		return _T("http://www.yoozhe.com/us/feedback.html");
	//case WEBUrl_GearHelp:
	//	return _T("http://www.yoozhe.com/us/jsfaq.html");
    }
    ASSERT(FALSE);
    return CString();
}
/************************************************************************/
/* 原始输入注册鼠标
/************************************************************************/
BOOL RegisitMouse(HWND hwnd)
{
	if(NULL == hwnd)
		return false;
	
	RAWINPUTDEVICE rid[2];
	rid[0].usUsagePage = 0x01;
	rid[0].usUsage = 0x02;
	rid[0].dwFlags = RIDEV_INPUTSINK;
	rid[0].hwndTarget = hwnd;

	rid[1].usUsagePage = 0x01;
	rid[1].usUsage = 0x06;
	rid[1].dwFlags = RIDEV_INPUTSINK;
	rid[1].hwndTarget = hwnd;

	return RegisterRawInputDevices(rid, 2, sizeof(RAWINPUTDEVICE));
}

/************************************************************************/
/* 取消原始输入注册鼠标
/************************************************************************/
BOOL UnRegisitMouse(HWND hwnd)
{
	UNREFERENCED_PARAMETER (hwnd);

	RAWINPUTDEVICE rid[2];
	rid[0].usUsagePage = 0x01;
	rid[0].usUsage = 0x02;
	rid[0].dwFlags = RIDEV_REMOVE;
	rid[0].hwndTarget = NULL;

	rid[1].usUsagePage = 0x01;
	rid[1].usUsage = 0x06;
	rid[1].dwFlags = RIDEV_REMOVE;
	rid[1].hwndTarget = NULL;

	return RegisterRawInputDevices(rid, 2, sizeof(RAWINPUTDEVICE));
}


/************************************************************************/
/* 原始输入注册键盘
/************************************************************************/
BOOL RegisitKeyBord(HWND hwnd)
{
	if(NULL == hwnd)
		return false;
			
	RAWINPUTDEVICE rid;
	rid.usUsagePage = 0x01;
	rid.usUsage = 0x06;
	rid.dwFlags = RIDEV_INPUTSINK;
	rid.hwndTarget = hwnd;

	return RegisterRawInputDevices(&rid, 1, sizeof(RAWINPUTDEVICE));
}

/************************************************************************/
/* 取消原始输入注册键盘
/************************************************************************/
BOOL UnRegisitKeyBord(HWND hwnd)
{
	if(NULL == hwnd)
		return false;
			
	RAWINPUTDEVICE rid;
	rid.usUsagePage = 0x01;
	rid.usUsage = 0x06;
	rid.dwFlags = RIDEV_INPUTSINK;
	rid.hwndTarget = NULL;

	return RegisterRawInputDevices(&rid, 1, sizeof(RAWINPUTDEVICE));
}

BOOL SetTopWindow(HWND hWnd)
{
	HWND hForeWnd = GetForegroundWindow();
	DWORD dwForeID = GetWindowThreadProcessId(hForeWnd, NULL);
	DWORD dwCurID = GetCurrentThreadId();
	AttachThreadInput(dwCurID, dwForeID, TRUE);
//	ShowWindow(hWnd, SW_SHOWNORMAL);
	SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSENDCHANGING);
	SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOSENDCHANGING);
//	SetForegroundWindow(hWnd);;
	AttachThreadInput(dwCurID, dwForeID, FALSE);
	return TRUE;
}

HRGN BitmapToRegion (HBITMAP hBmp, COLORREF cTransparentColor/* =  0*/, COLORREF cTolerance /*= 0x101010*/)
{
	HRGN hRgn = NULL;

	if (hBmp)
	{
		// Create a memory DC inside which we will scan the bitmap content
		HDC hMemDC = CreateCompatibleDC(NULL);
		if (hMemDC)
		{
			// Get bitmap size
			BITMAP bm;
			GetObject(hBmp, sizeof(bm), &bm);

			// Create a 32 bits depth bitmap and select it into the memory DC 
			BITMAPINFOHEADER RGB32BITSBITMAPINFO = {    
				sizeof(BITMAPINFOHEADER),    // biSize 
				bm.bmWidth,                    // biWidth; 
				bm.bmHeight,                // biHeight; 
				1,                            // biPlanes; 
				32,                            // biBitCount 
				BI_RGB,                        // biCompression; 
				0,                            // biSizeImage; 
				0,                            // biXPelsPerMeter; 
				0,                            // biYPelsPerMeter; 
				0,                            // biClrUsed; 
				0                            // biClrImportant; 
			};
			VOID * pbits32; 
			HBITMAP hbm32 = CreateDIBSection(hMemDC, (BITMAPINFO *)&RGB32BITSBITMAPINFO, DIB_RGB_COLORS, &pbits32, NULL, 0);
			if (hbm32)
			{
				HBITMAP holdBmp = (HBITMAP)SelectObject(hMemDC, hbm32);

				// Create a DC just to copy the bitmap into the memory DC
				HDC hDC = CreateCompatibleDC(hMemDC);
				if (hDC)
				{
					// Get how many bytes per row we have for the bitmap bits (rounded up to 32 bits)
					BITMAP bm32;
					GetObject(hbm32, sizeof(bm32), &bm32);
					while (bm32.bmWidthBytes % 4)
						bm32.bmWidthBytes++;

					// Copy the bitmap into the memory DC
					HBITMAP holdBmp = (HBITMAP)SelectObject(hDC, hBmp);
					BitBlt(hMemDC, 0, 0, bm.bmWidth, bm.bmHeight, hDC, 0, 0, SRCCOPY);

					// For better performances, we will use the ExtCreateRegion() function to create the
					// region. This function take a RGNDATA structure on entry. We will add rectangles by
					// amount of ALLOC_UNIT number in this structure.
#define ALLOC_UNIT    100
					DWORD maxRects = ALLOC_UNIT;
					HANDLE hData = GlobalAlloc(GMEM_MOVEABLE, sizeof(RGNDATAHEADER) + (sizeof(RECT) * maxRects));
					RGNDATA *pData = (RGNDATA *)GlobalLock(hData);
					pData->rdh.dwSize = sizeof(RGNDATAHEADER);
					pData->rdh.iType = RDH_RECTANGLES;
					pData->rdh.nCount = pData->rdh.nRgnSize = 0;
					SetRect(&pData->rdh.rcBound, MAXLONG, MAXLONG, 0, 0);

					// Keep on hand highest and lowest values for the "transparent" pixels
					BYTE lr = GetRValue(cTransparentColor);
					BYTE lg = GetGValue(cTransparentColor);
					BYTE lb = GetBValue(cTransparentColor);
					BYTE hr = min(0xff, lr + GetRValue(cTolerance));
					BYTE hg = min(0xff, lg + GetGValue(cTolerance));
					BYTE hb = min(0xff, lb + GetBValue(cTolerance));

					// Scan each bitmap row from bottom to top (the bitmap is inverted vertically)
					BYTE *p32 = (BYTE *)bm32.bmBits + (bm32.bmHeight - 1) * bm32.bmWidthBytes;
					for (int y = 0; y < bm.bmHeight; y++)
					{
						// Scan each bitmap pixel from left to right
						for (int x = 0; x < bm.bmWidth; x++)
						{
							// Search for a continuous range of "non transparent pixels"
							int x0 = x;
							LONG *p = (LONG *)p32 + x;
							while (x < bm.bmWidth)
							{
								BYTE b = GetRValue(*p);
								if (b >= lr && b <= hr)
								{
									b = GetGValue(*p);
									if (b >= lg && b <= hg)
									{
										b = GetBValue(*p);
										if (b >= lb && b <= hb)
											// This pixel is "transparent"
											break;
									}
								}
								p++;
								x++;
							}

							if (x > x0)
							{
								// Add the pixels (x0, y) to (x, y+1) as a new rectangle in the region
								if (pData->rdh.nCount >= maxRects)
								{
									GlobalUnlock(hData);
									maxRects += ALLOC_UNIT;
									hData = GlobalReAlloc(hData, sizeof(RGNDATAHEADER) + (sizeof(RECT) * maxRects), GMEM_MOVEABLE);
									pData = (RGNDATA *)GlobalLock(hData);
								}
								RECT *pr = (RECT *)&pData->Buffer;
								SetRect(&pr[pData->rdh.nCount], x0, y, x, y+1);
								if (x0 < pData->rdh.rcBound.left)
									pData->rdh.rcBound.left = x0;
								if (y < pData->rdh.rcBound.top)
									pData->rdh.rcBound.top = y;
								if (x > pData->rdh.rcBound.right)
									pData->rdh.rcBound.right = x;
								if (y+1 > pData->rdh.rcBound.bottom)
									pData->rdh.rcBound.bottom = y+1;
								pData->rdh.nCount++;

								// On Windows98, ExtCreateRegion() may fail if the number of rectangles is too
								// large (ie: > 4000). Therefore, we have to create the region by multiple steps.
								if (pData->rdh.nCount == 2000)
								{
									HRGN h = ExtCreateRegion(NULL, sizeof(RGNDATAHEADER) + (sizeof(RECT) * maxRects), pData);
									if (hRgn)
									{
										CombineRgn(hRgn, hRgn, h, RGN_OR);
										DeleteObject(h);
									}
									else
										hRgn = h;
									pData->rdh.nCount = 0;
									SetRect(&pData->rdh.rcBound, MAXLONG, MAXLONG, 0, 0);
								}
							}
						}

						// Go to next row (remember, the bitmap is inverted vertically)
						p32 -= bm32.bmWidthBytes;
					}

					// Create or extend the region with the remaining rectangles
					HRGN h = ExtCreateRegion(NULL, sizeof(RGNDATAHEADER) + (sizeof(RECT) * maxRects), pData);
					if (hRgn)
					{
						CombineRgn(hRgn, hRgn, h, RGN_OR);
						DeleteObject(h);
					}
					else
						hRgn = h;

					// Clean up
					GlobalFree(hData);
					SelectObject(hDC, holdBmp);
					DeleteDC(hDC);
				}

				DeleteObject(SelectObject(hMemDC, holdBmp));
			}

			DeleteDC(hMemDC);
		}    
	}

	return hRgn;
}


void WStringTOAsinString(CStringW strWide,CStringA& strout)
{
	int len = WideCharToMultiByte( CP_ACP,0,strWide.GetBuffer(),-1,NULL,0,NULL,NULL );  
	if(len>0)
	{
		char* szbuf = new char[len+1];
		memset(szbuf,0,len+1);
		WideCharToMultiByte(CP_ACP,0,strWide.GetBuffer(),strWide.GetLength(),szbuf,len,NULL,NULL);
		if(szbuf!=NULL)
		{
			strout=szbuf;
			delete[] szbuf;
			szbuf=NULL;
		}
	}
}


//标准串转宽字符串
void AnsiToWideString(CStringA strAnsi,CStringW& strout)
{
	int len=strAnsi.GetLength();
	if(len>0)
	{
		LPWSTR szbuf = new WCHAR[len+1];
		memset(szbuf,0,2*(len+1));
		MultiByteToWideChar(CP_ACP,0,strAnsi.GetBuffer(),strAnsi.GetLength(),szbuf,len);
		if(szbuf!=NULL)
		{
			strout=szbuf;
			delete[] szbuf;
			szbuf=NULL;
		}
	}
}

CStringA UT8ToAsinString(LPCSTR str)
{
	int unicodeLen=MultiByteToWideChar(CP_UTF8,0,str,-1,NULL,0);
	wchar_t* pUnicode;
	pUnicode=new wchar_t[unicodeLen+1];
	memset(pUnicode,0,(unicodeLen+1)*sizeof(wchar_t));

	MultiByteToWideChar(CP_UTF8,0,str,-1,(LPWSTR)pUnicode,unicodeLen);

	char * pTargetData=NULL;
	int targetLen=WideCharToMultiByte(CP_ACP,0,(LPWSTR)pUnicode,-1,(char *)pTargetData,0,NULL,NULL);

	pTargetData=new char[targetLen+1];
	memset(pTargetData,0,targetLen+1);

	WideCharToMultiByte(CP_ACP,0,(LPWSTR)pUnicode,-1,(char *)pTargetData,targetLen,NULL,NULL);

	//LPSTR szResult=new TCHAR[targetLen+1];
	//wsprintf(szResult,"%s",pTargetData);
	CStringA strRet=pTargetData;
	delete[] pUnicode;
	delete[] pTargetData;
	return strRet;
}

CStringW UT8ToWString(const char* strUT8)
{
	int len=strlen(strUT8);
	wchar_t* pBuff = new WCHAR[len+1];
	memset(pBuff,0,(len+1)*sizeof(wchar_t));

	int lenWide=MultiByteToWideChar(CP_UTF8,0,strUT8,len,(LPWSTR)pBuff,len);     
	UNREFERENCED_PARAMETER (lenWide);
	CStringW strRet=pBuff;
	delete[] pBuff;
	return strRet;   
};

CString GetFavPath()
{
	return GetExePath()+L"\\favicon";
}

CString GetSysFavPath()
{
	WCHAR szPath[MAX_PATH] = {0};
	SHGetSpecialFolderPath(NULL, szPath, CSIDL_FAVORITES, FALSE);
	return szPath;
}

CStringA GetFileNameNoExt(CStringA strFile)
{
	CStringA strRet = "";
	if(strFile.GetLength() > 0)
	{
		int iLen = strFile.GetLength();
		for(;iLen>0;iLen--)
		{
			if(strFile[iLen] == '.')
			{
				strRet = strFile.Left(iLen);
				break;
			}
		}
	}
	return strRet;
}

CString GetFileExt(CString strFile)
{
	CString strRet = L"";
	if(strFile.GetLength() > 0)
	{
		int iLen = strFile.GetLength();
		for(;iLen>0;iLen--)
		{
			if(strFile[iLen] == '.')
			{
				strRet = strFile.Right(strFile.GetLength()-iLen-1);
				break;
			}
		}
	}
	return strRet;
}


BOOL   IsFileExist(LPCTSTR   lpFileName)
{
	if(lpFileName==NULL)   
		return   FALSE;   
	BOOL   bExist   =   TRUE;   
	HANDLE   hFind;   
	WIN32_FIND_DATA   dataFind;   
	hFind   =   FindFirstFile(lpFileName,&dataFind);   
	if(hFind==   INVALID_HANDLE_VALUE)   
	{   
		bExist   =   FALSE;   
	}   
	FindClose(hFind);   
	return   bExist;   
}



