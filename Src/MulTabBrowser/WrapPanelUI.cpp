#include "StdAfx.h"
#include "WrapPanelUI.h"

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

namespace DuilibEx
{

	CWrapPanelUI::CWrapPanelUI(void)
		: m_nLineNum(1), m_nMaxCount(10)
	{
		
	}

	CWrapPanelUI::~CWrapPanelUI(void) {}

	LPVOID CWrapPanelUI::GetInterface(LPCTSTR pstrName)
	{
		if (_tcscmp(pstrName, _T("WrapPanel")) == 0)
			return static_cast<CWrapPanelUI*>(this);
		return __super::GetInterface(pstrName);
	}

	void CWrapPanelUI::DoEvent(DuiLib::TEventUI& event)
	{
		DuiLib::CHorizontalLayoutUI::DoEvent(event);
	}

	bool CWrapPanelUI::Add(CControlUI* pControl)
	{
		if( pControl == NULL) return false;
		if (GetCount() == m_nMaxCount)
		{
			m_rcItem.bottom += m_rcItem.bottom - m_rcItem.top;
			SetFixedHeight(m_rcItem.bottom - m_rcItem.top);
			GetParent()->SetFixedHeight(m_rcItem.bottom - m_rcItem.top);
			m_nLineNum++;
		}
		
		m_rcItem.right += pControl->GetFixedWidth();
		SetFixedWidth(m_rcItem.right-m_rcItem.left);

		if( m_pManager != NULL ) m_pManager->InitControls(pControl, this);
		if( IsVisible() )
		{
			NeedUpdate();
		}
		else pControl->SetInternVisible(false);
		if (GetCount() < 2)
			return m_items.Add(pControl);
		else
			return m_items.InsertAt(GetCount()-1,pControl);
	}

	//************************************
	// Method:    Remove
	// FullName:  CContainerUI::Remove
	// Access:    public 
	// Returns:   bool
	// Qualifier: Sam.Tan
	// Parameter: CControlUI* pControl
	// Note:      重写父类删除方法
	//************************************
	bool CWrapPanelUI::Remove(CControlUI* pControl)
	{
		if( pControl == NULL) return false;

		//if (GetCount() <= m_nMaxCount && m_nLineNum > 1 )
		//{
		//	SetFixedHeight(m_nLineHeight);
		//	GetParent()->SetFixedHeight(m_nLineHeight);
		//}

		for( int it = 0; it < m_items.GetSize(); it++ ) {
			if( static_cast<CControlUI*>(m_items[it]) == pControl ) {
				NeedUpdate();
				if( m_bAutoDestroy ) {
					if( m_bDelayedDestroy && m_pManager ) m_pManager->AddDelayedCleanup(pControl);             
					else delete pControl;
				}
				return m_items.Remove(it);
			}
		}
		return false;
	}

	//************************************
	// Method:    SetPos
	// FullName:  CWrapPanelUI::SetPos
	// Access:    public 
	// Returns:   void
	// Qualifier: Sam.Tan
	// Parameter: RECT rc
	// Note:      重新父类SetPos方法, 实现
	//            子元素自动换行显示
	//************************************
	void CWrapPanelUI::SetPos(RECT rc)
	{
		CControlUI::SetPos(rc);
	
		rc = m_rcItem;

		// Adjust for inset
		rc.left += m_rcInset.left;
		rc.top += m_rcInset.top;
		rc.right -= m_rcInset.right;
		rc.bottom -= m_rcInset.bottom;

		if( m_items.GetSize() == 0) {
			ProcessScrollBar(rc, 0, 0);
			return;
		}

		if( m_pVerticalScrollBar && m_pVerticalScrollBar->IsVisible() ) rc.right -= m_pVerticalScrollBar->GetFixedWidth();
		if( m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible() ) rc.bottom -= m_pHorizontalScrollBar->GetFixedHeight();

		// Determine the width of elements that are sizeable
		SIZE szAvailable = { rc.right - rc.left, rc.bottom - rc.top };
		if( m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible() ) 
			szAvailable.cx += m_pHorizontalScrollBar->GetScrollRange();

		int nAdjustables = 0;
		int cxFixed = 0;
		int nEstimateNum = 0;
		for( int it1 = 0; it1 < m_items.GetSize(); it1++ ) {
			CControlUI* pControl = static_cast<CControlUI*>(m_items[it1]);
			if( !pControl->IsVisible() ) continue;
			if( pControl->IsFloat() ) continue;
			SIZE sz = pControl->EstimateSize(szAvailable);
			if( sz.cx == 0 ) {
				nAdjustables++;
			}
			else {
				if( sz.cx < pControl->GetMinWidth() ) sz.cx = pControl->GetMinWidth();
				if( sz.cx > pControl->GetMaxWidth() ) sz.cx = pControl->GetMaxWidth();
			}
			cxFixed += sz.cx +  pControl->GetPadding().left + pControl->GetPadding().right;
			nEstimateNum++;
		}
		cxFixed += (nEstimateNum - 1) * m_iChildPadding;

		int cxExpand = 0;
		int cxNeeded = 0;
		if( nAdjustables > 0 ) cxExpand = MAX(0, (szAvailable.cx - cxFixed) / nAdjustables);
		// Position the elements
		SIZE szRemaining = szAvailable;
		int iPosX = rc.left;
		int iPosY = 0;

		if( m_pHorizontalScrollBar && m_pHorizontalScrollBar->IsVisible() ) {
			iPosX -= m_pHorizontalScrollBar->GetScrollPos();
		}
		int iAdjustable = 0;
		int cxFixedRemaining = cxFixed;
		for( int it2 = 0; it2 < m_items.GetSize(); it2++ ) {
			CControlUI* pControl = static_cast<CControlUI*>(m_items[it2]);
			if( !pControl->IsVisible() ) continue;
			if( pControl->IsFloat() ) {
				SetFloatPos(it2);
				continue;
			}
			RECT rcPadding = pControl->GetPadding();
			szRemaining.cx -= rcPadding.left;
			SIZE sz = pControl->EstimateSize(szRemaining);
			if( sz.cx == 0 ) {
				iAdjustable++;
				sz.cx = cxExpand;
				// Distribute remaining to last element (usually round-off left-overs)
				if( iAdjustable == nAdjustables ) {
					sz.cx = MAX(0, szRemaining.cx - rcPadding.right - cxFixedRemaining);
				}
				if( sz.cx < pControl->GetMinWidth() ) sz.cx = pControl->GetMinWidth();
				if( sz.cx > pControl->GetMaxWidth() ) sz.cx = pControl->GetMaxWidth();
			}
			else {
				if( sz.cx < pControl->GetMinWidth() ) sz.cx = pControl->GetMinWidth();
				if( sz.cx > pControl->GetMaxWidth() ) sz.cx = pControl->GetMaxWidth();

				cxFixedRemaining -= sz.cx;
			}

			sz.cy = pControl->GetFixedHeight();
			if( sz.cy == 0 ) sz.cy = rc.bottom - rc.top - rcPadding.top - rcPadding.bottom;
			if( sz.cy < 0 ) sz.cy = 0;
			if( sz.cy < pControl->GetMinHeight() ) sz.cy = pControl->GetMinHeight();
			if( sz.cy > pControl->GetMaxHeight() ) sz.cy = pControl->GetMaxHeight();

			if ( it2 % m_nMaxCount == 0 && it2 != 0)
			{
				iPosX = rc.left;
				iPosY += sz.cy + 1;
			}

			RECT rcCtrl = { iPosX + rcPadding.left, rc.top + rcPadding.top + iPosY,
				iPosX + sz.cx + rcPadding.left + rcPadding.right, rc.top + rcPadding.top + sz.cy + iPosY};
			pControl->SetPos(rcCtrl);
			
			m_nLineHeight = rcCtrl.bottom - rcCtrl.top;

			iPosX += sz.cx + m_iChildPadding + rcPadding.left + rcPadding.right;

			cxNeeded += sz.cx + rcPadding.left + rcPadding.right;
			szRemaining.cx -= sz.cx + m_iChildPadding + rcPadding.right;
		}
		cxNeeded += (nEstimateNum - 1) * m_iChildPadding;

		// Process the scrollbar
		ProcessScrollBar(rc, cxNeeded, 0);
	}

	void CWrapPanelUI::DoPaint(HDC hDC, const RECT& rcPaint)
	{
		DuiLib::CHorizontalLayoutUI::DoPaint(hDC, rcPaint);
	}

}