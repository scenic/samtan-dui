#pragma once
#include "..\\DuiLib\\UIlib.h"
#include <list>
#include "HistoryObj.h"
class CRestoreMenuWnd :public DuiLib::WindowImplBase
{
public:
	CRestoreMenuWnd(void);
	~CRestoreMenuWnd(void);

	virtual	LPCTSTR		GetWindowClassName() const;
	virtual DuiLib::CDuiString	 GetSkinFile();                
	virtual DuiLib::CDuiString	GetSkinFolder();
	virtual void	InitWindow(); 
	virtual void	Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled);
	
	
	void Init(POINT pt);
private: 
	DuiLib::CListUI* m_pList;
	std::list<CUzHistoryObj> restoreHistory;
	POINT point;
};

