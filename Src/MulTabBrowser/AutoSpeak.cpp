#include "StdAfx.h"
#include "AutoSpeak.h"
#include "GlobalFunc.h"

CAutoSpeak::CAutoSpeak():m_strLink(L""),
	m_strTitle(L""),
	m_strIconPath(L""),
	m_bSet(FALSE),
	m_bEnable(FALSE),
	m_bInRecycle(FALSE),
	m_hSpeakWnd(NULL),
	m_ptSpeakButton(0,0),
	m_ptSpeakEdit(0,0),
	m_dwStartTime(0),
	m_dwCurrentSpeakOrder(0),
	m_bAddRandString(FALSE)
{

}

CAutoSpeak::CAutoSpeak(HWND h):m_hSpeakWnd(h),
	m_strLink(L""),
	m_strTitle(L""),
	m_strIconPath(L""),
	m_bSet(FALSE),
	m_bEnable(FALSE),
	m_bInRecycle(FALSE),
	m_ptSpeakButton(0,0),
	m_ptSpeakEdit(0,0),
	m_dwStartTime(0),
	m_dwCurrentSpeakOrder(0),
	m_bAddRandString(FALSE)
{

}

CAutoSpeak::~CAutoSpeak()
{

}


CString CAutoSpeak::GetLink() const
{
	return m_strLink;
}
CString CAutoSpeak::GetTitle() const
{
	return m_strTitle;
}
CString CAutoSpeak::GetIconPath() const
{
	return m_strIconPath;
}
BOOL CAutoSpeak::IsEnable() const
{
	return m_bEnable;
}
BOOL CAutoSpeak:: IsSet() const
{
	return m_bSet;
}
BOOL CAutoSpeak::IsInRecycle() const
{
	return m_bInRecycle;
}
CPoint CAutoSpeak:: GetSpeakEditPt() const
{
	return m_ptSpeakEdit;
}
CPoint CAutoSpeak:: GetSpeakButtonPt() const
{
	return m_ptSpeakButton;
}

void CAutoSpeak:: SetInRecycle(BOOL b) 
{
	m_bInRecycle = b;
}

void   CAutoSpeak:: SetLink(const CString& str)
{
	m_strLink = str;
}
void  CAutoSpeak:: SetTitle(const CString& strTitle)
{
	m_strTitle = strTitle;
}
void   CAutoSpeak:: SetIconPath(const CString& str)
{
	m_strIconPath = str;
}
void  CAutoSpeak::  SetSet(const bool b)
{
	m_bSet = b;
}
void  CAutoSpeak::  SetEnable(const bool b)
{
	if(b)
		SetStartTime(GetTickCount());
	m_bEnable = b;
}
void  CAutoSpeak::  SetSpeakWnd(HWND h)
{
	m_hSpeakWnd = h;
}
HWND CAutoSpeak::   GetSpeakWnd() const 
{
	return m_hSpeakWnd;
}
void   CAutoSpeak:: SetSpeakEditPt(const CPoint& pt)
{
	m_ptSpeakEdit = pt;
}
void   CAutoSpeak:: SetSpeakButtonPt(const CPoint& pt)
{
	m_ptSpeakButton = pt;
}
CString CAutoSpeak:: GetLabelWndStr()
{
	CString str=L"";
	str.Format(L"%d",m_hLabelWnd);
	return str;
}
CString CAutoSpeak::GetSpeakWndStr()
{
	CString str=L"";
	str.Format(L"%d",m_hSpeakWnd);
	return str;
}

BOOL CAutoSpeak:: IsSpeakTIme()
{
	if(GetTickCount() - m_dwStartTime > static_cast<UINT>(m_nIntervalTime)*1000)
	{
		m_dwStartTime = GetTickCount();
		return TRUE;
	}
	return FALSE;
}

void CAutoSpeak:: NextSpeakOrder()
{
	while(1)
	{
		if( m_dwCurrentSpeakOrder == 0 )
		{
			if( m_strContent1 != L"")
			{
				m_dwCurrentSpeakOrder++;
				break;
			}
		}
		if( m_dwCurrentSpeakOrder == 1 )
		{
			if( m_strContent2 != L"")
			{
				m_dwCurrentSpeakOrder++;
				break;
			}
		}
		if( m_dwCurrentSpeakOrder == 2 )
		{
			if( m_strContent3 != L"")
			{
				m_dwCurrentSpeakOrder++;
				break;
			}
		}
		m_dwCurrentSpeakOrder++;
		if(m_dwCurrentSpeakOrder > 2)
			m_dwCurrentSpeakOrder = 0;
	}

}

CStringA CAutoSpeak::GetSpeakString()
{
	CStringA strRet = "";
	if(m_dwCurrentSpeakOrder - 1 == 0)
		WStringTOAsinString(m_strContent1,strRet);
	else if(m_dwCurrentSpeakOrder -1 == 1)
		WStringTOAsinString(m_strContent2,strRet);
	else if(m_dwCurrentSpeakOrder -1 == 2)
		WStringTOAsinString(m_strContent3,strRet);
	if(GetAddRandString())
	{
		strRet += GenerateRandString(8);
	}
	return strRet;
}

CStringA CAutoSpeak::GenerateRandString(int len)
{
	char* szbuff = new char[len+1];
	memset(szbuff,0,len+1);
	if(szbuff!=NULL && len > 0)
	{
		srand(GetTickCount());
		CStringA strRand="";
		int a=0;
		for(;a<len;a++) 
		{   
			szbuff[a] = ((rand()%74)+48);   
			while(!IsCharAlphaNumeric(szbuff[a]))   
			{   
				szbuff[a]=((rand()%74)+48); 
			}    
		} 
	}	
	CStringA str =  szbuff;
	delete[] szbuff;
	return str;
}
