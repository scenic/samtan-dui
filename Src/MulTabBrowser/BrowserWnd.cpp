#include "StdAfx.h"
#include "BrowserWnd.h"
#include "ControlEx.h"
#include "LablHistoryListWnd.h"
#include "UrlSearchListWnd.h"
#include "ScriptRecordWnd.h"
#include "AddGameAccountWnd.h"
#include "AccountSetupWnd.h"
#include "AddFavoWnd.h"
#include "InterView.h"
#include "BossKeyWnd.h"
#include "DuilibAutoSpeakDlg.h"
#include "KeySetListMgr.h"
#include "LoginWnd.h"
#include "SideAccountMgrWnd.h"
#include "SideFavsWnd.h"
#include "SideHistoryWnd.h"
#include "LabelItemUI.h"
#include "HistoryObj.h"
#include "Util.h"
#include "TypedUrlsMgr.h"
#include "MulTabBrowser.h"
#include "SidePanelUI.h"
#include "WrapPanelUI.h"
#include <algorithm>
#include "HistoryObj.h"
//////////////////////////////////////////////////////////////////////////
// BrowserWnd 浏览器窗口类

CBrowserWnd::CBrowserWnd(void) :
		m_bFullScreen(false),
		m_bEnableBossKey(false)
{
	m_pSideFavsWnd = NULL;
	m_pBossKeyWnd = NULL;
	m_pLoginWnd = NULL;
	m_pSideHistoryWnd = NULL;
	m_pSideAccountMgrWnd = NULL;
	pUrlWnd=NULL;
}

// 初始化
void CBrowserWnd::Init() {}

void CBrowserWnd::OnPrepare()
{
	m_pWebBrowserEventHandler = new DuiLib::CWebBrowserEventHandler;

	m_pSideAccountMgrWnd = new CSideAccountMgrWnd;
	m_pSideAccountMgrWnd->Create(*this, NULL, WS_SIZEBOX | WS_CHILD, 0);


	m_pSideFavsWnd = new CSideFavsWnd;
	m_pSideFavsWnd->Create(*this, NULL, WS_SIZEBOX | WS_CHILD, 0);


	m_pSideHistoryWnd = new CSideHistoryWnd;
	m_pSideHistoryWnd->Create(*this, NULL, WS_SIZEBOX | WS_CHILD, 0);

	CKeySetListMgr::Instance().ReadKeyToList();
	/* 导航栏控件 */
	m_pbtnUrlList = static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("btnUrlList")));
	m_peditUrl = static_cast<DuiLib::CEditUI*>(m_pm.FindControl(_T("editUrl")));

	/* 容器控件 */
	m_ptlayMain = static_cast<DuiLib::CTabLayoutUI*>(m_pm.FindControl(_T("tlayMain")));
	m_vlaySideMenu = static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("vlaySideMenu")));
	m_phlayLablesLine = static_cast<DuilibEx::CWrapPanelUI*>(m_pm.FindControl(_T("hlayLablesLine")));

	/* 标签栏控件 */
	m_pvlayLables = static_cast<DuiLib::CVerticalLayoutUI*>(m_pm.FindControl(_T("vlayLables")));
	m_pbtnNewLabel = static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("btnNewLabel")));
	m_pbtnLabClose = static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("btnLabClose")));
	m_pbtnHistoryList = static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("btnHistoryList")));
	m_pbtnNewLabel->SetTag(2);

	/* 侧边栏控件 */
	m_pbtnHiddenSide = static_cast<DuiLib::CButtonUI*>(m_pm.FindControl(_T("btnHiddenSide")));
	m_ptlaySideToolWnd = static_cast<DuiLib::CTabLayoutUI*>(m_pm.FindControl(_T("tlaySideToolWnd")));

	m_poptSideFavs = static_cast<DuiLib::COptionUI*>(m_pm.FindControl(_T("optSideFavs")));
	m_poptSideHistory = static_cast<DuiLib::COptionUI*>(m_pm.FindControl(_T("optSideHistory")));
	m_poptAccount = static_cast<DuiLib::COptionUI*>(m_pm.FindControl(_T("optAccount")));

	/* 侧边栏窗口Container */
	m_pCtnrSideAccountWnd = static_cast<DuilibEx::CSidePanelUI*>(m_pm.FindControl(_T("ctnrSideAccountWnd")));
	m_pCtnrSideFavsWnd = static_cast<DuilibEx::CSidePanelUI*>(m_pm.FindControl(_T("ctnrSideFavsWnd")));
	m_pCtnrSideHistoryWnd = static_cast<DuilibEx::CSidePanelUI*>(m_pm.FindControl(_T("ctnrSideHistoryWnd")));

	m_pCtnrSideAccountWnd->Attach(m_pSideAccountMgrWnd);
	m_pSideAccountMgrWnd->SetOwner(m_pCtnrSideAccountWnd);

	m_pCtnrSideFavsWnd->Attach(m_pSideFavsWnd);
	m_pSideFavsWnd->SetOwner(m_pCtnrSideFavsWnd);

	m_pCtnrSideHistoryWnd->Attach(m_pSideHistoryWnd);
	m_pSideHistoryWnd->SetOwner(m_pCtnrSideHistoryWnd);

	/* 浏览器客户区 */
	m_ptlayBrowserClient = static_cast<DuiLib::CTabLayoutUI*>(m_pm.FindControl(_T("tlayBrowserClient")));

	
	DuilibEx::CLabelItemUI* pLabelItem = static_cast<DuilibEx::CLabelItemUI*>(m_pm.FindControl(_T("labItem")));
	pLabelItem->ApplyAttributeList(_T("bkcolor=\"#FFFFFFFF\""));
	CInterView* pWeb=new CInterView;
	m_ptlayBrowserClient->Add(pWeb);
	m_ptlayBrowserClient->SelectItem(pWeb);
	
	//pWeb->SetWebBrowserEventHandler(m_pWebBrowserEventHandler);
	pWeb->UzNavigate(_T("www.baidu.com"));

	m_mapTableBrowser.insert(pair<DuilibEx::CLabelItemUI*, CInterView*>(pLabelItem, pWeb));
	

	//创建地址栏List
	pUrlWnd = new CUrlSearchListWnd(_T("Browser//url_search_list.xml"));
	pUrlWnd->SetParentWnd(this);
	pUrlWnd->CreateWnd(*this);

	
	/* 页游福利 */
	DuiLib::CActiveXUI* pGiftPage= static_cast<DuiLib::CActiveXUI*>(m_pm.FindControl(_T("giftPage")));
	if (pGiftPage)
	{
		IWebBrowser2* pWebBrowser = NULL;
		pGiftPage->GetControl(IID_IWebBrowser2, (void**)&pWebBrowser);
		if( pWebBrowser != NULL ) {
			pWebBrowser->Navigate(_T("http://www.yoozhe.com/us/daohang.html"), NULL, NULL, NULL, NULL);  
			pWebBrowser->Release();
		}
	}

	//* 开服区 */
	DuiLib::CActiveXUI* pServPage = static_cast<DuiLib::CActiveXUI*>(m_pm.FindControl(_T("servPage")));
	if (pServPage)
	{
		IWebBrowser2* pWebBrowser = NULL;
		pServPage->GetControl(IID_IWebBrowser2, (void**)&pWebBrowser);
		if( pWebBrowser != NULL ) {
			pWebBrowser->Navigate(_T("http://www.2114.com/?=321"), NULL, NULL, NULL, NULL);  
			pWebBrowser->Release();
		}
	}
}

// 控件通知消息处理
void CBrowserWnd::Notify(DuiLib::TNotifyUI& msg)
{
	if (msg.sType == _T("windowinit"))
	{
		OnPrepare();
	}
	else if(msg.sType == DUI_MSGTYPE_RETURN)
	{
		if(m_peditUrl->IsFocused())
			OnClickGoto();
	}
	else if (msg.sType == DUI_MSGTYPE_SELECTCHANGED)
	{
		/* 顶部功能选项卡 */
		if (msg.pSender->GetName() == _T("optGameGift"))
		{
			m_ptlayMain->SelectItem(0);
		}
		else if (msg.pSender->GetName() == _T("optBrowser"))
		{
			m_ptlayMain->SelectItem(1);
		}
		else if (msg.pSender->GetName() == _T("optServTable"))
		{
			m_ptlayMain->SelectItem(2);
		}

		else if (msg.pSender->GetName() == _T("labItem")) // 切换标签卡
		{
			std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator curIt = m_mapTableBrowser.find(static_cast<DuilibEx::CLabelItemUI*>(msg.pSender));
			m_ptlayBrowserClient->SelectItem(curIt->second);
			std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator it;

			for (it = m_mapTableBrowser.begin(); it != m_mapTableBrowser.end(); it++)
			{
				if (it != curIt)
					it->first->ApplyAttributeList(_T("bkcolor=\"#FFE1EBF5\""));
			}
			OnChangeView();
		}
	}
	else if (msg.sType == _T("click"))
	{
		/* 系统菜单栏 */
		if (msg.pSender->GetName() == _T("btnClose")) // 关闭
		{
			PostQuitMessage(0);
			return;
		}
		else if (msg.pSender->GetName() == _T("btnMin")) // 最小化
		{
			SendMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0);
			return;
		}
		else if (msg.pSender->GetName() == _T("btnMax")) // 最大化
		{
			SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE, 0);
			return;
		}
		else if ( msg.pSender->GetName() == _T("btnRestore")) // 恢复
		{
			SendMessage(WM_SYSCOMMAND, SC_RESTORE, 0);
			return;
		}
		else if (msg.pSender->GetName() == _T("btnLogin")) // 登录
		{
			m_pLoginWnd = new CLoginWnd;
			m_childWindows.push_back(m_pLoginWnd);

			m_pLoginWnd->Create(*this, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
			m_pLoginWnd->CenterWindow();
			m_pLoginWnd->ShowWindow(true);
			
		}
		else if(msg.pSender->GetName()==_T("btnBackward"))
		{
			GetCurentView()->GoBack();
		}
		else if(msg.pSender->GetName()==_T("btnForward"))
		{
			GetCurentView()->GoForward();
		}
		else if(msg.pSender->GetName()==_T("btnRefresh"))
		{
			GetCurentView()->Refresh();
		}

		/* 导航栏 */
		else if (msg.pSender == m_pbtnUrlList) // URL列表
		{
			CRect rect=m_peditUrl->GetPos();;
			POINT pt = {rect.left, rect.bottom};
			int nWidth = m_peditUrl->GetWidth();
			if(pUrlWnd!=NULL)
			{
				pUrlWnd->SetPos(pt,nWidth);
				return;
		}
		}
		else if (msg.pSender->GetName() == _T("btnAddFav"))
		{
			CAddFavoWnd* pAddFavoWnd = new CAddFavoWnd;
			m_childWindows.push_back(pAddFavoWnd);

			pAddFavoWnd->Create(*this, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
			pAddFavoWnd->CenterWindow();
			pAddFavoWnd->ShowWindow();
		}
		else if (msg.pSender->GetName() == _T("btnGoto")) // Goto按钮
		{
			OnClickGoto();
		}

		/* 工具栏控件 */
		else if (msg.pSender->GetName() == _T("btnGameDoctor")) // 游戏医生
		{
			::ShellExecute(*this, NULL, _T("GameDoctor.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnScriptRecord")) // 脚本录制
		{
			CScriptRecordWnd* pScriptRecordWnd = new CScriptRecordWnd;
			m_childWindows.push_back(pScriptRecordWnd);

			pScriptRecordWnd->Create(*this, _T("脚本录制"), WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
			pScriptRecordWnd->CenterWindow();
			pScriptRecordWnd->ShowWindow();
		}
		else if (msg.pSender->GetName() == _T("btnGameClock")) // 游戏闹钟
		{
			::ShellExecute(*this, NULL, _T("UzClock.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnIDCard")) // 身份证
		{
			::ShellExecute(*this, NULL, _T("IDCardGen.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnAnsweringMachine")) // 答题器
		{
			::ShellExecute(*this, NULL, _T("AnsweringMachine.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnUzShutDown")) // 自动关机
		{
			::ShellExecute(*this, NULL, _T("..\\Tools\\UzShutDown.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnSnapShot")) // 截图
		{
			::ShellExecute(*this, NULL, _T("..\\Tools\\SnapShot.exe"), NULL, NULL, SW_SHOWNORMAL);
		}
		else if (msg.pSender->GetName() == _T("btnBossKey")) // 老板键
		{
			m_pBossKeyWnd = new CBossKeyWnd;
			m_childWindows.push_back(m_pBossKeyWnd);

			m_pBossKeyWnd->Create(*this, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
			m_pBossKeyWnd->SetParent(this);
			m_pBossKeyWnd->CenterWindow();
			m_pBossKeyWnd->ShowWindow();
		}
		else if(msg.pSender->GetName()==_T("btn_setkey"))//键盘改键
		{
			CKeySetListMgr::Instance().ShowKeyListView(this);
		}
		else if (msg.pSender->GetName()==_T("btn_autospeak"))//自动喊话
		{
			CDuilibAutoSpeakDlg* pAutoSpeak=new CDuilibAutoSpeakDlg;
			m_childWindows.push_back(pAutoSpeak);

			pAutoSpeak->Create(*this, NULL, WS_OVERLAPPEDWINDOW | WS_SIZEBOX, 0);
			pAutoSpeak->CenterWindow();
			pAutoSpeak->ShowWindow();
			return;
		}
		/* 标签栏 */
		else if (msg.pSender == m_pbtnNewLabel) // 新建标签按钮
		{
			NewView();
		}
		else if (msg.pSender->GetName() == _T("btnLabClose")) // 关闭标签页
		{
			if(GetTabNum()==1)return;
			DuilibEx::CLabelItemUI* pDelCtl = static_cast<DuilibEx::CLabelItemUI*>(msg.pSender->GetParent());
			std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator it = m_mapTableBrowser.find(pDelCtl);
			m_phlayLablesLine->Remove(pDelCtl);
			int nCurSel = m_ptlayBrowserClient->GetCurSel();
			m_ptlayBrowserClient->Remove(it->second);
			m_mapTableBrowser.erase(it);
			m_ptlayBrowserClient->SelectItem(nCurSel-1);
			m_pbtnNewLabel->SetTag(m_pbtnNewLabel->GetTag()-1);
			CInterView *pView=(CInterView*)m_ptlayBrowserClient->GetItemAt(nCurSel-1);
			std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator curIt;
			for (curIt = m_mapTableBrowser.begin(); curIt != m_mapTableBrowser.end(); curIt++)
			{
				if(curIt->second==pView)
				{
					curIt->first->ApplyAttributeList(_T("bkcolor=\"#FFFFFFFF\""));
					break;
				}
			}

		}
		else if(msg.pSender->GetName()==_T("btn_dropdownlabel"))
		{
			((CMulTabBrowserApp*)AfxGetApp())->GetHisotry()->ShowRestoreLblMenu(msg.ptMouse);
		}
		else if (msg.pSender == m_pbtnHistoryList) // 标签栏历史记录按钮
		{
			CRect rect;
			GetWindowRect(*this ,&rect);
			POINT pt = {rect.right, rect.top};
			CLablHistoryListWnd* pHistoryList = new CLablHistoryListWnd(_T("label_history_list_wnd.xml"));
			pHistoryList->SetPos(*this, pt);
			pHistoryList->ShowWindow(TRUE);
		}

		/* 侧边功能栏 */
		else if (msg.pSender == m_pbtnHiddenSide) // 显示/隐藏侧边栏
		{
			m_vlaySideMenu->SetVisible(!m_vlaySideMenu->IsVisible());
			m_ptlaySideToolWnd->SetVisible(false);
			//CtnrWndStateSync();
		}

		/* 侧边栏工具选项 */
		else if (msg.pSender == m_poptAccount) // 账号按钮
		{
			ShowSideWnd(m_pSideAccountMgrWnd, 0);
		}
		else if (msg.pSender == m_poptSideFavs) // 收藏夹按钮
		{
			ShowSideWnd(m_pSideFavsWnd, 1);
		}
		else if (msg.pSender == m_poptSideHistory) // 历史记录按钮
		{
			ShowSideWnd(m_pSideHistoryWnd, 2);
		}
	}
	else if(msg.sType==DUI_MSGTYPE_EDIT_KEYDOWN)
	{
		if(msg.pSender->GetName()==_T("editUrl"))
		{
		
			if(msg.wParam==VK_DOWN||msg.wParam==VK_TAB)
			{
				if(pUrlWnd==NULL)
				{
					return;
				}
				if(!IsWindowVisible(pUrlWnd->GetHWND()))
				{
					return;
				}
				else 
				{
					pUrlWnd->EditPageDown();
				}
			}
			else if(msg.wParam==VK_UP)
			{
				if(pUrlWnd==NULL)
				{
					return;
				}
				if(!IsWindowVisible(pUrlWnd->GetHWND()))
				{
					return;
				}
				else
				{
					pUrlWnd->EditPageUp();
				}
			}
			else
			{
				CString strText=m_peditUrl->GetText();
				CRect rect=m_peditUrl->GetPos();;
				POINT pt = {rect.left, rect.bottom};
				int nWidth=m_peditUrl->GetWidth();
				if(pUrlWnd!=NULL)
				{
					pUrlWnd->SetPos(pt,nWidth);
					pUrlWnd->OnAddressEditChange(strText);
					return;
				}
			}
		}
	}
	else if(msg.sType==DUI_MSGTYPE_TEXTCHANGED)
	{
	
	}
	else if(msg.sType==DUI_MSGTYPE_KILLFOCUS)
	{
		if(msg.pSender->GetName()==_T("editUrl"))
		{
			pUrlWnd->ShowWindow(false);
		}
	}

}

// 创建窗口函数
LRESULT CBrowserWnd::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	Init();
	LONG styleValue = ::GetWindowLong(*this, GWL_STYLE);
	styleValue &= ~WS_CAPTION;
	::SetWindowLong(*this, GWL_STYLE, styleValue | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

	m_pm.Init(m_hWnd);

	DuiLib::CDialogBuilder builder;
	CDialogBuilderCallbackEx cb;
	DuiLib::CControlUI* pRoot = builder.Create(_T("Browser\\main.xml"), (UINT)0, &cb, &m_pm);
	ASSERT(pRoot && "Failed to parse XML");

	m_pm.AttachDialog(pRoot);
	m_pm.AddNotifier(this);

	return 0;
}

// 关闭窗口函数
LRESULT CBrowserWnd::OnClose(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

// 销毁窗口函数
LRESULT CBrowserWnd::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	::PostQuitMessage(0L);
	bHandled = FALSE;
	return 0;
}

//
LRESULT CBrowserWnd::OnNcActivate(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
	if (::IsIconic(*this))
	{
		bHandled = FALSE;
	}
	return (wParam == 0) ? TRUE : FALSE;
}

LRESULT CBrowserWnd::OnNcCalcSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CBrowserWnd::OnNcPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	return 0;
}

LRESULT CBrowserWnd::OnNcHitTest(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& /*bHandled*/)
{
	POINT pt;
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	::ScreenToClient(*this, &pt);

	RECT rcClient;
	::GetClientRect(*this, &rcClient);

	if ( !::IsZoomed(*this) ) {
		RECT rcSizeBox = m_pm.GetSizeBox();
		if ( pt.y < rcClient.top + rcSizeBox.top )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTTOPLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTTOPRIGHT;
			return HTTOP;
		}
		else if ( pt.y > rcClient.bottom - rcSizeBox.bottom )
		{
			if ( pt.x < rcClient.left + rcSizeBox.left )
				return HTBOTTOMLEFT;
			if ( pt.x > rcClient.right - rcSizeBox.right )
				return HTBOTTOMRIGHT;
			return HTBOTTOM;
		}
		if ( pt.x < rcClient.left + rcSizeBox.left )
			return HTLEFT;
		if ( pt.x > rcClient.right - rcSizeBox.right )
			return HTRIGHT;
	}

	RECT rcCaption = m_pm.GetCaptionRect();
	if ( pt.x >= rcClient.left + rcCaption.left && pt.x < rcClient.right - rcCaption.right
			&& pt.y >= rcCaption.top && pt.y < rcCaption.bottom )
	{
		DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_pm.FindControl(pt));
		if ( pControl && _tcsicmp(pControl->GetClass(), _T("ButtonUI")) != 0
				&& _tcsicmp(pControl->GetClass(), _T("OptionUI")) != 0)
			return HTCAPTION;
	}

	return HTCLIENT;
}

LRESULT CBrowserWnd::OnSize(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	SIZE szRoundCorner = m_pm.GetRoundCorner();
	if ( !::IsIconic(*this) && (szRoundCorner.cx != 0 || szRoundCorner.cy != 0) )
	{
		RECT rcClient;
		::GetClientRect(*this, &rcClient);
		RECT rc = { rcClient.left, rcClient.top + szRoundCorner.cx, rcClient.right, rcClient.bottom };
		HRGN hRgn1 = ::CreateRectRgnIndirect( &rc );
		HRGN hRgn2 = ::CreateRoundRectRgn(rcClient.left, rcClient.top, rcClient.right + 1,
			rcClient.bottom - szRoundCorner.cx, szRoundCorner.cx, szRoundCorner.cy);
		::CombineRgn( hRgn1, hRgn1, hRgn2, RGN_OR );
		::SetWindowRgn(*this, hRgn1, TRUE);
		::DeleteObject(hRgn1);
		::DeleteObject(hRgn2);
	}

	bHandled = FALSE;
	return 0;
}

LRESULT CBrowserWnd::OnGetMinMaxInfo(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled)
{
	MONITORINFO oMonitor = {};
	oMonitor.cbSize = sizeof(oMonitor);
	::GetMonitorInfo(::MonitorFromWindow(*this, MONITOR_DEFAULTTOPRIMARY), &oMonitor);
	CRect rcWork = oMonitor.rcWork;
	rcWork.OffsetRect(-rcWork.left, -rcWork.top);

	LPMINMAXINFO lpMMI = (LPMINMAXINFO) lParam;
	lpMMI->ptMaxPosition.x = rcWork.left;
	lpMMI->ptMaxPosition.y = rcWork.top;
	lpMMI->ptMaxSize.x     = rcWork.right;
	lpMMI->ptMaxSize.y     = rcWork.bottom;

	bHandled = FALSE;
	return 0;
}

LRESULT CBrowserWnd::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	if ( wParam == SC_CLOSE )
	{
		::PostQuitMessage(0L);
		bHandled = TRUE;
		return 0;
	}
	BOOL bZoomed = ::IsZoomed(*this);
	LRESULT lRes = DuiLib::CWindowWnd::HandleMessage(uMsg, wParam, lParam);
	if (::IsZoomed(*this) != bZoomed)
	{
		DuiLib::CControlUI* pbtnMax = m_pm.FindControl(_T("btnMax"));
		DuiLib::CControlUI* pbtnRestore = m_pm.FindControl(_T("btnRestore"));
		if (!bZoomed )
		{
			pbtnMax->SetVisible(false);
			pbtnRestore->SetVisible(true);
		}
		else
		{
			pbtnMax->SetVisible(true);
			pbtnRestore->SetVisible(false);
		}
	}
	return lRes;
}

void CBrowserWnd::SetUrlEditText(DuiLib::CDuiString strText)
{
	m_peditUrl->SetText(strText);
	m_peditUrl->SetSel(strText.GetLength(),-1);
}

LRESULT CBrowserWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL bHandled = TRUE;
	switch (uMsg)
	{
	case WM_CREATE:
		lRes = OnCreate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_CLOSE:
		lRes = OnClose(uMsg, wParam, lParam, bHandled);
		break;
	case WM_DESTROY:
		lRes = OnDestroy(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCACTIVATE:
		lRes = OnNcActivate(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCCALCSIZE:
		lRes = OnNcCalcSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCPAINT:
		lRes = OnNcPaint(uMsg, wParam, lParam, bHandled);
		break;
	case WM_NCHITTEST:
		lRes = OnNcHitTest(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SIZE:
		lRes = OnSize(uMsg, wParam, lParam, bHandled);
		break;
	case WM_GETMINMAXINFO:
		lRes = OnGetMinMaxInfo(uMsg, wParam, lParam, bHandled);
		break;
	case WM_SYSCOMMAND:
		lRes = OnSysCommand(uMsg, wParam, lParam, bHandled);
		break;
	case WM_KEYDOWN:
		{
			switch (wParam)
			{
			case VK_F11:
				OnFullScreen();
				break;
			}
		}
		break;
	case WM_HOTKEY:
		{
			if (static_cast<int>(wParam) == m_nBossKeyId)
			{
				OnBossKey();
				break;
			}
		}
		break;
	default:
		bHandled = FALSE;
	}
	if (bHandled)
	{
		return lRes;
	}
	if (m_pm.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return CWindowWnd::HandleMessage(uMsg, wParam, lParam);
}

CBrowserWnd::~CBrowserWnd(void) {}

void CBrowserWnd::NewView(CString str)
{
	
	DuilibEx::CLabelItemUI* pNewLabel = new DuilibEx::CLabelItemUI;

	m_phlayLablesLine->Add(pNewLabel);
	//m_pbtnNewLabel->SetTag(m_pbtnNewLabel->GetTag()+1);
	
	CInterView* pView = new CInterView;;
	//pView->SetWebBrowserEventHandler(m_pWebBrowserEventHandler);

	m_ptlayBrowserClient->Add(pView);
	m_ptlayBrowserClient->SelectItem(pView);
	
	pView->UzNavigate(str);

	m_mapTableBrowser.insert(pair<DuilibEx::CLabelItemUI*, CInterView*>(pNewLabel, pView));

	std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator curIt = m_mapTableBrowser.find(pNewLabel);
	m_ptlayBrowserClient->SelectItem(curIt->second);
	std::map<DuilibEx::CLabelItemUI*, CInterView*>::iterator it;

	pNewLabel->ApplyAttributeList(_T("bkcolor=\"#FFFFFFFF\""));
	for (it = m_mapTableBrowser.begin(); it != m_mapTableBrowser.end(); it++)
	{
		if (it != curIt)
			it->first->ApplyAttributeList(_T("bkcolor=\"#FFE1EBF5\""));
	}
}

CInterView* CBrowserWnd::GetCurentView()
{	
	int nCurSel = m_ptlayBrowserClient->GetCurSel();

	CInterView* pView=(CInterView*)m_ptlayBrowserClient->GetItemAt(nCurSel);
	if(pView!=NULL)
		return pView;
	return NULL;
}

// 窗口全屏处理函数
void CBrowserWnd::OnFullScreen()
{
	if (m_bFullScreen)
	{
		EndFullScreen();
		return;
	}
	::GetWindowRect(*this, &m_oldScreenRect);

	// 获取屏幕大小
	int nFullHeight = GetSystemMetrics(SM_CYSCREEN);
	int nFullWidth = GetSystemMetrics(SM_CXSCREEN);
	
	// 标题栏
	DuiLib::CHorizontalLayoutUI* phlayHeader = 
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayHeader")));
	phlayHeader->SetVisible(false);

	// 导航栏
	DuiLib::CHorizontalLayoutUI* phlayNavigationBar =
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayNavigationBar")));
	phlayNavigationBar->SetVisible(false);

	// 工具栏
	DuiLib::CHorizontalLayoutUI* phlayTools =
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayTools")));
	phlayTools->SetVisible(false);

	::MoveWindow(*this, 0, 0, nFullWidth, nFullHeight, TRUE);

	m_bFullScreen = true;
}

void CBrowserWnd::EndFullScreen()
{
	// 标题栏
	DuiLib::CHorizontalLayoutUI* phlayHeader = 
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayHeader")));
	phlayHeader->SetVisible(true);

	// 导航栏
	DuiLib::CHorizontalLayoutUI* phlayNavigationBar =
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayNavigationBar")));
	phlayNavigationBar->SetVisible(true);

	// 工具栏
	DuiLib::CHorizontalLayoutUI* phlayTools =
		static_cast<DuiLib::CHorizontalLayoutUI*>(m_pm.FindControl(_T("hlayTools")));
	phlayTools->SetVisible(true);

	::MoveWindow(*this, m_oldScreenRect.left, m_oldScreenRect.top,
		m_oldScreenRect.right-m_oldScreenRect.left, m_oldScreenRect.bottom-m_oldScreenRect.top, TRUE);

	m_bFullScreen = false;
}

void CBrowserWnd::OnBossKey()
{
	// 隐藏所有子窗口
	for (unsigned int i = 0; i < m_childWindows.size(); i++)
		m_childWindows[i]->ShowWindow(IsWindowVisible(*this) ? false : true);
	this->ShowWindow(IsWindowVisible(*this) ? false : true);
}
int CBrowserWnd::GetTabNum()
{
	return m_mapTableBrowser.size();
}

CInterView*CBrowserWnd::GetViewByPos(int nSel)
{
	CInterView* pView=NULL;
	pView=(CInterView*)m_ptlayBrowserClient->GetItemAt(nSel);
	if(pView!=NULL)
		return pView;
	return NULL;
}


void CBrowserWnd::OnChangeView()
{
	CInterView*pCurrentView=GetCurentView();
	if(pCurrentView!=NULL)
	{
		pCurrentView->GetCurrentKeyMess()->OnViewChange();
	}
}


void CBrowserWnd::OnClickGoto()
{
	if(::IsWindowVisible(pUrlWnd->GetHWND()))
	{
		pUrlWnd->ShowWindow(false);
	}
	CString strUrl=m_peditUrl->GetText();
	CInterView*pView=GetCurentView();
	pView->UzNavigate(strUrl);
}

void CBrowserWnd::ShowSideWnd(CSideWndBase* pSideWnd, int nIndex)
{
	if (pSideWnd->IsLock())
	{
		m_dockWnds.push_back(pSideWnd);
		m_ptlaySideToolWnd->SetVisible(false);
		::ShowWindow(*pSideWnd, !IsWindowVisible(*pSideWnd));
		for (int i = 0; i < m_dockWnds.size(); i++)
		{
			if (m_dockWnds[i] == pSideWnd)
				continue;
			::ShowWindow(*(m_dockWnds[i]), SW_HIDE);
		}
	}
	else
	{
		if (m_dockWnds.size() != 0)
		{
			std::vector<CSideWndBase*>::iterator it =  find(m_dockWnds.begin(), m_dockWnds.end(), pSideWnd);
			m_dockWnds.erase(it);
		}
		
		if (m_ptlaySideToolWnd->GetCurSel() == nIndex)
		{
			m_ptlaySideToolWnd->SetVisible(!m_ptlaySideToolWnd->IsVisible());
		}
		else
		{
			m_ptlaySideToolWnd->SetVisible(true);
			m_ptlaySideToolWnd->SelectItem(nIndex);
		}
	}
}

	
	
	




