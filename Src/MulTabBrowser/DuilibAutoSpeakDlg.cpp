#include "StdAfx.h"
#include "DuilibAutoSpeakDlg.h"

#include "GlobalFunc.h"
#include "resource.h"
#include "InterView.h"
#include "BrowserWnd.h"
#include "MulTabBrowser.h" 

CAutoSpeak CDuilibAutoSpeakDlg::s_AutoSpeak;

CDuilibAutoSpeakDlg::CDuilibAutoSpeakDlg(void)
{
	m_bMouseClickCapture_edit=FALSE;
	m_bMouseClickCapture_send=FALSE;
	m_Autospeak=NULL;
}


CDuilibAutoSpeakDlg::~CDuilibAutoSpeakDlg(void)
{
}


LPCTSTR CDuilibAutoSpeakDlg::GetWindowClassName()const
{
	return _T("DuilibAutoSpeakDlg");
}

DuiLib::CDuiString CDuilibAutoSpeakDlg::GetSkinFile()
{
	return _T("AutoSpeakSkin//AutoSpeak.xml");
}

DuiLib::CDuiString CDuilibAutoSpeakDlg::GetSkinFolder()
{
	return _T("");
	
}

void CDuilibAutoSpeakDlg::InitWindow()
{
	m_pList=static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("ListSpeak")));
	HMODULE hMod = LoadLibrary(GetExePath()+ _T("\\SpeakAid.dll"));
	if (NULL != hMod)
	{
		m_pfnSpeak = (Pfn_Speak2)GetProcAddress(hMod, "Speak2");
	}
	ReLoad();
	::SetTimer(GetHWND(),1,1000,NULL);
}

void CDuilibAutoSpeakDlg::Notify(DuiLib::TNotifyUI& msg)
{
	if(msg.sType==DUI_MSGTYPE_SELECTCHANGED)
	{
		DuiLib::CDuiString name = msg.pSender->GetName();
		DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintManager.FindControl(_T("switch")));
		if(name==_T("speak_option"))	
		{
			pControl->SelectItem(0);
			ReLoad();
			return;
		}	
		else if(name==_T("recycle_option"))
		{
			pControl->SelectItem(1);
			OnShowRecycleItems();
			return;
		}
	}
	if(msg.sType==DUI_MSGTYPE_CLICK)
	{
		if(msg.pSender->GetName()==_T("closebtn"))
		{
			this->Close();
			return;
		}
		else if(msg.pSender->GetName()==_T("btn_save"))
		{
			OnBnClickSave();
			return;
		}
		else if(msg.pSender->GetName()==_T("btn_begin_speak"))
		{
			OnBeginSpeak();
			return;
		}
		else if(msg.pSender->GetName()==_T("btn_reflash"))
		{
			ReLoad();
			return;
		}
		else if(msg.pSender->GetName()==_T("btn_stop_all"))
		{
			OnStopAllSpeak();
			return;
		}
		else if(msg.pSender->GetName()==_T("minbtn"))
		{
			SendMessage(WM_SYSCOMMAND, SC_MINIMIZE, 0); 
			return; 
		}
		else if(msg.pSender->GetName()==_T("speak_all_check"))
		{
			for(int i=0;i<m_pList->GetCount();i++)
			{
				DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
				DuiLib::CCheckBoxUI*pCheck=(DuiLib::CCheckBoxUI*)pElement->FindSubControl(_T("item_check"));
				if(!pCheck->GetCheck())
				{
					pCheck->SetCheck(true);
				}
				else
				{
					pCheck->SetCheck(false);
				}
			}
			return;
		}
		else if(msg.pSender->GetName()==_T("recycle_all_check"))
		{
			DuiLib::CListUI* pRecyclelist=(DuiLib::CListUI*)m_PaintManager.FindControl(_T("ListRecycle"));
			for(int i=0;i<pRecyclelist->GetCount();i++)
			{
				DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)pRecyclelist->GetItemAt(i);
				DuiLib::CCheckBoxUI*pCheck=(DuiLib::CCheckBoxUI*)pElement->FindSubControl(_T("item_check"));
				if(!pCheck->GetCheck())
				{
					pCheck->SetCheck(true);
				}
				else
				{
					pCheck->SetCheck(false);
				}
			}
			return;
		}
		else if(msg.pSender->GetName()==_T("labelbtn_startcheck"))
		{
			OnStartCheckSpeak();
			return;
		}
		else if(msg.pSender->GetName()==_T("labelbtn_stopcheck"))
		{
			OnStopCheckSpeak();
			return;
		}
		else if(msg.pSender->GetName()==_T("labelbtn_restorecheck"))
		{
			OnRestoreCheck();
		}


	}
	else if(msg.sType== DUI_MSGTYPE_ITEMCLICK)
	{
		if(msg.pSender->GetName()==_T("Autospeak_listElement"))
		{
			int nSel = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();
			if(-1==nSel)return;
			if(m_PaintManager.FindControl(msg.ptMouse)->GetName()==_T("item_del"))
			{

				DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(nSel);
				DuiLib::CLabelUI*m_text=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_windowhwnd"));
				CString strWnd=m_text->GetText();
				OnDel(nSel,strWnd);
			}
			else if(m_PaintManager.FindControl(msg.ptMouse)->GetName()==_T("item_oprate"))
			{
				DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(nSel);
				DuiLib::CLabelUI*m_text=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_windowhwnd"));
				CString strWnd=m_text->GetText();
				OnOperate(strWnd);

			}
			else if(m_PaintManager.FindControl(msg.ptMouse)->GetName()==_T("item_play"))
			{
				DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(nSel);
				DuiLib::CLabelUI*m_text=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_windowhwnd"));
				CString strWnd=m_text->GetText();
				OnRun(nSel,strWnd);
			}
		}
		else if(msg.pSender->GetName()==_T("Recycel_listElement"))
		{
			int nSel = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();
			if(-1==nSel)return;
			if(m_PaintManager.FindControl(msg.ptMouse)->GetName()==_T("item_restore"))
			{
				DuiLib::CListUI* pRecycle = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("ListRecycle")));
				DuiLib::CListContainerElementUI* pElement=(DuiLib::CListContainerElementUI*)pRecycle->GetItemAt(nSel);
				DuiLib::CLabelUI*pWnd=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_windowhwnd"));
				CString strWnd=pWnd->GetText();
				OnRestore(nSel,strWnd);
			}
		}
	}
}



LRESULT CDuilibAutoSpeakDlg::OnLButtonDown(UINT , WPARAM , LPARAM lParam, BOOL& bHandled)
{

	POINT pt; 
	pt.x = GET_X_LPARAM(lParam);
	pt.y = GET_Y_LPARAM(lParam);
	DuiLib::CControlUI* pControl = static_cast<DuiLib::CControlUI*>(m_PaintManager.FindControl(pt));
	if(pControl==NULL)return 0;
	if(pControl->GetName()==_T("MouseClickIcon_edit"))
	{
		HCURSOR hCur = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_RED));
		SetSystemCursor(hCur, 32512);
		ShowWindow(SW_HIDE);
		::ShowWindow(GetParent(this->m_hWnd),SW_SHOW);
		SetCapture(this->m_hWnd);
		m_bMouseClickCapture_edit=true;
		bHandled=FALSE;
		return 0;
	}
	else if(pControl->GetName()==_T("MouseClickIcon_send"))
	{
		HCURSOR hCur = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_CURSOR_GREEN));
		SetSystemCursor(hCur, 32512);
		ShowWindow(SW_HIDE);
		SetCapture(this->m_hWnd);
		m_bMouseClickCapture_send=true;
		bHandled=FALSE;
		return 0;
	}
	bHandled=FALSE;
	return 0;
}

LRESULT CDuilibAutoSpeakDlg::OnLButtonUp(UINT , WPARAM , LPARAM lParam, BOOL& bHandled)
{

	SystemParametersInfo(SPI_SETCURSORS, 0, NULL, SPIF_SENDCHANGE);
	if(m_bMouseClickCapture_edit||m_bMouseClickCapture_send)
	{
		CString str=_T("");
		POINT pt; 
		pt.x = GET_X_LPARAM(lParam);
		pt.y = GET_Y_LPARAM(lParam);
		ClientToScreen(GetHWND(),&pt);
		HWND hWnd = ::WindowFromPoint(pt);
		if(m_Autospeak->GetSpeakWnd() == hWnd)
		{
			POINT ptInWnd=pt;
			::ScreenToClient(hWnd, &ptInWnd);
			CString strText = _T("");
			if(m_bMouseClickCapture_edit)
			{
				m_Autospeak->SetSpeakEditPt(ptInWnd);
				strText.Format(L"设置成功，坐标(%d,%d)",ptInWnd.x,ptInWnd.y);
				DuiLib::CLabelUI* MouseEdit=static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_edit_point")));
				MouseEdit->SetText(strText);
			}
			if(m_bMouseClickCapture_send)
			{
				m_Autospeak->SetSpeakButtonPt(ptInWnd);
				strText.Format(_T("设置成功，坐标(%d,%d)"),ptInWnd.x,ptInWnd.y);
				DuiLib::CLabelUI* MouseSender=static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_send_point")));
				MouseSender->SetText(strText);
			}
			m_Autospeak->SetSpeakWnd(hWnd);
		}
		else
		{
			MessageBox(GetHWND(),_T("无效的目标窗口,目标窗口必须是与标签页对应的FLASH窗口！"),_T("错误"),MB_OK);

		}
		if(m_bMouseClickCapture_edit)m_bMouseClickCapture_edit=FALSE;
		if(m_bMouseClickCapture_send)m_bMouseClickCapture_send=FALSE;
		ReleaseCapture();
		ShowWindow(true);
	}
	bHandled = FALSE;
	return 0;
}

LRESULT CDuilibAutoSpeakDlg::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	if(uMsg==WM_NCLBUTTONDBLCLK)
	{
		return 0;
	}
	else if(uMsg==WM_TIMER)
	{
		OnTimer(wParam);
		return 0;
	}
	return __super::HandleMessage(uMsg, wParam, lParam);
}

LRESULT CDuilibAutoSpeakDlg::OnTimer(WPARAM wParam)
{
	if(1==wParam)
	{
		if(!m_lstAutoSpeak.empty() && m_pfnSpeak!=NULL)
		{
			//HWND hMain = NULL;
			std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
			for(;it!=m_lstAutoSpeak.end();it++)
			{
				if(it->IsEnable() && !it->IsInRecycle() && it->IsSet())
				{
					if(it->IsSpeakTIme())
					{
						if(::IsWindowVisible(GetHWND()) || 
							IsIconic(GetHWND()) ||
							AfxGetMainWnd()->IsIconic())  //只有当窗口没有隐藏的时候才喊话，窗口被隐藏的时候可能在喊话的时候对另一个窗口进行了设置。
						{
							it->NextSpeakOrder();
							m_pfnSpeak(it->GetSpeakWnd(),it->GetSpeakEditPt(),it->GetSpeakButtonPt(),it->GetSpeakString());	
						}
					}		
				}
			}
		}
	}
	return 0;
}

void CDuilibAutoSpeakDlg::ReLoad()
{
	if(m_lstAutoSpeak.empty())
	{
		LoadCurrentList();
	}
	else
	{
		RefreshList();
	}
}

void CDuilibAutoSpeakDlg::RefreshList()
{

	m_pList->RemoveAll();
	CString strTitle =_T("");
	CString strLink  =_T("");

	int iTabsNum =theApp.pBrowser->GetTabNum();
	for (int i = 1; i <= iTabsNum; i++)
	{
		CInterView* pWnd =theApp.pBrowser->GetViewByPos(i);
		if (pWnd)
		{
			HWND hChildwnd =NULL;
			GetSpeakWnd(pWnd->GetHostWindow(),_T("MacromediaFlashPlayerActiveX"),hChildwnd);
			if(hChildwnd !=NULL)
			{
				strLink = pWnd->GetUrl();
				std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
				for(;it!=m_lstAutoSpeak.end();it++)
				{
					if(it->GetSpeakWnd() == hChildwnd)
					{
						break;
					}
				}
				if(it==m_lstAutoSpeak.end())
				{
					CAutoSpeak as;
					as.SetLink(strLink);
					as.SetSpeakWnd(hChildwnd);
					as.SetLabelWnd(pWnd->GetHostWindow());
					as.SetTitle(_T("街机三国"));
					m_lstAutoSpeak.push_back(as);
				}
				else
				{
					it->SetLink(strLink);
					it->SetSpeakWnd(hChildwnd);
					it->SetTitle(_T("街机三国"));
					it->SetLabelWnd(pWnd->GetHostWindow());
				}
			}
		}
	}
	if(m_lstAutoSpeak.size() != 0)
	{
		std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
		std::list<CAutoSpeak>::iterator it1;
		int count = 0;
		HWND hWnd = NULL;
		for(;it!=m_lstAutoSpeak.end();it++)
		{
			hWnd = it->GetSpeakWnd();
			if(!IsWindow(hWnd))
			{
				it1 = it;
				it++;
				m_lstAutoSpeak.erase(it1);
				continue;
			}
			if(it->IsInRecycle())
			{
				it++;
				continue;
			}
			count = m_pList->GetCount();

			DuiLib::CDialogBuilder builder;
			DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("AutoSpeakSkin//listElement.xml"), (UINT)0, NULL, &m_PaintManager);  
			DuiLib::CLabelUI*m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
			m_KeyValue->SetText(it->GetTitle());

			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_state"));
			if(!it->IsEnable())
			{
				m_KeyValue->SetText(_T("自动喊话未启动"));
				m_KeyValue->SetTextColor(0xFFFF0000);
			}
			else
			{
				m_KeyValue->SetText(_T("自动喊话已启动"));
				m_KeyValue->SetTextColor(0xFF00FF00);
			}

			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_tip"));
			if(!it->IsSet())
			{
				m_KeyValue->SetText(_T("未设置"));
				m_KeyValue->SetTextColor(0xFFFF0000);
			}
			else
			{
				m_KeyValue->SetText(_T("已设置"));
				m_KeyValue->SetTextColor(0xFF00FF00);
			}

			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_windowhwnd"));
			m_KeyValue->SetText(it->GetSpeakWndStr());	
			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_labelhwnd"));
			m_KeyValue->SetText(it->GetLabelWndStr());
			m_pList->AddAt(pRoot,count);
		}
	}
}

void CDuilibAutoSpeakDlg::LoadCurrentList()
{
	
	m_lstAutoSpeak.clear();
	m_pList->RemoveAll();
	CString strTitle =_T("");
	CString strLink  = _T("");
	int iTabsNum =theApp.pBrowser->GetTabNum();
	for (int i = 1; i <= iTabsNum; i++)
	{
		CInterView* pWnd =theApp.pBrowser->GetViewByPos(i);
		if (pWnd)
		{
			HWND hChildwnd =NULL;
			GetSpeakWnd(pWnd->GetHostWindow(),_T("MacromediaFlashPlayerActiveX"),hChildwnd);
			if(hChildwnd !=NULL)
			{
				CAutoSpeak as;
				strLink = pWnd->GetUrl();
				as.SetLink(strLink);
				as.SetSpeakWnd(hChildwnd);
				as.SetLabelWnd(pWnd->GetHostWindow());
				as.SetTitle(_T("街机三国"));
				m_lstAutoSpeak.push_back(as);
			}
		}
	}

	if(m_lstAutoSpeak.size() != 0)
	{
		std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
		int count = 0;
		for(;it!=m_lstAutoSpeak.end();it++)
		{
			count = m_pList->GetCount();

			DuiLib::CDialogBuilder builder;
			DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("AutoSpeakSkin//listElement.xml"), (UINT)0, NULL, &m_PaintManager);  
			DuiLib::CLabelUI*m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
			CString str=it->GetTitle();
			m_KeyValue->SetText(str);

			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_state"));
			if(!it->IsEnable())
			{
				m_KeyValue->SetText(_T("自动喊话未启动"));
				m_KeyValue->SetTextColor(0xFFFF0000);
			}
			else
			{
				m_KeyValue->SetText(_T("自动喊话已启动"));
				m_KeyValue->SetTextColor(0xFF00FF00);
			}
			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_tip"));
			if(!it->IsSet())
			{
				m_KeyValue->SetText(_T("未设置"));
				m_KeyValue->SetTextColor(0xFFFF0000);
			}
			else
			{
				m_KeyValue->SetText(_T("已设置"));
				m_KeyValue->SetTextColor(0xFF00FF00);
			}

			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_windowhwnd"));
			m_KeyValue->SetText(it->GetSpeakWndStr());	
			m_KeyValue=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_labelhwnd"));
			m_KeyValue->SetText(it->GetLabelWndStr());
			m_pList->AddAt(pRoot,count);
		}
	}
}

void CDuilibAutoSpeakDlg::GetSpeakWnd(HWND ParentWnd,CString strSpeakWndClass,HWND& h) 
{ 
	HWND hChild = ::GetWindow(ParentWnd, GW_CHILD); 
	for(; hChild!=NULL ; hChild=::GetWindow(hChild,GW_HWNDNEXT)) 
	{ 
		TCHAR ClassName[30]={0}; 
		::GetClassName(hChild,ClassName,sizeof(ClassName)/sizeof(TCHAR)); 
		if(strSpeakWndClass == ClassName)
		{
			h = hChild;
			if(!::IsWindowVisible(h))
				continue;
			if(IsSmallWnd(h))
				continue;
			return;
		}
		else
		{
			h = NULL;
		}
		GetSpeakWnd(hChild,strSpeakWndClass,h); 
	} 
} 

bool CDuilibAutoSpeakDlg::IsSmallWnd(HWND h)
{
	CRect rect;
	::GetWindowRect(h,&rect);
	if(rect.right-rect.left < 50 || rect.bottom - rect.top < 50)
		return TRUE;
	return FALSE;
}

CAutoSpeak& CDuilibAutoSpeakDlg::GetSelectASItem(CString strWnd)
{
	std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
	CString strText = L"";
	for(;it!=m_lstAutoSpeak.end();it++)
	{
		strText = it->GetSpeakWndStr();
		if( strText == strWnd)
		{
			return *it;
		}
	}
	return s_AutoSpeak;
}

void CDuilibAutoSpeakDlg::OnOperate(CString strWnd)
{
	DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintManager.FindControl(_T("switch")));
	pControl->SelectItem(2);
	DuiLib::COptionUI* pOption = static_cast<DuiLib::COptionUI*>(m_PaintManager.FindControl(_T("speak_option")));
	pOption->Selected(false);
	CAutoSpeak& as=GetSelectASItem(strWnd);
	if(!IsWindow(as.GetSpeakWnd()))
	{
		MessageBox(this->GetHWND(),_T("指定游戏窗口无效或者不存在!"),_T("提示"),MB_OK);
		return;
	}
	if(as.IsSet())
	{
		DuiLib::CEditUI* pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content1")));
		if(pEdit!=NULL)pEdit->SetText(as.GetContent1());
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content2")));
		if(pEdit!=NULL)pEdit->SetText(as.GetContent2());
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content3")));
		if(pEdit!=NULL)pEdit->SetText(as.GetContent3());
		CString strSpeakEditPos = _T("");
		strSpeakEditPos.Format(_T("设置成功，坐标(%d,%d)"),as.GetSpeakEditPt().x,as.GetSpeakEditPt().y);
		DuiLib::CLabelUI* pLabel = static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_edit_point")));
		if(pLabel!=NULL)pLabel->SetText(strSpeakEditPos);
		CString strSpeakButtonPos = _T("");
		strSpeakButtonPos.Format(_T("设置成功，坐标(%d,%d)"),as.GetSpeakButtonPt().x,as.GetSpeakButtonPt().y);
		pLabel = static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_send_point")));
		if(pLabel!=NULL)pLabel->SetText(strSpeakButtonPos);
		CString strText = _T("");
		strText.Format(_T("%d"),as.GetIntervalTime());
		DuiLib::CComboUI* pCombo = static_cast<DuiLib::CComboUI*>(m_PaintManager.FindControl(_T("time_JG")));
		if(pCombo!=NULL)pCombo->SetText(strText);
		if(as.GetAddRandString())
		{
			DuiLib::CCheckBoxUI* pCheck = static_cast<DuiLib::CCheckBoxUI*>(m_PaintManager.FindControl(_T("page3_all_check")));
			if(pCheck!=NULL)pCheck->SetCheck(true);
		}
	}
	else
	{
		DuiLib::CEditUI* pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content1")));
		if(pEdit!=NULL)pEdit->SetText(_T("求关注!!!"));
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content2")));
		if(pEdit!=NULL)pEdit->SetText(_T("求组队!!!"));
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content3")));
		if(pEdit!=NULL)pEdit->SetText(_T("求鲜花!!!"));
		DuiLib::CLabelUI* pLabel = static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_edit_point")));
		if(pLabel!=NULL)pLabel->SetText(_T("到聊天输入框完成输入定位"));
		pLabel = static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("text_send_point")));
		if(pLabel!=NULL)pLabel->SetText(_T("到聊天发送框完成发送定位"));
		DuiLib::CComboUI* pCombo = static_cast<DuiLib::CComboUI*>(m_PaintManager.FindControl(_T("time_JG")));
		if(pCombo!=NULL)pCombo->SelectItem(0);
		DuiLib::CCheckBoxUI* pCheck = static_cast<DuiLib::CCheckBoxUI*>(m_PaintManager.FindControl(_T("page3_all_check")));
		if(pCheck!=NULL)pCheck->SetCheck(false);
	}
	
	DuiLib::CLabelUI* pLabeltitle = static_cast<DuiLib::CLabelUI*>(m_PaintManager.FindControl(_T("label_game_title")));
	pLabeltitle->SetText(as.GetTitle());
	m_Autospeak=&as;
}

void CDuilibAutoSpeakDlg::OnRun(int nSel,CString strWnd)
{
	std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
	for(;it!=m_lstAutoSpeak.end();it++)
	{
		CString strText = it->GetSpeakWndStr();
		if( strText == strWnd)
		{
			if(it->IsEnable())
			{
				it->SetEnable(FALSE);
			}
			else
			{
				if(!it->IsSet())
				{
					MessageBox(this->GetHWND(),_T("请先设置该记录!"),_T("提醒"),MB_OK);
					return ;
				}
				it->SetEnable(true);
			}
			DuiLib::CListContainerElementUI* pRoot=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(nSel);
			DuiLib::CLabelUI*p_state=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_state"));
			if(!it->IsEnable())
			{
				p_state->SetText(_T("自动喊话未启动"));
				p_state->SetTextColor(0xFFFF0000);
				DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_play"));
				pIcon->SetBkImage(_T("file='AutoSpeakSkin//speak2.png' source='17,0,34,13'"));
			}
			else
			{
				p_state->SetText(_T("自动喊话已启动"));
				p_state->SetTextColor(0xFF00FF00);
				DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_play"));
				pIcon->SetBkImage(_T("file='AutoSpeakSkin//speak1.png' source='17,0,34,13'"));
			}
			break;
		}
	}
}

void CDuilibAutoSpeakDlg::OnDel(int nSel,CString strWnd)
{
	std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
	for(;it!=m_lstAutoSpeak.end();it++)
	{
		CString strText = it->GetSpeakWndStr();
		if( strText == strWnd)
		{
			if(!it->IsInRecycle())
			{
				it->SetInRecycle(TRUE);
				m_pList->RemoveAt(nSel);
			}
			break;
		}
	}
}

void CDuilibAutoSpeakDlg::OnRestore(int nSel,CString strWnd)
{
	DuiLib::CListUI* pRecycle = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("ListRecycle")));
	std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
	for(;it!=m_lstAutoSpeak.end();it++)
	{
		CString strText = it->GetSpeakWndStr();
		if( strText == strWnd)
		{
			if(it->IsInRecycle())
			{
				it->SetInRecycle(FALSE);
				pRecycle->RemoveAt(nSel);
			}
			break;
		}
	}
}

void CDuilibAutoSpeakDlg::OnStopAllSpeak()
{
	if(!m_lstAutoSpeak.empty())
	{
		std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
		for(;it!=m_lstAutoSpeak.end();it++)
		{
			if(it->IsEnable())
			{
				it->SetEnable(FALSE);
			}
		}
		int nCount = m_pList->GetCount();
		for(int i=0;i<nCount;i++)
		{
			DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
			DuiLib::CLabelUI*pState=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_state"));
			pState->SetText(_T("自动喊话未启动"));
			pState->SetTextColor(0xFFFF0000);
			DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_play"));
			pIcon->SetBkImage(_T("file='AutoSpeakSkin//speak2.png' source='17,0,34,13'"));
		}
	}
}

void CDuilibAutoSpeakDlg::OnStartCheckSpeak()
{
	int nCount = m_pList->GetCount();
	for(int i=0;i<nCount;i++)
	{	
		DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
		DuiLib::CLabelUI*pWnd=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_windowhwnd"));
		CString strWnd=pWnd->GetText();
		DuiLib::CCheckBoxUI*pCheck=(DuiLib::CCheckBoxUI*)pElement->FindSubControl(_T("item_check"));
		if(pCheck->GetCheck())
		{
			if(!m_lstAutoSpeak.empty())
			{
				std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
				for(;it!=m_lstAutoSpeak.end();it++)
				{
					if(strWnd==it->GetSpeakWndStr())
					{
						if(!it->IsSet())
						{
							MessageBox(this->GetHWND(),_T("请先设置该记录!"),_T("提醒"),MB_OK);
							return ;
						}
						if(!it->IsEnable())
						{
							it->SetEnable(true);
							DuiLib::CLabelUI*pState=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_state"));
							pState->SetText(_T("自动喊话已启动"));
							pState->SetTextColor(0xFF00FF00);
							DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_play"));
							pIcon->SetBkImage(_T("file='AutoSpeakSkin//speak1.png' source='17,0,34,13'"));
						}
					}
				}
			}
		}
	}
}

void CDuilibAutoSpeakDlg::OnStopCheckSpeak()
{

	int nCount = m_pList->GetCount();
	for(int i=0;i<nCount;i++)
	{	
		DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)m_pList->GetItemAt(i);
		DuiLib::CLabelUI*pWnd=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_windowhwnd"));
		CString strWnd=pWnd->GetText();
		DuiLib::CCheckBoxUI*pCheck=(DuiLib::CCheckBoxUI*)pElement->FindSubControl(_T("item_check"));
		if(pCheck->GetCheck())
		{
			if(!m_lstAutoSpeak.empty())
			{
				std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
				for(;it!=m_lstAutoSpeak.end();it++)
				{
					if(strWnd==it->GetSpeakWndStr())
					{
						if(it->IsEnable())
						{
							it->SetEnable(false);
							DuiLib::CLabelUI*pState=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_state"));
							pState->SetText(_T("自动喊话未启动"));
							pState->SetTextColor(0xFFFF0000);
							DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_play"));
							pIcon->SetBkImage(_T("file='AutoSpeakSkin//speak2.png' source='17,0,34,13'"));
						}
					}
				}
			}
		}
	}
}

void CDuilibAutoSpeakDlg::OnRestoreCheck()
{
	DuiLib::CListUI* pRecycle = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("ListRecycle")));
	for(int i=0;i<pRecycle->GetCount();i++)
	{
		DuiLib::CListContainerElementUI*pElement=(DuiLib::CListContainerElementUI*)pRecycle->GetItemAt(i);
		DuiLib::CLabelUI*pWnd=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_windowhwnd"));
		CString strWnd=pWnd->GetText();
		DuiLib::CCheckBoxUI*pCheck=(DuiLib::CCheckBoxUI*)pElement->FindSubControl(_T("item_check"));
		if(pCheck->GetCheck())
		{
			if(!m_lstAutoSpeak.empty())
			{
				std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
				for(;it!=m_lstAutoSpeak.end();it++)
				{
					if(strWnd==it->GetSpeakWndStr())
					{
						if(it->IsInRecycle())
						{
							it->SetInRecycle(FALSE);
							pRecycle->RemoveAt(i);
							i--;
							break;
						}
					}
				}
			}
		}
	}
}

BOOL CDuilibAutoSpeakDlg::SaveAutoSpeak()
{
	if(m_Autospeak!=NULL)
	{
		if(m_Autospeak->GetSpeakEditPt().x == 0 && m_Autospeak->GetSpeakEditPt().y == 0)
		{
			MessageBox(GetHWND(),_T("聊天窗口的坐标没有设置"),_T("错误"),MB_OK);
			return FALSE;
		}

		if(m_Autospeak->GetSpeakButtonPt().x == 0 && m_Autospeak->GetSpeakButtonPt().y == 0)
		{
			MessageBox(GetHWND(),_T("发送按钮的坐标没有设置"),_T("错误"),MB_OK);
			return FALSE;
		}

		CString strContext1 = _T("");
		CString strContext2 = _T("");
		CString strContext3 = _T("");
		DuiLib::CEditUI* pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content1")));
		if(pEdit!=NULL)
		{
			strContext1=pEdit->GetText();
		}
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content2")));
		if(pEdit!=NULL)
		{
			strContext2=pEdit->GetText();
		}
		pEdit = static_cast<DuiLib::CEditUI*>(m_PaintManager.FindControl(_T("edit_content3")));
		if(pEdit!=NULL)
		{
			strContext3=pEdit->GetText();
		}

		if(strContext1.IsEmpty()&& strContext2.IsEmpty() && strContext3.IsEmpty())
		{
			MessageBox(GetHWND(),_T("喊话内容不能为空"),_T("错误"),MB_OK);
			return FALSE;
		}
		int nTime=0;
		DuiLib::CComboUI* pCombo = static_cast<DuiLib::CComboUI*>(m_PaintManager.FindControl(_T("time_JG")));
		if(pCombo!=NULL)
		{
			switch(pCombo->GetCurSel())
			{
			case 0:
				nTime=5;
				break;
			case 1:
				nTime=10;
				break;
			case 2:
				nTime=20;
				break;
			case 3:
				nTime=60;
				break;
			case 4:
				nTime=180;
				break;
			case 5:
				nTime=600;
				break;
			default:
				break;
			}
		}
		bool bAddRandString = false;
		DuiLib::CCheckBoxUI* pCheck = static_cast<DuiLib::CCheckBoxUI*>(m_PaintManager.FindControl(_T("page3_all_check")));
		if(pCheck!=NULL)
		{
			bAddRandString=pCheck->GetCheck();
		}
		m_Autospeak->SetEnable(false);
		m_Autospeak->SetSet(true);
		m_Autospeak->SetContent1(strContext1);
		m_Autospeak->SetContent2(strContext2);
		m_Autospeak->SetContent3(strContext3);
		m_Autospeak->SetIntervalTime(nTime);
		m_Autospeak->SetInRecycle(FALSE);
		m_Autospeak->SetAddRandString(bAddRandString);
	}
	return TRUE;
}

BOOL CDuilibAutoSpeakDlg::OnBeginSpeak()
{
	if(SaveAutoSpeak())
	{
		if(m_Autospeak!=NULL)
			{
				m_Autospeak->SetEnable(TRUE);
				DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintManager.FindControl(_T("switch")));
				pControl->SelectItem(0);	
				ReLoad();
				return TRUE;
			}
	}
	return FALSE;
}

void CDuilibAutoSpeakDlg::ClickLeftMouse(HWND hWnd,int x,int y)
{
	DWORD lParam = MAKELPARAM(x,y);
	::SendMessageA(hWnd,WM_LBUTTONDOWN,0,lParam);
	::SendMessageA(hWnd,WM_LBUTTONUP,0,lParam);
}

BOOL CDuilibAutoSpeakDlg::OnBnClickSave()
{
	if(SaveAutoSpeak())
	{
		if(m_Autospeak->GetSpeakWnd()!=NULL)
		{
		
			DuiLib::CTabLayoutUI* pControl = static_cast<DuiLib::CTabLayoutUI*>(m_PaintManager.FindControl(_T("switch")));
			pControl->SelectItem(0);	
			ReLoad();
			return TRUE;
		}
	}
	return FALSE;
}

void CDuilibAutoSpeakDlg::OnShowRecycleItems()
{
	DuiLib::CListUI* pRecycle = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("ListRecycle")));
	pRecycle->RemoveAll();
	if(!m_lstAutoSpeak.empty())  
	{
		std::list<CAutoSpeak>::iterator it = m_lstAutoSpeak.begin();
		int count = 0;
		for(;it!=m_lstAutoSpeak.end();it++)
		{
			if(it->IsInRecycle())
			{
				count = pRecycle->GetCount();
				DuiLib::CDialogBuilder builder;
				DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("AutoSpeakSkin//RecycleListElement.xml"), (UINT)0, NULL, &m_PaintManager);  
				DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
				if(pTitle!=NULL)pTitle->SetText(it->GetTitle());
				DuiLib::CLabelUI*pOprate=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_restore"));
				if(pOprate!=NULL)
				{
					pOprate->SetText(_T("恢复"));
					pOprate->SetTextColor(0xFF0013A5);
				}
				DuiLib::CLabelUI*pWnd=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_windowhwnd"));
				if(pWnd!=NULL)pWnd->SetText(it->GetSpeakWndStr());
				pRecycle->AddAt(pRoot,count);
			}
		}
	}

}