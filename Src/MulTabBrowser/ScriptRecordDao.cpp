#include "StdAfx.h"
#include "ScriptRecordDao.h"

CScriptRecordDao::CScriptRecordDao(void) {}

// 保存脚本文件
void CScriptRecordDao::Save()
{
}

// 加载脚本文件
void CScriptRecordDao::Load()
{
}

// 删除文件
void CScriptRecordDao::Delete(LPCTSTR /*lpFileName*/)
{
}

// 重命名
void CScriptRecordDao::Rename(LPCTSTR /*lpszOldFileName*/, LPCTSTR /*lpszNewFileName*/)
{
}

// 脚本文件另存为
void CScriptRecordDao::SaveAs()
{
}

// 编辑脚本文件
void CScriptRecordDao::Edit(LPCTSTR /*lpszFileName*/)
{
}

CScriptRecordDao::~CScriptRecordDao(void) {}