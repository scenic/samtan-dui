#pragma once
#define USE_DUI
#include "AutoSpeak.h"
#include "../DuiLib/UIlib.h"
#include <list>


typedef int (*Pfn_Speak2)(HWND hWnd, const POINT& pt, const POINT& ptSend,LPCSTR lpszMessage);


class CDuilibAutoSpeakDlg : public DuiLib::WindowImplBase
{
public:
	CDuilibAutoSpeakDlg(void);
	~CDuilibAutoSpeakDlg(void);


	virtual	LPCTSTR		GetWindowClassName() const;
	virtual DuiLib::CDuiString	 GetSkinFile();                
	virtual DuiLib::CDuiString	GetSkinFolder();
	virtual void	InitWindow(); 
	virtual void	Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT OnLButtonDown(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
	virtual LRESULT OnLButtonUp(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);
	
	LRESULT	OnTimer(WPARAM wParam);
	void	ClickLeftMouse(HWND hWnd,int x,int y);
	void	ReLoad();
	void	RefreshList();
	void	LoadCurrentList();
	void	GetSpeakWnd(HWND ParentWnd,CString strSpeakWndClass,HWND& h) ;
	bool	IsSmallWnd(HWND h);
	BOOL	SaveAutoSpeak();
	CAutoSpeak&	GetSelectASItem(CString strWnd);

	void	OnOperate(CString strWnd);
	void	OnRun(int nSel,CString strWnd);
	void	OnDel(int nSel,CString strWnd);
	void	OnRestore(int nSel,CString strWnd);
	void	OnShowRecycleItems();
	void	OnStopAllSpeak(); 
	BOOL	OnBnClickSave();
	BOOL	OnBeginSpeak();
	void	OnStopCheckSpeak();
	void	OnStartCheckSpeak();
	void	OnRestoreCheck();

private:

	static CAutoSpeak s_AutoSpeak;
	BOOL	m_bMouseClickCapture_edit;
	BOOL	m_bMouseClickCapture_send;
	DuiLib::CListUI*	m_pList;

	std::list<CAutoSpeak>	m_lstAutoSpeak;
	Pfn_Speak2	m_pfnSpeak;
	CAutoSpeak*	m_Autospeak;
};

