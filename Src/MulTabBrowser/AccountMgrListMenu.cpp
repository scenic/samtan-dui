#include "StdAfx.h"
#include "AccountMgrListMenu.h"
#include "../Include/UzDefine.h"

LRESULT CAccountMgrListMenu::OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	Close();
	bHandled = FALSE;
	return 0;
}

void CAccountMgrListMenu::Init(HWND hWndParent, POINT ptPos)
{
	Create(hWndParent, _T("MenuWnd"), UI_WNDSTYLE_FRAME, WS_EX_WINDOWEDGE);
	::ClientToScreen(hWndParent, &ptPos);
	m_paintWnd = hWndParent;
	::SetWindowPos(*this, NULL, ptPos.x, ptPos.y, 0, 0, SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE);
}

LRESULT CAccountMgrListMenu::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return __super::HandleMessage(uMsg, wParam, lParam);
}

void CAccountMgrListMenu::Notify(DuiLib::TNotifyUI& msg)
{
	if(msg.sType==DUI_MSGTYPE_ITEMCLICK)
	{
		int nSel = ((DuiLib::CListContainerElementUI *)msg.pSender)->GetIndex();
		if(-1==nSel)return;
		::SendMessage(m_paintWnd,ACCOUNT_ADD_ACCOUNT_LIST_MENU,0, nSel);
		Close();
	}
	return WindowImplBase::Notify(msg);
}