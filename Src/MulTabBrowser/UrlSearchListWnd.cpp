#include "StdAfx.h"
#include "ControlEx.h"
#include "UrlSearchListWnd.h"
#include "BrowserWnd.h"
#include "MulTabBrowser.h"
#include "HistoryObj.h"
#include "Util.h"
LRESULT CUrlSearchListWnd::OnKillFocus(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& bHandled)
{
	ShowWindow(false,false);
	bHandled = FALSE;
	return 0;
}

void CUrlSearchListWnd::CreateWnd(HWND hWndParent)
{
	Create(hWndParent, _T("CUrlSearchListWnd"), WS_OVERLAPPEDWINDOW|WS_VISIBLE, WS_EX_WINDOWEDGE);
	::SetWindowPos(*this,NULL,-2,-5,1,1,SWP_SHOWWINDOW);
}

void CUrlSearchListWnd::SetPos( POINT ptPos, int nWidth)
{
	::ClientToScreen(m_pParent->GetHWND(),&ptPos);
	::SetWindowPos(*this, NULL, ptPos.x , ptPos.y, nWidth , 150, SWP_NOACTIVATE);
}

void CUrlSearchListWnd::InitWindow()
{
	m_pUrlList = static_cast<DuiLib::CListUI*>(m_PaintManager.FindControl(_T("listUrl")));
}

void CUrlSearchListWnd::Notify(DuiLib::TNotifyUI &msg)
{
	if (msg.sType == DUI_MSGTYPE_ITEMCLICK)
	{
		
		DuiLib::CListContainerElementUI* pElement =  static_cast<DuiLib::CListContainerElementUI*>(m_pUrlList->GetItemAt(m_pUrlList->GetCurSel()));
		DuiLib::CLabelUI*pLink=(DuiLib::CLabelUI*)pElement->FindSubControl(_T("item_link"));
		((CBrowserWnd*)m_pParent)->SetUrlEditText(pLink->GetText());
		((CBrowserWnd*)m_pParent)->OnClickGoto();
		ShowWindow(false,false);
	}
}

void CUrlSearchListWnd::SetParentWnd(CWindowWnd* pParent)
{
	m_pParent = pParent;
}

LRESULT CUrlSearchListWnd::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	LRESULT lRes = 0;
	BOOL    bHandled = TRUE;

	switch ( uMsg )
	{
	case WM_KILLFOCUS:
		lRes = OnKillFocus(uMsg, wParam, lParam, bHandled);
		break;
	default:
		bHandled = FALSE;
	}

	if (bHandled || m_PaintManager.MessageHandler(uMsg, wParam, lParam, lRes))
	{
		return lRes;
	}

	return __super::HandleMessage(uMsg, wParam, lParam);
}

void CUrlSearchListWnd::EditPageDown()
{
	int nsel=m_pUrlList->GetCurSel();
	if(nsel!=m_pUrlList->GetCount()-1)
	{	
		nsel=nsel+1;
	}
	else if(nsel==m_pUrlList->GetCount()-1)
	{
		nsel=0;
	}
	m_pUrlList->SelectItem(nsel);
	DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_pUrlList->GetItemAt(nsel);
	DuiLib::CLabelUI*pLink=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_link"));
	((CBrowserWnd*)m_pParent)->SetUrlEditText(pLink->GetText());
	return;
}

void CUrlSearchListWnd::EditPageUp()
{
	int nsel=m_pUrlList->GetCurSel();
	if(nsel!=0)
	{
		nsel=nsel-1;
	}
	else if(nsel==0)
	{
		nsel=m_pUrlList->GetCount()-1;

	}
	m_pUrlList->SelectItem(nsel);
	DuiLib::CListContainerElementUI* m_keyElement=(DuiLib::CListContainerElementUI*)m_pUrlList->GetItemAt(nsel);
	DuiLib::CLabelUI*pLink=(DuiLib::CLabelUI*)m_keyElement->FindSubControl(_T("item_link"));
	((CBrowserWnd*)m_pParent)->SetUrlEditText(pLink->GetText());
	return;
}

void CUrlSearchListWnd::AddTypedUrl(const CTypedUrl* pTypedUrl)
{
	DuiLib::CDialogBuilder builder;
	DuiLib::CListContainerElementUI* pRoot = (DuiLib::CListContainerElementUI*)builder.Create(_T("Browser//DropDownlistElement.xml"), (UINT)0, NULL, &m_PaintManager);  
	DuiLib::CLabelUI*pLink=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_link"));
	pLink->SetText(pTypedUrl->GetUrl());
	DuiLib::CLabelUI*pTitle=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("item_title"));
	pTitle->SetText(pTypedUrl->GetTitle());
	DuiLib::CLabelUI*pIcon=(DuiLib::CLabelUI*)pRoot->FindSubControl(_T("Item_icon"));
	pIcon->SetBkImage(_T("KeySet//sina.png"));
	m_pUrlList->Add(pRoot);
}


BOOL CUrlSearchListWnd::AlreadyInList(const CString& strUrl)
{
	if(m_lstSearch.size()==0)
		return FALSE;
	std::list<CTypedUrl>::iterator it = m_lstSearch.begin();
	for(;it!=m_lstSearch.end();it++)
	{
		if(it->GetUrl() == strUrl)
			return TRUE;
	}

	return FALSE;
}

void CUrlSearchListWnd::OnAddressEditChange(CString strText)
{
	
	m_pUrlList->RemoveAll();
	m_lstSearch.clear();
	std::list<CUzHistoryObj> list =((CMulTabBrowserApp*) AfxGetApp())->GetHisotry()->GetHistroyList();
	std::list<CUzHistoryObj>::iterator it = list.begin();
	for(;it!=list.end();it++)
	{
		if (EHXLib::CUtil::IsMatched(it->GetUrlLink(),strText) ||EHXLib::CUtil::IsMatched(it->GetUrlTitle(),strText))
		{
			if(!AlreadyInList(it->GetUrlLink())) 
			{
				m_lstSearch.push_back(CTypedUrl(it->GetUrlLink(),it->GetUrlTitle()));
			}
		}
		if (m_lstSearch.size() >= 15)
		{
			break;
		}
	}
	std::list<CTypedUrl>::iterator it2 = m_lstSearch.begin();
	for(;it2!=m_lstSearch.end();it2++)
	{
		AddTypedUrl(&*it2);
	}

	int nHight=m_lstSearch.size()*25;
	RECT rc;
	::GetWindowRect(*this,&rc);
	SetWindowPos(*this,NULL,rc.left,rc.top,rc.right-rc.left,nHight,SWP_NOACTIVATE);
	m_pUrlList->SetFixedHeight(nHight);
	m_pUrlList->SelectItem(0);
	ShowWindow(true,false);


}