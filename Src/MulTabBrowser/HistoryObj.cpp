//*******************************************************************************
// COPYRIGHT NOTES
// ---------------
// This is a sample for BCGControlBar Library Professional Edition
// Copyright (C) 1998-2010 BCGSoft Ltd.
// All rights reserved.
//
// This source code can be used, distributed or modified
// only under terms and conditions 
// of the accompanying license agreement.
//*******************************************************************************
//
// HistoryObj.cpp: implementation of the CHistoryObj class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HistoryObj.h"
#include "GlobalFunc.h"
#include "MulTabBrowser.h"
#include "RestoreMenuWnd.h"
#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHistoryObj::CHistoryObj(const CString& strTitle, const CString& strURL,
						const UINT uiCommand)
	: m_strTitle  (strTitle)
	, m_strURL    (strURL)
	, m_uiCommand (uiCommand)
{
}

CHistoryObj::CHistoryObj(const CHistoryObj& rObj)
	: m_strTitle  (rObj.m_strTitle)
	, m_strURL    (rObj.m_strURL)
	, m_uiCommand (rObj.m_uiCommand)
{
}

CHistoryObj::~CHistoryObj()
{
}

CString CHistoryObj::GetDomain() const
{
	CString str;

	int pos1 = m_strURL.Find(_T("///"));
	if (pos1 != -1)
	{
		pos1 += 3;
	}
	else
	{
		pos1 = m_strURL.Find(_T("//"));
		if (pos1 != -1)
		{
			pos1 += 2;
		}
	}

	if (pos1 != -1)
	{
		int pos2 = m_strURL.Find(_T("/"), pos1);
		if (pos2 == -1)
		{
			pos2 = m_strURL.GetLength () - 1;
		}

		str = m_strURL.Left (pos2);
		str = str.Right (str.GetLength () - pos1);
		str.MakeLower ();
	}

	return str;
}


CHistoryManager::CHistoryManager()
{

}

CHistoryManager::~CHistoryManager()
{
	Clean();
}

void CHistoryManager::Clean()
{
	m_UzHistory.Clean();
	return;

	CString key;
	XHistoryArray* ar = NULL;
	POSITION pos = m_Map.GetStartPosition ();
	while (pos != NULL)
	{
		m_Map.GetNextAssoc (pos, key, ar);
		if (ar != NULL)
		{
			for (int i = 0; i < ar->GetSize(); i++)
			{
				CHistoryObj* pObj = ar->GetAt(i);
				if (pObj != NULL)
				{
					delete pObj;
				}
			}

			ar->RemoveAll ();
			delete ar;
		}
	}

	m_Map.RemoveAll ();
}

void CHistoryManager::Load()
{
	m_UzHistory.Load();
	return;

	Clean();
}

void CHistoryManager::Save() const
{
	m_UzHistory.Save();
}

BOOL CHistoryManager::Add(const CHistoryObj& rObj)
{
	CString str = rObj.GetDomain ();
	if (str.IsEmpty ())
	{
		return FALSE;
	}

	XHistoryArray* ar = NULL;

	if (!m_Map.Lookup (str, ar))
	{
		ar = new XHistoryArray;
		m_Map[str] = ar;
	}

	ASSERT(ar != NULL);

	BOOL bFound = FALSE;
	for (int i = 0; i < ar->GetSize (); i++)
	{
		CHistoryObj* pObj = ar->GetAt (i);
		if (pObj != NULL && _tcsicmp (pObj->GetTitle (), rObj.GetTitle ()) == 0)
		{
			bFound = TRUE;
			break;
		}
	}

	if (!bFound)
	{
		ar->Add (new CHistoryObj (rObj));
	}

	return !bFound;
}





BOOL CHistoryManager::Add(const CString& strTitle,const CString& strUrl,const CString& strIconPath)
{
	return m_UzHistory.Add(strTitle,strUrl,strIconPath);
}

void CHistoryManager::AnalyseHistory(HWND hTab,CString strTitle,CString strUrl)
{
	sHistoryInfo his;
	his.hWnd = hTab;
	his.strTitle = strTitle;
	his.strUrl = strUrl;
	BOOL bNeedInsert = TRUE;

	std::list<sHistoryInfo>::iterator it = m_lstHistoryInfo.begin();
	for(;it!=m_lstHistoryInfo.end();it++)
	{
		if(it->hWnd == hTab && it->strUrl == strUrl)  //如果同一标签的连接相同，就不用再次记录
		{
			if(it->strTitle != strTitle)  //如果连接相同，但是标题不同，就改下标题
			{
				UpdateTitle(his);
			}
			bNeedInsert = FALSE;	
			break;
		}
	}

	if(!bNeedInsert) return;

	CString strIconPath;// = CUtil::GetIconName(strUrl);
	m_lstHistoryInfo.push_back(his);
	this->Add(strTitle,strUrl,strIconPath);

}

void CHistoryManager::UpdateTitle(const sHistoryInfo& his)
{
	//先修改历史记录列表
	std::list<sHistoryInfo>::iterator it = m_lstHistoryInfo.begin();
	for(;it!=m_lstHistoryInfo.end();it++)
	{
		if(his.hWnd == it->hWnd)
		{
			it->strTitle = his.strTitle;
			//修改文件
			m_UzHistory.UpdateTitle(his.strUrl,his.strTitle);
		}
	}
}

std::list<CUzHistoryObj>& CHistoryManager::GetHistroyList()
{
	return m_UzHistory.GetHisoryList();
}

void CHistoryManager::SaveGoToUrl(const CString& strUrl)
{
	m_UzHistory.SaveGoToUrl(strUrl);
}

void CHistoryManager::LoadGoToUrl(std::list<CString>& lst)
{
	return m_UzHistory.LoadGoToUrl(lst);
}

void CHistoryManager::DeleteGoToUrl(const CString& strUrl)
{
	return m_UzHistory.DeleteGoToUrl(strUrl);
}

std::list<CUzHistoryObj>& CHistoryManager::GetRestoreLblList()
{
	return m_UzHistory.GetRestoreLblList();
}

std::list<CUzHistoryObj>& CHistoryManager::LoadRestoreLbl()
{
	return m_UzHistory.LoadRestoreLbl();
}

void CHistoryManager::SaveRestoreLbl()
{
	m_UzHistory.SaveRestoreLbl();
}

void CHistoryManager::InsertToRestoreLblList(CUzHistoryObj& obj)
{
	if(GetRestoreLblList().size()>=20)
		GetRestoreLblList().pop_front();
	GetRestoreLblList().push_back(obj);
}

void CHistoryManager::DeleteRestore()
{
	m_UzHistory.DeleteRestoreLbl();
}

void CHistoryManager::DeleteRestore(CString strLink)
{
	m_UzHistory.DeleteRestoreLbl(strLink);
}

void CHistoryManager::ShowRestoreLblMenu(POINT pt)
{
	CRestoreMenuWnd*pWnd=new CRestoreMenuWnd;
	pWnd->Init(pt);
}

void CHistoryManager::DeleteImage()
{
	if(m_listHbitmapMenu.size() !=0 )
	{
		std::vector<HBITMAP>::iterator it = m_listHbitmapMenu.begin();
		for(;it!=m_listHbitmapMenu.end();it++)
		{
			if(*it)
			{
				DeleteObject(*it);
			}
		}
	}
}



void CHistoryManager::SaveLastOpen(std::list<CUzHistoryObj>& list)
{
	m_UzHistory.SaveLastOpen(list);
}

void CHistoryManager::LoadLastOpen(std::list<CUzHistoryObj>& list)
{
	m_UzHistory.LoadLastOpen(list);
}

std::list<CUzHistoryObj>& CHistoryManager::GetLastOpenList()
{
	return m_lstLastOpen;
}

void CHistoryManager::ShowLastOpen()
{
	if(GetLastOpenList().size()!=0)
	{
		if(::MessageBox(AfxGetMainWnd()->GetSafeHwnd(),L"发现上次浏览器关闭有异常，是否恢复关闭前的标签页?",L"提示",MB_YESNO) == IDYES)
		{
			std::list<CUzHistoryObj> list = GetLastOpenList();
			std::list<CUzHistoryObj>::iterator it = list.begin();
			for(;it!=list.end();it++)
			{
				theApp.pBrowser->NewView();
			}
		}
	}
}

void CHistoryManager::RemoveLastOpen()
{
	m_UzHistory.RemoveLastOpen();
}

void CHistoryManager::DeleteHistory()
{
	m_UzHistory.DeleteHistory();
}

void CHistoryManager::DeleteHistoryDir(int days,CTime& timeLoad)
{
	m_UzHistory.DeleteHistoryDir(days,timeLoad);
}

void CHistoryManager::DeleteHistoryItem(CUzHistoryObj& obj)
{
	m_UzHistory.DeleteHistoryItem(obj);
}

CHistoryManager* GetHistory()
{
	return ((CMulTabBrowserApp*)AfxGetApp())->GetHisotry();
	
}

