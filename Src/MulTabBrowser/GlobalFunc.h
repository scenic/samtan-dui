
#if !defined(AFX_GLOBALFUNC_H__INCLUDED_)
#define AFX_GLOBALFUNC_H__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "mshtml.h"
#include "Psapi.h"
#define MAX_CLASSNAME    100
#define GROWBYTES  1024*1024

#ifdef  _UNICODE
#define _sscanf swscanf
#else
#define _sscanf sscanf_s
#endif

#define SAFE_RELEASE(pInterface)  \
if(pInterface) \
{\
pInterface->Release();\
pInterface = NULL;\
}\

#define SAFE_DELETE(pt) \
	if(pt){delete (pt); (pt)=NULL;}

typedef BOOL (CALLBACK *PROCENUMPROC)(DWORD, WORD, LPSTR, LPARAM);

typedef struct {
	DWORD dwPID;
	PROCENUMPROC lpProc;
	DWORD lParam;
	BOOL bEnd;
} EnumInfoStruct;

typedef struct 
{
	DWORD dwPosx;       //鼠标位置x坐标，窗口相对位置
	DWORD dwPosy;
	DWORD dwTimeSpan;   //时间间隔 单位毫秒
}MousePosInfo;

enum
{
    //WEBUrl_Update = 0,
    //WEBUrl_SFZ,
    WEBUrl_RecordingScripts,
    //WEBUrl_HomePage,
    WEBUrl_FeedBack,
    //WEBUrl_GearHelp
};

BOOL WINAPI EnumProcs(PROCENUMPROC lpProc, LPARAM lParam);


const char * const EditToken ="↓";

HWND FindChildWindow(HWND hParentWnd,LPCTSTR lpClassName);

HWND FindActiveWindow(HWND hParentWnd,LPCTSTR lpClassName);

HWND FindActiveWindowEx2(HWND hParentWnd,LPCTSTR lpClassName,LPCTSTR lpWindowName);

HWND FindWindowEx2(HWND hParentWnd,LPCTSTR lpClassName,LPCTSTR lpWindowName);

HWND  GetChildWindowFromPoint(HWND hWnd, POINT pt, LPCTSTR lpClassName);

HWND FindWindowEx3(HWND hAncestorWnd,
						 LPSTR lpParentWndClassName,
						 LPSTR lpWndClassName,
						 POINT point,
						 BOOL bFirst);

BOOL  IsHaveWndOn(HWND hWnd);

void TraceToLog(LPCTSTR lpInfo);

IWebBrowser2* FindWebBrowser(HWND hFindWnd);

IWebBrowser2* FindIWebBrowser(CString strUrl);

int GetSubStrPos(CString& strSrc,CString strtoken,int nIndex);

//得到当前exe文件的路径全名
CString GetExePath();

CString GetPathName( CString Filename);
CString GetSysDataPath();
CString GetFavPath();

//图像处理函数
BOOL _StretchBitmap (CDC* pDC, const CRect& rectClient, CBitmap& bmp);
BOOL _Stretch_ImageList (CDC* pDC, const CRect& rectClient, CImageList& ImageList, int iIndex);
BOOL _StretchImage (CDC* pDC, const CRect& rectClient, CImage& image);
void _DrawReflectedGradient (CDC* pDC, COLORREF clrS, COLORREF clrM, COLORREF clrF,
	const CRect& rect, BOOL bHorz);


void SetProcessCookiePath(CString strPath);
void SetDefaultCookiePath();

//字串分隔
void Split(CString source, CStringArray& dest, CString division);

//从窗口句柄，获得IWebBrowser2接口
CComPtr<IWebBrowser2> GetIWebBrowserInterface(HWND BrowserWnd);

IWebBrowser2* FindWebBrowser(HWND hFindWnd);

CString GetWebSiteUrl(int nID);
BOOL RegisitKeyBord(HWND hwnd);
BOOL RegisitMouse(HWND hwnd);

BOOL UnRegisitKeyBord(HWND hwnd);
BOOL UnRegisitMouse(HWND hwnd);

BOOL SetTopWindow(HWND hWnd);

HRGN BitmapToRegion (HBITMAP hBmp, COLORREF cTransparentColor =  0, COLORREF cTolerance = 0x101010);

void WStringTOAsinString(CStringW strWide,CStringA& strout);
void AnsiToWideString(CStringA strAnsi,CStringW& strout);
CStringA UT8ToAsinString(LPCSTR str);
CStringW UT8ToWString(const char* strUT8);
CStringA GetFileNameNoExt(CStringA strFile);
CString GetFileExt(CString strFile);

CString GetSysFavPath();  //获取IE收藏夹地址
BOOL    IsFileExist(LPCTSTR   lpFileName);   //判断文件是否存在
CString GetUzFavPath();  
CString GetUzDefaultXmlFavPath();

#endif
