#pragma once

#include <list>

#define INVALID_KEY -1

enum HotKeyID
{
    HOTKEY_AUTOSPEAK    = 10001,
    HOTKEY_BOSSKEY      = 10002,
	HOTKEY_ADDRESS_EDITBOX = 10003,
	HOTKEY_OPEN_NEW_PAGE = 10004,
	HOTKEY_CLOSE_TAB_WND = 10005,
	HOTKEY_FULL_SCREEN = 10006,
	HOTKEY_OPEN_FAV = 10007,
	HOTKEY_CHANGE_KEY = 10008,		// �ļ������ȼ�
};


static const TCHAR* VK_VALUE[0x100] =
{
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	_T("Backspace"), _T("Tab"), NULL, NULL, _T("Clear"), _T("Enter"), NULL, NULL,
	_T("Shift"), _T("Ctrl"), _T("Alt"), _T("Pause"), _T("Caps Lock"), NULL, NULL, NULL,
	NULL, NULL, NULL, _T("Esc"), NULL, NULL, NULL, NULL,
	_T("Space"), _T("PgUp"), _T("PgDn"), _T("End"), _T("Home"), _T("��"), _T("��"), _T("��"),
	_T("��"), NULL, _T("Pr Scrn"), NULL, NULL, _T("Insert"), _T("Delete"), NULL,
	_T("0"), _T("1"), _T("2"), _T("3"), _T("4"), _T("5"), _T("6"), _T("7"),
	_T("8"), _T("9"), NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, _T("A"), _T("B"), _T("C"), _T("D"), _T("E"), _T("F"), _T("G"),
	_T("H"), _T("I"), _T("J"), _T("K"), _T("L"), _T("M"), _T("N"), _T("O"),
	_T("P"), _T("Q"), _T("R"), _T("S"), _T("T"), _T("U"), _T("V"), _T("W"),
	_T("X"), _T("Y"), _T("Z"), NULL, NULL, NULL, NULL, NULL,
	_T("Num 0"), _T("Num 1"), _T("Num 2"), _T("Num 3"), _T("Num 4"), _T("Num 5"), _T("Num 6"), _T("Num 7"),
	_T("Num 8"), _T("Num 9"), _T("Num *"), _T("Num +"), _T("Num Enter"), _T("Num -"), _T("Del"), _T("Num /"),
	_T("F1"), _T("F2"), _T("F3"), _T("F4"), _T("F5"), _T("F6"), _T("F7"), _T("F8"),
	_T("F9"), _T("F10"), _T("F11"), _T("F12"), _T("F13"), _T("F14"), _T("F15"), _T("F16"),
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	_T("Num Lock"), NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, _T(";"), _T("="), _T(","), _T("-"), _T("."), _T("/"),
	_T("`"), NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, _T("["), _T("\\"), _T("]"), _T("'"), NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
};

typedef struct _HotKey
{
    int nHotKeyID;
    bool bCtrl;
    bool bShift;
    int nHotKey;
}HotKey;

class CHotKeyMgr
{
    CHotKeyMgr(void);
    ~CHotKeyMgr(void);

public:
    static CHotKeyMgr& Instance();
    static const TCHAR* KeyValue(int nKey);
    BOOL AddHotKey(int nID, bool bCtrl, bool bShift, int nKey, bool bSave = true);
    BOOL RemoveHotKey(int nID);
    BOOL FindHotKey(int nID, bool& bCtrl, bool& bShift, int& nKey);
    void LoadHotKeys();
    void UnLoadHotKeys();

private:
    HotKey* FindHotKey(int nID);
    void SaveHotKey(int nID, bool bCtrl, bool bShift, int nKey);
    void DeleteHotKey(int nID);
    BOOL ReadHotKey(int nID);

private:
    std::list<HotKey*> m_lsHotkeys;
    CString m_strIniFile;
};

