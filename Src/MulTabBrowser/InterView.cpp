#include "StdAfx.h"
#include "InterView.h"
#include "GlobalFunc.h"
#include "../Include/UzDefine.h"
#include "../History/Public/UzHistory.h"
#include "../History/Public/UzHistoryObj.h"
#include "HistoryObj.h"
CInterView::CInterView(void)
{
	m_KeyMess=new CInterViewKeyMess();
	ApplyAttributeList(_T("name=\"ie\" clsid=\"{8856F961-340A-11D0-A96B-00C04FD705A2}\""));
}


CInterView::~CInterView(void)
{
	if(m_KeyMess!=NULL)
	{
		delete m_KeyMess;
	}
}

HWND CInterView::GetCurGameWnd(HWND hFindWnd, LPCTSTR lpClassName, LPCTSTR lpAncestorWndClassName)
{
	//���Գ���
	HWND hGameMainWnd = hFindWnd;
	if (lstrlen(lpAncestorWndClassName)> 0)
	{
		hGameMainWnd = FindActiveWindow(hFindWnd, lpAncestorWndClassName);
	}

	if(hGameMainWnd != NULL)
	{
		HWND hGameWnd = FindActiveWindowEx2(hGameMainWnd, lpClassName, _T(""));
		return hGameWnd;
	}

	return NULL;	
}

CString CInterView::GetUrl()
{
	return m_strLink;
}

BOOL CInterView::SetViewUrl(CString strUrl)
{
	if(strUrl.GetLength()>0 && strUrl.Find(_T("about:blank"))==-1)
	{
		m_strLink=strUrl;
		return TRUE;
	}
	return FALSE;
}

BOOL CInterView::UzNavigate(CString strUrl)
{
	strUrl.Trim();

	//BOOL bRet = FALSE;
	if(lstrlen(strUrl)>0)
	{
		DWORD dwType;
		CString strServer;
		CString strObject;
		INTERNET_PORT wPort;

		if (!AfxParseURL(strUrl, dwType, strServer, strObject, wPort))
		{
			// AfxParseUrl returns FALSE, try to add http:// as prefix
			CString strPrefix = strUrl.Left(7).MakeLower();
			if (strPrefix != _T("http://")) {
				CString strTemp (strUrl);
				strUrl = _T("http://") + strTemp;
			}

			if (!AfxParseURL(strUrl, dwType, strServer, strObject, wPort))
				return FALSE;

			int nIndex = strUrl.MakeLower().Find(strServer.MakeLower());
			if (nIndex >= 0)
			{
				strUrl = strUrl.Left(nIndex) + strServer + strObject;
			}
		}
	}
	m_strLink=strUrl;
	Navigate2(strUrl);
	AddURLToHistory(_T(""),strUrl);
	return TRUE;
}

CInterViewKeyMess*CInterView::GetCurrentKeyMess()
{
	return m_KeyMess;
}

CHistoryObj* CInterView::AddURLToHistory (const CString& strTitle, const CString& strURL)
{
	ASSERT (m_arHistory.GetSize () <= HISTORY_LEN);
	for (int i = 0; i < m_arHistory.GetSize (); i ++)
	{
		CHistoryObj* pObj = m_arHistory [i];
		ASSERT (pObj != NULL);

		if (pObj->GetTitle () == strTitle &&
			pObj->GetURL () == strURL)
		{
			return pObj;
		}
	}

	if (m_arHistory.GetSize () == HISTORY_LEN)
	{
		delete m_arHistory [0];
		m_arHistory.RemoveAt (0);
	}

	CHistoryObj* pObj = new CHistoryObj (strTitle, strURL, 
		ID_HISTORY_FIRST + (UINT)m_arHistory.GetSize ());
	m_arHistory.InsertAt (0, pObj);
	return pObj;
}


