/*!@file
*******************************************************************************************************
<PRE>
模块名		：收藏夹的头文件
文件名		：ManageFavHeader
相关文件		：
文件实现功能：保存收藏夹管理界面中所有窗口通用的一写结构体、工具函数
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/6/7 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once

// 定义一个结构体，用来保存树的数据
struct TREESORT
{
	int nIndex;				// 用来记录要调换顺序的索引号
	CString strParentText;	// 用来记录父节点的文本
	CString strText;		// 用来记录当前节点的文本

	TREESORT()
	{
		nIndex = -1;
		strParentText = "";
		strText = "";
	}

	TREESORT(int Index, CString ParentText, CString NodeText)
	{
		nIndex = Index;
		strParentText = ParentText;
		strText = NodeText;
	}

	~TREESORT()
	{
		nIndex = -1;
		strParentText = "";
		strText = "";
	}

	// 运算符“=”重载
	TREESORT operator =(TREESORT& TreeSort)
	{
		nIndex = TreeSort.nIndex;
		strParentText = TreeSort.strParentText;
		strText = TreeSort.strText;
	}

	// 索引不变交换树结构
	bool SwapTree(TREESORT& TreeSort)
	{
		if (-1 == TreeSort.nIndex)
		{
			return false;
		}

		//int Index = TreeSort.nIndex;
		CString ParentText = TreeSort.strParentText;
		CString sText = TreeSort.strText;

		TreeSort.strParentText = this->strParentText;
		TreeSort.strText = this->strText;

		this->strParentText = ParentText;
		this->strText = sText;
	}
};

// 定义一个结构体，用来保存XML中的List网址连接信息
struct LIST_LINK
{
	CString strTitle;				// 网址连接标题
	CString strURL;					// 网址连接地址
	CString strParentFolderName;	// 父结点文件夹名

	LIST_LINK()
	{
		strTitle = "";
		strURL = "";
		strParentFolderName = "";
	}
};

// 工具类，用来提供一些收藏夹可能用到的工具
class CFavToolFunc
{
public:
	CFavToolFunc(void){};
	~CFavToolFunc(void){};

	// CStringW转CStringA
	void WStringTOAsinString(CStringW strWide,CStringA& strout)
	{
		int len = WideCharToMultiByte( CP_ACP,0,strWide.GetBuffer(),-1,NULL,0,NULL,NULL );  
		if(len>0)
		{
			char* szbuf = new char[len+1];
			memset(szbuf,0,len+1);
			WideCharToMultiByte(CP_ACP,0,strWide.GetBuffer(),strWide.GetLength(),szbuf,len,NULL,NULL);
			if(szbuf!=NULL)
			{
				strout=szbuf;
				delete[] szbuf;
				szbuf=NULL;
			}
		}
	}

	// CString转Char*
	char* CStringToChar(CString& str)
	{
		//获取宽字节字符的大小，大小是按字节计算的
		int len = WideCharToMultiByte(CP_ACP,0,str,str.GetLength(),NULL,0,NULL,NULL);

		//为多字节字符数组申请空间，数组大小为按字节计算的宽字节字节大小
		char * pFileName = new char[len+1];   //以字节为单位
		memset(pFileName,0, len+1);
		//宽字节编码转换成多字节编码
		WideCharToMultiByte(CP_ACP,0,str,str.GetLength(),pFileName,len,NULL,NULL);

		//pFileName[len+1] = '/0';   //多字节字符以'/0'结束

		return pFileName;
	}
};