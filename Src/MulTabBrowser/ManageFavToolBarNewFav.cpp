#include "StdAfx.h"
#include "ManageFavToolBarNewFav.h"
#include "ManageFavToolBarNewFolder.h"
#include "ManageFavToolBarTree.h"
#include "../Include/UzDefine.h"

void CManageFavToolBarNewFav::Init(HWND hWndParent)
{
	Create(hWndParent, _T(""), UI_WNDSTYLE_FRAME, 0L, 0, 0, 800, 572);
	m_parentHwnd = hWndParent;
	CenterWindow();
}

LRESULT CManageFavToolBarNewFav::HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// 下拉框选中时
	if (uMsg == FAV_ITEM_TREE_CLICK)
	{
		// 获取下拉框文本
		CString strText = *((CString*)lParam);
		CStringW strWText = strText;
		m_PaintManager.FindControl(_T("Button_ToolBarNewPos"))->SetText(strWText);
	}

	// 下拉框新建文件夹时
	if (uMsg == FAV_NEW_FOLDER_ADD)
	{
		// 获取编辑框
		CString strFolderName = *((CString*)wParam);
		
		// 获取下拉框
		CString strNewPos = *((CString*)lParam);
		
		TREESORT TreeSort;
		TreeSort.strParentText = strNewPos;
		TreeSort.strText = strFolderName;

		int nCont = 0;
		// 遍历结构体,获取同父结点的个数
		for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
		{
			// 判断树的名字是否唯一
			if (i->strText == TreeSort.strText)
			{
				nCont = -1;
				break;
			}

			if (i->strParentText == TreeSort.strParentText)
			{
				nCont++;
			}
		}

		TreeSort.nIndex = nCont;
		if (-1 != nCont)
		{
			m_TreeSort.push_back(TreeSort);
		}

		// 把消息发给父窗口
		::SendMessage(m_parentHwnd, FAV_NEW_FOLDER_ADD, wParam, lParam);
	}

	return __super::HandleMessage(uMsg, wParam, lParam);
}

void CManageFavToolBarNewFav::OnClick(DuiLib::TNotifyUI &msg )
{
	// 关闭按钮
	if (msg.pSender->GetName() == _T("ToolBarClosebtn") || msg.pSender->GetName() == _T("ToolBarCancel"))
	{
			Close();
			//PostQuitMessage(0);
	}
	// 新建文件夹
	else if (msg.pSender->GetName() == _T("ToolBarNewFolder"))
	{
		CManageFavToolBarNewFolder* pNewFolder = new CManageFavToolBarNewFolder(_T("SideFavWnd//ManageFav_ToolBarNewFolder.xml"));
		if (pNewFolder == NULL)
		{
			return ;
		}

		pNewFolder->Init(*this);
		pNewFolder->ShowWindow(TRUE);

		// 添加结点到新建文件夹
		for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
		{
			pNewFolder->AddTreeSort(i->nIndex, i->strParentText, i->strText);
		}

	}
	// 下拉按钮
	else if (msg.pSender->GetName() == _T("Button_ToolBarNewPos"))
	{
		RECT ItemRect = ((DuiLib::CButtonUI *)msg.pSender)->GetPos();
		POINT pt = {ItemRect.left, ItemRect.bottom};

		CManageFavToolBarTree* m_ToolBarTree = new CManageFavToolBarTree(_T("SideFavWnd//ManageFav_ToolBarTree.xml"));
		if (m_ToolBarTree == NULL)
		{
			return ;
		}

		m_ToolBarTree->Init(*this, pt, ItemRect.right - ItemRect.left);
		// 添加树列表
		AddListToTree(m_ToolBarTree, m_ToolBarTree->GetCocalFavNode());		

		m_ToolBarTree->ShowWindow(TRUE);
	}
}

// 添加树链表
void CManageFavToolBarNewFav::AddTreeSort(int nIndex, CString strParentText, CString strText)
{
	TREESORT TreeSort;
	TreeSort.nIndex = nIndex;
	TreeSort.strParentText = strParentText;
	TreeSort.strText = strText;

	// 将结点添加到List尾
	m_TreeSort.push_back(TreeSort);
}

// 从链表添加结点
bool CManageFavToolBarNewFav::AddListToTree(CManageFavToolBarTree* ToolBarTree, DuiLib::CTreeNodeUI* parent)
{
	CString strParentText = parent->GetItemText();
	// 遍历结构体,添加第一层子结点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText == i->strParentText)
		{
			ToolBarTree->AddTreeNode(i->strText, parent, i->nIndex);
		}
	}
	// 分两次添加的原因是避免因为交换链表结点导致的子节点的父节点未被创建导致的添加失败问题
	// 遍历树结构，添加后面层的子节点
	for (std::list<TREESORT>::iterator i = m_TreeSort.begin(); i != m_TreeSort.end(); i++)
	{
		if (strParentText != i->strParentText)
		{
			ToolBarTree->AddTreeNode(i->strText, ToolBarTree->GetNode(i->strParentText), i->nIndex);
		}
	}
	return true;
}
