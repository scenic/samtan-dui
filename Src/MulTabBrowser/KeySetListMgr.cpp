#include "StdAfx.h"
#include "KeySetListMgr.h"
#include "InterView.h"
#include "GlobalFunc.h"
#include "../tinyxml2/Public/tinyxml2.h"
#include "../DuiLib/UIlib.h"
#include "DuLibKeySet.h"
#include "MulTabBrowser.h"
#include "../Include/UzDefine.h"
CKeySetListMgr::CKeySetListMgr(void)
{

	CString szPath = GetSysDataPath();
	if (!PathFileExists(szPath))
		CreateDirectory(szPath, NULL);
	m_strIniFile = szPath + _T("\\ChangeKey.dat");
	if(!IsXmlFileExist(m_strIniFile))
	{
		CreateXmlFile();
	}
	pFrame=NULL;
	m_hChangeKey = NULL;
	m_HookState = false;
	CHotKeyMgr::Instance().AddHotKey(HOTKEY_CHANGE_KEY, true, false, VK_F1);
}


CKeySetListMgr::~CKeySetListMgr(void)
{
	
}

CKeySetListMgr&CKeySetListMgr:: Instance()
{
	static CKeySetListMgr Mgr;
	return Mgr;
}

const TCHAR*CKeySetListMgr:: KeyValue(int nKey)
{
	const TCHAR* pStr = NULL;
	if ((nKey >= 0) && (nKey < 0x100))
		pStr = VK_VALUE[nKey];
	return pStr;
}

BOOL CKeySetListMgr::IsXmlFileExist(const CString& strFile)
{
	if(strFile== L"")   
		return   FALSE;   
	BOOL   bExist   =   TRUE;   
	HANDLE   hFind;   
	WIN32_FIND_DATA   dataFind;   
	hFind   =   FindFirstFile(strFile,&dataFind);   
	if(hFind==   INVALID_HANDLE_VALUE)   
	{   
		bExist   =   FALSE;   
	}   
	FindClose(hFind);   
	return   bExist; 
}

void CKeySetListMgr::CreateXmlFile()
{
	tinyxml2::XMLDocument doc;

	//xml声明
	tinyxml2::XMLDeclaration *pDel = doc.NewDeclaration("xml version=\"1.0\" encoding=\"UTF-8\"");
	if (NULL == pDel) {
		return ;
	}
	doc.LinkEndChild(pDel);

	tinyxml2::XMLElement *pListElement = doc.NewElement("keys");
	if (NULL == pListElement)
		return;
	doc.LinkEndChild(pListElement);

	CStringA strXmlA = "";
	WStringTOAsinString(m_strIniFile,strXmlA);
	doc.SaveFile(strXmlA);
}

BOOL  CKeySetListMgr::AddKey(KeyMess m_keymess)
{

    m_KeyList.push_back(m_keymess);
	return TRUE;
}

BOOL CKeySetListMgr::IsKeyUsed(int nID)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==nID)
		{
			return TRUE;
		}
		if(m_KeyList[i].type==TYPE_LXKEY)
		{
			int lenth=m_KeyList[i].detils.GetLength();
			int pos=m_KeyList[i].detils.ReverseFind(',');
			int KeyID=GetKeyValueByStr(m_KeyList[i].detils.Right(lenth-pos-1));
			if(KeyID==nID)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL CKeySetListMgr::IsKeyUsed(int nID,int endkey)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==endkey)
		{
			return TRUE;
		}
		if(m_KeyList[i].type==TYPE_LXKEY)
		{
			int lenth=m_KeyList[i].detils.GetLength();
			int pos=m_KeyList[i].detils.ReverseFind(',');
			int KeyID=GetKeyValueByStr(m_KeyList[i].detils.Right(lenth-pos-1));
			if(KeyID==endkey&&nID!=m_KeyList[i].nID)
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}
BOOL CKeySetListMgr::InsertKey(int pos,KeyMess m_keymess)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==m_keymess.nID)
		{
			return FALSE;
		}
	}
	std::vector<KeyMess>::iterator it=m_KeyList.begin();
	m_KeyList.insert(it,pos,m_keymess);
	return TRUE;
}

int CKeySetListMgr::GetKeyPos(int nID)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==nID)
		{
			return i;
		}
	}
	return -1;
}

BOOL CKeySetListMgr::FindKey(int nID)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==nID)
		{
			return TRUE;
		}
	}
	return FALSE;
}

KeyMess CKeySetListMgr::FindKeyMess(int nID)
{
	KeyMess m_mess;
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==nID)
		{
			m_mess=m_KeyList[i];
		}
	}
	return m_mess;
}

BOOL CKeySetListMgr::EditKey(int nID,const KeyMess& m_mess)
{
	for (unsigned int i=0;i< m_KeyList.size(); i++)
	{
		if(m_KeyList[i].nID==nID)
		{
			if(IsKeyUsed(m_mess.nID)&&nID!=m_mess.nID)
			{
				
				return FALSE;
			}
			m_KeyList[i].nID=m_mess.nID;
			m_KeyList[i].type=m_mess.type;
			m_KeyList[i].used=m_mess.used;
			m_KeyList[i].detils=m_mess.detils;
			if(!SetKeyValue(nID,m_mess))return FALSE;
			return TRUE;
		}
	}
	return FALSE;
}

BOOL CKeySetListMgr::RemoveKey(int nID)
{
	
	std::vector<KeyMess>::iterator it=m_KeyList.begin();
	for(;it!=m_KeyList.end();it++)
	{
		if(it->nID==nID)
		{
			m_KeyList.erase(it);
			DelKey(nID);
			return TRUE;
		}
	}
	return FALSE;
}

void CKeySetListMgr::RemoveAllKeys()
{
	m_KeyList.clear();
}

void CKeySetListMgr::EnableKey(bool flag)
{
	for(unsigned int i=0;i<m_KeyList.size();i++)
	{
		m_KeyList[i].used=flag;
	}
}

void CKeySetListMgr::SaveKey(KeyMess m_keymess)
{
	if(!IsXmlFileExist(m_strIniFile))
	{
		CreateXmlFile();
	}
	CStringA strXmlA = "";
	WStringTOAsinString(m_strIniFile,strXmlA);
	tinyxml2::XMLDocument doc;
	if (tinyxml2::XML_NO_ERROR != doc.LoadFile(strXmlA))
		return;

	tinyxml2::XMLElement *pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		return;
	}

	tinyxml2::XMLElement *pKeyElement = doc.NewElement("key");
	if (NULL == pKeyElement)
	{
		return;
	}
	pRoot->LinkEndChild(pKeyElement);

	tinyxml2::XMLElement *pKeyValueElement = doc.NewElement("KeyValue");
	if (NULL == pKeyValueElement)
	{
		return;
	}

	CStringA temp="";
	CString textTemp;
	textTemp.Format(_T("%d"),m_keymess.nID);
	WStringTOAsinString(textTemp,temp);

	pKeyValueElement->LinkEndChild(doc.NewText(temp));//给节点设置值
	pKeyElement->LinkEndChild(pKeyValueElement);


	tinyxml2::XMLElement *pUsedElement = doc.NewElement("used");
	if (NULL == pUsedElement)
	{
		return;
	}

	if(m_keymess.used)
	{
		pUsedElement->LinkEndChild(doc.NewText("1"));//给节点设置值
	}
	else
	{
		pUsedElement->LinkEndChild(doc.NewText("0"));//给节点设置值
	}
	pKeyElement->LinkEndChild(pUsedElement);

	int type=(static_cast<int>(m_keymess.type));
	textTemp.Format(_T("%d"),type);
	WStringTOAsinString(textTemp,temp);

	tinyxml2::XMLElement *pTypeElement = doc.NewElement("type");
	if (NULL == pTypeElement)
	{
		return;
	}

	pTypeElement->LinkEndChild(doc.NewText(temp));//给节点设置值
	pKeyElement->LinkEndChild(pTypeElement);

	WStringTOAsinString(m_keymess.detils,temp);
	tinyxml2::XMLElement *pDetailElement = doc.NewElement("detail");
	if (NULL == pDetailElement)
	{
		return;
	}

	pDetailElement->LinkEndChild(doc.NewText(temp));//给节点设置值
	pKeyElement->LinkEndChild(pDetailElement);
	doc.SaveFile(strXmlA);
}




void CKeySetListMgr::ReadKeyToList()
{
	CStringA strXmlA = "";
	WStringTOAsinString(m_strIniFile,strXmlA);
	tinyxml2::XMLDocument doc;
	if (tinyxml2::XML_NO_ERROR != doc.LoadFile(strXmlA))
		return;

	tinyxml2::XMLElement *pRoot = doc.RootElement();
	if (NULL == pRoot)
	{
		return;
	}

	tinyxml2::XMLElement *pKeyElement = pRoot->FirstChildElement("key");

	while (NULL != pKeyElement)  
	{  
		tinyxml2::XMLElement *pKeyValueElement = pKeyElement->FirstChildElement("KeyValue");//获取第一个值为Value的子节点 默认返回第一个子节点

		if (NULL == pKeyValueElement)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}

		const char* pKeyValueContent= pKeyValueElement->GetText();
		if (NULL == pKeyValueContent)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}

		KeyMess temp;
		temp.nID = atoi(pKeyValueContent);
		temp.used=false;

		tinyxml2::XMLElement *pUsedElement = pKeyElement->FirstChildElement("used");//获取第一个值为Value的子节点 默认返回第一个子节点
		if (NULL != pUsedElement)
		{
			const char* pUsedContent= pUsedElement->GetText();
			if (NULL != pUsedContent)
			{
				if(atoi(pUsedContent))
				{
					temp.used=true;
				}
			}
		}

		tinyxml2::XMLElement *pTypeElement = pKeyElement->FirstChildElement("type");//获取第一个值为Value的子节点 默认返回第一个子节点
		if (NULL == pTypeElement)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}

		const char* pTypeContent= pTypeElement->GetText();
		if (NULL == pTypeContent)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}

		switch(atoi(pTypeContent))
		{
		case -1:
			temp.type=TYPE_NULL;
			break;
		case 0:
			temp.type=TYPE_LXKEY;
			break;
		case 1:
			temp.type=TYPE_SETKEY;
			break;
		case 2:
			temp.type=TYPE_MOUSEPRESS;
			break;
		case 3:
			temp.type=TYPE_MOUSELXPRESS;
			break;
		default:
			break;
		}
		
		tinyxml2::XMLElement *pDetailElement = pKeyElement->FirstChildElement("detail");//获取第一个值为Value的子节点 默认返回第一个子节点
		if (NULL == pDetailElement)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}

		const char* pDetailContent= pDetailElement->GetText();
		if (NULL == pDetailContent)
		{
			pKeyElement = pKeyElement->NextSiblingElement();
			continue;
		}
		temp.detils = pDetailContent;

		AddKey(temp);
		pKeyElement = pKeyElement->NextSiblingElement();
	}

	doc.SaveFile(strXmlA);	
}
//修改
BOOL CKeySetListMgr::SetKeyValue(int nID,KeyMess m_keymess)
{
	CStringA strXmlA = "";
	WStringTOAsinString(m_strIniFile,strXmlA);
	try
	{
		tinyxml2::XMLDocument doc;
		if (tinyxml2::XML_NO_ERROR != doc.LoadFile(strXmlA))
			return FALSE;

		tinyxml2::XMLElement *pRoot = doc.RootElement();//获取根节点
		if (NULL == pRoot)
		{
			return FALSE;
		}

		tinyxml2::XMLElement *pKeyElement = pRoot->FirstChildElement("key");

		while (pKeyElement)
		{
			tinyxml2::XMLElement *pKeyValueElement = pKeyElement->FirstChildElement("KeyValue");//获取第一个值为Value的子节点 默认返回第一个子节点
			if (NULL == pKeyValueElement)
			{
				pKeyElement = pKeyElement->NextSiblingElement();
				continue;
			}

			const char* pKeyValueContent= pKeyValueElement->GetText();
			if (NULL == pKeyValueContent)
			{
				pKeyElement = pKeyElement->NextSiblingElement();
				continue;
			}

			if (nID == atoi(pKeyValueContent))
			{
				tinyxml2::XMLNode *pChildNode=pKeyValueElement->FirstChild();
				if(NULL==pChildNode)return FALSE;
				CStringA KeyID;
				KeyID.Format("%d",m_keymess.nID);
				pChildNode->SetValue(KeyID);

				tinyxml2::XMLElement *pKeyUsedElement = pKeyElement->FirstChildElement("used");
				if(NULL==pKeyUsedElement)return FALSE;
				pChildNode=pKeyUsedElement->FirstChild();
				if(m_keymess.used)
				{
					pChildNode->SetValue("1");
				}
				else
				{
					pChildNode->SetValue("0");
				}

				tinyxml2::XMLElement *pKeyTypeElement = pKeyElement->FirstChildElement("type");
				if(NULL==pKeyTypeElement)return FALSE;
				pChildNode=pKeyTypeElement->FirstChild();
				int type=(static_cast<int>(m_keymess.type));
				CStringA keyType;
				keyType.Format("%d",type);
				pChildNode->SetValue(keyType);

				tinyxml2::XMLElement *pKeyDetialElement = pKeyElement->FirstChildElement("detail");
				if(NULL==pKeyDetialElement)return FALSE;
				pChildNode=pKeyDetialElement->FirstChild();
				CStringA keyDetial;
				WStringTOAsinString(m_keymess.detils,keyDetial);
				pChildNode->SetValue(keyDetial);

				doc.SaveFile(strXmlA);
				return TRUE;
			}

			pKeyElement = pKeyElement->NextSiblingElement();
		}
	}
	catch(...)
	{
		return FALSE;
	}
	return FALSE;
}

void CKeySetListMgr::DelKey(int nID)
{
	CStringA strXmlA = "";
	WStringTOAsinString(m_strIniFile,strXmlA);
	try
	{
		tinyxml2::XMLDocument doc;
		if (tinyxml2::XML_NO_ERROR != doc.LoadFile(strXmlA))
			return;

		tinyxml2::XMLElement *pRoot = doc.RootElement();//获取根节点
		if (NULL == pRoot)
		{
			return;
		}

		tinyxml2::XMLElement *pKeyElement = pRoot->FirstChildElement("key");

		while (pKeyElement)
		{
			tinyxml2::XMLElement *pKeyValueElement = pKeyElement->FirstChildElement("KeyValue");//获取第一个值为Value的子节点 默认返回第一个子节点
			if (NULL == pKeyValueElement)
			{
				pKeyElement = pKeyElement->NextSiblingElement();
				continue;
			}

			const char* pKeyValueContent= pKeyValueElement->GetText();
			if (NULL == pKeyValueContent)
			{
				pKeyElement = pKeyElement->NextSiblingElement();
				continue;
			}

			if (nID == atoi(pKeyValueContent))
			{
				doc.DeleteNode(pKeyElement);
				doc.SaveFile(strXmlA);
				return;
			}

			pKeyElement = pKeyElement->NextSiblingElement();
		}
	}
	catch(...)
	{}
}


KeyMess CKeySetListMgr::GetLastKey()
{
	int count=m_KeyList.size();
	return m_KeyList.at(count-1);
}

unsigned int CKeySetListMgr::GetCount()
{
	return m_KeyList.size();
}


int CKeySetListMgr::GetKeyValueByStr(CString str)
{
	int nRet = 0;
	int len = 0x100;
	CString strKey=L"";;
	for(int i=0;i<len;i++)
	{
		if(VK_VALUE[i]!=NULL && str == VK_VALUE[i])
		{
			nRet = i;
			break;
		}
	}
	return nRet;
	return 0;
}

bool CKeySetListMgr::ISGameView(HWND& hCurGameWnd)
{
	CInterView *pCurGameView =theApp.pBrowser->GetCurentView();
	hCurGameWnd = CInterView::GetCurGameWnd(pCurGameView->GetHostWindow(), _T("MacromediaFlashPlayerActiveX"),_T(""));
	if(hCurGameWnd == NULL)
	{
		hCurGameWnd = CInterView::GetCurGameWnd(pCurGameView->GetHostWindow(), _T("Unity.WebPlayer"),_T(""));

		if(NULL == hCurGameWnd)
		{
			hCurGameWnd = CInterView::GetCurGameWnd(pCurGameView->GetHostWindow(), _T("Plus5GameWindowClass"),_T(""));

		}
		if (NULL == hCurGameWnd)
		{
			hCurGameWnd = CInterView::GetCurGameWnd(pCurGameView->GetHostWindow(), _T("AfxWnd90su"),_T(""));
		}


		if(NULL == hCurGameWnd)
		{

			return false;
		}

	}
	return true;
}

bool CKeySetListMgr::AddHookKey()
{
	// 获取游戏窗口句柄
	HWND hCurGameWnd=NULL;
	if(!ISGameView(hCurGameWnd))
	{
		MessageBox(NULL,_T("请先运行游戏窗口"),_T("注意"),MB_OK);
		return false;
	}
	// 当钩子存在时先卸载钩子，然后进行加载
	if (NULL != m_hChangeKey)
	{
		DelHookKey();
	}

	// 动态加载ChangeKey.DLL
	typedef BOOL (* STARTPROC)(int*, int*, int, string*, HWND);//定义函数指针类型
	STARTPROC StartMaskKey = 0;

	m_hChangeKey = LoadLibrary(GetExePath()+ _T("\\ChangeKey.dll"));//动态加载Dll

	if(m_hChangeKey)
	{
		StartMaskKey=(STARTPROC)GetProcAddress(m_hChangeKey,"StartMaskKey");//获取Dll的导出函数
	}

	if (NULL == StartMaskKey)
	{
		ASSERT (FALSE);
		return false;
	}

	// 保存元素个数
	CInterView *pView =theApp.pBrowser->GetCurentView();
	int nCount = m_KeyList.size();
	int StopNum=0;
	for(unsigned int i=0;i<m_KeyList.size();i++)
	{
		if(m_KeyList.at(i).type==TYPE_LXKEY)
		{
			
			if (pView->GetCurrentKeyMess()->IsKeyUsed(m_KeyList.at(i).nID))
			{
					StopNum++;
			}
		}
	}
	nCount+=StopNum;
	if (nCount <= 0)
	{
		return false;
	}
	int* dwVK = new int[nCount];
	int* pType = new int[nCount];
	std::string* strKey = new std::string[nCount];

	// 用来记录有效值的个数
	int KeyCount= 0;

	// 获取各个元素的值
	for (int pos = 0; pos < nCount; pos++)
	{
		if(pos==nCount-StopNum)
		{
			break;
		}
		if (!pView->GetCurrentKeyMess()->IsKeyUsed(m_KeyList.at(pos).nID))
		{
	
			continue;
		}
		//如果是连续按键，将终止按键添加进hook中
		if(m_KeyList.at(pos).type==TYPE_LXKEY)
		{
			//添加终止键
			int lenth=m_KeyList[pos].detils.GetLength();
			int ps=m_KeyList[pos].detils.ReverseFind(',');

			int nID=GetKeyValueByStr(m_KeyList[pos].detils.Right(lenth-ps-1));
			dwVK[KeyCount]=nID;
			pType[KeyCount]=TYPE_STOP+1;
			CString str;
			str.Format(L"%d",m_KeyList[pos].nID);
			CStringA stra(str.GetBuffer());
			m_KeyList.at(pos).detils.ReleaseBuffer();
			strKey[KeyCount] = stra.GetBuffer();
			stra.ReleaseBuffer();
			KeyCount++;

			//添加连续按键
			dwVK[KeyCount] = m_KeyList.at(pos).nID;
			pType[KeyCount] = m_KeyList.at(pos).type + 1;
			CString temp=m_KeyList[pos].detils.Left(ps);
			CStringA strb(temp.GetBuffer());
			temp.ReleaseBuffer();
			strKey[KeyCount] = strb.GetBuffer();
			strb.ReleaseBuffer();
			KeyCount++;
		}

		else
		{
		dwVK[KeyCount] = m_KeyList.at(pos).nID;
		pType[KeyCount] = m_KeyList.at(pos).type + 1;

		
		// CString 转String
		CStringA stra(m_KeyList.at(pos).detils.GetBuffer());
		m_KeyList.at(pos).detils.ReleaseBuffer();
		strKey[KeyCount] = stra.GetBuffer();
		stra.ReleaseBuffer();
		KeyCount++;
		}
	}

	// 调用ChangeKey.DLL启用Hook
	StartMaskKey(dwVK, pType, KeyCount,strKey, hCurGameWnd);

	// 释放开辟的内存
	if (NULL != dwVK)
	{
		delete [] dwVK;
		dwVK = NULL;
	}

	if (NULL != pType)
	{
		delete[] pType;
		pType = NULL;
	}

	if (NULL != strKey)
	{
		delete[] strKey;
		strKey = NULL;
	}

	// 设置钩子状态为开启
	m_HookState = true;
	return true;
}

// 卸载Hook
bool CKeySetListMgr::DelHookKey()
{
	if (NULL == m_hChangeKey)
	{
		return true;
	}

	typedef BOOL (* STOPMASK)();//定义函数指针类型
	STOPMASK StopMaskKey = 0;

	StopMaskKey = (STOPMASK)GetProcAddress(m_hChangeKey,"StopMaskKey");//获取Dll的导出函数

	if (NULL == StopMaskKey)
	{
		return false;
	}

	// 卸载Hook
	if (StopMaskKey())
	{
		FreeLibrary (m_hChangeKey);
		m_hChangeKey = NULL;
	}
	
	// 设置钩子状态为关闭
	m_HookState = false;
	return true;
}

// 获取钩子状态
bool CKeySetListMgr::GetHookState()
{
	return m_HookState;
}


void CKeySetListMgr::ShowKeyListView(DuiLib::CWindowWnd* pParentWnd)
{
	HRESULT Hr = ::CoInitialize(NULL);
	if( FAILED(Hr) ) return ;
	if(pFrame!=NULL)return;
	pFrame = new CDulibKeySetWnd();
	if( pFrame == NULL ) return ;
	pFrame->Create(pParentWnd->GetHWND(), _T(""), WS_OVERLAPPEDWINDOW, 0L, 0, 0, 800, 572);
	pFrame->CenterWindow();
	::ShowWindow(*pFrame, SW_SHOW);
	::CoUninitialize();
}


void CKeySetListMgr::ChangeLableState()
{
	if(pFrame!=NULL)
	{
		pFrame->PostMessage(SET_HOOK_STATE,0,0);
	}
}


CDulibKeySetWnd*CKeySetListMgr::GetKeyListView()
{
	return pFrame;
}

void CKeySetListMgr::DestroyKeySetWindow()
{
	pFrame=NULL;
}


bool CKeySetListMgr::IsCurrentViewKeyUsed(int key)
{
	CInterView *pCurGameView =theApp.pBrowser->GetCurentView();
	if(pCurGameView==NULL)
	{
	return false;
	}
	bool oldState=pCurGameView->GetCurrentKeyMess()->IsKeyUsed(key);
	return oldState;
	return true;
}

void CKeySetListMgr::SetCurrentViewKey(int key,bool used)
{
	CInterView *pCurGameView =theApp.pBrowser->GetCurentView();
	if(pCurGameView==NULL)
	{
		return;
	}
	pCurGameView->GetCurrentKeyMess()->SetCurrentKey(key,used);
}

bool CKeySetListMgr::CheckString(const CString& str)
{
	bool bIsVali = false;

	if (str.IsEmpty())
	{
		return bIsVali;
	}
	int nStrLength = str.GetLength();

	// 判断指针第一个和最后一个字符，是否在A-Z或0-9之间
	if ((str[0] >= 'A' && str[0] <= 'Z'&& str[nStrLength-1] >= 'A'&& str[nStrLength-1] <= 'Z') || (str[0] >= '0' && str[0] <= '9'&& str[nStrLength-1] >= '0'&& str[nStrLength-1] <= '9'))
	{
		// 当指针只有一个字母且在A-Z之间时，直接返回true
		if (nStrLength == 1)
		{
			bIsVali = true;
			return bIsVali;
		}
		else if (str[1] != '(' || nStrLength < 5)
		{
			bIsVali = false;
			return bIsVali;
		}
		int  nlet = 0;
		// 判断格式是否正确
		for (int i=0;i<nStrLength; i++)
		{
			// 判断逗号间字母个数,每遇见一个字母则+1
			if (str[i]  >=  'A' && str[i] <= 'Z')
			{
				nlet++;
			}
			if (nlet > 1)
			{
				bIsVali = false;
				return bIsVali;
			}
			// 判断逗号
			if (str[i] == ',')
			{
				// 遇到逗号时判断逗号间有多少个字母，如果不只一个则返回，是一个则清零
				if (nlet > 1)
				{
					bIsVali = false;
					return bIsVali;
				}
				else
				{
					nlet = 0;
				}

				//判断逗号是否小于第四位
				if (i<4)
				{
					bIsVali = false;
					return bIsVali;
				}
				// 判断逗号左边为）  右边为字母  且左边第二个不为（
				if (str[i-2] != '(' && str[i-1] == ')' && ((str[i+1] >='A' && str[i+1] <= 'Z')|| (str[i+1] >='0' && str[i+1] <= '9')))
				{
					// 判断")"到"("之间是否为数字
					for (int x=1; str[i-1-x] != '(' && (i-1-x) > 0; x++)
					{
						if (str[i-1-x] < '0' || str[i-1-x] > '9' || (i-1-x-1) == 0)
						{
							bIsVali = false;
							return bIsVali;
						}
					}
					// 判断逗号后面的字母或数字是否为最后一个字符
					if ((nStrLength-1) == (i + 1))
					{
						bIsVali = true;
						return bIsVali;
					}
					// 如果逗号后面不是最后一个字母，判断字母后面有无'('
					else if (str[i+2] == '(')
					{
						bIsVali = true;
					}
					else
					{
						bIsVali = false;
						return bIsVali;
					}
				}
				else //if (pKey[i-2] != '(' && pKey[i-1] == ')' && pKey[i+1] >='A' && pKey[i+1] <= 'Z')
				{
					bIsVali = false;
					return bIsVali;
				}
			}
		}
	}
	else //	if (pKey[0] >= 'A' && pKey[0] <= 'Z'&& pKey[nStrLength-1] >= 'A'&& pKey[nStrLength-1] <= 'Z')
	{
		return bIsVali;
	}
	return bIsVali;
	return true;
}

void CKeySetListMgr::GetEachItemText(const CString &str,vector<CString>& strArray)
{
	int pos=-1;
	int flag=0;
	for(int i=0;i<=str.GetLength();i++)
	{
		pos++;
		if(str[i]==','||str[i]=='\0')
		{
			CString temp=str.Mid(flag,pos);
			strArray.push_back(temp);
			flag=i+1;
			pos=-1;
		}	
	}
}
