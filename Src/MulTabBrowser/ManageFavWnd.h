/*!@file
*******************************************************************************************************
<PRE>
模块名		："管理收藏夹"界面类
文件名		：ManageFavWnd
相关文件		：ManageFavWnd.cpp/Duilib相关库
文件实现功能：实现管理收藏夹界面的逻辑，使外部只需要调用这一个类即可实现收藏夹界面所有操作
作者			：孙文
版本			：1.0
-------------------------------------------------------
备注：此类引用了Duilib库
-------------------------------------------------------
修改记录：
日期			版本			修改人		修改内容
2014/5/30 	1.0			孙文			创建
</PRE>
******************************************************************************************************/
#pragma once
#include <atlstr.h>
#include <list>
#include "ManageFavHeader.h"

class CInterView;
class CFavToolFunc;
class CManageFavXML;
class CManageFavWnd : public DuiLib::WindowImplBase
{
public:
	CManageFavWnd(void);
	~CManageFavWnd(void);

public:
	virtual LPCTSTR    GetWindowClassName() const   {   return _T("ManageFavWnd");  }
	virtual DuiLib::CDuiString GetSkinFile()                {   return _T("SideFavWnd//ManageFav.xml");    }
	virtual DuiLib::CDuiString GetSkinFolder()              {   return _T("");  }
	void Init(HWND hWndParent);
	virtual void       InitWindow();

public:
// 功能接口函数:
/*---------------------------List控件--------------------------------------------------*/
	// 添加一行List数据，参数分别为 网址图标、标题、网址,nIndex为-1时表示在最后一行添加
	bool AddListLine(CString& strIconURL, CString& strTitle, CString& strURL, int nIndex = -1);
	// 删除一行List数据，nIndex为删除行的索引
	bool DelListLine(int& nIndex);
	// 通过标题获取控件,返回为空时代表不存在
	DuiLib::CListContainerElementUI* GetListLine(CString& strTitle);

/*---------------------------Tree控件--------------------------------------------------*/
	// 添加一行树strText为树的Text,parent为父节点默认为本地收藏夹，nIndex为-1时表示在父节点最后添加
	bool AddTreeNode(CString& strText, DuiLib::CTreeNodeUI* parent = NULL, int nIndex = -1);
	// 删除一行Tree数据，strText为删除行的Text
	bool DelTreeLine(CString& strText);
	// 删除TreeNode所有子结点
	bool DelAllTreeNode(DuiLib::CTreeNodeUI* pParentNode);
	// 通过名字获取本地收藏夹下的子结点，返回为NULL时表示未找到
	DuiLib::CTreeNodeUI* GetNode(CString& strText);
	// 上移树控件,pTreeNode为要上移的Node
	bool UpTreeNode(DuiLib::CTreeNodeUI* pTreeNode);
	// 下移树控件，pTreeNode为要下移的Node
	bool DownTreeNode(DuiLib::CTreeNodeUI* pTreeNode);

/*----------------------------功能函数--------------------------------------------------*/
	// 复制函数，实现拷贝功能
	void CopyToClipboard(CStringA strData);
	//// CStringW转CStringA
	//void WStringTOAsinString(CStringW strWide,CStringA& strout);

private:
	// 添加"本地收藏夹"树结点
	bool AddLocalFavNode();
	// 通过名字获取指定父结点下的子节点，返回为NULL时表示未找到
	DuiLib::CTreeNodeUI* GetChildNode(CString& strText, DuiLib::CTreeNodeUI* parent);
	// 添加当前同父级节点到链表,NodeChild为父节点下一级子节点
	bool AddNodeToList(DuiLib::CTreeNodeUI* NodeChild);
	// 删除当前同父级所有子结点,NodeChild为父节点下一级子节点
	bool DelParentChildNode(DuiLib::CTreeNodeUI* NodeChild);
	// 从链表添加结点
	bool AddListToTree(DuiLib::CTreeNodeUI* parent);

/*----------------------------XML函数--------------------------------------------------*/
	// 加载XML到List链表
	bool LoadXmlToList();
	// 从文件夹链表添加到树控件
	bool ShowFolderListToTree();
	// 从网址收藏链表添加到List控件
	bool ShowLinkListToList(const CString& strParentName);
protected:
// 消息响应:
	virtual void OnClick(DuiLib::TNotifyUI& msg);
	virtual void OnHeaderClick(DuiLib::TNotifyUI& msg);
	virtual void Notify(DuiLib::TNotifyUI& msg);
	virtual LRESULT HandleMessage(UINT uMsg, WPARAM wParam, LPARAM lParam);	//自定义消息

private:
	DuiLib::CListUI* m_pList;					// List列表
	DuiLib::CListContainerElementUI* m_pListCE;	// 自定义List控件
	DuiLib::CTreeViewUI*	m_pTreeView;		// 树控件
	DuiLib::CTreeNodeUI*	m_pTreeCocalFav;	// 树控件的本地收藏夹结点
	int m_nListIndex;					// 定义点击的List Index
	CString m_strTreeNodeText;			// 定义点击的Tree Text
	std::list<TREESORT> m_TreeSort;		// 树排序的链表

	CInterView*	m_InterView;			// 一个View对象
	CFavToolFunc* m_FavToolFunc;		// 收藏夹工具类
	CManageFavXML* m_ManageFavXML;		// XML类对象

	std::list<TREESORT> m_TreeFolderXML;	// XML中的文件夹链表
	std::list<LIST_LINK> m_ListLinkXML;		// XML中的网址收藏链表
};