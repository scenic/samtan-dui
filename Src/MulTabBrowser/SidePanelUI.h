#pragma once

class CSideWndBase;

namespace DuilibEx
{

	class CSidePanelUI : public DuiLib::CContainerUI
	{
	public:
		CSidePanelUI(void);
		~CSidePanelUI(void);

		void		Attach(CSideWndBase* pHostWnd);
		void		UnAttcah();
		CSideWndBase*	GetHostWindow();

		LPCTSTR		GetClass() const { return _T("SidePanelUI"); }
		LPVOID		GetInterface(LPCTSTR pstrName);
		void		DoEvent(DuiLib::TEventUI& event);
		void		SetVisible(bool bVisible = true);
		void		SetInternVisible(bool bVisible = true);
		void		SetPos(RECT rc);

	private:
		CSideWndBase*	m_pHostWnd;
	};
} // namespace DuilibEx